#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>

#include "scttime.h"

int main (int argc, char **argv){
    char datastring[22];
    
    
    assert(!strcmp(scttime_rfc3339(0, datastring), "1970-01-01T00:00:00Z"));
    assert(!strcmp(scttime_rfc3339(777777777, datastring), "1994-08-25T01:22:57Z"));
    assert(!strcmp(scttime_rfc3339((time_t)((~ (uint32_t)0)>>1), datastring), "2038-01-19T03:14:07Z"));
    assert(!strcmp(scttime_rfc3339(1392879390, datastring), "2014-02-20T06:56:30Z"));
    
    printf("[ok] %s %x %s\n", "scttime", sct_version, __TIMESTAMP__);
    return 0;
}
