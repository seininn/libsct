/*

    sctos: Miscellaneous time Routines
    Sulaiman (seininn) Mustafa
    Version 1:0:1
    Date: 2014-01-21
    
    This module is a part of libstc.
*/


#ifndef SCTTIME_H
#define SCTTIME_H
#include <time.h>
#include <stdbool.h>
#include "sctstd.h"

extern bool scttime_config_debug;
extern int scttime_config_version[3];

char *scttime_rfc3339(time_t ts, char buf[static 22]);

#endif
