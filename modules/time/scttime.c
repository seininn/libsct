/*

    sctos: Miscellaneous time Routines
    Sulaiman (seininn) Mustafa
    Version 1:0:1
    Date: 2014-01-21
    
    This module is a part of libstc.
    
*/


#define _POSIX_C_SOURCE 200809L
#include <time.h>
#include <stdio.h>
#include <stdbool.h>


char *scttime_rfc3339(time_t ts, char buf[static 22]) {
    // Security risk. Make sure the an overflow is not posible.
    // Not thread safe: using gmtime
    struct tm *times;
    //struct tm j;
    times = gmtime_r(&ts, (struct tm[1]){{0}});
    
    sprintf(buf, "%04d-%02d-%02dT%02d:%02d:%02dZ",  times->tm_year+1900, 
                                                    times->tm_mon+1, 
                                                    times->tm_mday, 
                                                    times->tm_hour, 
                                                    times->tm_min, 
                                                    times->tm_sec);
    return buf;
}
