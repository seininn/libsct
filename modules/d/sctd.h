#ifndef SCTD_H
#define SCTD_H

// STATIC SECTION
/*
sctdoc:sctd:

    sctcd: A dictionary (hashtable) module

    Sulaiman (seininn) Mustafa     2014-06-09
    
    This module is a part of libstc, a modular, c11-compliant, 
    toolkit library. use stcmod to determine this module's 
    dependencies, if any.
    
    -----------------------------------------------------------------------
    
    WARNING: THIS MODULE MAKES EXTENSIVE USE OF MACROS. DO NOT PASS 
             ARGUMENTS THAT HAVE SIDE EFFECTS AS PARAMATERS.
             
    
    This module provides dictionary (hashtable) functionality.
    
    Features:
        A. Dictionaries have drop-in compatibility with normal C arrays.

    Anti-features:
        A. For now, dictionaries aren't implemented as hash tables. O(N)
        B. Macros. Most of the module's api are implemented using macros 
           that can reuse their arguments multiple times. Passing 
           anything with side-effects is undefined behaviour.
        C. Dictionaries are not stateless. Simultantus access by more than 
           a single thread is undefined behaviour. Dictionaries can be 
           shared as long as access is serialized.
           
    EXAMPLES
        // What follows is a short drama drama
        uint64_t *birthdays = sctd_new(uint64_t);
        
        sctd_set(birthdays, "daddy bear", 19600324);
        sctd_set(birthdays, "mommy bear", 19611121);
        sctd_set(birthdays, "teddy bear", 19800705);
        
        sctd_get(birthdays, "teddy bear"); // == 19800705
        sctd_set(birthdays, "teddy bear", 19800706); // replaces old value
        
        sct_delete(birthdays, "mommy bear");
        
        for (int i = 0; i < 2; ++i) birthdays[i]; // daddy's and teddy's bdays
        
        sctd_free(birthdays); // car accident :(
    
    DEBUGING
    
    SCT offers a verbose mode as a debuging aid. To enable this mode, set 
    sct_sct_debug to where want the verbose output to be written to (stderr,
    fopen()).
    
*/

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

struct sctd__internal_s {
    uint8_t type;
    char **key_list;
    int rv;
};

#define SCTD__META(P) ((struct sctd__internal_s *) SCTA__EXTERN_META(P)[0])
#define SCTD__TYPE 0x11
#define SCTD__TYPE_CHECK(P) (SCTD__META(P)->type == SCTD__TYPE)

// PARTIALS SECTION: AUTOMATICLY GENERATED
//--delete
#ifndef SCTD_PART_DELETE
#define SCTD_PART_DELETE

bool sctd_delete_(void **q, char *key);
#define sctd_delete(dict, key) (sctd_delete_(((void **) &(dict)), key))

/*
sctdoc:sctd_delete:

NAME
    sctd_delete

SYNOPSIS
    bool sctd_delete(type *dict, char *key)
    
DESCRIPTION
    This function deletes the key-value pair that has `key' as its key.

RETURN
    true if successful, and false otherwise.
    
SEE ALSO
    sctd_get
    sctd_set

*/

#endif


    
//--free
#ifndef SCTD_PART_FREE
#define SCTD_PART_FREE
void sctd_free(void *);

/*
sctdoc:sctd_free:

NAME
    sctd_free

SYNOPSIS
    void sctd_free(void *);
    
DESCRIPTION
    Frees a dictionary returned by sctd_new.

RETURN
    None

SEE ALSO
    sctd_new
*/

#endif


    
//--get
#ifndef SCTD_PART_GET
#define SCTD_PART_GET

int sctd_get_index(void *p, char *key);
#define sctd_isset(dict, key) ((sctd_get_index(dict, key) != -1))
#define sctd_get(dict, key) ((sctd_isset(dict, key))?(dict[SCTD__META(dict)->rv]):(dict[scta_length(dict)]))

/*
sctdoc:sctd_get:

NAME
    sctd_get

SYNOPSIS
    type sctd_get(type *dict, char *key)
    
DESCRIPTION
    This function retrieves the value that is associated with the string 
    `key' from the dictionary `dict'.
    
    If no key-value pair is found, the zero value of the type is 
    returned. To differentiate between this value and a real key 
    value that happens to be zero, use sctd_isset.

WARNING
    This api endpoint is implemented as a macro. it's first parameter is 
    evaluated more than once.

RETURN
    a value of type `type'

EXAMPLE
    int *student_count = sctd_new(int);
    sctd_set(student_count, "Hateen High", 323);
    sctd_get(student_count, "Hateen High"); // 323
    sctd_get(student_count, "Hateen Elementry"); // 0

SEE ALSO
    sctd_isset
    sctd_set
    sctd_delete
*/


/*
sctdoc:sctd_isset:

NAME
    sctd_isset

SYNOPSIS
    bool sctd_isset(type *dict, char *key)
    
DESCRIPTION
    this function tests whether dictionary `dict' has a value associated 
    with key `key'.

RETURN
    `true' if matching key-value pair is found, `false' otherwise or on 
    error.

EXAMPLE
    int *student_count = sctd_new(int);
    sctd_set(student_count, "Hateen High", 323);
    sctd_isset(student_count, "Hateen High"); // true
    sctd_isset(student_count, "Hateen Elementry"); // false


SEE ALSO
    sctd_get
*/


#endif


    
//--keys
#ifndef SCTD_PART_KEYS
#define SCTD_PART_KEYS
const char** sctd_keys(void *dict);

/*
sctdoc:sctd_keys:

NAME
    sctd_keys

SYNOPSIS
    const char** sctd_keys(void *dict);
    
DESCRIPTION
    Returns a null-terminated list of all keys in the dictionary.

RETURN
    Returns a constant pointer to `char *' on success, and `NULL' 
    on failure. The pointer MUST NOT be freed as it points to an
    data structure.
*/

#endif


    
//--new
#ifndef SCTD_PART_NEW
#define SCTD_PART_NEW
void* sctd_new_(size_t size);
#define sctd_new(type) (sctd_new_(sizeof(type)))

/*
sctdoc:sctd_new:

NAME
    sctd_new

SYNOPSIS
    type* sctd_new(type);
    
DESCRIPTION
    This function allocates and returns an dictionary of type `type'.

RETURN
    A pointer of type `type *'. Must be freed using sctd_free to avoid
    memory leaks.

EXAMPLE
    int *numbers = sctd_new(int);
    bool *states = sctd_new(bool);

SEE ALSO
    sctd_free
*/

#endif


    
//--set
#ifndef SCTD_PART_SET
#define SCTD_PART_SET

int sctd_get_or_reserve(void **q, char *key);
#define sctd_set(dict, key, value) ((sctd_get_or_reserve(((void **) &(dict)), key)!=-1)?(((dict)[SCTD__META(dict)->rv])=(value), true):false)

/*
sctdoc:sctd_set:

NAME
    sctd_set

SYNOPSIS
    bool sctd_set(type *dict, char *key, type value)
    
DESCRIPTION
    This function stores the `key'-`value' pair in the dictionary `dict'.
    if a pair with that same key already exists in the dictionary, its 
    old value is replaced with the new one.

WARNING
    This api endpoint is implemented as a macro. it's first parameter is 
    evaluated more than once.
    
RETURN
    `true' if successful, and `false' otherwise.

EXAMPLE
    int *student_count = sctd_new(int);
    sctd_set(student_count, "Hateen High", 323);
    sctd_get(student_count, "Hateen High"); // 323

SEE ALSO
    sctd_get
    sctd_delete
*/

#endif


    
#endif
