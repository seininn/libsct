#define SCT__INTERNAL__
#include "sctd.h"

// STATIC SECTION
#include "sctstd.h"
#include "sctd.h"
#include "scta.h"




// PARTIALS SECTION: AUTOMATICLY GENERATED
//--delete
#include <string.h>
#include <stdlib.h>

bool sctd_delete_(void **q, char *key){
    void *p = q?*q:NULL;
    // input checks
    if (!p) ERETURN(false, "required dict is null");
    if (!key) ERETURN(false, "required `key' is null");
    if (!SCTD__TYPE_CHECK(p)) ERETURN(false, "expecting dict to be a dictionary, got something else.");    

    // logic
    int i = sctd_get_index(p, key);
    
    if (i == -1) ERETURN(false, "key %s was not found", key);
    
    if (!scta_delete(p, i)) ERETURN(false, "could not delete key-value pair. dict might be inconsistent!");
    free(SCTD__META(p)->key_list[i]);
    if (!scta_delete(SCTD__META(p)->key_list, i)) ERETURN(false, "could not delete key-value pair. dict is definitely inconsistent!");

    *q=p;
    SCTD__META(p)->rv = i;
    return i;    
}



    
//--free
#include <stdlib.h>
void sctd_free(void *p){
    if (!SCTD__TYPE_CHECK(p)) abort();
    for (int i = 0; SCTD__META(p)->key_list[i]; ++i) free(SCTD__META(p)->key_list[i]);
    scta_free(SCTD__META(p)->key_list);
    free(SCTD__META(p));
    scta_free(p);
}



    
//--get
#include <string.h>
int sctd_get_index(void *p, char *key){
    int i;
    char **key_list;
    // input checks
    if (!p) ERETURN(-1, "required `p' is null");
    if (!key) ERETURN(-1, "required `key' is null");
    if (!SCTD__TYPE_CHECK(p)) ERETURN(-1, "expecting `p' to be a dictionary, got something else.");    
    
    // logic
    key_list = SCTD__META(p)->key_list;
    
    for (i = 0; key_list[i] && strcmp(key_list[i], key); ++i);
    
    if (i == scta_length(key_list)) {
        SCTD__META(p)->rv = -1;
        return -1;
    }
    else {
        SCTD__META(p)->rv = i;
        return i;
    }
}



    
//--keys
const char** sctd_keys(void *dict){
    // input checks
    if (!dict) ERETURN(false, "required `dict' is null");
    if (!SCTD__TYPE_CHECK(dict)) ERETURN(false, "expecting `dict' to be a dictionary, got something else.");    
    // logic
    return (const char **) SCTD__META(dict)->key_list;
}



    
//--new
#include <stdlib.h>
void* sctd_new_(size_t size){
    struct sctd__internal_s *dict_meta; // metadata for the dictionary
    void *p = scta_macro_new_(size);
    
    if (!p) ERETURN(NULL, "^");
    
    dict_meta = SCTA__EXTERN_META(p)[0] = malloc(sizeof(struct sctd__internal_s));
    
    if (!dict_meta) {
        scta_free(p);
        ERETURN(NULL, "could not allocate memory via malloc");
    }
    
    dict_meta->key_list = scta_new(char *); // scta_free

    if (!dict_meta->key_list) {
        scta_free(p);
        free(dict_meta);
        ERETURN(NULL, "^");
    }
    
    dict_meta->type = SCTD__TYPE; // 
    
    return p;
}



    
//--set
#include <string.h>
#include <stdlib.h>
char *dups(char *s){
    int l;
    char *r = malloc(l=(strlen(s)+1));
    if (!r) return NULL;
    else {
        strncpy(r, s, l);
        return r;
    }
}

int sctd_get_or_reserve(void **q, char *key){
    void *p = q?*q:NULL;
    // input checks
    if (!p) ERETURN(-1, "required dict is null");
    if (!key) ERETURN(-1, "required `key' is null");
    if (!SCTD__TYPE_CHECK(p)) ERETURN(-1, "expecting dict to be a dictionary, got something else.");    

    // logic
    int i = sctd_get_index(p, key);
        
    if (i == -1) {
        i = scta_length(p);
        if (!scta_set_length(p, i+1)) {
            *q=p;
            ERETURN(-1, "^");
        }
        if (!scta_push(SCTD__META(p)->key_list, dups(key))) {
            *q=p;
            ERETURN(-1, "^");    
        }
        if (scta_length(p) != scta_length(SCTD__META(p)->key_list)) {
            *q=p;
            ERETURN(-1, "the dictionary and it's key list have differing lengths: %d %d", 
                        scta_length(p), scta_length(SCTD__META(p)));    
        }
    }

    *q=p;
    SCTD__META(p)->rv = i;
    return i;    
}



    
