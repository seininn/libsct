#define SCT__INTERNAL__
#include "sctstd.h"
#include "sctd.h"
#include "scta.h"

//HEADER
#ifndef SCTD_PART_SET
#define SCTD_PART_SET

int sctd_get_or_reserve(void **q, char *key);
#define sctd_set(dict, key, value) ((sctd_get_or_reserve(((void **) &(dict)), key)!=-1)?(((dict)[SCTD__META(dict)->rv])=(value), true):false)

/*
sctdoc:sctd_set:

NAME
    sctd_set

SYNOPSIS
    bool sctd_set(type *dict, char *key, type value)
    
DESCRIPTION
    This function stores the `key'-`value' pair in the dictionary `dict'.
    if a pair with that same key already exists in the dictionary, its 
    old value is replaced with the new one.

WARNING
    This api endpoint is implemented as a macro. it's first parameter is 
    evaluated more than once.
    
RETURN
    `true' if successful, and `false' otherwise.

EXAMPLE
    int *student_count = sctd_new(int);
    sctd_set(student_count, "Hateen High", 323);
    sctd_get(student_count, "Hateen High"); // 323

SEE ALSO
    sctd_get
    sctd_delete
*/

#endif

//LIB
#include <string.h>
#include <stdlib.h>
char *dups(char *s){
    int l;
    char *r = malloc(l=(strlen(s)+1));
    if (!r) return NULL;
    else {
        strncpy(r, s, l);
        return r;
    }
}

int sctd_get_or_reserve(void **q, char *key){
    void *p = q?*q:NULL;
    // input checks
    if (!p) ERETURN(-1, "required dict is null");
    if (!key) ERETURN(-1, "required `key' is null");
    if (!SCTD__TYPE_CHECK(p)) ERETURN(-1, "expecting dict to be a dictionary, got something else.");    

    // logic
    int i = sctd_get_index(p, key);
        
    if (i == -1) {
        i = scta_length(p);
        if (!scta_set_length(p, i+1)) {
            *q=p;
            ERETURN(-1, "^");
        }
        if (!scta_push(SCTD__META(p)->key_list, dups(key))) {
            *q=p;
            ERETURN(-1, "^");    
        }
        if (scta_length(p) != scta_length(SCTD__META(p)->key_list)) {
            *q=p;
            ERETURN(-1, "the dictionary and it's key list have differing lengths: %d %d", 
                        scta_length(p), scta_length(SCTD__META(p)));    
        }
    }

    *q=p;
    SCTD__META(p)->rv = i;
    return i;    
}


//TEST
#ifdef test_set
#include <assert.h>
#include <stdio.h>

int main() {
    sct_debug = stderr;
    int *test;
    test = sctd_new(int);
    assert(test);
    assert(!sctd_isset(test, "one"));
    assert(sctd_get(test, "one") == 0);
    assert(sctd_get(test, "two") == 0);
    assert(sctd_get(test, "") == 0);
    
    assert(sctd_set(test, "one", 1));
    assert(sctd_isset(test, "one"));
    assert(sctd_get(test, "one") == 1);
    
    
    assert(sctd_get(test, "minus one") == 0);
    assert(!sctd_isset(test, "minus one"));
    assert(sctd_set(test, "minus one", -1));
    assert(sctd_isset(test, "minus one"));
    assert(sctd_get(test, "minus one") == -1);
    assert(sctd_get(test, "one") == 1);

    assert(sctd_get(test, "zero") == 0);
    assert(!sctd_isset(test, "zero"));    
    assert(sctd_set(test, "zero", 0));
    assert(sctd_isset(test, "zero"));
    assert(sctd_get(test, "zero") == 0); 
    
    assert(sctd_set(test, "one", 0));
    assert(sctd_isset(test, "one"));
    assert(sctd_get(test, "one") == 0);
    
    assert(sctd_get(test, "two") == 0);
    assert(!sctd_isset(test, "two"));
    
    assert(scta_length(SCTD__META(test)->key_list)==3);
    
    sctd_free(test);
    return 0;
}

#endif
