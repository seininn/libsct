#define SCT__INTERNAL__
#include "sctstd.h"
#include "sctd.h"
#include "scta.h"

//HEADER
#ifndef SCTD_PART_FREE
#define SCTD_PART_FREE
void sctd_free(void *);

/*
sctdoc:sctd_free:

NAME
    sctd_free

SYNOPSIS
    void sctd_free(void *);
    
DESCRIPTION
    Frees a dictionary returned by sctd_new.

RETURN
    None

SEE ALSO
    sctd_new
*/

#endif

//LIB
#include <stdlib.h>
void sctd_free(void *p){
    if (!SCTD__TYPE_CHECK(p)) abort();
    for (int i = 0; SCTD__META(p)->key_list[i]; ++i) free(SCTD__META(p)->key_list[i]);
    scta_free(SCTD__META(p)->key_list);
    free(SCTD__META(p));
    scta_free(p);
}


//TEST
#ifdef test_free
#include <assert.h>
#include <stdio.h>

int main() {
    sct_debug = stderr;
    sctd_free(sctd_new(void *));
    return 0;
}

#endif
