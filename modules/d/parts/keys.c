#define SCT__INTERNAL__
#include "sctstd.h"
#include "sctd.h"
#include "scta.h"

//HEADER
#ifndef SCTD_PART_KEYS
#define SCTD_PART_KEYS
const char** sctd_keys(void *dict);

/*
sctdoc:sctd_keys:

NAME
    sctd_keys

SYNOPSIS
    const char** sctd_keys(void *dict);
    
DESCRIPTION
    Returns a null-terminated list of all keys in the dictionary.

RETURN
    Returns a constant pointer to `char *' on success, and `NULL' 
    on failure. The pointer MUST NOT be freed as it points to an
    data structure.
*/

#endif

//LIB
const char** sctd_keys(void *dict){
    // input checks
    if (!dict) ERETURN(false, "required `dict' is null");
    if (!SCTD__TYPE_CHECK(dict)) ERETURN(false, "expecting `dict' to be a dictionary, got something else.");    
    // logic
    return (const char **) SCTD__META(dict)->key_list;
}


//TEST
#ifdef test_keys
#include <assert.h>
#include <stdio.h>
#include <string.h>

int main() {
    sct_debug = stderr;
    int *test;
    const char **l;
    test = sctd_new(int);
    assert(test);
    
    assert(sctd_set(test, "a", 1));
    assert(sctd_set(test, "a", 1));
    assert(sctd_set(test, "b", 2));
    assert(sctd_set(test, "c", 3));
    assert(l = sctd_keys(test));
    assert(!strcmp(l[0], "a"));
    assert(!strcmp(l[1], "b"));
    assert(!strcmp(l[2], "c"));
    
    sctd_free(test);
    return 0;
}

#endif
