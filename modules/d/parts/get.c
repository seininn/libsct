#define SCT__INTERNAL__
#include "sctstd.h"
#include "sctd.h"
#include "scta.h"

//HEADER
#ifndef SCTD_PART_GET
#define SCTD_PART_GET

int sctd_get_index(void *p, char *key);
#define sctd_isset(dict, key) ((sctd_get_index(dict, key) != -1))
#define sctd_get(dict, key) ((sctd_isset(dict, key))?(dict[SCTD__META(dict)->rv]):(dict[scta_length(dict)]))

/*
sctdoc:sctd_get:

NAME
    sctd_get

SYNOPSIS
    type sctd_get(type *dict, char *key)
    
DESCRIPTION
    This function retrieves the value that is associated with the string 
    `key' from the dictionary `dict'.
    
    If no key-value pair is found, the zero value of the type is 
    returned. To differentiate between this value and a real key 
    value that happens to be zero, use sctd_isset.

WARNING
    This api endpoint is implemented as a macro. it's first parameter is 
    evaluated more than once.

RETURN
    a value of type `type'

EXAMPLE
    int *student_count = sctd_new(int);
    sctd_set(student_count, "Hateen High", 323);
    sctd_get(student_count, "Hateen High"); // 323
    sctd_get(student_count, "Hateen Elementry"); // 0

SEE ALSO
    sctd_isset
    sctd_set
    sctd_delete
*/


/*
sctdoc:sctd_isset:

NAME
    sctd_isset

SYNOPSIS
    bool sctd_isset(type *dict, char *key)
    
DESCRIPTION
    this function tests whether dictionary `dict' has a value associated 
    with key `key'.

RETURN
    `true' if matching key-value pair is found, `false' otherwise or on 
    error.

EXAMPLE
    int *student_count = sctd_new(int);
    sctd_set(student_count, "Hateen High", 323);
    sctd_isset(student_count, "Hateen High"); // true
    sctd_isset(student_count, "Hateen Elementry"); // false


SEE ALSO
    sctd_get
*/


#endif

//LIB
#include <string.h>
int sctd_get_index(void *p, char *key){
    int i;
    char **key_list;
    // input checks
    if (!p) ERETURN(-1, "required `p' is null");
    if (!key) ERETURN(-1, "required `key' is null");
    if (!SCTD__TYPE_CHECK(p)) ERETURN(-1, "expecting `p' to be a dictionary, got something else.");    
    
    // logic
    key_list = SCTD__META(p)->key_list;
    
    for (i = 0; key_list[i] && strcmp(key_list[i], key); ++i);
    
    if (i == scta_length(key_list)) {
        SCTD__META(p)->rv = -1;
        return -1;
    }
    else {
        SCTD__META(p)->rv = i;
        return i;
    }
}


//TEST
#ifdef test_get
#include <assert.h>
#include <stdio.h>

int main() {
    sct_debug = stderr;
    int *test;
    
    test = sctd_new(int);
    assert(test);
    assert(!sctd_isset(test, "one"));
    assert(sctd_get(test, "one") == 0);
    assert(sctd_get(test, "two") == 0);
    assert(sctd_get(test, "") == 0);
        
    sctd_free(test);
    return 0;
}

#endif
