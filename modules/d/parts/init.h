/*
sctdoc:sctd:

    sctcd: A dictionary (hashtable) module

    Sulaiman (seininn) Mustafa     2014-06-09
    
    This module is a part of libstc, a modular, c11-compliant, 
    toolkit library. use stcmod to determine this module's 
    dependencies, if any.
    
    -----------------------------------------------------------------------
    
    WARNING: THIS MODULE MAKES EXTENSIVE USE OF MACROS. DO NOT PASS 
             ARGUMENTS THAT HAVE SIDE EFFECTS AS PARAMATERS.
             
    
    This module provides dictionary (hashtable) functionality.
    
    Features:
        A. Dictionaries have drop-in compatibility with normal C arrays.

    Anti-features:
        A. For now, dictionaries aren't implemented as hash tables. O(N)
        B. Macros. Most of the module's api are implemented using macros 
           that can reuse their arguments multiple times. Passing 
           anything with side-effects is undefined behaviour.
        C. Dictionaries are not stateless. Simultantus access by more than 
           a single thread is undefined behaviour. Dictionaries can be 
           shared as long as access is serialized.
           
    EXAMPLES
        // What follows is a short drama drama
        uint64_t *birthdays = sctd_new(uint64_t);
        
        sctd_set(birthdays, "daddy bear", 19600324);
        sctd_set(birthdays, "mommy bear", 19611121);
        sctd_set(birthdays, "teddy bear", 19800705);
        
        sctd_get(birthdays, "teddy bear"); // == 19800705
        sctd_set(birthdays, "teddy bear", 19800706); // replaces old value
        
        sct_delete(birthdays, "mommy bear");
        
        for (int i = 0; i < 2; ++i) birthdays[i]; // daddy's and teddy's bdays
        
        sctd_free(birthdays); // car accident :(
    
    DEBUGING
    
    SCT offers a verbose mode as a debuging aid. To enable this mode, set 
    sct_sct_debug to where want the verbose output to be written to (stderr,
    fopen()).
    
*/

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

struct sctd__internal_s {
    uint8_t type;
    char **key_list;
    int rv;
};

#define SCTD__META(P) ((struct sctd__internal_s *) SCTA__EXTERN_META(P)[0])
#define SCTD__TYPE 0x11
#define SCTD__TYPE_CHECK(P) (SCTD__META(P)->type == SCTD__TYPE)
