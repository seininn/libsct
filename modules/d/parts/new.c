#define SCT__INTERNAL__
#include "sctstd.h"
#include "sctd.h"
#include "scta.h"

//HEADER
#ifndef SCTD_PART_NEW
#define SCTD_PART_NEW
void* sctd_new_(size_t size);
#define sctd_new(type) (sctd_new_(sizeof(type)))

/*
sctdoc:sctd_new:

NAME
    sctd_new

SYNOPSIS
    type* sctd_new(type);
    
DESCRIPTION
    This function allocates and returns an dictionary of type `type'.

RETURN
    A pointer of type `type *'. Must be freed using sctd_free to avoid
    memory leaks.

EXAMPLE
    int *numbers = sctd_new(int);
    bool *states = sctd_new(bool);

SEE ALSO
    sctd_free
*/

#endif

//LIB
#include <stdlib.h>
void* sctd_new_(size_t size){
    struct sctd__internal_s *dict_meta; // metadata for the dictionary
    void *p = scta_macro_new_(size);
    
    if (!p) ERETURN(NULL, "^");
    
    dict_meta = SCTA__EXTERN_META(p)[0] = malloc(sizeof(struct sctd__internal_s));
    
    if (!dict_meta) {
        scta_free(p);
        ERETURN(NULL, "could not allocate memory via malloc");
    }
    
    dict_meta->key_list = scta_new(char *); // scta_free

    if (!dict_meta->key_list) {
        scta_free(p);
        free(dict_meta);
        ERETURN(NULL, "^");
    }
    
    dict_meta->type = SCTD__TYPE; // 
    
    return p;
}


//TEST
#ifdef test_new
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    sct_debug = stderr;
    sctd_free(sctd_new(char[3000]));
    return 0;
}

#endif
