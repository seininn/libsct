#define SCT__INTERNAL__
#include "sctstd.h"
#include "sctd.h"
#include "scta.h"

//HEADER
#ifndef SCTD_PART_DELETE
#define SCTD_PART_DELETE

bool sctd_delete_(void **q, char *key);
#define sctd_delete(dict, key) (sctd_delete_(((void **) &(dict)), key))

/*
sctdoc:sctd_delete:

NAME
    sctd_delete

SYNOPSIS
    bool sctd_delete(type *dict, char *key)
    
DESCRIPTION
    This function deletes the key-value pair that has `key' as its key.

RETURN
    true if successful, and false otherwise.
    
SEE ALSO
    sctd_get
    sctd_set

*/

#endif

//LIB
#include <string.h>
#include <stdlib.h>

bool sctd_delete_(void **q, char *key){
    void *p = q?*q:NULL;
    // input checks
    if (!p) ERETURN(false, "required dict is null");
    if (!key) ERETURN(false, "required `key' is null");
    if (!SCTD__TYPE_CHECK(p)) ERETURN(false, "expecting dict to be a dictionary, got something else.");    

    // logic
    int i = sctd_get_index(p, key);
    
    if (i == -1) ERETURN(false, "key %s was not found", key);
    
    if (!scta_delete(p, i)) ERETURN(false, "could not delete key-value pair. dict might be inconsistent!");
    free(SCTD__META(p)->key_list[i]);
    if (!scta_delete(SCTD__META(p)->key_list, i)) ERETURN(false, "could not delete key-value pair. dict is definitely inconsistent!");

    *q=p;
    SCTD__META(p)->rv = i;
    return i;    
}


//TEST
#ifdef test_delete
#include <assert.h>
#include <stdio.h>

int main() {
    sct_debug = stderr;
    int *test;
    test = sctd_new(int);
    assert(test);
    assert(sctd_set(test, "a", 1));
    assert(sctd_set(test, "a", 1));
    assert(sctd_set(test, "b", 2));
    assert(sctd_set(test, "c", 3));
    assert(sctd_delete(test, "b"));
    assert(sctd_get(test, "a") == 1);
    assert(sctd_get(test, "c") == 3);
    assert(test[0] == 1);
    assert(test[1] == 3);
    assert(scta_length(SCTD__META(test)->key_list)==2);
    
    sctd_free(test);
    return 0;
}

#endif
