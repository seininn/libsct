#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>

#include "sctml.h"
#include "scta.h"
#include "scts.h"

int main (int argc, char **argv){
    char *p[6];
        
    assert(!strcmp(p[0]=sctml_percent_encode(" !\"#$%&'()*+,-./", NULL), 
        "%20%21%22%23%24%25%26%27%28%29%2a%2b%2c%2d%2e%2f"));
    assert(!strcmp(p[1]=sctml_percent_encode("a s!d\"f#g$h%j&k'l(z)x*c+v,b-n.m/q", NULL), 
        "a%20s%21d%22f%23g%24h%25j%26k%27l%28z%29x%2ac%2bv%2cb%2dn%2em%2fq"));
    assert(!strcmp(p[2]=sctml_percent_encode(" !\"#$%&'()*+,-./", ""), 
        "%20%21%22%23%24%25%26%27%28%29%2a%2b%2c%2d%2e%2f"));
    assert(!strcmp(p[3]=sctml_percent_encode(" !\"#$%&'()*+,-./", " ()+ /"), 
        " %21%22%23%24%25%26%27()%2a+%2c%2d%2e/"));
        
    assert(!strcmp(p[4]=sctml_xml_escape("<>&\"'", NULL), 
        "&lt;&gt;&amp;&quot;&apos;"));
    assert(!strcmp(p[5]=sctml_xml_escape("<>&\"'", ">"), 
        "&lt;>&amp;&quot;&apos;"));
    
    
    for (int i= 0; i< 6; ++i) sctml_free_string(p[i]);
    
    char **list = sctml_get_tags("<test id='temp'>111</test><test>22</test>  <test>3</test>", "test");
    assert(list);
    assert(!strcmp(list[0], "<test id='temp'>111</test>"));
    assert(!strcmp(list[1], "<test>22</test>"));
    assert(!strcmp(list[2], "<test>3</test>"));
    assert(!list[3]);
    sctml_free_list(list);
    
    list = sctml_get_tags("   <test>3</test>    ", "test");
    assert(list);
    assert(!strcmp(list[0], "<test>3</test>"));
    assert(!list[1]);
    sctml_free_list(list);
    
    list = sctml_get_tags("   <test><test>hi mom</test></test> <test></test>   ", "test");
    assert(list);
    assert(!strcmp(list[0], "<test><test>hi mom</test></test>"));
    assert(!strcmp(list[1], "<test></test>"));
    assert(!list[2]);
    sctml_free_list(list);
    
    assert(!sctml_get_tags("   <test><test>hi mom</test></test> <test>   ", "test"));
    assert(!sctml_get_tags("   <test><test>hi mom</test>   ", "test"));
    
    list = sctml_get_tags_from_file("test.d/test.xml", "title");
    assert(list);
    assert(!strcmp(list[0], "<title>some text</title>"));
    assert(!list[1]);
    sctml_free_list(list);
    
    
    
    char *z;
    #define TEST_STRIP_TAGS(TAG, RESULT)\
    z=sctml_strip_tags(TAG);\
    assert(!(!RESULT && z));\
    assert(!(RESULT && !z));\
    if (RESULT) assert(!(strcmp(z, RESULT?RESULT:"(NULL)")));\
    free(z);
    
    TEST_STRIP_TAGS("<jaja>1</jaja>", "1")
    TEST_STRIP_TAGS("<jaja some=stuff>22</jaja>", "22")
    TEST_STRIP_TAGS("<jaja some=stuff></jaja>", "")
    TEST_STRIP_TAGS("<jaja some=stuff>", NULL)
    
    
    
    
    
    printf("[ok] %s %x %s\n", "sctml", sct_version, __TIMESTAMP__);
    return 0;
}
