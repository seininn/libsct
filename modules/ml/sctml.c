/*

    sctml: Usefull HTML Routines
    Sulaiman (seininn) Mustafa
    Date: 2014-01-21
    
*/

#define SCT__INTERNAL__
#define _POSIX_C_SOURCE 200809L

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <unistd.h>
#include <errno.h>

#include "scta.h"
#include "scts.h"
#include "sctos.h"



char *sctml_percent_encode(char *s, char *exceptions){
    // implemented what I needed, prob. doesn't conferm to spec. Fix later
    char *out = scts_new(""); // output dynamic buffer
    
    if (!exceptions) exceptions = "";
    
    for (;*s;++s) {
        if ((*s >= 0x20 && *s < 0x30) && !strchr(exceptions, *s)) {
            char buf[4];
            snprintf(buf, 4, "%%%x", *s);
            scts_append(&out, buf);
        }
        else scts_append(&out, ((char [2]) {*s, 0}));
    }
    return out;
}

char *sctml_xml_escape(char *s, char *exceptions){
    // implemented what I needed, prob. doesn't conferm to spec. Fix later
    char *out = scts_new(""), *ptr;
    char *list_in = "\"'<>&";
    char *list_out[] = {"quot", "apos", "lt", "gt", "amp"};
    
    if (!exceptions) exceptions = "";
    
    for (;*s;++s) {
        if ((ptr = strchr(list_in, *s)) && !strchr(exceptions, *s)) {
            char buf[7] = {0};
            strcat(buf, "&");
            strcat(buf, list_out[ptr-list_in]);
            strcat(buf, ";");
            scts_append(&out, buf);
        }
        else scts_append(&out, ((char [2]) {*s, 0}));
    }
    return out;    
}


void sctml_free_string(char *s) {
    scts_free(s);
}


char **sctml_get_tags(char *string, char *tag){
    #define ERROR_PROC {\
        scta_free(opening);\
        scta_free(closing);\
        for (int i = 0; i < scta_length(list); ++i) free(list[i]);\
        scta_free(list);\
        return NULL;\
    }
        
    #define OP_JUMP_CALC {\
        ++c;\
        i+=opening_length;\
        {\
            char *tc = strstr(string+i, ">");\
            if (tc) {\
                i=tc-string;\
            }\
            else ERROR_PROC\
        }\
    }
        
    
    
    int tag_length = strlen(tag);
    
    char *opening, *closing;
    int opening_length, closing_length;
    
    char *t;
    
    // <tag
    t = scta_new(char);
    scta_push_array(t, "<", 1);
    scta_push_array(t, tag, tag_length+1);
    opening = t;
    opening_length = tag_length+1;
    
    // </tag>
    t = scta_new(char);
    scta_push_array(t, "</", 2);
    scta_push_array(t, tag, tag_length);
    scta_push_array(t, ">", 2);
    closing = t;
    closing_length = tag_length+3;
    
    
    char **list = scta_new(char *);

    while (*string) {
        // first occurence
        int c = 0; // match counter 
        int i = 0; // index
        
        string = strstr(string, opening);
        
        // if not found break, else skip to after opining string
        if (!string) break;
        else OP_JUMP_CALC

        
        // ignore submatches
        while (*(string+i) && c) {
            if (!strncmp(string+i, opening, opening_length)) {
                OP_JUMP_CALC
                continue;
            }
            if (!strncmp(string+i, closing, closing_length)) {
                --c;
                i+=closing_length;
                continue;
            }
            ++i;
        }
        
        if (c) {
            //error
            ERROR_PROC
        }
        if (i) { //match
            char *p = malloc(i+1);
            if (!p) {
                //error
                ERROR_PROC
            }
            strncpy(p, string, i);
            p[i] = 0;
            scta_push(list, p);
        }
        
        string = string+i;
    }
    scta_free(opening);
    scta_free(closing);
    scta_push(list, NULL);
    return list;
}

void sctml_free_list(char **list) {
    if (scta_length(list)) for (int i = 0; list[i]; ++i) free(list[i]);
    scta_free(list);
}


char *sctml_strip_tags(char *tag){
    char *start = strchr(tag, '>');
    char *end = strrchr(tag, '<');
    
    if (!start || !end) {
        return NULL;
    }
    
    if (start > end) {
        return NULL;
    }
    
    return strndup(start+1, end-start-1);
}



char **sctml_get_tags_from_file(char *filename, char *tag){
    // Also consider using lim for a more robust method (will be equivilant to js getTextContent)
    // HERE
    char *buf;
    if (!(buf = sctos_load(filename))) {
        ERETURN(NULL, "failed to load `%s'. enable sctos debuging for further information.\n", filename);
    }
    char **ret = sctml_get_tags(buf, tag);
    scta_free(buf);
    return ret;
}

