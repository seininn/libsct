/*

    sctml: Usefull XML/HTML Routines
    Sulaiman (seininn) Mustafa
    Date: 2014-01-21
    
*/

#ifndef sctml_H
#define sctml_H

#include <stdbool.h>
#include <stdio.h>
#include "sctstd.h"


char **sctml_get_tags(char *string, char *tag);
char **sctml_get_tags_from_file(char *filename, char *tag);
char *sctml_strip_tags(char *tag);

void sctml_free_list(char **list);
void sctml_free_string(char *s);

char *sctml_percent_encode(char *s, char *exceptions);
char *sctml_xml_escape(char *s, char *exceptions);

#endif
