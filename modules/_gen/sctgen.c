#define SCT__INTERNAL__
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "sctgen.h"

FILE *sctgen_config_debug = NULL;
int sctgen_config_version[3] = {1, 1, 1};

/*
bool sctgen_sort_multi(size_t size, bool ascending, ...) {
    bool swapped = false;
    int alen = 0;
    va_list ap;
    
    va_start(ap, ascending);
    while (true) {
        char *v = va_arg(ap, char *);
        if (v) ++alen;
        else break;
    }
    va_end(ap);
    
    // you might want to implement a better alorithm later (but more than 3 vars is unlickly) 
    for (int i = 0; i < alen; ++i) {
        va_start(ap, ascending);
        char r, *a, *b = va_arg(ap, char *);
        for (int j = 0; j < alen-i-1; ++j) {
            a = b;
            b = va_arg(ap, char *);
            r = memcmp(a, b, size);
            if ((r < 0 && !ascending) || (r > 0 && ascending)) {
                for (int i = 0; i < size; ++i) sctgen_swap(a[i], b[i]);
                if (!swapped) swapped = true;
            }
        }
        va_end(ap);
    }
    return true;

}  */
