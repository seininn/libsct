#include "sctgen.h"
#include <assert.h>

int main (){
    int a, b, c;
    
    a = 1; b = 2;
    assert(!sctgen_sort_ascending(a, b));
    assert(a == 1);
    assert(b == 2);
    
    a = 2; b = 1;
    assert(sctgen_sort_ascending(a, b));
    assert(a == 1);
    assert(b == 2);
    
    a = 1; b = 2;
    assert(sctgen_sort_descending(a, b));
    assert(a == 2);
    assert(b == 1);
    
    a = 2; b = 1;
    assert(!sctgen_sort_descending(a, b));
    assert(a == 2);
    assert(b == 1);
    
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            for (int k = 0; k < 3; ++k) {
                a = i;
                b = j;
                c = k;
                sctgen_sort_ascending_3(a, b, c);
                assert(a <= b);
                assert(b <= c);
                
                sctgen_sort_descending_3(a, b, c);
                assert(a >= b);
                assert(b >= c);
            }
        }
    }
    
    
    printf("[ok] %10s %d:%d:%d    %s\n", "sctgen", 
    sctgen_config_version[0], sctgen_config_version[1], 
    sctgen_config_version[2], __TIMESTAMP__);
    return 0;
}
