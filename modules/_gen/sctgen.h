#ifndef SCTGEN_H
#define SCTGEN_H

#include <stdio.h>
#include <stdbool.h>

extern FILE *sctgen_config_debug;
extern int sctgen_config_version[3];

//bool sctgen_sort_multi(size_t size, bool ascending, ...);

#define sctgen_swap(A, B) (A=A^B,B=A^B,A=A^B,true)
#define sctgen_sort_ascending(A, B) (A>B?(sctgen_swap(A, B)):false)
#define sctgen_sort_descending(A, B) (A<B?(sctgen_swap(A, B)):false)
#define sctgen_sort_ascending_3(A, B, C) (sctgen_sort_ascending(A, B), sctgen_sort_ascending(B, C), sctgen_sort_ascending(A, B))
#define sctgen_sort_descending_3(A, B, C) (sctgen_sort_descending(A, B), sctgen_sort_descending(B, C), sctgen_sort_descending(A, B))


#endif
