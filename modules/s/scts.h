#ifndef SCTS_H
#define SCTS_H

// STATIC SECTION
/*
sctdoc:scta:

    scts: A UTF-8-Aware String Module

    Sulaiman (seininn) Mustafa     2014-04-27
    
    This module is a part of libstc, a modular, c11-compliant, 
    toolkit library. use stcmod to determine this module's 
    dependencies, if any.
    
    -----------------------------------------------------------------------
    
    EXAMPLES
    
    API
    
    DEBUGING
    
    SCT offers a verbose mode as a debuging aid. To enable this mode, set 
    sct_debug to where want the verbose output to be written to (stderr,
    fopen()).
    
*/

#include <stddef.h>
#include <stdio.h>
#include "scta.h"
#include "sctstd.h"

#ifdef __STDC_UTF_32__ //TODO remove and enable the following
#include <uchar.h>
#else
#include <stdint.h>
typedef uint32_t char32_t; 
#endif


#define SCTS_STOP                   ((int) 0)
#define SCTS_APPEND                 ((int) 1)
#define SCTS_INSERT                 ((int) 2)
#define SCTS_JOIN                   ((int) 3)
#define SCTS_REPLACE                ((int) 4)
#define SCTS_SPLIT                  ((int) 5)
#define SCTS_SUBSTRING              ((int) 6)
#define SCTS_SELECT                 ((int) 8)
#define SCTS_FILTER                 ((int) 9)

#define SCTS_EMPTY                  ((int) 10)
#define SCTS_NOT_EMPTY              ((int) 11)
#define SCTS_CONTAINS               ((int) 12)
#define SCTS_NOT_CONTAINS           ((int) 13)
#define SCTS_MATCHES                ((int) 14)
#define SCTS_NOT_MATCHES            ((int) 15)
#define SCTS_ASSERT_LENGTH          ((int) 16)

#define SCTS_PRINT                  ((int) 17)


// PARTIALS SECTION: AUTOMATICLY GENERATED
//--append
#ifndef SCTS_PART_APPEND
#define SCTS_PART_APPEND

char32_t* scts_append_iu(char32_t *primary, char32_t *secondary);
char32_t* scts_append_mu(char32_t **primary, char32_t *secondary);
char* scts_append_ic(char *primary, char *secondary);
char* scts_append_mc(char **primary, char *secondary);
#define scts_append(primary, secondary) (_Generic( (0, primary), \
    char32_t *:         scts_append_iu, \
    char32_t **:        scts_append_mu, \
    const char32_t *:   scts_append_iu, \
    const char32_t **:  scts_append_mu, \
    char *:             scts_append_ic, \
    char **:            scts_append_mc, \
    const char *:       scts_append_ic, \
    const char **:      scts_append_mc  \
    )(primary, secondary))

/*
sctdoc:scts_append:

NAME
    scts_append

SYNOPSIS
    [char*][char32_t*] scts_append([char*][char32_t*] primary, [char*][char32_t*] secondary)
    
    char32_t* scts_append_iu(char32_t *primary, char32_t *secondary)
    char32_t* scts_append_mu(char32_t **primary, char32_t *secondary)
    char* scts_append_ic(char *primary, char *secondary)
    char* scts_append_mc(char **primary, char *secondary)
    
DESCRIPTION
    `scts_append' concatentates `secondary' to `primary' and returns the 
    result.

RETURN
    Upon success, this function returns (a posibly moved) pointer `primary' if 
    a mutating version is used, or a pointer to a new object if a non-mutating 
    version is used. The new object pointer must be freed using scts_free to 
    avoid memory leaks.

EXAMPLE
    scts_append("abc", "def") // "abcdef"

SEE ALSO
    scts_new
    scts_insert
    scts_free
*/

#endif


    
//--cached_length
#ifndef SCTS_PART_CACHED_LENGTH
#define SCTS_PART_CACHED_LENGTH

int scts_cached_length(void *primary);
/*
sctdoc:scts_cached_length:

NAME
    scts_cached_length

SYNOPSIS
    int scts_cached_length([char*][char32_t*] primary)
    
    int scts_cached_length(void *primary)
    
DESCRIPTION
    This function takes a string and returns the number of elements the 
    string has memory for (excluding the terminating null). This function 
    does not scan the string in its entrity, and only checks the cached 
    value stored in the string itself.
    
    To reetrate, this function does not report the same conceptual value 
    that the other length functions report. This function returns the number 
    of "cells" the allocated memory can store, whereas the others report 
    actual lengths.
    
    This function is only designed to work with new strings returned by 
    this module and not on general string litrals. Passing string litrals,
    strings allocated by malloc, or any strings other than those allocated
    by this module is undefined behaviour.
    
    It goes without saying that changes in the string's content will not 
    be taken into account (adding \0 midstring).
    
    DO NOT USE THIS FUNCTION UNLESS YOU ARE CONFIDANT THAT THE STRINGS MEET
    THE CRITERIA DESCRIBED ABOVE.
    
    Consider using the other length functions named at the end of this 
    document.

RETURN
    This function returns byte length required to store a sequenced unicode 
    string, or -1 if the string contains invalid codepoints or ill-formed 
    byte sequences.

EXAMPLE
    scts_legacy_length(scts_new("سلام"))  == ‭8
    scts_legacy_length(U"سلام") == ‭4
    scts_legacy_length("upon")  == ‭4

SEE ALSO
    scts_length
    scts_cached_length
*/

#endif

    
//--codepoint
#ifndef SCTS_PART_CODEPOINT
#define SCTS_PART_CODEPOINT
char32_t scts_codepoint(char *c);
/*
sctdoc:scts_codepoint:

NAME
    scts_codepoint

SYNOPSIS
    char32_t scts_codepoint(char *c);
    
DESCRIPTION
    This function returns the codepoint of the character sequence pointed to
    by `c'.

RETURN
    This function returns the codepoint on success, and -1 sequince is 
    ill-formed.

EXAMPLE
    scts_codepoint("٦") == 0x666;

SEE ALSO
    scts_sequence_codepoint
    scts_sequence_length
*/

#endif


    
//--depprint
#ifndef SCTS_PART_DEPPRINT
#define SCTS_PART_DEPPRINT
int scts_depprint_u(FILE *stream, char32_t* primary);
int scts_depprint_c(FILE *stream, char* primary);
#define scts_depprint(stream, primary) (_Generic( (0, primary), \
    char32_t *:         scts_depprint_u, \
    char *:             scts_depprint_c, \
    const char32_t *:   scts_depprint_u, \
    const char *:       scts_depprint_c \
    )(stream, primary))

//TODO full format support with ... vararg
//TODO write tests for print
//TODO unifiy return values and meanings

/*
sctdoc:scts_depprint:

NAME
    scts_depprint

SYNOPSIS
    int generic_dummy([char*][char32_t*] primary)
    
    int generic_dummy_c(char *primary)
    int generic_dummy_u(char32_t *primary)
    
DESCRIPTION
    ...

RETURN
    ...

EXAMPLE
    ...

SEE ALSO
    ...
*/

#endif


    
//--filter
#ifndef SCTS_PART_FILTER
#define SCTS_PART_FILTER
bool scts_filter_c(char ***list_ptr, int criteria, char *what);
bool scts_filter_u(char32_t ***list_ptr, int criteria, char32_t *what);
#define scts_filter(list, criteria, what) (_Generic( (0, (list)), \
    char32_t **:       scts_filter_u, \
    const char32_t **: scts_filter_u, \
    char **:       scts_filter_c, \
    const char **: scts_filter_c \
    )(&(list), (criteria), (what)))

/*
sctdoc:scts_filter:

NAME
    scts_filter

SYNOPSIS

    bool scts_filter(   [char**][char32_t**] list, 
                        int criteria, 
                        [char**][char32_t**] what)
    
    bool scts_filter_c(char ***list_ptr, int criteria, char *what);
    bool scts_filter_u(char32_t ***list_ptr, int criteria, char32_t *what);
    
DESCRIPTION
    This function/macro accepts `list' and filters out elements based on the 
    criteria and `what'.
    
    CAUTION: This function should ONLY be used on lists returned by this 
             library.
    
    There are 6 criterias by which strings are filtered out:
        
        SCTS_EMPTY                  Filters out empty strings
        SCTS_NOT_EMPTY              Inverse of SCTS_EMPTY
        SCTS_CONTAINS               Filters out strings containing the sub-
                                    string `what'.
        SCTS_NOT_CONTAINS           Filters out strings that do not contain 
                                    the substring `what'.
        SCTS_MATCHES                Filters out strings that match `what'.
        SCTS_NOT_MATCHES            Filters out strings that don't match `what'.

RETURN
    This function returns false if an internal error occurs, and true 
    otherwise.

EXAMPLE
    char **l;
    l = scts_split("My name is bobias!", " ");
    scts_filter(l, SCTS_EMPTY, NULL);
    scts_filter(l, SCTS_CONTAINS, "a");
    char *r = scts_join("", l);
    puts(r); // Output: "Myis"
    scts_free(r);
    scts_free(l);
    
*/

#endif


    
//--find
#ifndef SCTS_PART_FIND
#define SCTS_PART_FIND

int scts_find_u(char32_t *restrict haystack, char32_t *restrict needle);
int scts_find_c(char *restrict haystack,  char *restrict needle);

#define scts_find(haystack, needle) (_Generic( (0, haystack), \
    char32_t *:         scts_find_u, \
    const char32_t *:   scts_find_u, \
    char *:             scts_find_c, \
    const char *:       scts_find_c \
    )(haystack, needle))

/*
sctdoc:scts_find:

NAME
    scts_find

SYNOPSIS
    int scts_find([char*][char32_t*] heystack, [char*][char32_t*] needle)
    
    int scts_find_u(char32_t *haystack, char32_t *needle)
    int scts_find_c(char *haystack, char *needle)
    
DESCRIPTION
    This function locates the substring `needle' in the string `haystack', and
    returns the index at which it starts.

RETURN
    This function returns the starting point of the substring; and -1 if the 
    string was not found, `needle' is null, or if an error occured.

EXAMPLE
    scts_find("abcdef", "e"); // 4

SEE ALSO
    scts_matches
    scts_replace
*/

#endif


    
//--flow
#ifndef SCTS_PART_FLOW
#define SCTS_PART_FLOW

// Definition of operation/premitive constants is int init.h

char32_t* scts_flow_iu(char32_t *primary, ...);
char32_t* scts_flow_mu(char32_t **primary, ...);
char* scts_flow_ic(char *primary, ...);
char* scts_flow_mc(char **primary, ...);

#define scts_flow(primary, ...) (_Generic( (0, primary), \
    char32_t *:         scts_flow_iu, \
    char32_t **:        scts_flow_mu, \
    const char32_t *:   scts_flow_iu, \
    const char32_t **:  scts_flow_mu, \
    char *:         scts_flow_ic, \
    char **:        scts_flow_mc, \
    const char *:   scts_flow_ic, \
    const char **:  scts_flow_mc \
    )(primary, __VA_ARGS__, SCTS_STOP))
/*
sctdoc:scts_flow:

NAME
    scts_flow

SYNOPSIS
    [char*][char32_t*] scts_flow([char*][char32_t*] primary, ...)
    
    char* scts_flow_ic(char *primary, ...)//
    char* scts_flow_mc(char **primary, ...)//
    char32_t* scts_flow_iu(char32_t *primary, ...)
    char32_t* scts_flow_mu(char32_t **primary, ...)
    
DESCRIPTION
    preform string operations in a sequential form; appling the result 
    of an operation to the one that follows it, and provides premative 
    flow breqaking facilities.
    
    
PRIMATIVES
    The input stream of each primative must be compatible with the
    output stream of the perceeding primative. There are two types 
    of stream types: a singleton stream (s), and a list stream (l).
    
    A singleton stream consists of a single string that is operated 
    on by primatives and passed from one primative to the next.
    
    A list stream is a set of strings that is operated on as a single 
    unit by primatives and passed from one primative to the next.
    
    The the input type of the fist primative must be that of a 
    singleton stream.
    
    
    SCTS_STOP  [s -> s]    
        Every call to scts_flow_* MUST have this primative as the 
        last argument. If the generic macro is used, ending the 
        argument list with SCTS_STOP is optional.
        
    SCTS_APPEND  [s -> s]
        Identical to `scts_append'.

    SCTS_INSERT  [s -> s]
        Identical to `scts_append'.

    SCTS_REPLACE  [s -> s]
        Identical to `scts_append'.

    SCTS_SUBSTRING  [s -> s]
        Identical to `scts_append'.

    SCTS_SPLIT  [s -> s]
        Identical to `scts_append'.

    SCTS_JOIN  
        
        [s -> s] 
            Identical to `scts_join'.
        [l -> s] ([char*][char32_t*] joint)
            Takes a string that is used to join the elements of the 
            stream list.
        
    SCTS_SELECT  [l -> s] (int i)
        sets the ith element of the stream list as the stream 
        string.
        
    SCTS_FILTER  [l -> l]
        Identical to `scts_filter'.
                
    SCTS_NOT_EMPTY  [s -> s] ()
        stops the stream and returns an empty string if the stream 
        string is empty.
        
    SCTS_CONTAINS  
    
        [s -> s] ([char*][char32_t*] string)
            stops the stream and returns an empty string if the stream
            string does NOT contain `string'.
        [l -> l] ([char*][char32_t*] string)
            stops the stream and returns an empty string if the stream
            list does NOT contain `string'.
        
    SCTS_NOT_CONTAINS  
        
        [s -> s] ([char*][char32_t*] string)
            stops the stream and returns an empty string if the stream
            string contains `string'.
        [l -> l] ([char*][char32_t*] string)
            stops the stream and returns an empty string if the stream
            list contains `string'.
        
    SCTS_NOT_MATCHES  [s -> s] ([char*][char32_t*] string)
        stops the stream and returns an empty string if the stream
        string matchs `string'.
        
    SCTS_ASSERT_LENGTH [l -> l] (int i)
        stops the stream and returns an empty string if the length 
        of the stream list is nit `i'
        
    ----
    SCTS_PRINT
        A special operation that prints the stream, be it string or list,
        to the standard error for the library `sct_debug'
            
    
        

RETURN
    ...

EXAMPLE
    ...

SEE ALSO
    ...
*/

#endif


    
//--insert
#ifndef SCTS_PART_INSERT
#define SCTS_PART_INSERT

char32_t* scts_insert_iu(char32_t *primary, char32_t *secondary, int where);
char32_t* scts_insert_mu(char32_t **primary, char32_t *secondary, int where);
char* scts_insert_ic(char *primary, char *secondary, int where);
char* scts_insert_mc(char **primary, char *secondary, int where);
#define scts_insert(primary, secondary, where) (_Generic( (0, primary), \
    char32_t *:         scts_insert_iu, \
    char32_t **:        scts_insert_mu, \
    const char32_t *:   scts_insert_iu, \
    const char32_t **:  scts_insert_mu, \
    char *:             scts_insert_ic, \
    char **:            scts_insert_mc, \
    const char *:       scts_insert_ic, \
    const char **:      scts_insert_mc  \
    )(primary, secondary, where))

/*
sctdoc:scts_insert:

NAME
    scts_insert

SYNOPSIS    
    [char*][char32_t*] scts_insert([char*][char32_t*] primary, [char*][char32_t*] secondary, int where)
    
    char32_t* scts_insert_iu(char32_t *primary, char32_t *secondary, int where)
    char32_t* scts_insert_mu(char32_t **primary, char32_t *secondary, int where)
    char* scts_insert_ic(char *primary, char *secondary, int where)
    char* scts_insert_mc(char **primary, char *secondary, int where)
    
DESCRIPTION
    Function `scts_insert' inserts `secondary' at `where'. `where' supports 
    negative indexing.

RETURN
    Upon success, this function returns (a posibly moved) pointer `primary' if 
    a mutating version is used, or a pointer to a new object if a non-mutating 
    version is used. The new object pointer must be freed using scts_free to 
    avoid memory leaks.

EXAMPLE
    scts_insert("there.", "hi ", 0); // "hi there."

SEE ALSO
    scts_append
    
*/

#endif


    
//--join
#ifndef SCTS_PART_JOIN
#define SCTS_PART_JOIN
char32_t* scts_join_iu(char32_t *primary, char32_t **list);
char32_t* scts_join_mu(char32_t **primary, char32_t **list);
char* scts_join_ic(char *primary, char **list);
char* scts_join_mc(char **primary, char **list);
#define scts_join(primary, list) (_Generic( (0, primary), \
    char32_t *:         scts_join_iu, \
    char32_t **:        scts_join_mu, \
    const char32_t *:   scts_join_iu, \
    const char32_t **:  scts_join_mu, \
    char *:             scts_join_ic, \
    char **:            scts_join_mc, \
    const char *:       scts_join_ic, \
    const char **:      scts_join_mc  \
    )(primary, list))

/*
sctdoc:scts_join:

NAME
    scts_join

SYNOPSIS
    [char*][char32_t*] scts_join([char*][char32_t*] primary, [char*[]][char32_t*[]] list)
    
    char32_t* scts_join_iu(char32_t *primary, char32_t **list)
    char32_t* scts_join_mu(char32_t **primary, char32_t **list)
    char* scts_join_ic(char *primary, char **list)
    char* scts_join_mc(char **primary, char **list)
    
DESCRIPTION
    This function joins the elements of the null-terminated array `list' with 
    `primary' as a separator.

RETURN
    Upon success, this function returns (a posibly moved) pointer `primary' if 
    a mutating version is used, or a pointer to a new object if a non-mutating 
    version is used. The new object pointer must be freed using scts_free to 
    avoid memory leaks.

EXAMPLE
    char *list[] = {"a", "b", "c"};
    scts_join(", ", list); // "a, b, c"

SEE ALSO
    scts_split
    scts_append
    scts_insert
*/

#endif


    
//--legacy_length
#ifndef SCTS_PART_LEGACY_LENGTH
#define SCTS_PART_LEGACY_LENGTH

int scts_legacy_length_u(char32_t * restrict primary);
int scts_legacy_length_c(char * restrict primary);

#define scts_legacy_length(primary) (_Generic( (0, primary), \
    char32_t *:scts_legacy_length_u, \
    const char32_t *:scts_legacy_length_u, \
    char *: scts_legacy_length_c, \
    const char *: scts_legacy_length_c \
    )(primary))

/*
sctdoc:scts_legacy_length:

NAME
    scts_legacy_length

SYNOPSIS
    int scts_legacy_length([char*][char32_t*] primary)
    
    int scts_legacy_length_c(char *primary)
    int scts_legacy_length_u(char32_t *primary)
    
DESCRIPTION
    This function takes a string and returns the number of bytes required to 
    store its utf-8 representation. 
    it contains. This function also checks if the string contains invalid 
    codepoints or ill-formed byte sequences as defined by the unicode standard.

RETURN
    This function returns byte length required to store a sequenced unicode 
    string, or -1 if the string contains invalid codepoints or ill-formed 
    byte sequences.

EXAMPLE
    scts_legacy_length("سلام")  == ‭8
    scts_legacy_length(U"سلام") == ‭8
    scts_legacy_length("upon")  == ‭4

SEE ALSO
    scts_length
    scts_cached_length
*/

#endif

    
//--length
#ifndef SCTS_PART_LENGTH
#define SCTS_PART_LENGTH
int scts_length_c(char *primary);
int scts_length_u(char32_t *primary);
int scts_length_l(void **primary); //TODO add to the macro bellow: WARNING:  the compiler seems to select the fuction when 
                                      //                                        given an argument with type (char32_t *).
                                      //                                        remove length_l after modding the macro and run.
                                      //                                        problem appeared in new.c:53

#define scts_length(primary) (_Generic( (0, primary), \
    char32_t *:scts_length_u, \
    const char32_t *:scts_length_u, \
    char *: scts_length_c, \
    const char *: scts_length_c \
    )(primary))

/*
sctdoc:scts_length:

NAME
    scts_length

SYNOPSIS
    int scts_length([char*][char32_t*] primary)
    
    int scts_length_c(char *primary)
    int scts_length_u(char32_t *primary)
    
DESCRIPTION
    This function takes a string and returns the number of unicode codepoints
    it contains. This function also checks if the string contains invalid 
    codepoints or ill-formed byte sequences as defined by the unicode standard.

RETURN
    This function returns the unicode length of a given string, or -1 if the 
    string contains invalid codepoints or ill-formed byte sequences.
    
EXAMPLE
    scts_length("سلام")  == ‭4
    scts_length(U"سلام") == ‭4
    scts_length("upon")  == ‭4

SEE ALSO
    scts_legacy_length
    scts_cached_length
    
*/

#endif

    
//--matches
#ifndef SCTS_PART_MATCHES
#define SCTS_PART_MATCHES
bool scts_matches_u(char32_t *primary, char32_t *secondary);
bool scts_matches_c(char *primary, char *secondary);
#define scts_matches(primary, secondary) (_Generic( (0, primary), \
    char32_t *:         scts_matches_u, \
    const char32_t *:   scts_matches_u, \
    char *:             scts_matches_c, \
    const char *:       scts_matches_c \
    )(primary, secondary))

/*
sctdoc:scts_matches:

NAME
    scts_matches

SYNOPSIS
    int generic_dummy([char*][char32_t*] primary)
    
    int generic_dummy_c(char *primary)
    int generic_dummy_u(char32_t *primary)
    
DESCRIPTION
    ...

RETURN
    ...

EXAMPLE
    ...

SEE ALSO
    ...
*/

#endif


    
//--new
#ifndef SCTS_PART_new
#define SCTS_PART_new
char* scts_new_c(char *primary);
char32_t* scts_new_u(char32_t* primary);
void scts_free_pointer(void *primary);
void scts_free_list(void *primary);

#define scts_new(primary) (_Generic( (0, primary), \
    char32_t *:scts_new_u, \
    const char32_t *:scts_new_u, \
    char *: scts_new_c, \
    const char *: scts_new_c \
    )(primary))
    
    
#define scts_free(primary) (_Generic( (0, primary), \
    const char32_t *: scts_free_pointer, \
    const char *: scts_free_pointer, \
    char32_t *: scts_free_pointer, \
    char *: scts_free_pointer, \
    const char32_t **: scts_free_list, \
    const char **: scts_free_list, \
    char32_t **: scts_free_list, \
    char **: scts_free_list \
    )(primary))
#endif


/*
sctdoc:scts_new:

NAME
    scts_new

SYNOPSIS
    [char32_t*][char*] scts_new([char32_t*][char*] primary)
    
    char*       scts_new_c(char     *primary)
    char32_t*   scts_new_u(char32_t *primary)
    
DESCRIPTION
    Clones a string. primary can be `NULL' to produce an empty string.

RETURN
    This function returns a pointer to the new string, or NULL upon error. A 
    pointer returned by this function must be freed.
    
SEE ALSO
    scts_transcode
    scts_substring
    scts_free
*/



/*
sctdoc:scts_free:

NAME
    scts_free

SYNOPSIS
    void scts_free([char32_t**][char**][char32_t*][char*] primary)
    
    void scts_free_pointer(void *primary)
    void scts_free_list(void **primary)
    
DESCRIPTION
    This function frees any object allocated and returned by this module.

SEE ALSO
    scts_new
*/



    
//--replace
#ifndef SCTS_PART_REPLACE
#define SCTS_PART_REPLACE
char* scts_replace_mc(char **primary, char *target, char *replacement);
char* scts_replace_ic(char *primary, char *target, char *replacement);
char32_t* scts_replace_mu(char32_t **primary, char32_t *target, char32_t *replacement);
char32_t* scts_replace_iu(char32_t *primary, char32_t *target, char32_t *replacement);
#define scts_replace(primary, target, replacement) (_Generic( (0, primary), \
    char32_t *:         scts_replace_iu, \
    char32_t **:        scts_replace_mu, \
    const char32_t *:   scts_replace_iu, \
    const char32_t **:  scts_replace_mu, \
    char *:             scts_replace_ic, \
    char **:            scts_replace_mc, \
    const char *:       scts_replace_ic, \
    const char **:      scts_replace_mc  \
    )(primary, target, replacement))

/*
sctdoc:scts_replace:

NAME
    scts_replace

SYNOPSIS
    int generic_dummy([char*][char32_t*] primary)
    
    int generic_dummy_c(char *primary)
    int generic_dummy_u(char32_t *primary)
    
DESCRIPTION
    ...

RETURN
    ...

EXAMPLE
    ...

SEE ALSO
    ...
*/

#endif


    
//--sequence_codepoint
#ifndef SCTS_PART_SEQUENCE_CODEPOINT
#define SCTS_PART_SEQUENCE_CODEPOINT
char* scts_sequence_codepoint(char32_t c, char buf[restrict static 5]);
/*
sctdoc:scts_sequence_codepoint:

NAME
    scts_sequence_codepoint

SYNOPSIS
    char* scts_sequence_codepoint(char32_t c, char buf[restrict static 5])
    
DESCRIPTION
    This function is the inverse of scts_codepoint; it takes a codepoint and 
    generates its sequenced representation. The second argument must be an 
    array that can hold at least 5 elements.

RETURN
    This function returns `buf' on success, and NULL if `c' is an invalid 
    codepoint.

EXAMPLE
    scts_sequence_codepoint(0x666, buf); // ==  "٦"

SEE ALSO
    scts_codepoint
    scts_sequence_length
*/

#endif


    
//--sequence_length
#ifndef SCTS_PART_SEQUENCE_LENGTH
#define SCTS_PART_SEQUENCE_LENGTH
int scts_sequence_length_c(char *c);
int scts_sequence_length_u(char32_t c);
#define scts_sequence_length(primary) (_Generic( (0, primary), \
    char32_t:scts_sequence_length_u, \
    const char32_t:scts_sequence_length_u, \
    char *: scts_sequence_length_c, \
    const char *: scts_sequence_length_c \
    )(primary))

/*
sctdoc:scts_sequence_length:

NAME
    scts_sequence_length

SYNOPSIS
    int scts_sequence_length([char*][char32_t*] primary)
    
    int scts_sequence_length_c(char *c);
    int scts_sequence_length_u(char32_t c);
    
DESCRIPTION
    scts_sequence_length returns the number of bytes required to store a 
    the utf8 representation of a given codepoint `c'.

RETURN
    This function returns a non-negative number upon success and -1 if the 
    codepoint is not valid.

EXAMPLE
    scts_sequence_length("😻") == 4;

SEE ALSO
    scts_sequence_codepoint
*/

#endif


    
//--split
#ifndef SCTS_PART_SPLIT
#define SCTS_PART_SPLIT

char32_t** scts_split_u(char32_t *primary, char32_t *delemeter);
char** scts_split_c(char *primary, char *delemeter);

#define scts_split(primary, split) (_Generic( (0, primary), \
    char32_t *:         scts_split_u, \
    const char32_t *:   scts_split_u, \
    char *:             scts_split_c, \
    const char *:       scts_split_c \
    )(primary, split))

/*
sctdoc:scts_split:

NAME
    scts_split

SYNOPSIS
    [char**][char32_t**] 
        scts_split([char*][char32_t*] primary, [char*][char32_t*] delemeter)
    
    char32_t** scts_split_u(char32_t *primary, char32_t *delemeter)
    char** scts_split_c(char *primary, char *delemeter)
    
DESCRIPTION
    This function splits `primary' into multiple strings using `delemeter' as 
    the seperator, and returns the result as a string list.

RETURN
    Returns a list of strings on success, and NULL on failure. The resulting 
    list must be freed using `scts_free' or equivilant.

EXAMPLE
    scts_split("sir fella"); // === (char[]) {[0]="sir", [1]="fella", [2]=NULL}

SEE ALSO
    scts_join
    scts_free
*/

#endif


    
//--substring
#ifndef SCTS_PART_SUBSTRING
#define SCTS_PART_SUBSTRING
char32_t* scts_substring_iu(char32_t *primary, int from, int to);
char32_t* scts_substring_mu(char32_t **primary, int from, int to);
char* scts_substring_ic(char *primary, int from, int to);
char* scts_substring_mc(char **primary, int from, int to);
#define scts_substring(primary, from, to) (_Generic( (0, primary), \
    char32_t *:         scts_substring_iu, \
    char32_t **:        scts_substring_mu, \
    const char32_t *:   scts_substring_iu, \
    const char32_t **:  scts_substring_mu, \
    char *:             scts_substring_ic, \
    char **:            scts_substring_mc, \
    const char *:       scts_substring_ic, \
    const char **:      scts_substring_mc  \
    )(primary, from, to))

#define SCTS_EOS ((int)((~((unsigned int) 0))>>1)) //TODO switch to the standard def later

/*
sctdoc:scts_substring:

NAME
    scts_substring

SYNOPSIS
    [char*][char32_t*] scts_substring(
        [char*][&char*][char32_t*][&char32_t*] primary, int from, int to)
    
    char32_t* scts_substring_iu(char32_t *primary, int from, int to)
    char32_t* scts_substring_mu(char32_t **primary, int from, int to)
    char* scts_substring_ic(char *primary, int from, int to)
    char* scts_substring_mc(char **primary, int from, int to)
    
DESCRIPTION
    This function takes a string and computes a substring. if a mutable version
    is used, `primary' will point to the calculated substring. both functions
    return the substring.
    
    Indices can be negative, indicating the location from the end of the 
    string.
    
    SCTS_EOS is a special value that when passed as `to' denotes the end of the 
    string.
    
RETURN
    If a mutable function is called, the function returns the substring pointer 
    that is stored in `primary', and NULL if an error occurs. `primary' will 
    not be affected unless the call is successfull.
    
    If an imutable function is called, the function returns a new string 
    which must be freed to avoid memory leaks, and NULL if an error occurs.

EXAMPLE
    // mutable = scts_new("سر فلا كبا بك الفرس")
    scts_substring(&mutable, 0, 3) // mutable == "سر "
    scts_substring(mutable,  0, 1) // mutable == "سر "; returns "س"
    
    // immutable = "سلام"
    scts_substring(immutable, -1, SCTS_EOS) // ; returns "م"

SEE ALSO
    scts_new
    scts_transcode
*/

#endif


    
//--transcode
#ifndef SCTS_PART_TRANSCODE
#define SCTS_PART_TRANSCODE
char32_t* scts_transcode_c(char * restrict primary);
char* scts_transcode_u(char32_t * restrict primary);

#define scts_transcode(primary) (_Generic( (0, primary), \
    char32_t *:scts_transcode_u, \
    const char32_t *:scts_transcode_u, \
    char *: scts_transcode_c, \
    const char *: scts_transcode_c \
    )(primary))

/*
sctdoc:scts_transcode:

NAME
    scts_transcode

SYNOPSIS
    [char32_t*][char*] scts_transcode( [char*][char32_t*] primary)
    
    char32_t*   scts_transcode_c(char *primary)
    char*       scts_transcode_u(char32_t *primary)
    
DESCRIPTION
    Accepts a legacy string or a unicode string and transcodes it into the 
    oposit encoding.

RETURN
    This function returns a pointer to the new string, or NULL upon error. A 
    pointer returned by this function must be freed.

SEE ALSO
    scts_new
    scts_substring
*/


#endif

    
#endif
