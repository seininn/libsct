#define SCT__INTERNAL__
#include "scts.h"

// STATIC SECTION



// PARTIALS SECTION: AUTOMATICLY GENERATED
//--append
char32_t* scts_append_iu(char32_t *primary, char32_t *secondary){
    char32_t *out;
    int length;
    
    if (!primary)  ERETURN(NULL, "null string supplied");
    if ((length = scts_length(primary)) == -1) ERETURN(NULL, "malformed string supplied");
    if (!(out = scts_insert_iu(primary, secondary, length))) ERETURN(NULL, "internal call failed");
    
    
    return out;
}

char32_t* scts_append_mu(char32_t **primary, char32_t *secondary){
    char32_t *n;
    
    if (!primary || !(*primary) || !secondary)  ERETURN(NULL, "null string supplied");
    
    n = scts_append_iu(*primary, secondary);
    if (!n) ERETURN(NULL, "internal call to imutable counterpart failed");
    scts_free(*primary);
    *primary = n;
    return n;
}

char* scts_append_ic(char *primary, char *secondary){
    char32_t *n, *m;
    
    if (!primary || !secondary) ERETURN(NULL, "null string supplied");
    
    n = scts_transcode(primary);
    m = scts_transcode(secondary);
    
    if (!n) ERETURN(NULL, "could not transcode byte sequence");
    if (!scts_append_mu(&n, m)) {
        scts_free(n);
        scts_free(m);
        ERETURN(NULL, "internal call to mutable unicode counterpart failed");
    }
    primary = scts_transcode(n);
    if (!primary) {
        scts_free(n);
        scts_free(m);
        ERETURN(NULL, "internal library failure"); // should never fail to transcode happen unless something is broken
    }
    scts_free(n);
    scts_free(m);
    return primary;
}


char* scts_append_mc(char **primary, char *secondary){
    char *n;

    if (!primary || !(*primary) || !secondary)  ERETURN(NULL, "null string supplied");
    
    n = scts_append_ic(*primary, secondary);
    if (!n) ERETURN(NULL, "internal call to imutable counterpart failed");
    scts_free(*primary);
    *primary = n;
    return n;
}



    
//--cached_length
int scts_cached_length(void *primary){
    // input checks
    if (!primary) return 0;
    // logic
    return scta_length(primary);
}


    
//--codepoint
char32_t scts_codepoint(char *c){
    // Unicode Standard 6.2: Section 3.9: Page 95
    // (char32_t) -1 error could should be fine because it's not a valid code point
    // validation and string length assertions are preformed in 
    // byte_character_size
    int size = scts_sequence_length(c);
    if (size == -1) ERETURN(-1, "^");
    
    if (size == 1) return c[0];
    else if (size == 2) {
        return  (((char32_t) 0x1f & c[0]) << 6) | 
                ((char32_t) 0x3f & c[1]) ;
    }
    else if (size == 3) {
        return  (((char32_t) 0x0f & c[0]) << 12) | 
                (((char32_t) 0x3f & c[1]) << 6) |
                ((char32_t) 0x3f & c[2]) ;
    }
    else if (size == 4) {
        return  (((char32_t) 0x07 & c[0]) << 18) | 
                (((char32_t) 0x3f & c[1]) << 12) |
                (((char32_t) 0x3f & c[2]) << 6) |
                ((char32_t) 0x3f & c[3]);
    }    
    // If this occurs, something very wrong is happening in scts_byte_character_size
    else ERETURN((char32_t) -1, "could not map codepoint to well-formed byte sequence"); 
}



    
//--depprint
int scts_depprint_u(FILE *stream, char32_t* primary){
    char seq[5];
    int i = 0;
    // input checks
    if (!primary) ERETURN(-1, "cannot depprint NULL string");
    // logic
    for (i = 0; primary[i]; ++i) {
        if (!scts_sequence_codepoint(primary[i], seq)) ERETURN(-1, "Could not sequence codepoint");
        fputs(seq, stream);
    }
    return i;
}

int scts_depprint_c(FILE *stream, char* primary){
    if (!primary) ERETURN(-1, "cannot depprint NULL string");
    return fputs(primary, stream);
}


    
//--filter

bool matches_u(void *a, void *b) {
    return scts_matches_u(a, b);
}

bool not_matches_u(void *a, void *b) {
    return !matches_u(a, b);
}

bool contains_u(void *a, void *b) {
    return (scts_find_u(a, b)!=-1);
}

bool not_contains_u(void *a, void *b) {
    return !contains_u(a, b);
}

bool matches_c(void *a, void *b) {
    return scts_matches_c(a, b);
}

bool not_matches_c(void *a, void *b) {
    return !matches_c(a, b);
}

bool contains_c(void *a, void *b) {
    return (scts_find_c(a, b)!=-1);
}

bool not_contains_c(void *a, void *b) {
    return !contains_c(a, b);
}


bool scts_filter_u(char32_t ***list_ptr, int criteria, char32_t *what){
    // input checks
    if (!list_ptr || !list_ptr[0]) ERETURN(false, "list_ptr is null or points to null"); 
    switch (criteria) {
        case SCTS_EMPTY:
        case SCTS_NOT_EMPTY:
        case SCTS_CONTAINS:
        case SCTS_NOT_CONTAINS:
        case SCTS_MATCHES:
        case SCTS_NOT_MATCHES:
            break;
        default:
            ERETURN(false, "`criteria' is not a valid criteria"); 
    }
    if (!what && ((criteria!=SCTS_EMPTY) && (criteria!=SCTS_NOT_EMPTY))) ERETURN(false, "what is null"); 
    
    // logic
    
    switch (criteria) {
    
        case SCTS_EMPTY:
            what = (char32_t[]) {0};
            if (!(scta_filter((*list_ptr), what, matches_u, scts_free_pointer)))
                ERETURN(false, "^");
            break;
        
        case SCTS_NOT_EMPTY:
            what = (char32_t[]) {0};
            if (!(scta_filter((*list_ptr), what, not_matches_u, scts_free_pointer)))
                ERETURN(false, "^");
            break;
        
        case SCTS_CONTAINS:
            if (!(scta_filter((*list_ptr), what, contains_u, scts_free_pointer)))
                ERETURN(false, "^");
            break;
        case SCTS_NOT_CONTAINS:
            if (!(scta_filter((*list_ptr), what, not_contains_u, scts_free_pointer)))
                ERETURN(false, "^");
            break;
            
        case SCTS_MATCHES:
            if (!(scta_filter((*list_ptr), what, matches_u, scts_free_pointer)))
                ERETURN(false, "^");
            break;
            
        case SCTS_NOT_MATCHES:
            if (!(scta_filter((*list_ptr), what, not_matches_u, scts_free_pointer)))
                ERETURN(false, "^");
            break;
            
        default:
            ERETURN(false, "`criteria' is not a valid criteria"); 
    }
    
    return true;
}



bool scts_filter_c(char ***list_ptr, int criteria, char *what){
    // input checks
    if (!list_ptr || !list_ptr[0]) ERETURN(false, "list_ptr is null or points to null"); 
    switch (criteria) {
        case SCTS_EMPTY:
        case SCTS_NOT_EMPTY:
        case SCTS_CONTAINS:
        case SCTS_NOT_CONTAINS:
        case SCTS_MATCHES:
        case SCTS_NOT_MATCHES:
            break;
        default:
            ERETURN(false, "`criteria' is not a valid criteria"); 
    }
    if (!what && ((criteria!=SCTS_EMPTY) && (criteria!=SCTS_NOT_EMPTY))) ERETURN(false, "what is null"); 
    
    // logic
    
    switch (criteria) {
    
        case SCTS_EMPTY:
            what = "";
            if (!(scta_filter((*list_ptr), what, matches_c, scts_free_pointer)))
                ERETURN(false, "^");
            break;
        
        case SCTS_NOT_EMPTY:
            what = "";
            if (!(scta_filter((*list_ptr), what, not_matches_c, scts_free_pointer)))
                ERETURN(false, "^");
            break;
        
        case SCTS_CONTAINS:
            if (!(scta_filter((*list_ptr), what, contains_c, scts_free_pointer)))
                ERETURN(false, "^");
            break;
        case SCTS_NOT_CONTAINS:
            if (!(scta_filter((*list_ptr), what, not_contains_c, scts_free_pointer)))
                ERETURN(false, "^");
            break;
            
        case SCTS_MATCHES:
            if (!(scta_filter((*list_ptr), what, matches_c, scts_free_pointer)))
                ERETURN(false, "^");
            break;
            
        case SCTS_NOT_MATCHES:
            if (!(scta_filter((*list_ptr), what, not_matches_c, scts_free_pointer)))
                ERETURN(false, "^");
            break;
            
        default:
            ERETURN(false, "`criteria' is not a valid criteria"); 
    }
    
    return true;
}




    
//--find
int scts_find_u(char32_t *restrict haystack, char32_t *restrict needle){
    // input checks
    if (!haystack || !needle) ERETURN(-1, "null string supplied for the %s paramater", ((!haystack)?"first":"second"));
    
    // logic
    // nieve implimintation for now... add support later or maybe different function: scts_search?
    if (!needle[0]) return -1; // definition: to be more inline with other string APIs
    for (int i = 0; haystack[i]; ++i) {
        int j = 0;
        for (; haystack[i+j] == needle[j] && needle[j] != 0; ++j);
        if (needle[j] == 0) return i;   
        else i+=j;
    }
    return -1;
}

int scts_find_c(char *restrict haystack, char *restrict needle){
    // input checks
    if (!haystack || !needle) ERETURN(-1, "null string supplied for the %s paramater", ((!haystack)?"first":"second"));
    
    // logic
    char32_t *h = scts_transcode(haystack);
    char32_t *n = scts_transcode(needle);
    
    if (!h || !n) ERETURN(-1, "failed to transcode the %s paramater", ((!haystack)?"first":"second"));

    int result = scts_find_u(h, n);
    scts_free(h);
    scts_free(n);
    
    return result;
}



    
//--flow
#include <stdarg.h>

char32_t* scts_flow_u_(char32_t *primary, va_list ar){
    #define type_string 1
    #define type_list 2
    int index = 0; // clearer error messages

    #define CHECK_INPUT_IS_STRING  if (last_output_type != type_string) {\
        EPRINT("incompatible flow: list -> string at argument %d", index);\
        goto SUPER_ERROR;\
    }
    
    #define CHECK_INPUT_IS_LIST  if (last_output_type != type_list) {\
        EPRINT("incompatible flow: list -> string at argument %d", index);\
        goto SUPER_ERROR;\
    }
    
    // input checks
    if (!primary) primary = (char32_t[]) {0};

    
    // logic
    if (!(primary = scts_new(primary))) ERETURN(NULL, "^"); // primary will be
                                                            // used as the acc
                                                            // variable
    int last_output_type = type_string; // last_output_type stores the output type 
                                     // of the last operation preformed. This is
                                     // used to make sure that the input of the 
                                     // next operation is compatible with that 
                                     // output
                                                            
    char32_t **list = NULL;
    
    while (true) {
        int op = va_arg(ar, int);
        ++index;
        
        switch (op) {
            case SCTS_STOP:
                goto SUPER_DONE;
                break;
             
            // str -> str
            case SCTS_APPEND: {
                CHECK_INPUT_IS_STRING;
                char32_t* secondary = va_arg(ar, char32_t*);
                if (!scts_append(&primary, secondary)) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                break;
            }
                
            case SCTS_INSERT: {
                CHECK_INPUT_IS_STRING;
                char32_t* secondary = va_arg(ar, char32_t*);
                int where = va_arg(ar, int);
                if (!scts_insert(&primary, secondary, where)) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                break;            
            }
            
            case SCTS_JOIN: {
                if (last_output_type == type_string) {
                    char32_t** l = va_arg(ar, char32_t**);
                    if (!scts_join(&primary, l)) {
                        EPRINT("broken flow at operation %d: ^", index);
                        goto SUPER_ERROR;
                    }
                    break;
                }
                else {
                    // note: type switch
                    primary = va_arg(ar, char32_t*);
                    if (!(primary = scts_new(primary))) {
                        EPRINT("enternal error at operation %d: ^", index);
                        goto SUPER_ERROR;
                    }
                    if (!scts_join(&primary, list)) {
                        EPRINT("broken flow at operation %d: ^", index);
                        goto SUPER_ERROR;
                    }
                    scts_free(list);
                    last_output_type = type_string;
                    break;            
                }            
            }
            
            case SCTS_REPLACE: {
                CHECK_INPUT_IS_STRING;
                char32_t* target = va_arg(ar, char32_t*);
                char32_t* replacement = va_arg(ar, char32_t*);
                if (!scts_replace(&primary, target, replacement)) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                break;            
            }
            
            case SCTS_SUBSTRING: {
                CHECK_INPUT_IS_STRING;
                int from = va_arg(ar, int);
                int to = va_arg(ar, int);
                if (!scts_substring(&primary, from, to)) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                break;                            
            }
            
            // str -> list
            case SCTS_SPLIT: {
                CHECK_INPUT_IS_STRING;
                char32_t *del = va_arg(ar, char32_t*);
                
                if (!(list = scts_split(primary, del))) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                
                scts_free(primary);
                last_output_type = type_list;
                break;            
            }

            case SCTS_SELECT: {
                CHECK_INPUT_IS_LIST;
                int n = va_arg(ar, int);
                
                if (n >= scta_length(list) || n < 0) {
                    EPRINT("enternal error at operation %d: out of bonds: [%d] element of %d-element list requested.", index, n, scta_length(list));
                    goto SUPER_ERROR;
                }
                primary = scts_new(list[n]);
                scts_free(list);
                last_output_type = type_string;
                break;            
            }
            
            // list -> list
            case SCTS_FILTER: {
                CHECK_INPUT_IS_LIST;
                int criteria = va_arg(ar, int);
                char32_t* what = va_arg(ar, char32_t*);
                if (!scts_filter(list, criteria, what)) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                break;
            }
                
            // str -- flow control --> str
            case SCTS_NOT_EMPTY: {
                CHECK_INPUT_IS_STRING;
                if (primary[0] == 0) goto SUPER_DONE;
                break;
            }
            case SCTS_CONTAINS: {
                if (last_output_type == type_string) {
                    char32_t *needle = va_arg(ar, char32_t*);    
                    if (!needle) {
                        EPRINT("enternal error at operation %d: required argument is null.", index);
                        goto SUPER_ERROR;
                    }
                    if (scts_find(primary, needle) == -1) {
                        scts_free(primary);
                        primary = scts_new(((char32_t *){0}));
                        if (!primary) {
                            // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                            EPRINT("broken flow at operation %d: ^", index);
                            return NULL;
                        }
                        goto SUPER_DONE;
                    }
                    break;
                }
                else {
                    char32_t *match = va_arg(ar, char32_t*);
                    bool found = false;
                    
                    if (!match) {
                        EPRINT("enternal error at operation %d: required argument is null.", index);
                        goto SUPER_ERROR;
                    }
                    for (int i = 0; list[i]; ++i) if (scts_matches(list[i], match)) {
                        found = true;
                        break;
                    }
                    if (!found) {
                        scts_free(list);
                        primary = scts_new(((char32_t *){0}));
                        if (!primary) {
                            // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                            EPRINT("broken flow at operation %d: ^", index);
                            return NULL;
                        }                
                        last_output_type = type_string;
                        goto SUPER_DONE;
                    }
                    break;
                }
            }
            case SCTS_NOT_CONTAINS: {
                if (last_output_type == type_string) {
                    char32_t *needle = va_arg(ar, char32_t*);
                    
                    if (!needle) {
                        EPRINT("enternal error at operation %d: required argument is null.", index);
                        goto SUPER_ERROR;
                    }
                    if (scts_find(primary, needle) != -1) {
                        scts_free(primary);
                        primary = scts_new(((char32_t *){0}));
                        if (!primary) {
                            // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                            EPRINT("broken flow at operation %d: ^", index);
                            return NULL;
                        }
                        goto SUPER_DONE;
                    }
                    break;
                }
                else {
                    char32_t *match = va_arg(ar, char32_t*);
                    bool found = false;
                    
                    if (!match) {
                        EPRINT("enternal error at operation %d: required argument is null.", index);
                        goto SUPER_ERROR;
                    }
                    for (int i = 0; list[i]; ++i) if (scts_matches(list[i], match)) {
                        found = true;
                        break;
                    }
                    if (found) {
                        scts_free(list);
                        primary = scts_new(((char32_t *){0}));
                        if (!primary) {
                            // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                            EPRINT("broken flow at operation %d: ^", index);
                            return NULL;
                        }                
                        last_output_type = type_string;
                        goto SUPER_DONE;
                    }
                    break;
                }
            }
            case SCTS_NOT_MATCHES: {
                CHECK_INPUT_IS_STRING;
                char32_t *match = va_arg(ar, char32_t*);
                
                if (!match) {
                    EPRINT("enternal error at operation %d: required argument is null.", index);
                    goto SUPER_ERROR;
                }
                if (scts_matches(primary, match)) {
                    scts_free(primary);
                    primary = scts_new(((char32_t *){0}));
                    if (!primary) {
                        // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                        EPRINT("broken flow at operation %d: ^", index);
                        return NULL;
                    }
                    goto SUPER_DONE;
                }
                break;
            }
            
            
            case SCTS_PRINT: {
                if (!sct_debug) break;
                if (last_output_type == type_string) {
                    fprintf(sct_debug, "[%d] type:string -> '", index);
                    scts_depprint(sct_debug, primary);
                    fprintf(sct_debug, "'\n");
                }
                else {
                    fprintf(sct_debug, "[%d] type:list[%d] -> ['", index, scta_length(list));
                    for (int i = 0; list[i]; ++i) {
                        if (i) fprintf(sct_debug, "', '");
                        scts_depprint(sct_debug, list[i]);
                    }
                    fprintf(sct_debug, "']\n");
                }    
                break;
            }
            
            // list -- flow control --> list
            case SCTS_ASSERT_LENGTH: {
                CHECK_INPUT_IS_LIST;
                int length = va_arg(ar, int);
                if (scta_length(list) != length) {
                    scts_free(list);
                    primary = scts_new(((char32_t *){0}));
                    if (!primary) {
                        // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                        EPRINT("broken flow at operation %d: ^", index);
                        return NULL;
                    }                
                    last_output_type = type_string;
                    goto SUPER_DONE;
                }
                break;
            }

            default:
                    EPRINT("enternal error at operation %d: operation not implemented.", index);
                    goto SUPER_ERROR;
        }
    }
    
    SUPER_ERROR:
    if (last_output_type == type_string) scts_free(primary);
    else scts_free(list);
    return NULL;
    
    SUPER_DONE:
    if (last_output_type != type_string) {
        EPRINT("flow terminates in a list which cannot be returned");
        goto SUPER_ERROR;
    }
    // free temp list
    return primary;
    
    #undef type_string
    #undef type_list
    #undef CHECK_INPUT_IS_STRING
    #undef CHECK_INPUT_IS_LIST
}

char32_t* scts_flow_iu(char32_t *primary, ...){
    va_list ar;
    va_start(ar, primary);
    
    char32_t *r = scts_flow_u_(primary, ar);
    
    va_end(ar);
    return r;
}

char32_t* scts_flow_mu(char32_t **primary, ...){
    va_list ar;
    va_start(ar, primary);
    
    char32_t *r = scts_flow_u_(*primary, ar);
    if (!r) {
        va_end(ar);
        ERETURN(NULL, "internal call to imutable counterpart failed");
    }
    scts_free(*primary);
    *primary = r;
    return r;
}


/* XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX 
    
    USE GEN_SCTS_FLOWC TO GENERATE THE FOLLOWING SECTION AND TEST IT
    THEN COPY IT OVER HERE.
    
    DONT FORGET TO UPDATE THE TESTS IF NEEDED
    
XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX  */

char* scts_flow_c_(char *primary, va_list ar){
    #define type_string 1
    #define type_list 2
    int index = 0; // clearer error messages

    #define CHECK_INPUT_IS_STRING  if (last_output_type != type_string) {\
        EPRINT("incompatible flow: list -> string at argument %d", index);\
        goto SUPER_ERROR;\
    }
    
    #define CHECK_INPUT_IS_LIST  if (last_output_type != type_list) {\
        EPRINT("incompatible flow: list -> string at argument %d", index);\
        goto SUPER_ERROR;\
    }
    
    // input checks
    if (!primary) primary = (char[]) {0};

    
    // logic
    if (!(primary = scts_new(primary))) ERETURN(NULL, "^"); // primary will be
                                                            // used as the acc
                                                            // variable
    int last_output_type = type_string; // last_output_type stores the output type 
                                     // of the last operation preformed. This is
                                     // used to make sure that the input of the 
                                     // next operation is compatible with that 
                                     // output
                                                            
    char **list = NULL;
    
    while (true) {
        int op = va_arg(ar, int);
        ++index;
        
        switch (op) {
            case SCTS_STOP:
                goto SUPER_DONE;
                break;
             
            // str -> str
            case SCTS_APPEND: {
                CHECK_INPUT_IS_STRING;
                char* secondary = va_arg(ar, char*);
                if (!scts_append(&primary, secondary)) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                break;
            }
                
            case SCTS_INSERT: {
                CHECK_INPUT_IS_STRING;
                char* secondary = va_arg(ar, char*);
                int where = va_arg(ar, int);
                if (!scts_insert(&primary, secondary, where)) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                break;            
            }
            
            case SCTS_JOIN: {
                if (last_output_type == type_string) {
                    char** l = va_arg(ar, char**);
                    if (!scts_join(&primary, l)) {
                        EPRINT("broken flow at operation %d: ^", index);
                        goto SUPER_ERROR;
                    }
                    break;
                }
                else {
                    // note: type switch
                    primary = va_arg(ar, char*);
                    if (!(primary = scts_new(primary))) {
                        EPRINT("enternal error at operation %d: ^", index);
                        goto SUPER_ERROR;
                    }
                    if (!scts_join(&primary, list)) {
                        EPRINT("broken flow at operation %d: ^", index);
                        goto SUPER_ERROR;
                    }
                    scts_free(list);
                    last_output_type = type_string;
                    break;            
                }            
            }
            
            case SCTS_REPLACE: {
                CHECK_INPUT_IS_STRING;
                char* target = va_arg(ar, char*);
                char* replacement = va_arg(ar, char*);
                if (!scts_replace(&primary, target, replacement)) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                break;            
            }
            
            case SCTS_SUBSTRING: {
                CHECK_INPUT_IS_STRING;
                int from = va_arg(ar, int);
                int to = va_arg(ar, int);
                if (!scts_substring(&primary, from, to)) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                break;                            
            }
            
            // str -> list
            case SCTS_SPLIT: {
                CHECK_INPUT_IS_STRING;
                char *del = va_arg(ar, char*);
                
                if (!(list = scts_split(primary, del))) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                
                scts_free(primary);
                last_output_type = type_list;
                break;            
            }

            case SCTS_SELECT: {
                CHECK_INPUT_IS_LIST;
                int n = va_arg(ar, int);
                
                if (n >= scta_length(list) || n < 0) {
                    EPRINT("enternal error at operation %d: out of bonds: [%d] element of %d-element list requested.", index, n, scta_length(list));
                    goto SUPER_ERROR;
                }
                primary = scts_new(list[n]);
                scts_free(list);
                last_output_type = type_string;
                break;            
            }
            
            // list -> list
            case SCTS_FILTER: {
                CHECK_INPUT_IS_LIST;
                int criteria = va_arg(ar, int);
                char* what = va_arg(ar, char*);
                if (!scts_filter(list, criteria, what)) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                break;
            }
                
            // str -- flow control --> str
            case SCTS_NOT_EMPTY: {
                CHECK_INPUT_IS_STRING;
                if (primary[0] == 0) goto SUPER_DONE;
                break;
            }
            case SCTS_CONTAINS: {
                if (last_output_type == type_string) {
                    char *needle = va_arg(ar, char*);    
                    if (!needle) {
                        EPRINT("enternal error at operation %d: required argument is null.", index);
                        goto SUPER_ERROR;
                    }
                    if (scts_find(primary, needle) == -1) {
                        scts_free(primary);
                        primary = scts_new(((char *){0}));
                        if (!primary) {
                            // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                            EPRINT("broken flow at operation %d: ^", index);
                            return NULL;
                        }
                        goto SUPER_DONE;
                    }
                    break;
                }
                else {
                    char *match = va_arg(ar, char*);
                    bool found = false;
                    
                    if (!match) {
                        EPRINT("enternal error at operation %d: required argument is null.", index);
                        goto SUPER_ERROR;
                    }
                    for (int i = 0; list[i]; ++i) if (scts_matches(list[i], match)) {
                        found = true;
                        break;
                    }
                    if (!found) {
                        scts_free(list);
                        primary = scts_new(((char *){0}));
                        if (!primary) {
                            // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                            EPRINT("broken flow at operation %d: ^", index);
                            return NULL;
                        }                
                        last_output_type = type_string;
                        goto SUPER_DONE;
                    }
                    break;
                }
            }
            case SCTS_NOT_CONTAINS: {
                if (last_output_type == type_string) {
                    char *needle = va_arg(ar, char*);
                    
                    if (!needle) {
                        EPRINT("enternal error at operation %d: required argument is null.", index);
                        goto SUPER_ERROR;
                    }
                    if (scts_find(primary, needle) != -1) {
                        scts_free(primary);
                        primary = scts_new(((char *){0}));
                        if (!primary) {
                            // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                            EPRINT("broken flow at operation %d: ^", index);
                            return NULL;
                        }
                        goto SUPER_DONE;
                    }
                    break;
                }
                else {
                    char *match = va_arg(ar, char*);
                    bool found = false;
                    
                    if (!match) {
                        EPRINT("enternal error at operation %d: required argument is null.", index);
                        goto SUPER_ERROR;
                    }
                    for (int i = 0; list[i]; ++i) if (scts_matches(list[i], match)) {
                        found = true;
                        break;
                    }
                    if (found) {
                        scts_free(list);
                        primary = scts_new(((char *){0}));
                        if (!primary) {
                            // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                            EPRINT("broken flow at operation %d: ^", index);
                            return NULL;
                        }                
                        last_output_type = type_string;
                        goto SUPER_DONE;
                    }
                    break;
                }
            }
            case SCTS_NOT_MATCHES: {
                CHECK_INPUT_IS_STRING;
                char *match = va_arg(ar, char*);
                
                if (!match) {
                    EPRINT("enternal error at operation %d: required argument is null.", index);
                    goto SUPER_ERROR;
                }
                if (scts_matches(primary, match)) {
                    scts_free(primary);
                    primary = scts_new(((char *){0}));
                    if (!primary) {
                        // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                        EPRINT("broken flow at operation %d: ^", index);
                        return NULL;
                    }
                    goto SUPER_DONE;
                }
                break;
            }
            
            
            case SCTS_PRINT: {
                if (!sct_debug) break;
                if (last_output_type == type_string) {
                    fprintf(sct_debug, "[%d] type:string -> '", index);
                    scts_depprint(sct_debug, primary);
                    fprintf(sct_debug, "'\n");
                }
                else {
                    fprintf(sct_debug, "[%d] type:list[%d] -> ['", index, scta_length(list));
                    for (int i = 0; list[i]; ++i) {
                        if (i) fprintf(sct_debug, "', '");
                        scts_depprint(sct_debug, list[i]);
                    }
                    fprintf(sct_debug, "']\n");
                }    
                break;
            }
            
            // list -- flow control --> list
            case SCTS_ASSERT_LENGTH: {
                CHECK_INPUT_IS_LIST;
                int length = va_arg(ar, int);
                if (scta_length(list) != length) {
                    scts_free(list);
                    primary = scts_new(((char *){0}));
                    if (!primary) {
                        // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                        EPRINT("broken flow at operation %d: ^", index);
                        return NULL;
                    }                
                    last_output_type = type_string;
                    goto SUPER_DONE;
                }
                break;
            }

            default:
                    EPRINT("enternal error at operation %d: operation not implemented.", index);
                    goto SUPER_ERROR;
        }
    }
    
    SUPER_ERROR:
    if (last_output_type == type_string) scts_free(primary);
    else scts_free(list);
    return NULL;
    
    SUPER_DONE:
    if (last_output_type != type_string) {
        EPRINT("flow terminates in a list which cannot be returned");
        goto SUPER_ERROR;
    }
    // free temp list
    return primary;
    
    #undef type_string
    #undef type_list
    #undef CHECK_INPUT_IS_STRING
    #undef CHECK_INPUT_IS_LIST
}

char* scts_flow_ic(char *primary, ...){
    va_list ar;
    va_start(ar, primary);
    
    char *r = scts_flow_c_(primary, ar);
    
    va_end(ar);
    return r;
}

char* scts_flow_mc(char **primary, ...){
    va_list ar;
    va_start(ar, primary);
    
    char *r = scts_flow_c_(*primary, ar);
    if (!r) {
        va_end(ar);
        ERETURN(NULL, "internal call to imutable counterpart failed");
    }
    scts_free(*primary);
    *primary = r;
    return r;
}





    
//--insert
char32_t* scts_insert_iu(char32_t *primary, char32_t *secondary, int where){
    // input checks
    if (!primary || !secondary) ERETURN(NULL, "null string supplied");
    
    // logic
    int primary_length = scts_length(primary);
    int secondary_length = scts_length(secondary);
    
    if (primary_length == -1 || secondary_length == -1) ERETURN(NULL, "invalid string supplied");
    
    // if negative indexing is used
    if (where < 0) where+=primary_length;
    
    // bound checks
    if (where < 0 || where > primary_length) ERETURN(NULL, "`where' paramater is out of "
                                                 "bounds; supplied string length "
                                                 "is %d, effective `where' "
                                                 "value is %d", primary_length, where);
    
    char32_t *out = scts_new(primary);
    if (!out) ERETURN(NULL,  "Could not allocate memory for new string");
    if (!scta_insert_array(out, where, secondary, secondary_length)) {
        scts_free(out);
        ERETURN(NULL,  "Could not complete insertion");
    }
    return out;
}

char32_t* scts_insert_mu(char32_t **primary, char32_t *secondary, int where){
    char32_t *n;
    
    if (!primary || !(*primary) || !secondary)  ERETURN(NULL, "null string supplied");
    
    n = scts_insert_iu(*primary, secondary, where);
    if (!n) ERETURN(NULL, "internal call to imutable counterpart failed");
    scts_free(*primary);
    *primary = n;
    return n;
}

char* scts_insert_ic(char *primary, char *secondary, int where){
    char32_t *n, *m;
    
    if (!primary || !secondary) ERETURN(NULL, "null string supplied");
    
    n = scts_transcode(primary);
    m = scts_transcode(secondary);
    
    if (!n) ERETURN(NULL, "could not transcode byte sequence");
    if (!scts_insert_mu(&n, m, where)) {
        scts_free(n);
        scts_free(m);
        ERETURN(NULL, "internal call to mutable unicode counterpart failed");
    }
    primary = scts_transcode(n);
    if (!primary) {
        scts_free(n);
        scts_free(m);
        ERETURN(NULL, "internal library failure"); // should never fail to transcode happen unless something is broken
    }
    scts_free(n);
    scts_free(m);
    return primary;
}


char* scts_insert_mc(char **primary, char *secondary, int where){
    char *n;

    if (!primary || !(*primary) || !secondary)  ERETURN(NULL, "null string supplied");
    
    n = scts_insert_ic(*primary, secondary, where);
    if (!n) ERETURN(NULL, "internal call to imutable counterpart failed");
    scts_free(*primary);
    *primary = n;
    return n;
}



    
//--join
char32_t* scts_join_iu(char32_t *primary, char32_t **list){
    int total_length = 0;
    int total_count = 0;
    // input checks
    if (!primary || !list) ERETURN(NULL, "null string or list supplied");
    
    // logic
    
    // if empty list, return empty string
    if (!list[0]) return scts_new(((char32_t[]) {0}));
    
    // calculate total memory needed to join strings with primary
    for (total_count = 0; list[total_count]; ++total_count) {
        int temp;
        if ((temp = scts_length(list[total_count])) == -1) ERETURN(NULL, "invalid string in `list'");
        total_length += temp;
    }
    {// continued ^
        int temp = scts_length(primary);
        if (temp == -1) ERETURN(NULL, "`primary' is an invalid string")
        total_length += temp*total_count;
    }
    
    
    // allocating string large enough to hold everything
    char32_t *out = scta_new(char32_t);
    if (!scta_set_length(out, total_length)) ERETURN(NULL, "memory reservation failure");
    
    { // copy stuff
        int cur = 0;
        for (int i = 0; i < total_count; ++i) {
            for (int j = 0; list[i][j]; ++j) out[cur++] = list[i][j];
            if (list[i+1]) for (int j = 0; primary[j]; ++j) out[cur++] = primary[j]; 
        }
    }
    return out;   
}

char32_t* scts_join_mu(char32_t **primary, char32_t **list){
    char32_t *n;
    
    if (!primary || !list)  ERETURN(NULL, "null string supplied");
    
    n = scts_join_iu(*primary, list);
    if (!n) ERETURN(NULL, "internal call to imutable counterpart failed");
    scts_free(*primary);
    *primary = n;
    return n;
}

char* scts_join_ic(char *primary, char **list){
    char32_t *n, **m;
    
    if (!primary || !list) ERETURN(NULL, "null string supplied");
    
    n = scts_transcode(primary);
    if (!n) ERETURN(NULL, "could not transcode byte sequence");
    
    if (!(m = scta_new(char32_t *))) ERETURN(NULL, "memory reservation failure");
    for (int i = 0; list[i]; ++i) {
        if (!scta_push(m, scts_transcode(list[i]))) ERETURN(NULL, "memory reservation failure");
        if (m[scta_length(m)-1] == NULL) ERETURN(NULL, "could not transcode byte sequence");
    }
    
    if (!scts_join_mu(&n, m)) {
        scts_free(n);
        scts_free(m);
        ERETURN(NULL, "internal call to mutable unicode counterpart failed");
    }
    primary = scts_transcode(n);
    if (!primary) {
        scts_free(n);
        scts_free(m);
        ERETURN(NULL, "internal library failure"); // should never fail to transcode happen unless something is broken
    }
    scts_free(n);
    scts_free(m);
    return primary;
}

char* scts_join_mc(char **primary, char **list){
    char *n;

    if (!primary || !list)  ERETURN(NULL, "null string supplied");
    
    n = scts_join_ic(*primary, list);
    if (!n) ERETURN(NULL, "internal call to imutable counterpart failed");
    scts_free(*primary);
    *primary = n;
    return n;
}

    
//--legacy_length
#include <string.h>

int scts_legacy_length_c(char * restrict primary){
    // input checks
    if (!primary) return 0;
    // logic
    //TODO add checks #security
    return strlen(primary);
}

int scts_legacy_length_u(char32_t * restrict primary){
    register size_t temp = 0;
    register size_t total = 0;
    // input checks
    if (!primary) return 0;
    // logic

    for (register size_t i = 0; primary[i]; ++i) {
        if ((temp = scts_sequence_length(primary[i])) == -1) ERETURN(-1, "invalid codepoint detected %u", primary[i]);
        total += temp;
    }
    return total;
    
}


    
//--length
#include <string.h>
int scts_length_c(char * restrict primary){
    register size_t i = 0;
    register size_t j = 0;
    // input checks
    if (!primary) return 0;
    // logic
    while (primary[j]) {
        register int s = scts_sequence_length(primary+j);
        if (s == -1) ERETURN(-1, "^");
        j += s;
        ++i;
    }
    return i;
    
}
int scts_length_u(char32_t * restrict primary){
    register size_t i = 0;
    // input checks
    if (!primary) return 0;
    // logic
    while (primary[i]) {
        if (scts_sequence_length(primary[i]) == -1) ERETURN(-1, "^");
        ++i;
    }
    return i;
}
/*
size_t scts_length_l(void ** restrict primary){
    register size_t i = 0;
    // input checks
    if (!primary) return 0;
    // logic
    while (primary[i++]);
    return i-1;
}*/



    
//--matches
#include <string.h>
bool scts_matches_u(char32_t *primary, char32_t *secondary){
    // input checks
    if (!primary || !secondary) ERETURN(NULL, "null string supplied for the %s paramater", ((!primary)?"first":"second"));
    // logic
    for (register int i = 0; primary[i] || secondary[i]; ++i) if (primary[i] != secondary[i]) return false;
    return true;
}

bool scts_matches_c(char *primary, char *secondary){
    // input checks
    if (!primary || !secondary) ERETURN(NULL, "null string supplied for the %s paramater", ((!primary)?"first":"second"));
    // logic
    return (bool) !strcmp(primary, secondary);
}


    
//--new
#include <string.h> //TODO remove this, and replace all string library calls with scts 
char* scts_new_c(char *primary){
    // input checks
    if (!primary) primary="";
    // logic
    char *out = scta_new(char);
    if (!out) ERETURN(NULL, "failed to allocate memory new dynamic string.");
    if (! scta_insert_array(out, 0, primary, scts_legacy_length(primary))) {
        scta_free(out);
        ERETURN(NULL, "failed to copy primary into dynamic string.");
    }
    return out;
}


char32_t* scts_new_u(char32_t *primary){
    // input checks
    if (!primary) primary= (char32_t[1]) {0}; //TODO use U syntax
    // logic
    char32_t *out = scta_new(char32_t);
    if (!out) ERETURN(NULL, "failed to allocate memory new dynamic string.");
    if (! scta_insert_array(out, 0, primary, scts_length(primary))) { 
        scta_free(out);
        ERETURN(NULL, "failed to copy primary into dynamic string.");
    }
    return out;
}


void scts_free_pointer(void *primary){
    scta_free(primary);
}

void scts_free_list(void *primary){
    void **p = (void **) primary;
    for (int i = 0; p[i]; ++i) scta_free(p[i]);
    scta_free(p);
}



    
//--replace

char32_t* scts_replace_iu(char32_t *primary, char32_t *target, char32_t *replacement){
    // input checks
    if (!primary || !target || !replacement) ERETURN(NULL, "null string supplied");
    // logic
    //TODO Too slow: unwind and enterlance
    int target_length = scts_length(target);
    if (target_length == -1) ERETURN(NULL, "`target' is an invalid string");
    
    char32_t *out = scta_new(char32_t);
    int i = 0;
    while (true) {
        int ti = scts_find(primary+i, target);
        if (ti == -1) {
            if (!scts_append(&out, primary+i)) ERETURN(NULL, "failed to append terminating part of string");
            break;
        }
        else {
            //TODO switch to scts_append_substring() or scts_insert_substring()            
            char32_t *sub = scts_substring(primary+i, 0, ti);
            if (!sub) {
                scts_free(out);
                ERETURN(NULL, "could not extract substring from `primary': [%d:%d]", i, i+ti);
            }
            if (!scts_append(&out, sub)) ERETURN(NULL, "failed to append initial part of string");
            if (!scts_append(&out, replacement)) ERETURN(NULL, "failed to append replacement");
            scts_free(sub);
            i+=(ti+target_length);
        }
    }
    return out;
}

char32_t* scts_replace_mu(char32_t **primary, char32_t *target, char32_t *replacement){
    char32_t *n;
    
    if (!primary || !target || !replacement)  ERETURN(NULL, "null string supplied");
    
    n = scts_replace_iu(*primary, target, replacement);
    if (!n) ERETURN(NULL, "internal call to imutable counterpart failed");
    scts_free(*primary);
    *primary = n;
    return n;
}

char* scts_replace_ic(char *primary, char *target, char *replacement){
    char32_t *n, *m, *l;
    
    if (!primary || !target) ERETURN(NULL, "null string supplied");
    
    n = scts_transcode(primary);
    m = scts_transcode(target);
    l = scts_transcode(replacement);
    
    if (!n) ERETURN(NULL, "could not transcode byte sequence");
    if (!scts_replace_mu(&n, m, l)) {
        scts_free(n);
        scts_free(m);
        scts_free(l);
        ERETURN(NULL, "internal call to mutable unicode counterpart failed");
    }
    primary = scts_transcode(n);
    if (!primary) {
        scts_free(n);
        scts_free(m);
        scts_free(l);
        ERETURN(NULL, "internal library failure"); // should never fail to transcode happen unless something is broken
    }
    scts_free(n);
    scts_free(m);
    scts_free(l);
    return primary;
}


char* scts_replace_mc(char **primary, char *target, char *replacement){
    char *n;

    if (!primary || !(*primary) || !target)  ERETURN(NULL, "null string supplied");
    
    n = scts_replace_ic(*primary, target, replacement);
    if (!n) ERETURN(NULL, "internal call to imutable counterpart failed");
    scts_free(*primary);
    *primary = n;
    return n;
}



    
//--sequence_codepoint
char* scts_sequence_codepoint(char32_t c, char buf[restrict static 5]){
    // Unicode Standard 6.2: Section 3.9: Page 95
    int size = scts_sequence_length(c);
    
    if (!size) ERETURN(NULL, "^");
    
    if (size == 1) {
        buf[1] = 0;
        buf[0] = c;
    }
    else if (size == 2) {
        buf[2] = 0;
        buf[1] = (c & 0x3f) | 0x80;
        c>>=6;
        buf[0] = (c & 0x1f) | 0xc0;
    }
    else if (size == 3) {
        buf[3] = 0;
        buf[2] = (c & 0x3f) | 0x80;
        c>>=6;
        buf[1] = (c & 0x3f) | 0x80;
        c>>=6;
        buf[0] = (c & 0xf) | 0xe0;
    }
    else if (size == 4) {
        buf[4] = 0;
        buf[3] = (c & 0x3f) | 0x80;
        c>>=6;
        buf[2] = (c & 0x3f) | 0x80;
        c>>=6;
        buf[1] = (c & 0x3f) | 0x80;
        c>>=6;
        buf[0] = (c & 0x7) | 0xf0;        
    }
    // If this occurs, something very wrong is happening in scts_byte_character_size
    else ERETURN(NULL, "could not map codepoint to well-formed byte sequence"); 
    
    return buf;    
}



    
//--sequence_length
int scts_sequence_length_c(char *character){    
    // Unicode Standard 6.2: Section 3.9: Page 95
    uint8_t * restrict c = (uint8_t *) character;
    register int minlen = 0;
    
    if (!c) ERETURN(-1, "Null pointer");
    
    // assure that string is not abruptly short and get max max length posible if
    // short. #security 
    for (;c[minlen] && minlen < 5; ++minlen);
    
    // direct implementation of the spec.
    if (c[0] >= 0x0 && c[0] <= 0x7f) {
        return 1;
    }
    else if (c[0] >= 0xc2 && c[0] <=  0xdf && 
                minlen >= 2 && c[1] >= 0x80 && c[1] <= 0xbf) {
        return 2;   
    }
    else if (c[0] == 0xe0 && minlen >= 3 && 
                c[1] >= 0xA0 && c[1] <= 0xbf &&
                c[2] >= 0x80 && c[2] <= 0xbf ) {
        return 3;   
    }
    else if (c[0] >= 0xe1 && c[0] <= 0xec && minlen >= 3 && 
                c[1] >= 0x80 && c[1] <= 0xbf &&
                c[2] >= 0x80 && c[2] <= 0xbf ) {
        return 3;   
    } 
    else if (c[0] == 0xed && minlen >= 3 && 
                c[1] >= 0x80 && c[1] <= 0x9f &&
                c[2] >= 0x80 && c[2] <= 0xbf ) {
        return 3;   
    }
    else if (c[0] >= 0xee && c[0] <= 0xef && minlen >= 3 && 
                c[1] >= 0x80 && c[1] <= 0xbf &&
                c[2] >= 0x80 && c[2] <= 0xbf ) {
        return 3;   
    }
    else if (c[0] == 0xf0 && minlen >= 4 && 
                c[1] >= 0x90 && c[1] <= 0xbf &&
                c[2] >= 0x80 && c[2] <= 0xbf &&
                c[3] >= 0x80 && c[3] <= 0xbf ) {
        return 4;   
    }
    else if (c[0] >= 0xf1 && c[0] <= 0xf3 && minlen >= 4 && 
                c[1] >= 0x80 && c[1] <= 0xbf &&
                c[2] >= 0x80 && c[2] <= 0xbf &&
                c[3] >= 0x80 && c[3] <= 0xbf ) {
        return 4;   
    }
    else if (c[0] == 0xf4 && minlen >= 4 && 
                c[1] >= 0x80 && c[1] <= 0x8f &&
                c[2] >= 0x80 && c[2] <= 0xbf &&
                c[3] >= 0x80 && c[3] <= 0xbf ) {
        return 4;   
    }
    else ERETURN(-1, "ill-formed byte sequence");
}

int scts_sequence_length_u(char32_t c){
    // Unicode Standard 6.2: Section 3.9: Page 95
    #define else_range_return(start, end, ret) else if (c >= start && c <= end) return ret;
    if (false) return 0;
    else_range_return(0,        0x7f,       1)
    else_range_return(0x80,     0x7ff,      2)
    else_range_return(0x800,    0xfff,      3)
    else_range_return(0x1000,   0xcfff,     3)
    else_range_return(0xd000,   0xd7ff,     3)
    else_range_return(0xe000,   0xffff,     3)
    else_range_return(0x10000,  0x3ffff,    4)
    else_range_return(0x40000,  0xfffff,    4)
    else_range_return(0x100000, 0x10ffff,   4)
    else ERETURN(-1, "None-valid codepoint: U+%lu", (unsigned long int) c);
    #undef range_return
}



    
//--split
char32_t** scts_split_u(char32_t *restrict primary, char32_t *restrict delemeter){
    // input checks
    if (!primary || !delemeter) ERETURN(NULL, "null string supplied for the %s paramater", ((!primary)?"first":"second"));
    
    // logic
    int i = 0, where;
    int length = scts_length(delemeter);
    if (length == -1) ERETURN(NULL, "^");
    char32_t **out = scta_new(char32_t *);
    char32_t *shard;
    
    if (!out) ERETURN(NULL, "^");

    while (true) {
        where = scts_find(primary+i, delemeter);
        
        if (where != -1) shard = scts_substring(primary+i, 0, where);
        else shard = scts_new(primary+i);

        if (!shard) {
            scts_free(out);
            ERETURN(NULL, "^");
        }
        if (!scta_push(out, shard)) {
            scts_free(out);
            ERETURN(NULL, "^");
        }
        
        if (where == -1) break;
        i += (where+length);
    }
    
    return out;
}

char** scts_split_c(char *restrict primary, char *restrict delemeter){
    // input checks
    if (!primary || !delemeter) ERETURN(NULL, "null string supplied for the %s paramater", ((!primary)?"first":"second"));
    
    //TODO redo using string.h functions; no need to be unicode aware here
    //TODO add custom tests for _u 
    char32_t *p = scts_transcode(primary);
    char32_t *s = scts_transcode(delemeter);
    char32_t **o = scts_split_u(p, s);
    if (!o) ERETURN(NULL, "split failed");
    char **out = scta_new(char *);
    scta_set_length(out, scta_length(o));
    for (int i = 0; o[i]; ++i) out[i] = scts_transcode(o[i]);
    scts_free(p);
    scts_free(s);
    scts_free(o);
    return out;
}



    
//--substring
char32_t* scts_substring_iu(char32_t *primary, int from, int to){
    // input checks
    if (!primary) ERETURN(NULL, "null string supplied");
    // logic
    int length = scts_length(primary);
    
    // if negative indexing is used
    if (from < 0) from+=length;
    if (to < 0) to+=length;
    
    // bound checks
    if (from < 0 || from > length) ERETURN(NULL, "`from' paramater is out of "
                                                 "bounds; supplied string length "
                                                 "is %d, effective `from' "
                                                 "value is %d", length, from);
    if (to == SCTS_EOS) to = length;
    if (to < 0 || to < from || to > length) ERETURN(NULL,  
                                            "`to' paramater is out of bounds; "
                                            "supplied string length is %d, "
                                            "effective `from' value is %d, "
                                            "and effective `to' value is %d", 
                                            length, from, to);
    
    // get a new string that is large enough to hold the substring
    char32_t *out = scta_new(char32_t);
    if (!out) ERETURN(NULL,  "Could not allocate memory for new string");
    if (!scta_set_length(out, to-from)) {
        scts_free(out);
        ERETURN(NULL,  "Could not allocate memory for new string length %d", to-from);
    }
    
    for (register int i = from; i != to; ++i) out[i-from] = primary[i];
    return out;
}

char32_t* scts_substring_mu(char32_t **primary, int from, int to){
    char32_t *n;
    
    if (!primary || !(*primary))  ERETURN(NULL, "null string supplied");
    
    n = scts_substring_iu(*primary, from, to);
    if (!n) ERETURN(NULL, "internal call to imutable counterpart failed");
    scts_free(*primary);
    *primary = n;
    return n;
}

char* scts_substring_ic(char *primary, int from, int to){
    char32_t *n;
    
    if (!primary) ERETURN(NULL, "null string supplied");
    
    n = scts_transcode(primary);
    if (!n) ERETURN(NULL, "could not transcode byte sequence");
    if (!scts_substring_mu(&n, from, to)) {
        scts_free(n);
        ERETURN(NULL, "internal call to mutable unicode counterpart failed");
    }
    primary = scts_transcode(n);
    if (!primary) {
        scts_free(n);
        ERETURN(NULL, "internal library failure"); // should never fail to transcode happen unless something is broken
    }
    scts_free(n);
    return primary;
}

char* scts_substring_mc(char **primary, int from, int to){
    char *n;

    if (!primary || !(*primary))  ERETURN(NULL, "null string supplied");
    
    n = scts_substring_ic(*primary, from, to);
    if (!n) ERETURN(NULL, "internal call to imutable counterpart failed");
    scts_free(*primary);
    *primary = n;
    return n;
}


    
//--transcode
char32_t* scts_transcode_c(char * restrict primary){
    // note: scta always terminates an array with a null/0
    //       that's why strings are not terminated here. #security
    
    // input checks
    if (!primary) primary = "";
    // logic
    
    int olen = scts_length(primary);    // primary codepoint count and utf8 byte sequence validation
    if (olen == -1) ERETURN(NULL, "^");
    
    char32_t *out = scta_new(char32_t);  // output string
    
    // check that out is as large as we need it to be
    if (!out) ERETURN(NULL, "failed to reserve a new string");
    if (!scta_set_length(out, olen)) ERETURN(NULL, "failed to set new string length to the required size. size requested was %d bytes", olen);
    
    // loop through primary and store codepoints in out
    register int pcur = 0;
    register int ocur = 0;
    while (primary[pcur]) {
        if (ocur >= olen) ERETURN(NULL, "buffer overrun error");
        out[ocur++] = scts_codepoint(primary+pcur);
        pcur += scts_sequence_length(primary+pcur);
    }
    
    return out;
}

char* scts_transcode_u(char32_t * restrict primary){
    // note: scta always terminates an array with a null/0
    //       that's why strings are not terminated here. #security
    
    // input checks
    if (!primary) primary = (char32_t []) {0};
    
    // logic
    int olen = scts_legacy_length(primary);    // primary legacy length
    char *out = scta_new(char);                 // output string
    
    // check that out is as large as we need it to be
    if (!out) ERETURN(NULL, "failed to reserve a new string");
    if (!scta_set_length(out, olen)) ERETURN(NULL, "failed to set new string length to the required size. size requested was %d bytes", olen);
    
    int cur = 0;
    for (int i = 0; primary[i]; ++i) {
        char seq[5];
        scts_sequence_codepoint(primary[i], seq);
        if (seq[0]) out[cur++] = seq[0]; else continue;
        if (seq[1]) out[cur++] = seq[1]; else continue;
        if (seq[2]) out[cur++] = seq[2]; else continue;
        if (seq[3]) out[cur++] = seq[3]; else continue;
    }
    return out;
}


    
