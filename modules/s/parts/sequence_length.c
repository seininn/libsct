#define SCT__INTERNAL__
#include "scts.h"

//HEADER
#ifndef SCTS_PART_SEQUENCE_LENGTH
#define SCTS_PART_SEQUENCE_LENGTH
int scts_sequence_length_c(char *c);
int scts_sequence_length_u(char32_t c);
#define scts_sequence_length(primary) (_Generic( (0, primary), \
    char32_t:scts_sequence_length_u, \
    const char32_t:scts_sequence_length_u, \
    char *: scts_sequence_length_c, \
    const char *: scts_sequence_length_c \
    )(primary))

/*
sctdoc:scts_sequence_length:

NAME
    scts_sequence_length

SYNOPSIS
    int scts_sequence_length([char*][char32_t*] primary)
    
    int scts_sequence_length_c(char *c);
    int scts_sequence_length_u(char32_t c);
    
DESCRIPTION
    scts_sequence_length returns the number of bytes required to store a 
    the utf8 representation of a given codepoint `c'.

RETURN
    This function returns a non-negative number upon success and -1 if the 
    codepoint is not valid.

EXAMPLE
    scts_sequence_length("😻") == 4;

SEE ALSO
    scts_sequence_codepoint
*/

#endif

//LIB
int scts_sequence_length_c(char *character){    
    // Unicode Standard 6.2: Section 3.9: Page 95
    uint8_t * restrict c = (uint8_t *) character;
    register int minlen = 0;
    
    if (!c) ERETURN(-1, "Null pointer");
    
    // assure that string is not abruptly short and get max max length posible if
    // short. #security 
    for (;c[minlen] && minlen < 5; ++minlen);
    
    // direct implementation of the spec.
    if (c[0] >= 0x0 && c[0] <= 0x7f) {
        return 1;
    }
    else if (c[0] >= 0xc2 && c[0] <=  0xdf && 
                minlen >= 2 && c[1] >= 0x80 && c[1] <= 0xbf) {
        return 2;   
    }
    else if (c[0] == 0xe0 && minlen >= 3 && 
                c[1] >= 0xA0 && c[1] <= 0xbf &&
                c[2] >= 0x80 && c[2] <= 0xbf ) {
        return 3;   
    }
    else if (c[0] >= 0xe1 && c[0] <= 0xec && minlen >= 3 && 
                c[1] >= 0x80 && c[1] <= 0xbf &&
                c[2] >= 0x80 && c[2] <= 0xbf ) {
        return 3;   
    } 
    else if (c[0] == 0xed && minlen >= 3 && 
                c[1] >= 0x80 && c[1] <= 0x9f &&
                c[2] >= 0x80 && c[2] <= 0xbf ) {
        return 3;   
    }
    else if (c[0] >= 0xee && c[0] <= 0xef && minlen >= 3 && 
                c[1] >= 0x80 && c[1] <= 0xbf &&
                c[2] >= 0x80 && c[2] <= 0xbf ) {
        return 3;   
    }
    else if (c[0] == 0xf0 && minlen >= 4 && 
                c[1] >= 0x90 && c[1] <= 0xbf &&
                c[2] >= 0x80 && c[2] <= 0xbf &&
                c[3] >= 0x80 && c[3] <= 0xbf ) {
        return 4;   
    }
    else if (c[0] >= 0xf1 && c[0] <= 0xf3 && minlen >= 4 && 
                c[1] >= 0x80 && c[1] <= 0xbf &&
                c[2] >= 0x80 && c[2] <= 0xbf &&
                c[3] >= 0x80 && c[3] <= 0xbf ) {
        return 4;   
    }
    else if (c[0] == 0xf4 && minlen >= 4 && 
                c[1] >= 0x80 && c[1] <= 0x8f &&
                c[2] >= 0x80 && c[2] <= 0xbf &&
                c[3] >= 0x80 && c[3] <= 0xbf ) {
        return 4;   
    }
    else ERETURN(-1, "ill-formed byte sequence");
}

int scts_sequence_length_u(char32_t c){
    // Unicode Standard 6.2: Section 3.9: Page 95
    #define else_range_return(start, end, ret) else if (c >= start && c <= end) return ret;
    if (false) return 0;
    else_range_return(0,        0x7f,       1)
    else_range_return(0x80,     0x7ff,      2)
    else_range_return(0x800,    0xfff,      3)
    else_range_return(0x1000,   0xcfff,     3)
    else_range_return(0xd000,   0xd7ff,     3)
    else_range_return(0xe000,   0xffff,     3)
    else_range_return(0x10000,  0x3ffff,    4)
    else_range_return(0x40000,  0xfffff,    4)
    else_range_return(0x100000, 0x10ffff,   4)
    else ERETURN(-1, "None-valid codepoint: U+%lu", (unsigned long int) c);
    #undef range_return
}


//TEST
#ifdef test_sequence_length

#include <assert.h>
#include <stdio.h>

int main() {
    char *ltr;
    sct_debug = stderr;
    ltr = ""; assert(scts_sequence_length(ltr) == 1);
    ltr = "s"; assert(scts_sequence_length(ltr) == 1);
    ltr = "ﺔ"; assert(scts_sequence_length(ltr) == 3);
    ltr = "س"; assert(scts_sequence_length(ltr) == 2);
    ltr = "1"; assert(scts_sequence_length(ltr) == 1);
    ltr = "句"; assert(scts_sequence_length(ltr) == 3);
    ltr = "😻"; assert(scts_sequence_length(ltr) == 4);
    
    sct_debug = NULL;
    assert(scts_sequence_length((char *) NULL) == (int) -1);
    sct_debug = stderr;
    
    assert(scts_sequence_length((char32_t) 0x663) == 2);
    assert(scts_sequence_length((char32_t) 0xa) == 1);
    assert(scts_sequence_length((char32_t) 0xf906) == 3);
    assert(scts_sequence_length((char32_t) 0x1F63B) == 4);
    assert(scts_sequence_length((char32_t) 0) == 1);
    
    sct_debug = NULL;
    assert(scts_sequence_length((char32_t) 0xffffff) == (int) -1);
    assert(scts_sequence_length("\xE0\x9F\x80") == (int) -1);
    sct_debug = stderr;
}

#endif
