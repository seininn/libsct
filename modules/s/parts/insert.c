#define SCT__INTERNAL__
#include "scts.h"

//HEADER
#ifndef SCTS_PART_INSERT
#define SCTS_PART_INSERT

char32_t* scts_insert_iu(char32_t *primary, char32_t *secondary, int where);
char32_t* scts_insert_mu(char32_t **primary, char32_t *secondary, int where);
char* scts_insert_ic(char *primary, char *secondary, int where);
char* scts_insert_mc(char **primary, char *secondary, int where);
#define scts_insert(primary, secondary, where) (_Generic( (0, primary), \
    char32_t *:         scts_insert_iu, \
    char32_t **:        scts_insert_mu, \
    const char32_t *:   scts_insert_iu, \
    const char32_t **:  scts_insert_mu, \
    char *:             scts_insert_ic, \
    char **:            scts_insert_mc, \
    const char *:       scts_insert_ic, \
    const char **:      scts_insert_mc  \
    )(primary, secondary, where))

/*
sctdoc:scts_insert:

NAME
    scts_insert

SYNOPSIS    
    [char*][char32_t*] scts_insert([char*][char32_t*] primary, [char*][char32_t*] secondary, int where)
    
    char32_t* scts_insert_iu(char32_t *primary, char32_t *secondary, int where)
    char32_t* scts_insert_mu(char32_t **primary, char32_t *secondary, int where)
    char* scts_insert_ic(char *primary, char *secondary, int where)
    char* scts_insert_mc(char **primary, char *secondary, int where)
    
DESCRIPTION
    Function `scts_insert' inserts `secondary' at `where'. `where' supports 
    negative indexing.

RETURN
    Upon success, this function returns (a posibly moved) pointer `primary' if 
    a mutating version is used, or a pointer to a new object if a non-mutating 
    version is used. The new object pointer must be freed using scts_free to 
    avoid memory leaks.

EXAMPLE
    scts_insert("there.", "hi ", 0); // "hi there."

SEE ALSO
    scts_append
    
*/

#endif

//LIB
char32_t* scts_insert_iu(char32_t *primary, char32_t *secondary, int where){
    // input checks
    if (!primary || !secondary) ERETURN(NULL, "null string supplied");
    
    // logic
    int primary_length = scts_length(primary);
    int secondary_length = scts_length(secondary);
    
    if (primary_length == -1 || secondary_length == -1) ERETURN(NULL, "invalid string supplied");
    
    // if negative indexing is used
    if (where < 0) where+=primary_length;
    
    // bound checks
    if (where < 0 || where > primary_length) ERETURN(NULL, "`where' paramater is out of "
                                                 "bounds; supplied string length "
                                                 "is %d, effective `where' "
                                                 "value is %d", primary_length, where);
    
    char32_t *out = scts_new(primary);
    if (!out) ERETURN(NULL,  "Could not allocate memory for new string");
    if (!scta_insert_array(out, where, secondary, secondary_length)) {
        scts_free(out);
        ERETURN(NULL,  "Could not complete insertion");
    }
    return out;
}

char32_t* scts_insert_mu(char32_t **primary, char32_t *secondary, int where){
    char32_t *n;
    
    if (!primary || !(*primary) || !secondary)  ERETURN(NULL, "null string supplied");
    
    n = scts_insert_iu(*primary, secondary, where);
    if (!n) ERETURN(NULL, "internal call to imutable counterpart failed");
    scts_free(*primary);
    *primary = n;
    return n;
}

char* scts_insert_ic(char *primary, char *secondary, int where){
    char32_t *n, *m;
    
    if (!primary || !secondary) ERETURN(NULL, "null string supplied");
    
    n = scts_transcode(primary);
    m = scts_transcode(secondary);
    
    if (!n) ERETURN(NULL, "could not transcode byte sequence");
    if (!scts_insert_mu(&n, m, where)) {
        scts_free(n);
        scts_free(m);
        ERETURN(NULL, "internal call to mutable unicode counterpart failed");
    }
    primary = scts_transcode(n);
    if (!primary) {
        scts_free(n);
        scts_free(m);
        ERETURN(NULL, "internal library failure"); // should never fail to transcode happen unless something is broken
    }
    scts_free(n);
    scts_free(m);
    return primary;
}


char* scts_insert_mc(char **primary, char *secondary, int where){
    char *n;

    if (!primary || !(*primary) || !secondary)  ERETURN(NULL, "null string supplied");
    
    n = scts_insert_ic(*primary, secondary, where);
    if (!n) ERETURN(NULL, "internal call to imutable counterpart failed");
    scts_free(*primary);
    *primary = n;
    return n;
}


//TEST
#ifdef test_insert

#include <assert.h>
#include <stdio.h>
#include <string.h>

int main() {
    sct_debug = stderr;
    char32_t p[] = {0x666, 0x666, 0x666, 0};
    char32_t s[] = {0x665, 0x665, 0};
    char32_t *r;
    
    { // basic tests
        assert((r = scts_insert_iu(p, s, 0)));
        assert(!memcmp(r, ((char32_t[]){0x665, 0x665, 0x666, 0x666, 0x666, 0}), sizeof(char32_t)*5));
        scts_free(r);

        assert((r = scts_insert_iu(p, s, 1)));
        assert(!memcmp(r, ((char32_t[]){0x666, 0x665, 0x665, 0x666, 0x666, 0}), sizeof(char32_t)*5));
        scts_free(r);

        assert((r = scts_insert_iu(p, s, 2)));
        assert(!memcmp(r, ((char32_t[]){0x666, 0x666, 0x665, 0x665, 0x666, 0}), sizeof(char32_t)*5));
        scts_free(r);

        assert((r = scts_insert_iu(p, s, 3)));
        assert(!memcmp(r, ((char32_t[]){0x666, 0x666, 0x666, 0x665, 0x665, 0}), sizeof(char32_t)*5));
        scts_free(r);
        
        sct_debug = NULL;
        assert(!(r = scts_insert_iu(p, s, 4)));
        sct_debug = stderr;

        assert((r = scts_insert_iu(p, s, -1)));
        assert(!memcmp(r, ((char32_t[]){0x666, 0x666, 0x665, 0x665, 0x666, 0}), sizeof(char32_t)*5));
        scts_free(r);

        assert((r = scts_insert_iu(p, s, -2)));
        assert(!memcmp(r, ((char32_t[]){0x666, 0x665, 0x665, 0x666, 0x666, 0}), sizeof(char32_t)*5));
        scts_free(r);

        assert((r = scts_insert_iu(p, s, -3)));
        assert(!memcmp(r, ((char32_t[]){0x665, 0x665, 0x666, 0x666, 0x666, 0}), sizeof(char32_t)*5));
        scts_free(r);

        sct_debug = NULL;
        assert(!(r = scts_insert_iu(p, s, -4)));
        sct_debug = stderr;
    }
    
    { // mutable 
        char *m = scts_new("hi");
        assert(scts_insert(&m, " ", 2));
        assert(scts_insert(&m, "there.", 3));
        assert(scts_insert(&m, ", سليمان", -1));
        assert(!strcmp(m, "hi there, سليمان."));
        scts_free(m);
    }
    return 0;
}

#endif
