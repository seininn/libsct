#define SCT__INTERNAL__
#include "scts.h"

//HEADER
#ifndef SCTS_PART_MATCHES
#define SCTS_PART_MATCHES
bool scts_matches_u(char32_t *primary, char32_t *secondary);
bool scts_matches_c(char *primary, char *secondary);
#define scts_matches(primary, secondary) (_Generic( (0, primary), \
    char32_t *:         scts_matches_u, \
    const char32_t *:   scts_matches_u, \
    char *:             scts_matches_c, \
    const char *:       scts_matches_c \
    )(primary, secondary))

/*
sctdoc:scts_matches:

NAME
    scts_matches

SYNOPSIS
    int generic_dummy([char*][char32_t*] primary)
    
    int generic_dummy_c(char *primary)
    int generic_dummy_u(char32_t *primary)
    
DESCRIPTION
    ...

RETURN
    ...

EXAMPLE
    ...

SEE ALSO
    ...
*/

#endif

//LIB
#include <string.h>
bool scts_matches_u(char32_t *primary, char32_t *secondary){
    // input checks
    if (!primary || !secondary) ERETURN(NULL, "null string supplied for the %s paramater", ((!primary)?"first":"second"));
    // logic
    for (register int i = 0; primary[i] || secondary[i]; ++i) if (primary[i] != secondary[i]) return false;
    return true;
}

bool scts_matches_c(char *primary, char *secondary){
    // input checks
    if (!primary || !secondary) ERETURN(NULL, "null string supplied for the %s paramater", ((!primary)?"first":"second"));
    // logic
    return (bool) !strcmp(primary, secondary);
}

//TEST
#ifdef test_matches

#include <assert.h>
#include <stdio.h>

int main() {
    sct_debug = stderr;
    assert(scts_matches("cheese", "cheese"));
    assert(scts_matches(((char32_t[]){0x667, 0x666, 0x665, 0}), ((char32_t[]){0x667, 0x666, 0x665, 0})));
    assert(!scts_matches("cheese", "bees"));
    assert(!scts_matches(((char32_t[]){0x667, 0x666, 0x665, 0}), ((char32_t[]){0})));
    return 0;
}

#endif
