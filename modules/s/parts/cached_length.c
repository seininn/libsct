#define SCT__INTERNAL__
#include "scts.h"

//HEADER
#ifndef SCTS_PART_CACHED_LENGTH
#define SCTS_PART_CACHED_LENGTH

int scts_cached_length(void *primary);
/*
sctdoc:scts_cached_length:

NAME
    scts_cached_length

SYNOPSIS
    int scts_cached_length([char*][char32_t*] primary)
    
    int scts_cached_length(void *primary)
    
DESCRIPTION
    This function takes a string and returns the number of elements the 
    string has memory for (excluding the terminating null). This function 
    does not scan the string in its entrity, and only checks the cached 
    value stored in the string itself.
    
    To reetrate, this function does not report the same conceptual value 
    that the other length functions report. This function returns the number 
    of "cells" the allocated memory can store, whereas the others report 
    actual lengths.
    
    This function is only designed to work with new strings returned by 
    this module and not on general string litrals. Passing string litrals,
    strings allocated by malloc, or any strings other than those allocated
    by this module is undefined behaviour.
    
    It goes without saying that changes in the string's content will not 
    be taken into account (adding \0 midstring).
    
    DO NOT USE THIS FUNCTION UNLESS YOU ARE CONFIDANT THAT THE STRINGS MEET
    THE CRITERIA DESCRIBED ABOVE.
    
    Consider using the other length functions named at the end of this 
    document.

RETURN
    This function returns byte length required to store a sequenced unicode 
    string, or -1 if the string contains invalid codepoints or ill-formed 
    byte sequences.

EXAMPLE
    scts_legacy_length(scts_new("سلام"))  == ‭8
    scts_legacy_length(U"سلام") == ‭4
    scts_legacy_length("upon")  == ‭4

SEE ALSO
    scts_length
    scts_cached_length
*/

#endif
//LIB
int scts_cached_length(void *primary){
    // input checks
    if (!primary) return 0;
    // logic
    return scta_length(primary);
}

//TEST
#ifdef test_cached_length

#include <assert.h>
#include <stdio.h>

int main() {
    sct_debug = stderr;
    char *s = scts_new("hello");
    assert(scts_cached_length(s) == 5);
    scts_free(s);
    
    return 0;
}

#endif
