#define SCT__INTERNAL__
#include "scts.h"

//HEADER
#ifndef SCTS_PART_LEGACY_LENGTH
#define SCTS_PART_LEGACY_LENGTH

int scts_legacy_length_u(char32_t * restrict primary);
int scts_legacy_length_c(char * restrict primary);

#define scts_legacy_length(primary) (_Generic( (0, primary), \
    char32_t *:scts_legacy_length_u, \
    const char32_t *:scts_legacy_length_u, \
    char *: scts_legacy_length_c, \
    const char *: scts_legacy_length_c \
    )(primary))

/*
sctdoc:scts_legacy_length:

NAME
    scts_legacy_length

SYNOPSIS
    int scts_legacy_length([char*][char32_t*] primary)
    
    int scts_legacy_length_c(char *primary)
    int scts_legacy_length_u(char32_t *primary)
    
DESCRIPTION
    This function takes a string and returns the number of bytes required to 
    store its utf-8 representation. 
    it contains. This function also checks if the string contains invalid 
    codepoints or ill-formed byte sequences as defined by the unicode standard.

RETURN
    This function returns byte length required to store a sequenced unicode 
    string, or -1 if the string contains invalid codepoints or ill-formed 
    byte sequences.

EXAMPLE
    scts_legacy_length("سلام")  == ‭8
    scts_legacy_length(U"سلام") == ‭8
    scts_legacy_length("upon")  == ‭4

SEE ALSO
    scts_length
    scts_cached_length
*/

#endif
//LIB
#include <string.h>

int scts_legacy_length_c(char * restrict primary){
    // input checks
    if (!primary) return 0;
    // logic
    //TODO add checks #security
    return strlen(primary);
}

int scts_legacy_length_u(char32_t * restrict primary){
    register size_t temp = 0;
    register size_t total = 0;
    // input checks
    if (!primary) return 0;
    // logic

    for (register size_t i = 0; primary[i]; ++i) {
        if ((temp = scts_sequence_length(primary[i])) == -1) ERETURN(-1, "invalid codepoint detected %u", primary[i]);
        total += temp;
    }
    return total;
    
}

//TEST
#ifdef test_legacy_length

#include <assert.h>
#include <stdio.h>

int main() {
    sct_debug = stderr;    
    assert(scts_legacy_length(((char32_t []) {'U', '+', 0x666, 0x666, 0x666, '\n', 0})) == 9); //TODO use U".." notation
    assert(scts_legacy_length("عربي" "e") == 9);
    return 0;
}

#endif
