#define SCT__INTERNAL__
#include "scts.h"

//HEADER
#ifndef SCTS_PART_JOIN
#define SCTS_PART_JOIN
char32_t* scts_join_iu(char32_t *primary, char32_t **list);
char32_t* scts_join_mu(char32_t **primary, char32_t **list);
char* scts_join_ic(char *primary, char **list);
char* scts_join_mc(char **primary, char **list);
#define scts_join(primary, list) (_Generic( (0, primary), \
    char32_t *:         scts_join_iu, \
    char32_t **:        scts_join_mu, \
    const char32_t *:   scts_join_iu, \
    const char32_t **:  scts_join_mu, \
    char *:             scts_join_ic, \
    char **:            scts_join_mc, \
    const char *:       scts_join_ic, \
    const char **:      scts_join_mc  \
    )(primary, list))

/*
sctdoc:scts_join:

NAME
    scts_join

SYNOPSIS
    [char*][char32_t*] scts_join([char*][char32_t*] primary, [char*[]][char32_t*[]] list)
    
    char32_t* scts_join_iu(char32_t *primary, char32_t **list)
    char32_t* scts_join_mu(char32_t **primary, char32_t **list)
    char* scts_join_ic(char *primary, char **list)
    char* scts_join_mc(char **primary, char **list)
    
DESCRIPTION
    This function joins the elements of the null-terminated array `list' with 
    `primary' as a separator.

RETURN
    Upon success, this function returns (a posibly moved) pointer `primary' if 
    a mutating version is used, or a pointer to a new object if a non-mutating 
    version is used. The new object pointer must be freed using scts_free to 
    avoid memory leaks.

EXAMPLE
    char *list[] = {"a", "b", "c"};
    scts_join(", ", list); // "a, b, c"

SEE ALSO
    scts_split
    scts_append
    scts_insert
*/

#endif

//LIB
char32_t* scts_join_iu(char32_t *primary, char32_t **list){
    int total_length = 0;
    int total_count = 0;
    // input checks
    if (!primary || !list) ERETURN(NULL, "null string or list supplied");
    
    // logic
    
    // if empty list, return empty string
    if (!list[0]) return scts_new(((char32_t[]) {0}));
    
    // calculate total memory needed to join strings with primary
    for (total_count = 0; list[total_count]; ++total_count) {
        int temp;
        if ((temp = scts_length(list[total_count])) == -1) ERETURN(NULL, "invalid string in `list'");
        total_length += temp;
    }
    {// continued ^
        int temp = scts_length(primary);
        if (temp == -1) ERETURN(NULL, "`primary' is an invalid string")
        total_length += temp*total_count;
    }
    
    
    // allocating string large enough to hold everything
    char32_t *out = scta_new(char32_t);
    if (!scta_set_length(out, total_length)) ERETURN(NULL, "memory reservation failure");
    
    { // copy stuff
        int cur = 0;
        for (int i = 0; i < total_count; ++i) {
            for (int j = 0; list[i][j]; ++j) out[cur++] = list[i][j];
            if (list[i+1]) for (int j = 0; primary[j]; ++j) out[cur++] = primary[j]; 
        }
    }
    return out;   
}

char32_t* scts_join_mu(char32_t **primary, char32_t **list){
    char32_t *n;
    
    if (!primary || !list)  ERETURN(NULL, "null string supplied");
    
    n = scts_join_iu(*primary, list);
    if (!n) ERETURN(NULL, "internal call to imutable counterpart failed");
    scts_free(*primary);
    *primary = n;
    return n;
}

char* scts_join_ic(char *primary, char **list){
    char32_t *n, **m;
    
    if (!primary || !list) ERETURN(NULL, "null string supplied");
    
    n = scts_transcode(primary);
    if (!n) ERETURN(NULL, "could not transcode byte sequence");
    
    if (!(m = scta_new(char32_t *))) ERETURN(NULL, "memory reservation failure");
    for (int i = 0; list[i]; ++i) {
        if (!scta_push(m, scts_transcode(list[i]))) ERETURN(NULL, "memory reservation failure");
        if (m[scta_length(m)-1] == NULL) ERETURN(NULL, "could not transcode byte sequence");
    }
    
    if (!scts_join_mu(&n, m)) {
        scts_free(n);
        scts_free(m);
        ERETURN(NULL, "internal call to mutable unicode counterpart failed");
    }
    primary = scts_transcode(n);
    if (!primary) {
        scts_free(n);
        scts_free(m);
        ERETURN(NULL, "internal library failure"); // should never fail to transcode happen unless something is broken
    }
    scts_free(n);
    scts_free(m);
    return primary;
}

char* scts_join_mc(char **primary, char **list){
    char *n;

    if (!primary || !list)  ERETURN(NULL, "null string supplied");
    
    n = scts_join_ic(*primary, list);
    if (!n) ERETURN(NULL, "internal call to imutable counterpart failed");
    scts_free(*primary);
    *primary = n;
    return n;
}
//TEST
#ifdef test_join

#include <assert.h>
#include <stdio.h>
#include <string.h>

int main() {
    sct_debug = stderr;
    char *p;
    assert(!strcmp((p=scts_join(", ", ((char *[]){"bees", "frogs", "birds", "rabbits", NULL}))), "bees, frogs, birds, rabbits"));
    scts_free(p);
    assert(!strcmp((p=scts_join(", ", ((char *[]){"bees", "frogs", NULL}))), "bees, frogs"));
    scts_free(p);
    assert(!strcmp((p=scts_join(", ", ((char *[]){"bees", NULL}))), "bees"));
    scts_free(p);
    assert(!strcmp((p=scts_join(", ", ((char *[]){NULL}))), ""));
    scts_free(p);
    assert(!strcmp((p=scts_join("", ((char *[]){"bees", "frogs", NULL}))), "beesfrogs"));
    scts_free(p);
    
    sct_debug = NULL;
    assert(!scts_join((char *)NULL, ((char *[]){"bees", "frogs", NULL})));
    sct_debug = stderr;    
    
    return 0;
}

#endif
