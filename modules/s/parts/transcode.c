#define SCT__INTERNAL__
#include "scts.h"

//HEADER
#ifndef SCTS_PART_TRANSCODE
#define SCTS_PART_TRANSCODE
char32_t* scts_transcode_c(char * restrict primary);
char* scts_transcode_u(char32_t * restrict primary);

#define scts_transcode(primary) (_Generic( (0, primary), \
    char32_t *:scts_transcode_u, \
    const char32_t *:scts_transcode_u, \
    char *: scts_transcode_c, \
    const char *: scts_transcode_c \
    )(primary))

/*
sctdoc:scts_transcode:

NAME
    scts_transcode

SYNOPSIS
    [char32_t*][char*] scts_transcode( [char*][char32_t*] primary)
    
    char32_t*   scts_transcode_c(char *primary)
    char*       scts_transcode_u(char32_t *primary)
    
DESCRIPTION
    Accepts a legacy string or a unicode string and transcodes it into the 
    oposit encoding.

RETURN
    This function returns a pointer to the new string, or NULL upon error. A 
    pointer returned by this function must be freed.

SEE ALSO
    scts_new
    scts_substring
*/


#endif
//LIB
char32_t* scts_transcode_c(char * restrict primary){
    // note: scta always terminates an array with a null/0
    //       that's why strings are not terminated here. #security
    
    // input checks
    if (!primary) primary = "";
    // logic
    
    int olen = scts_length(primary);    // primary codepoint count and utf8 byte sequence validation
    if (olen == -1) ERETURN(NULL, "^");
    
    char32_t *out = scta_new(char32_t);  // output string
    
    // check that out is as large as we need it to be
    if (!out) ERETURN(NULL, "failed to reserve a new string");
    if (!scta_set_length(out, olen)) ERETURN(NULL, "failed to set new string length to the required size. size requested was %d bytes", olen);
    
    // loop through primary and store codepoints in out
    register int pcur = 0;
    register int ocur = 0;
    while (primary[pcur]) {
        if (ocur >= olen) ERETURN(NULL, "buffer overrun error");
        out[ocur++] = scts_codepoint(primary+pcur);
        pcur += scts_sequence_length(primary+pcur);
    }
    
    return out;
}

char* scts_transcode_u(char32_t * restrict primary){
    // note: scta always terminates an array with a null/0
    //       that's why strings are not terminated here. #security
    
    // input checks
    if (!primary) primary = (char32_t []) {0};
    
    // logic
    int olen = scts_legacy_length(primary);    // primary legacy length
    char *out = scta_new(char);                 // output string
    
    // check that out is as large as we need it to be
    if (!out) ERETURN(NULL, "failed to reserve a new string");
    if (!scta_set_length(out, olen)) ERETURN(NULL, "failed to set new string length to the required size. size requested was %d bytes", olen);
    
    int cur = 0;
    for (int i = 0; primary[i]; ++i) {
        char seq[5];
        scts_sequence_codepoint(primary[i], seq);
        if (seq[0]) out[cur++] = seq[0]; else continue;
        if (seq[1]) out[cur++] = seq[1]; else continue;
        if (seq[2]) out[cur++] = seq[2]; else continue;
        if (seq[3]) out[cur++] = seq[3]; else continue;
    }
    return out;
}

//TEST
#ifdef test_transcode

#include <assert.h>
#include <string.h>
#include <stdio.h>

int main() {
    char32_t *u; 
    char *c;
    
    sct_debug = stderr;
    
    u = scts_transcode_c("السلام عليكم");
    assert(u != NULL);
    assert(scts_length(u) == 12);
    assert(scts_cached_length(u) == 12);
    assert(u[0] == 0x627);
    assert(u[2] == 0x633);
    assert(u[6] == ' ');
    assert(u[7] == 0x639);
    assert(u[11] == 0x645);
    assert(u[12] == 0);
    assert(!strcmp(c=scts_transcode_u(u), "السلام عليكم"));
    scts_free(u);
    scts_free(c);
    
    assert(u = scts_transcode("😻😻😻😻"));
    assert(!strcmp(c=scts_transcode(u), "😻😻😻😻"));
    assert(scts_length(u) == 4);
    assert(scts_length(c) == 4);
    assert(scts_cached_length(u) == 4);
    assert(scts_cached_length(c) == 16);
    assert(scts_legacy_length(u) == 16);
    assert(scts_legacy_length(c) == 16);
    scts_free(u);
    scts_free(c);
        
    return 0;
}

#endif
