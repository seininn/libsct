#define SCT__INTERNAL__
#include "scts.h"

//HEADER
#ifndef SCTS_PART_new
#define SCTS_PART_new
char* scts_new_c(char *primary);
char32_t* scts_new_u(char32_t* primary);
void scts_free_pointer(void *primary);
void scts_free_list(void *primary);

#define scts_new(primary) (_Generic( (0, primary), \
    char32_t *:scts_new_u, \
    const char32_t *:scts_new_u, \
    char *: scts_new_c, \
    const char *: scts_new_c \
    )(primary))
    
    
#define scts_free(primary) (_Generic( (0, primary), \
    const char32_t *: scts_free_pointer, \
    const char *: scts_free_pointer, \
    char32_t *: scts_free_pointer, \
    char *: scts_free_pointer, \
    const char32_t **: scts_free_list, \
    const char **: scts_free_list, \
    char32_t **: scts_free_list, \
    char **: scts_free_list \
    )(primary))
#endif


/*
sctdoc:scts_new:

NAME
    scts_new

SYNOPSIS
    [char32_t*][char*] scts_new([char32_t*][char*] primary)
    
    char*       scts_new_c(char     *primary)
    char32_t*   scts_new_u(char32_t *primary)
    
DESCRIPTION
    Clones a string. primary can be `NULL' to produce an empty string.

RETURN
    This function returns a pointer to the new string, or NULL upon error. A 
    pointer returned by this function must be freed.
    
SEE ALSO
    scts_transcode
    scts_substring
    scts_free
*/



/*
sctdoc:scts_free:

NAME
    scts_free

SYNOPSIS
    void scts_free([char32_t**][char**][char32_t*][char*] primary)
    
    void scts_free_pointer(void *primary)
    void scts_free_list(void **primary)
    
DESCRIPTION
    This function frees any object allocated and returned by this module.

SEE ALSO
    scts_new
*/


//LIB
#include <string.h> //TODO remove this, and replace all string library calls with scts 
char* scts_new_c(char *primary){
    // input checks
    if (!primary) primary="";
    // logic
    char *out = scta_new(char);
    if (!out) ERETURN(NULL, "failed to allocate memory new dynamic string.");
    if (! scta_insert_array(out, 0, primary, scts_legacy_length(primary))) {
        scta_free(out);
        ERETURN(NULL, "failed to copy primary into dynamic string.");
    }
    return out;
}


char32_t* scts_new_u(char32_t *primary){
    // input checks
    if (!primary) primary= (char32_t[1]) {0}; //TODO use U syntax
    // logic
    char32_t *out = scta_new(char32_t);
    if (!out) ERETURN(NULL, "failed to allocate memory new dynamic string.");
    if (! scta_insert_array(out, 0, primary, scts_length(primary))) { 
        scta_free(out);
        ERETURN(NULL, "failed to copy primary into dynamic string.");
    }
    return out;
}


void scts_free_pointer(void *primary){
    scta_free(primary);
}

void scts_free_list(void *primary){
    void **p = (void **) primary;
    for (int i = 0; p[i]; ++i) scta_free(p[i]);
    scta_free(p);
}


//TEST
#ifdef test_new
#include <string.h>
#include <assert.h>
#include <stdio.h>

int main() {
    char *c, **l;
    char32_t *u;
    
    // testing normal string
    c = scts_new("hello");
    assert(c);
    assert(!strcmp(c, "hello"));
    scts_free(c);
    
    // testing empty string
    c = scts_new("");
    assert(c);
    assert(!strcmp(c, ""));
    scts_free(c);
    
    // testing lengths
    l = scta_new(char *);
    scta_push(l, scts_new("hello"));
    scta_push(l, scts_new("my"));
    scta_push(l, scts_new("name"));
    scta_push(l, scts_new("is"));
    scts_free(l);
    
    // testing normal string
    u = scts_new(((char32_t []) {'h', 'e', 'l', 'l', 'o', 0}));
    assert(u);
    assert(!memcmp(u, ((char32_t []) {'h', 'e', 'l', 'l', 'o', 0}) , sizeof(char32_t)*6 ));
    scts_free(u);
    return 0;
}
#endif
