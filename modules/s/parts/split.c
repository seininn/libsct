#define SCT__INTERNAL__
#include "scts.h"

//HEADER
#ifndef SCTS_PART_SPLIT
#define SCTS_PART_SPLIT

char32_t** scts_split_u(char32_t *primary, char32_t *delemeter);
char** scts_split_c(char *primary, char *delemeter);

#define scts_split(primary, split) (_Generic( (0, primary), \
    char32_t *:         scts_split_u, \
    const char32_t *:   scts_split_u, \
    char *:             scts_split_c, \
    const char *:       scts_split_c \
    )(primary, split))

/*
sctdoc:scts_split:

NAME
    scts_split

SYNOPSIS
    [char**][char32_t**] 
        scts_split([char*][char32_t*] primary, [char*][char32_t*] delemeter)
    
    char32_t** scts_split_u(char32_t *primary, char32_t *delemeter)
    char** scts_split_c(char *primary, char *delemeter)
    
DESCRIPTION
    This function splits `primary' into multiple strings using `delemeter' as 
    the seperator, and returns the result as a string list.

RETURN
    Returns a list of strings on success, and NULL on failure. The resulting 
    list must be freed using `scts_free' or equivilant.

EXAMPLE
    scts_split("sir fella"); // === (char[]) {[0]="sir", [1]="fella", [2]=NULL}

SEE ALSO
    scts_join
    scts_free
*/

#endif

//LIB
char32_t** scts_split_u(char32_t *restrict primary, char32_t *restrict delemeter){
    // input checks
    if (!primary || !delemeter) ERETURN(NULL, "null string supplied for the %s paramater", ((!primary)?"first":"second"));
    
    // logic
    int i = 0, where;
    int length = scts_length(delemeter);
    if (length == -1) ERETURN(NULL, "^");
    char32_t **out = scta_new(char32_t *);
    char32_t *shard;
    
    if (!out) ERETURN(NULL, "^");

    while (true) {
        where = scts_find(primary+i, delemeter);
        
        if (where != -1) shard = scts_substring(primary+i, 0, where);
        else shard = scts_new(primary+i);

        if (!shard) {
            scts_free(out);
            ERETURN(NULL, "^");
        }
        if (!scta_push(out, shard)) {
            scts_free(out);
            ERETURN(NULL, "^");
        }
        
        if (where == -1) break;
        i += (where+length);
    }
    
    return out;
}

char** scts_split_c(char *restrict primary, char *restrict delemeter){
    // input checks
    if (!primary || !delemeter) ERETURN(NULL, "null string supplied for the %s paramater", ((!primary)?"first":"second"));
    
    //TODO redo using string.h functions; no need to be unicode aware here
    //TODO add custom tests for _u 
    char32_t *p = scts_transcode(primary);
    char32_t *s = scts_transcode(delemeter);
    char32_t **o = scts_split_u(p, s);
    if (!o) ERETURN(NULL, "split failed");
    char **out = scta_new(char *);
    scta_set_length(out, scta_length(o));
    for (int i = 0; o[i]; ++i) out[i] = scts_transcode(o[i]);
    scts_free(p);
    scts_free(s);
    scts_free(o);
    return out;
}


//TEST
#ifdef test_split

#include <string.h>
#include <assert.h>
#include <stdio.h>

int main() {
    sct_debug = stderr;
    char **s;
    assert(s = scts_split("a bb  ccc   dddd", " "));
    assert(scta_length(s) == 7);
    assert(!strcmp(s[0], "a"));
    assert(!strcmp(s[1], "bb"));
    assert(!strcmp(s[2], ""));
    assert(!strcmp(s[3], "ccc"));
    assert(!strcmp(s[4], ""));
    assert(!strcmp(s[5], ""));
    assert(!strcmp(s[6], "dddd"));
    scts_free(s);
    
    assert(s = scts_split(" a ", " "));
    assert(scta_length(s) == 3);
    assert(!strcmp(s[0], ""));
    assert(!strcmp(s[1], "a"));
    assert(!strcmp(s[2], ""));
    scts_free(s);
    
    assert(s = scts_split("فلافل", " "));
    assert(scta_length(s) == 1);
    assert(!strcmp(s[0], "فلافل"));
    scts_free(s);
    
    assert(s = scts_split("", " "));
    assert(scta_length(s) == 1);
    assert(!strcmp(s[0], ""));
    scts_free(s);
    
    assert(s = scts_split("فلافل", ""));
    assert(scta_length(s) == 1);
    assert(!strcmp(s[0], "فلافل"));
    scts_free(s);
    
    assert(s = scts_split(" ", " "));
    assert(scta_length(s) == 2);
    assert(!strcmp(s[0], ""));
    assert(!strcmp(s[1], ""));
    scts_free(s);
    
    assert(s = scts_split("", ""));
    assert(scta_length(s) == 1);
    assert(!strcmp(s[0], ""));
    scts_free(s);
    return 0;
}

#endif
