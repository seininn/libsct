#define SCT__INTERNAL__
#include "scts.h"

//HEADER
#ifndef SCTS_PART_CODEPOINT
#define SCTS_PART_CODEPOINT
char32_t scts_codepoint(char *c);
/*
sctdoc:scts_codepoint:

NAME
    scts_codepoint

SYNOPSIS
    char32_t scts_codepoint(char *c);
    
DESCRIPTION
    This function returns the codepoint of the character sequence pointed to
    by `c'.

RETURN
    This function returns the codepoint on success, and -1 sequince is 
    ill-formed.

EXAMPLE
    scts_codepoint("٦") == 0x666;

SEE ALSO
    scts_sequence_codepoint
    scts_sequence_length
*/

#endif

//LIB
char32_t scts_codepoint(char *c){
    // Unicode Standard 6.2: Section 3.9: Page 95
    // (char32_t) -1 error could should be fine because it's not a valid code point
    // validation and string length assertions are preformed in 
    // byte_character_size
    int size = scts_sequence_length(c);
    if (size == -1) ERETURN(-1, "^");
    
    if (size == 1) return c[0];
    else if (size == 2) {
        return  (((char32_t) 0x1f & c[0]) << 6) | 
                ((char32_t) 0x3f & c[1]) ;
    }
    else if (size == 3) {
        return  (((char32_t) 0x0f & c[0]) << 12) | 
                (((char32_t) 0x3f & c[1]) << 6) |
                ((char32_t) 0x3f & c[2]) ;
    }
    else if (size == 4) {
        return  (((char32_t) 0x07 & c[0]) << 18) | 
                (((char32_t) 0x3f & c[1]) << 12) |
                (((char32_t) 0x3f & c[2]) << 6) |
                ((char32_t) 0x3f & c[3]);
    }    
    // If this occurs, something very wrong is happening in scts_byte_character_size
    else ERETURN((char32_t) -1, "could not map codepoint to well-formed byte sequence"); 
}


//TEST
#ifdef test_codepoint

#include <assert.h>
#include <stdio.h>
#include <string.h>

int main() {
    assert(0 == scts_codepoint(""));
    assert(0x636 == scts_codepoint("ض"));
    assert(0xF906 == scts_codepoint("句"));
    assert(0x219D == scts_codepoint("↝"));
    assert(0x55 == scts_codepoint("U"));
    assert(0x1F63B == scts_codepoint("😻"));
    
    sct_debug = NULL;
    assert(scts_codepoint(NULL) == (char32_t) -1);
    assert(scts_codepoint("\xE0\x9F\x80") == (char32_t) -1);
    sct_debug = stderr;
    return 0;
}

#endif
