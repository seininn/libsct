#define SCT__INTERNAL__
#include "scts.h"

//HEADER
#ifndef SCTS_PART_LENGTH
#define SCTS_PART_LENGTH
int scts_length_c(char *primary);
int scts_length_u(char32_t *primary);
int scts_length_l(void **primary); //TODO add to the macro bellow: WARNING:  the compiler seems to select the fuction when 
                                      //                                        given an argument with type (char32_t *).
                                      //                                        remove length_l after modding the macro and run.
                                      //                                        problem appeared in new.c:53

#define scts_length(primary) (_Generic( (0, primary), \
    char32_t *:scts_length_u, \
    const char32_t *:scts_length_u, \
    char *: scts_length_c, \
    const char *: scts_length_c \
    )(primary))

/*
sctdoc:scts_length:

NAME
    scts_length

SYNOPSIS
    int scts_length([char*][char32_t*] primary)
    
    int scts_length_c(char *primary)
    int scts_length_u(char32_t *primary)
    
DESCRIPTION
    This function takes a string and returns the number of unicode codepoints
    it contains. This function also checks if the string contains invalid 
    codepoints or ill-formed byte sequences as defined by the unicode standard.

RETURN
    This function returns the unicode length of a given string, or -1 if the 
    string contains invalid codepoints or ill-formed byte sequences.
    
EXAMPLE
    scts_length("سلام")  == ‭4
    scts_length(U"سلام") == ‭4
    scts_length("upon")  == ‭4

SEE ALSO
    scts_legacy_length
    scts_cached_length
    
*/

#endif
//LIB
#include <string.h>
int scts_length_c(char * restrict primary){
    register size_t i = 0;
    register size_t j = 0;
    // input checks
    if (!primary) return 0;
    // logic
    while (primary[j]) {
        register int s = scts_sequence_length(primary+j);
        if (s == -1) ERETURN(-1, "^");
        j += s;
        ++i;
    }
    return i;
    
}
int scts_length_u(char32_t * restrict primary){
    register size_t i = 0;
    // input checks
    if (!primary) return 0;
    // logic
    while (primary[i]) {
        if (scts_sequence_length(primary[i]) == -1) ERETURN(-1, "^");
        ++i;
    }
    return i;
}
/*
size_t scts_length_l(void ** restrict primary){
    register size_t i = 0;
    // input checks
    if (!primary) return 0;
    // logic
    while (primary[i++]);
    return i-1;
}*/


//TEST
#ifdef test_length

#include <assert.h>
#include <stdio.h>

int main() {
    sct_debug = stderr;
    char *n = NULL;
    assert(scts_length("hello") == 5);
    
    sct_debug = NULL;
    assert(scts_length("\xd2") == -1);
    sct_debug = stderr;
    
    assert(scts_length("عربي" "e") == 5);
    assert(scts_length(((char32_t []) {'h', 'e', 'l', 'l', 'o', 0})) == 5); //TODO use U".." notation
    assert(scts_length(((char32_t []) {'U', '+', 0x666, 0x666, 0x666, '\n', 0})) == 6); //TODO use U".." notation
    //assert(scts_length("عربي" "e"); //TODO use U".." notation with arabic
    assert(scts_length("") == 0);
    assert(scts_length( ((char32_t []) {0})) == 0);
    assert(scts_length(n) == 0);
    
    return 0;
}

#endif
