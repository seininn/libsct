#define SCT__INTERNAL__
#include "scts.h"

//HEADER
#ifndef SCTS_PART_REPLACE
#define SCTS_PART_REPLACE
char* scts_replace_mc(char **primary, char *target, char *replacement);
char* scts_replace_ic(char *primary, char *target, char *replacement);
char32_t* scts_replace_mu(char32_t **primary, char32_t *target, char32_t *replacement);
char32_t* scts_replace_iu(char32_t *primary, char32_t *target, char32_t *replacement);
#define scts_replace(primary, target, replacement) (_Generic( (0, primary), \
    char32_t *:         scts_replace_iu, \
    char32_t **:        scts_replace_mu, \
    const char32_t *:   scts_replace_iu, \
    const char32_t **:  scts_replace_mu, \
    char *:             scts_replace_ic, \
    char **:            scts_replace_mc, \
    const char *:       scts_replace_ic, \
    const char **:      scts_replace_mc  \
    )(primary, target, replacement))

/*
sctdoc:scts_replace:

NAME
    scts_replace

SYNOPSIS
    int generic_dummy([char*][char32_t*] primary)
    
    int generic_dummy_c(char *primary)
    int generic_dummy_u(char32_t *primary)
    
DESCRIPTION
    ...

RETURN
    ...

EXAMPLE
    ...

SEE ALSO
    ...
*/

#endif

//LIB

char32_t* scts_replace_iu(char32_t *primary, char32_t *target, char32_t *replacement){
    // input checks
    if (!primary || !target || !replacement) ERETURN(NULL, "null string supplied");
    // logic
    //TODO Too slow: unwind and enterlance
    int target_length = scts_length(target);
    if (target_length == -1) ERETURN(NULL, "`target' is an invalid string");
    
    char32_t *out = scta_new(char32_t);
    int i = 0;
    while (true) {
        int ti = scts_find(primary+i, target);
        if (ti == -1) {
            if (!scts_append(&out, primary+i)) ERETURN(NULL, "failed to append terminating part of string");
            break;
        }
        else {
            //TODO switch to scts_append_substring() or scts_insert_substring()            
            char32_t *sub = scts_substring(primary+i, 0, ti);
            if (!sub) {
                scts_free(out);
                ERETURN(NULL, "could not extract substring from `primary': [%d:%d]", i, i+ti);
            }
            if (!scts_append(&out, sub)) ERETURN(NULL, "failed to append initial part of string");
            if (!scts_append(&out, replacement)) ERETURN(NULL, "failed to append replacement");
            scts_free(sub);
            i+=(ti+target_length);
        }
    }
    return out;
}

char32_t* scts_replace_mu(char32_t **primary, char32_t *target, char32_t *replacement){
    char32_t *n;
    
    if (!primary || !target || !replacement)  ERETURN(NULL, "null string supplied");
    
    n = scts_replace_iu(*primary, target, replacement);
    if (!n) ERETURN(NULL, "internal call to imutable counterpart failed");
    scts_free(*primary);
    *primary = n;
    return n;
}

char* scts_replace_ic(char *primary, char *target, char *replacement){
    char32_t *n, *m, *l;
    
    if (!primary || !target) ERETURN(NULL, "null string supplied");
    
    n = scts_transcode(primary);
    m = scts_transcode(target);
    l = scts_transcode(replacement);
    
    if (!n) ERETURN(NULL, "could not transcode byte sequence");
    if (!scts_replace_mu(&n, m, l)) {
        scts_free(n);
        scts_free(m);
        scts_free(l);
        ERETURN(NULL, "internal call to mutable unicode counterpart failed");
    }
    primary = scts_transcode(n);
    if (!primary) {
        scts_free(n);
        scts_free(m);
        scts_free(l);
        ERETURN(NULL, "internal library failure"); // should never fail to transcode happen unless something is broken
    }
    scts_free(n);
    scts_free(m);
    scts_free(l);
    return primary;
}


char* scts_replace_mc(char **primary, char *target, char *replacement){
    char *n;

    if (!primary || !(*primary) || !target)  ERETURN(NULL, "null string supplied");
    
    n = scts_replace_ic(*primary, target, replacement);
    if (!n) ERETURN(NULL, "internal call to imutable counterpart failed");
    scts_free(*primary);
    *primary = n;
    return n;
}


//TEST
#ifdef test_replace

#include <assert.h>
#include <stdio.h>
#include <string.h>

int main() {
    sct_debug = stderr;
    char* t;
    assert(scts_matches((t=scts_replace("I was at the cheese factory", "was", "am now")), "I am now at the cheese factory"));
    scts_free(t);
    assert(scts_matches((t=scts_replace("I was at the cheese factory", "", "am now")), "I was at the cheese factory"));
    scts_free(t);
    assert(scts_matches((t=scts_replace("I was at the cheese factory", "at ", "")), "I was the cheese factory"));
    scts_free(t);
    assert(scts_matches((t=scts_replace("I was at the cheese factory", "jebrish", "yoda")), "I was at the cheese factory"));
    scts_free(t);
    assert(scts_matches((t=scts_replace("I was at the cheese factory", "at the cheese factory", "سليمان")), "I was سليمان"));
    scts_free(t);
    return 0;
}

#endif
