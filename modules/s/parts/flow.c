#define SCT__INTERNAL__
#include "scts.h"

//HEADER
#ifndef SCTS_PART_FLOW
#define SCTS_PART_FLOW

// Definition of operation/premitive constants is int init.h

char32_t* scts_flow_iu(char32_t *primary, ...);
char32_t* scts_flow_mu(char32_t **primary, ...);
char* scts_flow_ic(char *primary, ...);
char* scts_flow_mc(char **primary, ...);

#define scts_flow(primary, ...) (_Generic( (0, primary), \
    char32_t *:         scts_flow_iu, \
    char32_t **:        scts_flow_mu, \
    const char32_t *:   scts_flow_iu, \
    const char32_t **:  scts_flow_mu, \
    char *:         scts_flow_ic, \
    char **:        scts_flow_mc, \
    const char *:   scts_flow_ic, \
    const char **:  scts_flow_mc \
    )(primary, __VA_ARGS__, SCTS_STOP))
/*
sctdoc:scts_flow:

NAME
    scts_flow

SYNOPSIS
    [char*][char32_t*] scts_flow([char*][char32_t*] primary, ...)
    
    char* scts_flow_ic(char *primary, ...)//
    char* scts_flow_mc(char **primary, ...)//
    char32_t* scts_flow_iu(char32_t *primary, ...)
    char32_t* scts_flow_mu(char32_t **primary, ...)
    
DESCRIPTION
    preform string operations in a sequential form; appling the result 
    of an operation to the one that follows it, and provides premative 
    flow breqaking facilities.
    
    
PRIMATIVES
    The input stream of each primative must be compatible with the
    output stream of the perceeding primative. There are two types 
    of stream types: a singleton stream (s), and a list stream (l).
    
    A singleton stream consists of a single string that is operated 
    on by primatives and passed from one primative to the next.
    
    A list stream is a set of strings that is operated on as a single 
    unit by primatives and passed from one primative to the next.
    
    The the input type of the fist primative must be that of a 
    singleton stream.
    
    
    SCTS_STOP  [s -> s]    
        Every call to scts_flow_* MUST have this primative as the 
        last argument. If the generic macro is used, ending the 
        argument list with SCTS_STOP is optional.
        
    SCTS_APPEND  [s -> s]
        Identical to `scts_append'.

    SCTS_INSERT  [s -> s]
        Identical to `scts_append'.

    SCTS_REPLACE  [s -> s]
        Identical to `scts_append'.

    SCTS_SUBSTRING  [s -> s]
        Identical to `scts_append'.

    SCTS_SPLIT  [s -> s]
        Identical to `scts_append'.

    SCTS_JOIN  
        
        [s -> s] 
            Identical to `scts_join'.
        [l -> s] ([char*][char32_t*] joint)
            Takes a string that is used to join the elements of the 
            stream list.
        
    SCTS_SELECT  [l -> s] (int i)
        sets the ith element of the stream list as the stream 
        string.
        
    SCTS_FILTER  [l -> l]
        Identical to `scts_filter'.
                
    SCTS_NOT_EMPTY  [s -> s] ()
        stops the stream and returns an empty string if the stream 
        string is empty.
        
    SCTS_CONTAINS  
    
        [s -> s] ([char*][char32_t*] string)
            stops the stream and returns an empty string if the stream
            string does NOT contain `string'.
        [l -> l] ([char*][char32_t*] string)
            stops the stream and returns an empty string if the stream
            list does NOT contain `string'.
        
    SCTS_NOT_CONTAINS  
        
        [s -> s] ([char*][char32_t*] string)
            stops the stream and returns an empty string if the stream
            string contains `string'.
        [l -> l] ([char*][char32_t*] string)
            stops the stream and returns an empty string if the stream
            list contains `string'.
        
    SCTS_NOT_MATCHES  [s -> s] ([char*][char32_t*] string)
        stops the stream and returns an empty string if the stream
        string matchs `string'.
        
    SCTS_ASSERT_LENGTH [l -> l] (int i)
        stops the stream and returns an empty string if the length 
        of the stream list is nit `i'
        
    ----
    SCTS_PRINT
        A special operation that prints the stream, be it string or list,
        to the standard error for the library `sct_debug'
            
    
        

RETURN
    ...

EXAMPLE
    ...

SEE ALSO
    ...
*/

#endif

//LIB
#include <stdarg.h>

char32_t* scts_flow_u_(char32_t *primary, va_list ar){
    #define type_string 1
    #define type_list 2
    int index = 0; // clearer error messages

    #define CHECK_INPUT_IS_STRING  if (last_output_type != type_string) {\
        EPRINT("incompatible flow: list -> string at argument %d", index);\
        goto SUPER_ERROR;\
    }
    
    #define CHECK_INPUT_IS_LIST  if (last_output_type != type_list) {\
        EPRINT("incompatible flow: list -> string at argument %d", index);\
        goto SUPER_ERROR;\
    }
    
    // input checks
    if (!primary) primary = (char32_t[]) {0};

    
    // logic
    if (!(primary = scts_new(primary))) ERETURN(NULL, "^"); // primary will be
                                                            // used as the acc
                                                            // variable
    int last_output_type = type_string; // last_output_type stores the output type 
                                     // of the last operation preformed. This is
                                     // used to make sure that the input of the 
                                     // next operation is compatible with that 
                                     // output
                                                            
    char32_t **list = NULL;
    
    while (true) {
        int op = va_arg(ar, int);
        ++index;
        
        switch (op) {
            case SCTS_STOP:
                goto SUPER_DONE;
                break;
             
            // str -> str
            case SCTS_APPEND: {
                CHECK_INPUT_IS_STRING;
                char32_t* secondary = va_arg(ar, char32_t*);
                if (!scts_append(&primary, secondary)) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                break;
            }
                
            case SCTS_INSERT: {
                CHECK_INPUT_IS_STRING;
                char32_t* secondary = va_arg(ar, char32_t*);
                int where = va_arg(ar, int);
                if (!scts_insert(&primary, secondary, where)) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                break;            
            }
            
            case SCTS_JOIN: {
                if (last_output_type == type_string) {
                    char32_t** l = va_arg(ar, char32_t**);
                    if (!scts_join(&primary, l)) {
                        EPRINT("broken flow at operation %d: ^", index);
                        goto SUPER_ERROR;
                    }
                    break;
                }
                else {
                    // note: type switch
                    primary = va_arg(ar, char32_t*);
                    if (!(primary = scts_new(primary))) {
                        EPRINT("enternal error at operation %d: ^", index);
                        goto SUPER_ERROR;
                    }
                    if (!scts_join(&primary, list)) {
                        EPRINT("broken flow at operation %d: ^", index);
                        goto SUPER_ERROR;
                    }
                    scts_free(list);
                    last_output_type = type_string;
                    break;            
                }            
            }
            
            case SCTS_REPLACE: {
                CHECK_INPUT_IS_STRING;
                char32_t* target = va_arg(ar, char32_t*);
                char32_t* replacement = va_arg(ar, char32_t*);
                if (!scts_replace(&primary, target, replacement)) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                break;            
            }
            
            case SCTS_SUBSTRING: {
                CHECK_INPUT_IS_STRING;
                int from = va_arg(ar, int);
                int to = va_arg(ar, int);
                if (!scts_substring(&primary, from, to)) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                break;                            
            }
            
            // str -> list
            case SCTS_SPLIT: {
                CHECK_INPUT_IS_STRING;
                char32_t *del = va_arg(ar, char32_t*);
                
                if (!(list = scts_split(primary, del))) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                
                scts_free(primary);
                last_output_type = type_list;
                break;            
            }

            case SCTS_SELECT: {
                CHECK_INPUT_IS_LIST;
                int n = va_arg(ar, int);
                
                if (n >= scta_length(list) || n < 0) {
                    EPRINT("enternal error at operation %d: out of bonds: [%d] element of %d-element list requested.", index, n, scta_length(list));
                    goto SUPER_ERROR;
                }
                primary = scts_new(list[n]);
                scts_free(list);
                last_output_type = type_string;
                break;            
            }
            
            // list -> list
            case SCTS_FILTER: {
                CHECK_INPUT_IS_LIST;
                int criteria = va_arg(ar, int);
                char32_t* what = va_arg(ar, char32_t*);
                if (!scts_filter(list, criteria, what)) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                break;
            }
                
            // str -- flow control --> str
            case SCTS_NOT_EMPTY: {
                CHECK_INPUT_IS_STRING;
                if (primary[0] == 0) goto SUPER_DONE;
                break;
            }
            case SCTS_CONTAINS: {
                if (last_output_type == type_string) {
                    char32_t *needle = va_arg(ar, char32_t*);    
                    if (!needle) {
                        EPRINT("enternal error at operation %d: required argument is null.", index);
                        goto SUPER_ERROR;
                    }
                    if (scts_find(primary, needle) == -1) {
                        scts_free(primary);
                        primary = scts_new(((char32_t *){0}));
                        if (!primary) {
                            // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                            EPRINT("broken flow at operation %d: ^", index);
                            return NULL;
                        }
                        goto SUPER_DONE;
                    }
                    break;
                }
                else {
                    char32_t *match = va_arg(ar, char32_t*);
                    bool found = false;
                    
                    if (!match) {
                        EPRINT("enternal error at operation %d: required argument is null.", index);
                        goto SUPER_ERROR;
                    }
                    for (int i = 0; list[i]; ++i) if (scts_matches(list[i], match)) {
                        found = true;
                        break;
                    }
                    if (!found) {
                        scts_free(list);
                        primary = scts_new(((char32_t *){0}));
                        if (!primary) {
                            // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                            EPRINT("broken flow at operation %d: ^", index);
                            return NULL;
                        }                
                        last_output_type = type_string;
                        goto SUPER_DONE;
                    }
                    break;
                }
            }
            case SCTS_NOT_CONTAINS: {
                if (last_output_type == type_string) {
                    char32_t *needle = va_arg(ar, char32_t*);
                    
                    if (!needle) {
                        EPRINT("enternal error at operation %d: required argument is null.", index);
                        goto SUPER_ERROR;
                    }
                    if (scts_find(primary, needle) != -1) {
                        scts_free(primary);
                        primary = scts_new(((char32_t *){0}));
                        if (!primary) {
                            // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                            EPRINT("broken flow at operation %d: ^", index);
                            return NULL;
                        }
                        goto SUPER_DONE;
                    }
                    break;
                }
                else {
                    char32_t *match = va_arg(ar, char32_t*);
                    bool found = false;
                    
                    if (!match) {
                        EPRINT("enternal error at operation %d: required argument is null.", index);
                        goto SUPER_ERROR;
                    }
                    for (int i = 0; list[i]; ++i) if (scts_matches(list[i], match)) {
                        found = true;
                        break;
                    }
                    if (found) {
                        scts_free(list);
                        primary = scts_new(((char32_t *){0}));
                        if (!primary) {
                            // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                            EPRINT("broken flow at operation %d: ^", index);
                            return NULL;
                        }                
                        last_output_type = type_string;
                        goto SUPER_DONE;
                    }
                    break;
                }
            }
            case SCTS_NOT_MATCHES: {
                CHECK_INPUT_IS_STRING;
                char32_t *match = va_arg(ar, char32_t*);
                
                if (!match) {
                    EPRINT("enternal error at operation %d: required argument is null.", index);
                    goto SUPER_ERROR;
                }
                if (scts_matches(primary, match)) {
                    scts_free(primary);
                    primary = scts_new(((char32_t *){0}));
                    if (!primary) {
                        // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                        EPRINT("broken flow at operation %d: ^", index);
                        return NULL;
                    }
                    goto SUPER_DONE;
                }
                break;
            }
            
            
            case SCTS_PRINT: {
                if (!sct_debug) break;
                if (last_output_type == type_string) {
                    fprintf(sct_debug, "[%d] type:string -> '", index);
                    scts_depprint(sct_debug, primary);
                    fprintf(sct_debug, "'\n");
                }
                else {
                    fprintf(sct_debug, "[%d] type:list[%d] -> ['", index, scta_length(list));
                    for (int i = 0; list[i]; ++i) {
                        if (i) fprintf(sct_debug, "', '");
                        scts_depprint(sct_debug, list[i]);
                    }
                    fprintf(sct_debug, "']\n");
                }    
                break;
            }
            
            // list -- flow control --> list
            case SCTS_ASSERT_LENGTH: {
                CHECK_INPUT_IS_LIST;
                int length = va_arg(ar, int);
                if (scta_length(list) != length) {
                    scts_free(list);
                    primary = scts_new(((char32_t *){0}));
                    if (!primary) {
                        // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                        EPRINT("broken flow at operation %d: ^", index);
                        return NULL;
                    }                
                    last_output_type = type_string;
                    goto SUPER_DONE;
                }
                break;
            }

            default:
                    EPRINT("enternal error at operation %d: operation not implemented.", index);
                    goto SUPER_ERROR;
        }
    }
    
    SUPER_ERROR:
    if (last_output_type == type_string) scts_free(primary);
    else scts_free(list);
    return NULL;
    
    SUPER_DONE:
    if (last_output_type != type_string) {
        EPRINT("flow terminates in a list which cannot be returned");
        goto SUPER_ERROR;
    }
    // free temp list
    return primary;
    
    #undef type_string
    #undef type_list
    #undef CHECK_INPUT_IS_STRING
    #undef CHECK_INPUT_IS_LIST
}

char32_t* scts_flow_iu(char32_t *primary, ...){
    va_list ar;
    va_start(ar, primary);
    
    char32_t *r = scts_flow_u_(primary, ar);
    
    va_end(ar);
    return r;
}

char32_t* scts_flow_mu(char32_t **primary, ...){
    va_list ar;
    va_start(ar, primary);
    
    char32_t *r = scts_flow_u_(*primary, ar);
    if (!r) {
        va_end(ar);
        ERETURN(NULL, "internal call to imutable counterpart failed");
    }
    scts_free(*primary);
    *primary = r;
    return r;
}


/* XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX 
    
    USE GEN_SCTS_FLOWC TO GENERATE THE FOLLOWING SECTION AND TEST IT
    THEN COPY IT OVER HERE.
    
    DONT FORGET TO UPDATE THE TESTS IF NEEDED
    
XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX  */

char* scts_flow_c_(char *primary, va_list ar){
    #define type_string 1
    #define type_list 2
    int index = 0; // clearer error messages

    #define CHECK_INPUT_IS_STRING  if (last_output_type != type_string) {\
        EPRINT("incompatible flow: list -> string at argument %d", index);\
        goto SUPER_ERROR;\
    }
    
    #define CHECK_INPUT_IS_LIST  if (last_output_type != type_list) {\
        EPRINT("incompatible flow: list -> string at argument %d", index);\
        goto SUPER_ERROR;\
    }
    
    // input checks
    if (!primary) primary = (char[]) {0};

    
    // logic
    if (!(primary = scts_new(primary))) ERETURN(NULL, "^"); // primary will be
                                                            // used as the acc
                                                            // variable
    int last_output_type = type_string; // last_output_type stores the output type 
                                     // of the last operation preformed. This is
                                     // used to make sure that the input of the 
                                     // next operation is compatible with that 
                                     // output
                                                            
    char **list = NULL;
    
    while (true) {
        int op = va_arg(ar, int);
        ++index;
        
        switch (op) {
            case SCTS_STOP:
                goto SUPER_DONE;
                break;
             
            // str -> str
            case SCTS_APPEND: {
                CHECK_INPUT_IS_STRING;
                char* secondary = va_arg(ar, char*);
                if (!scts_append(&primary, secondary)) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                break;
            }
                
            case SCTS_INSERT: {
                CHECK_INPUT_IS_STRING;
                char* secondary = va_arg(ar, char*);
                int where = va_arg(ar, int);
                if (!scts_insert(&primary, secondary, where)) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                break;            
            }
            
            case SCTS_JOIN: {
                if (last_output_type == type_string) {
                    char** l = va_arg(ar, char**);
                    if (!scts_join(&primary, l)) {
                        EPRINT("broken flow at operation %d: ^", index);
                        goto SUPER_ERROR;
                    }
                    break;
                }
                else {
                    // note: type switch
                    primary = va_arg(ar, char*);
                    if (!(primary = scts_new(primary))) {
                        EPRINT("enternal error at operation %d: ^", index);
                        goto SUPER_ERROR;
                    }
                    if (!scts_join(&primary, list)) {
                        EPRINT("broken flow at operation %d: ^", index);
                        goto SUPER_ERROR;
                    }
                    scts_free(list);
                    last_output_type = type_string;
                    break;            
                }            
            }
            
            case SCTS_REPLACE: {
                CHECK_INPUT_IS_STRING;
                char* target = va_arg(ar, char*);
                char* replacement = va_arg(ar, char*);
                if (!scts_replace(&primary, target, replacement)) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                break;            
            }
            
            case SCTS_SUBSTRING: {
                CHECK_INPUT_IS_STRING;
                int from = va_arg(ar, int);
                int to = va_arg(ar, int);
                if (!scts_substring(&primary, from, to)) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                break;                            
            }
            
            // str -> list
            case SCTS_SPLIT: {
                CHECK_INPUT_IS_STRING;
                char *del = va_arg(ar, char*);
                
                if (!(list = scts_split(primary, del))) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                
                scts_free(primary);
                last_output_type = type_list;
                break;            
            }

            case SCTS_SELECT: {
                CHECK_INPUT_IS_LIST;
                int n = va_arg(ar, int);
                
                if (n >= scta_length(list) || n < 0) {
                    EPRINT("enternal error at operation %d: out of bonds: [%d] element of %d-element list requested.", index, n, scta_length(list));
                    goto SUPER_ERROR;
                }
                primary = scts_new(list[n]);
                scts_free(list);
                last_output_type = type_string;
                break;            
            }
            
            // list -> list
            case SCTS_FILTER: {
                CHECK_INPUT_IS_LIST;
                int criteria = va_arg(ar, int);
                char* what = va_arg(ar, char*);
                if (!scts_filter(list, criteria, what)) {
                    EPRINT("broken flow at operation %d: ^", index);
                    goto SUPER_ERROR;
                }
                break;
            }
                
            // str -- flow control --> str
            case SCTS_NOT_EMPTY: {
                CHECK_INPUT_IS_STRING;
                if (primary[0] == 0) goto SUPER_DONE;
                break;
            }
            case SCTS_CONTAINS: {
                if (last_output_type == type_string) {
                    char *needle = va_arg(ar, char*);    
                    if (!needle) {
                        EPRINT("enternal error at operation %d: required argument is null.", index);
                        goto SUPER_ERROR;
                    }
                    if (scts_find(primary, needle) == -1) {
                        scts_free(primary);
                        primary = scts_new(((char *){0}));
                        if (!primary) {
                            // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                            EPRINT("broken flow at operation %d: ^", index);
                            return NULL;
                        }
                        goto SUPER_DONE;
                    }
                    break;
                }
                else {
                    char *match = va_arg(ar, char*);
                    bool found = false;
                    
                    if (!match) {
                        EPRINT("enternal error at operation %d: required argument is null.", index);
                        goto SUPER_ERROR;
                    }
                    for (int i = 0; list[i]; ++i) if (scts_matches(list[i], match)) {
                        found = true;
                        break;
                    }
                    if (!found) {
                        scts_free(list);
                        primary = scts_new(((char *){0}));
                        if (!primary) {
                            // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                            EPRINT("broken flow at operation %d: ^", index);
                            return NULL;
                        }                
                        last_output_type = type_string;
                        goto SUPER_DONE;
                    }
                    break;
                }
            }
            case SCTS_NOT_CONTAINS: {
                if (last_output_type == type_string) {
                    char *needle = va_arg(ar, char*);
                    
                    if (!needle) {
                        EPRINT("enternal error at operation %d: required argument is null.", index);
                        goto SUPER_ERROR;
                    }
                    if (scts_find(primary, needle) != -1) {
                        scts_free(primary);
                        primary = scts_new(((char *){0}));
                        if (!primary) {
                            // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                            EPRINT("broken flow at operation %d: ^", index);
                            return NULL;
                        }
                        goto SUPER_DONE;
                    }
                    break;
                }
                else {
                    char *match = va_arg(ar, char*);
                    bool found = false;
                    
                    if (!match) {
                        EPRINT("enternal error at operation %d: required argument is null.", index);
                        goto SUPER_ERROR;
                    }
                    for (int i = 0; list[i]; ++i) if (scts_matches(list[i], match)) {
                        found = true;
                        break;
                    }
                    if (found) {
                        scts_free(list);
                        primary = scts_new(((char *){0}));
                        if (!primary) {
                            // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                            EPRINT("broken flow at operation %d: ^", index);
                            return NULL;
                        }                
                        last_output_type = type_string;
                        goto SUPER_DONE;
                    }
                    break;
                }
            }
            case SCTS_NOT_MATCHES: {
                CHECK_INPUT_IS_STRING;
                char *match = va_arg(ar, char*);
                
                if (!match) {
                    EPRINT("enternal error at operation %d: required argument is null.", index);
                    goto SUPER_ERROR;
                }
                if (scts_matches(primary, match)) {
                    scts_free(primary);
                    primary = scts_new(((char *){0}));
                    if (!primary) {
                        // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                        EPRINT("broken flow at operation %d: ^", index);
                        return NULL;
                    }
                    goto SUPER_DONE;
                }
                break;
            }
            
            
            case SCTS_PRINT: {
                if (!sct_debug) break;
                if (last_output_type == type_string) {
                    fprintf(sct_debug, "[%d] type:string -> '", index);
                    scts_depprint(sct_debug, primary);
                    fprintf(sct_debug, "'\n");
                }
                else {
                    fprintf(sct_debug, "[%d] type:list[%d] -> ['", index, scta_length(list));
                    for (int i = 0; list[i]; ++i) {
                        if (i) fprintf(sct_debug, "', '");
                        scts_depprint(sct_debug, list[i]);
                    }
                    fprintf(sct_debug, "']\n");
                }    
                break;
            }
            
            // list -- flow control --> list
            case SCTS_ASSERT_LENGTH: {
                CHECK_INPUT_IS_LIST;
                int length = va_arg(ar, int);
                if (scta_length(list) != length) {
                    scts_free(list);
                    primary = scts_new(((char *){0}));
                    if (!primary) {
                        // XXX warning: irregular return to avoid free path SUPPER_ERROR: could lead to bugs, be warned
                        EPRINT("broken flow at operation %d: ^", index);
                        return NULL;
                    }                
                    last_output_type = type_string;
                    goto SUPER_DONE;
                }
                break;
            }

            default:
                    EPRINT("enternal error at operation %d: operation not implemented.", index);
                    goto SUPER_ERROR;
        }
    }
    
    SUPER_ERROR:
    if (last_output_type == type_string) scts_free(primary);
    else scts_free(list);
    return NULL;
    
    SUPER_DONE:
    if (last_output_type != type_string) {
        EPRINT("flow terminates in a list which cannot be returned");
        goto SUPER_ERROR;
    }
    // free temp list
    return primary;
    
    #undef type_string
    #undef type_list
    #undef CHECK_INPUT_IS_STRING
    #undef CHECK_INPUT_IS_LIST
}

char* scts_flow_ic(char *primary, ...){
    va_list ar;
    va_start(ar, primary);
    
    char *r = scts_flow_c_(primary, ar);
    
    va_end(ar);
    return r;
}

char* scts_flow_mc(char **primary, ...){
    va_list ar;
    va_start(ar, primary);
    
    char *r = scts_flow_c_(*primary, ar);
    if (!r) {
        va_end(ar);
        ERETURN(NULL, "internal call to imutable counterpart failed");
    }
    scts_free(*primary);
    *primary = r;
    return r;
}




//TEST
#ifdef test_flow

#include <assert.h>
#include <stdio.h>

int main() {
    /* 
        add proper tests
        commit git
        move print to io
    */
    
    sct_debug = stderr;
    
    {
        char32_t *ll[200] = {0};
        char32_t *t;
        t = scts_flow((ll[0] = scts_transcode("hel-yahoo-lo")), 
            SCTS_SUBSTRING, 1, -2, 
            SCTS_APPEND, (ll[1]=scts_transcode("ly")),
            SCTS_SPLIT, ((char32_t[]) {'-', 0}),
            SCTS_ASSERT_LENGTH, 3,
            SCTS_NOT_CONTAINS, (ll[4]=scts_transcode("ysadfoo")), 
            SCTS_CONTAINS, (ll[5]=scts_transcode("yahoo")), 
            SCTS_SELECT, 1,
            SCTS_SPLIT, ((char32_t[]) {'h', 0}), 
            SCTS_JOIN, (ll[2]=scts_transcode("r")), 
            SCTS_NOT_MATCHES, (ll[3]=scts_transcode("yasoo")),
            SCTS_JOIN, ((char32_t*[]) {ll[7]=scts_transcode("you "), ll[8]=scts_transcode(" zebra"), 0}),
            SCTS_SPLIT, ((char32_t[]) {' ', 0}),
            SCTS_FILTER, SCTS_CONTAINS, ((char32_t[]) {'o', 0}),
            SCTS_SELECT, 0);
        
        assert(scts_matches(t, ll[6] = scts_transcode("zebra")));
        
        for (int i = 0; i < 200; ++i) if (ll[i]) {
            scts_free(ll[i]);
            ll[i] = NULL;
        } else break;
        
        scts_free(t);
    }
    
    {
        char32_t *ll[200] = {0};
        char32_t *t;
        t = scts_transcode("أهلا وسهلا يا قمر");
        assert(scts_flow(&t, SCTS_APPEND, ll[0]=scts_transcode(" cheese "), SCTS_SUBSTRING, 5, -1));
        assert(scts_matches(t, ll[1] = scts_transcode("وسهلا يا قمر cheese")));
        for (int i = 0; i < 200; ++i) if (ll[i]) {
            scts_free(ll[i]);
            ll[i] = NULL;
        } else break;
        scts_free(t);
    }
    
    
    // XXX AUTOGENERATED TOO, SEE THE ABOVE XXX COMMENT
    {
        char *t;
        t = scts_flow("hel-yahoo-lo", SCTS_SUBSTRING, 1, -2, 
            SCTS_APPEND, "ly",
            SCTS_SPLIT, "-",
            SCTS_ASSERT_LENGTH, 3,
            SCTS_NOT_CONTAINS, "ysadfoo", 
            SCTS_CONTAINS, "yahoo", 
            SCTS_SELECT, 1,
            SCTS_SPLIT, "h", 
            SCTS_JOIN, "r", 
            SCTS_NOT_MATCHES, "yasoo",
            SCTS_JOIN, ((char*[]) {"you ", " zebra", 0}),
            SCTS_SPLIT, " ",
            SCTS_FILTER, SCTS_CONTAINS, "o",
            SCTS_SELECT, 0);
        
        assert(scts_matches(t, "zebra"));
        scts_free(t);
    }
    {
        char *t;
        t = scts_new("أهلا وسهلا يا قمر");
        assert(scts_flow(&t, SCTS_APPEND, " cheese ", SCTS_SUBSTRING, 5, -1));
        assert(scts_matches(t, "وسهلا يا قمر cheese"));
        scts_free(t);
    }
    
    
    return 0;
}

#endif
