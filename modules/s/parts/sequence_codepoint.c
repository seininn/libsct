#define SCT__INTERNAL__
#include "scts.h"

//HEADER
#ifndef SCTS_PART_SEQUENCE_CODEPOINT
#define SCTS_PART_SEQUENCE_CODEPOINT
char* scts_sequence_codepoint(char32_t c, char buf[restrict static 5]);
/*
sctdoc:scts_sequence_codepoint:

NAME
    scts_sequence_codepoint

SYNOPSIS
    char* scts_sequence_codepoint(char32_t c, char buf[restrict static 5])
    
DESCRIPTION
    This function is the inverse of scts_codepoint; it takes a codepoint and 
    generates its sequenced representation. The second argument must be an 
    array that can hold at least 5 elements.

RETURN
    This function returns `buf' on success, and NULL if `c' is an invalid 
    codepoint.

EXAMPLE
    scts_sequence_codepoint(0x666, buf); // ==  "٦"

SEE ALSO
    scts_codepoint
    scts_sequence_length
*/

#endif

//LIB
char* scts_sequence_codepoint(char32_t c, char buf[restrict static 5]){
    // Unicode Standard 6.2: Section 3.9: Page 95
    int size = scts_sequence_length(c);
    
    if (!size) ERETURN(NULL, "^");
    
    if (size == 1) {
        buf[1] = 0;
        buf[0] = c;
    }
    else if (size == 2) {
        buf[2] = 0;
        buf[1] = (c & 0x3f) | 0x80;
        c>>=6;
        buf[0] = (c & 0x1f) | 0xc0;
    }
    else if (size == 3) {
        buf[3] = 0;
        buf[2] = (c & 0x3f) | 0x80;
        c>>=6;
        buf[1] = (c & 0x3f) | 0x80;
        c>>=6;
        buf[0] = (c & 0xf) | 0xe0;
    }
    else if (size == 4) {
        buf[4] = 0;
        buf[3] = (c & 0x3f) | 0x80;
        c>>=6;
        buf[2] = (c & 0x3f) | 0x80;
        c>>=6;
        buf[1] = (c & 0x3f) | 0x80;
        c>>=6;
        buf[0] = (c & 0x7) | 0xf0;        
    }
    // If this occurs, something very wrong is happening in scts_byte_character_size
    else ERETURN(NULL, "could not map codepoint to well-formed byte sequence"); 
    
    return buf;    
}


//TEST
#ifdef test_sequence_codepoint

#include <assert.h>
#include <stdio.h>
#include <string.h>

int main() {
    sct_debug = stderr;
    
    char buf[5];
    assert(!strcmp(scts_sequence_codepoint(0, buf), ""));
    assert(!strcmp(scts_sequence_codepoint(0x666, buf), "٦"));
    assert(!strcmp(scts_sequence_codepoint(0x23, buf), "#"));
    assert(!strcmp(scts_sequence_codepoint(0xF906, buf), "句"));
    assert(!strcmp(scts_sequence_codepoint(0x1F63B, buf), "😻"));
    return 0;
}

#endif
