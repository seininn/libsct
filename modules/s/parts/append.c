#define SCT__INTERNAL__
#include "scts.h"

//HEADER
#ifndef SCTS_PART_APPEND
#define SCTS_PART_APPEND

char32_t* scts_append_iu(char32_t *primary, char32_t *secondary);
char32_t* scts_append_mu(char32_t **primary, char32_t *secondary);
char* scts_append_ic(char *primary, char *secondary);
char* scts_append_mc(char **primary, char *secondary);
#define scts_append(primary, secondary) (_Generic( (0, primary), \
    char32_t *:         scts_append_iu, \
    char32_t **:        scts_append_mu, \
    const char32_t *:   scts_append_iu, \
    const char32_t **:  scts_append_mu, \
    char *:             scts_append_ic, \
    char **:            scts_append_mc, \
    const char *:       scts_append_ic, \
    const char **:      scts_append_mc  \
    )(primary, secondary))

/*
sctdoc:scts_append:

NAME
    scts_append

SYNOPSIS
    [char*][char32_t*] scts_append([char*][char32_t*] primary, [char*][char32_t*] secondary)
    
    char32_t* scts_append_iu(char32_t *primary, char32_t *secondary)
    char32_t* scts_append_mu(char32_t **primary, char32_t *secondary)
    char* scts_append_ic(char *primary, char *secondary)
    char* scts_append_mc(char **primary, char *secondary)
    
DESCRIPTION
    `scts_append' concatentates `secondary' to `primary' and returns the 
    result.

RETURN
    Upon success, this function returns (a posibly moved) pointer `primary' if 
    a mutating version is used, or a pointer to a new object if a non-mutating 
    version is used. The new object pointer must be freed using scts_free to 
    avoid memory leaks.

EXAMPLE
    scts_append("abc", "def") // "abcdef"

SEE ALSO
    scts_new
    scts_insert
    scts_free
*/

#endif

//LIB
char32_t* scts_append_iu(char32_t *primary, char32_t *secondary){
    char32_t *out;
    int length;
    
    if (!primary)  ERETURN(NULL, "null string supplied");
    if ((length = scts_length(primary)) == -1) ERETURN(NULL, "malformed string supplied");
    if (!(out = scts_insert_iu(primary, secondary, length))) ERETURN(NULL, "internal call failed");
    
    
    return out;
}

char32_t* scts_append_mu(char32_t **primary, char32_t *secondary){
    char32_t *n;
    
    if (!primary || !(*primary) || !secondary)  ERETURN(NULL, "null string supplied");
    
    n = scts_append_iu(*primary, secondary);
    if (!n) ERETURN(NULL, "internal call to imutable counterpart failed");
    scts_free(*primary);
    *primary = n;
    return n;
}

char* scts_append_ic(char *primary, char *secondary){
    char32_t *n, *m;
    
    if (!primary || !secondary) ERETURN(NULL, "null string supplied");
    
    n = scts_transcode(primary);
    m = scts_transcode(secondary);
    
    if (!n) ERETURN(NULL, "could not transcode byte sequence");
    if (!scts_append_mu(&n, m)) {
        scts_free(n);
        scts_free(m);
        ERETURN(NULL, "internal call to mutable unicode counterpart failed");
    }
    primary = scts_transcode(n);
    if (!primary) {
        scts_free(n);
        scts_free(m);
        ERETURN(NULL, "internal library failure"); // should never fail to transcode happen unless something is broken
    }
    scts_free(n);
    scts_free(m);
    return primary;
}


char* scts_append_mc(char **primary, char *secondary){
    char *n;

    if (!primary || !(*primary) || !secondary)  ERETURN(NULL, "null string supplied");
    
    n = scts_append_ic(*primary, secondary);
    if (!n) ERETURN(NULL, "internal call to imutable counterpart failed");
    scts_free(*primary);
    *primary = n;
    return n;
}


//TEST
#ifdef test_append

#include <assert.h>
#include <stdio.h>
#include <string.h>

int main() {
    sct_debug = stderr;
    { // mutable 
        char *m = scts_new("the abjds:");
        assert(scts_append(&m, " ا"));
        assert(scts_append(&m, " ب"));
        assert(scts_append(&m, " ج"));
        assert(scts_append(&m, " د"));
        assert(!strcmp(m, "the abjds: ا ب ج د"));
        scts_free(m);
    }
    return 0;
}

#endif
