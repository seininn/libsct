#define SCT__INTERNAL__
#include "scts.h"

//HEADER
#ifndef SCTS_PART_FILTER
#define SCTS_PART_FILTER
bool scts_filter_c(char ***list_ptr, int criteria, char *what);
bool scts_filter_u(char32_t ***list_ptr, int criteria, char32_t *what);
#define scts_filter(list, criteria, what) (_Generic( (0, (list)), \
    char32_t **:       scts_filter_u, \
    const char32_t **: scts_filter_u, \
    char **:       scts_filter_c, \
    const char **: scts_filter_c \
    )(&(list), (criteria), (what)))

/*
sctdoc:scts_filter:

NAME
    scts_filter

SYNOPSIS

    bool scts_filter(   [char**][char32_t**] list, 
                        int criteria, 
                        [char**][char32_t**] what)
    
    bool scts_filter_c(char ***list_ptr, int criteria, char *what);
    bool scts_filter_u(char32_t ***list_ptr, int criteria, char32_t *what);
    
DESCRIPTION
    This function/macro accepts `list' and filters out elements based on the 
    criteria and `what'.
    
    CAUTION: This function should ONLY be used on lists returned by this 
             library.
    
    There are 6 criterias by which strings are filtered out:
        
        SCTS_EMPTY                  Filters out empty strings
        SCTS_NOT_EMPTY              Inverse of SCTS_EMPTY
        SCTS_CONTAINS               Filters out strings containing the sub-
                                    string `what'.
        SCTS_NOT_CONTAINS           Filters out strings that do not contain 
                                    the substring `what'.
        SCTS_MATCHES                Filters out strings that match `what'.
        SCTS_NOT_MATCHES            Filters out strings that don't match `what'.

RETURN
    This function returns false if an internal error occurs, and true 
    otherwise.

EXAMPLE
    char **l;
    l = scts_split("My name is bobias!", " ");
    scts_filter(l, SCTS_EMPTY, NULL);
    scts_filter(l, SCTS_CONTAINS, "a");
    char *r = scts_join("", l);
    puts(r); // Output: "Myis"
    scts_free(r);
    scts_free(l);
    
*/

#endif

//LIB

bool matches_u(void *a, void *b) {
    return scts_matches_u(a, b);
}

bool not_matches_u(void *a, void *b) {
    return !matches_u(a, b);
}

bool contains_u(void *a, void *b) {
    return (scts_find_u(a, b)!=-1);
}

bool not_contains_u(void *a, void *b) {
    return !contains_u(a, b);
}

bool matches_c(void *a, void *b) {
    return scts_matches_c(a, b);
}

bool not_matches_c(void *a, void *b) {
    return !matches_c(a, b);
}

bool contains_c(void *a, void *b) {
    return (scts_find_c(a, b)!=-1);
}

bool not_contains_c(void *a, void *b) {
    return !contains_c(a, b);
}


bool scts_filter_u(char32_t ***list_ptr, int criteria, char32_t *what){
    // input checks
    if (!list_ptr || !list_ptr[0]) ERETURN(false, "list_ptr is null or points to null"); 
    switch (criteria) {
        case SCTS_EMPTY:
        case SCTS_NOT_EMPTY:
        case SCTS_CONTAINS:
        case SCTS_NOT_CONTAINS:
        case SCTS_MATCHES:
        case SCTS_NOT_MATCHES:
            break;
        default:
            ERETURN(false, "`criteria' is not a valid criteria"); 
    }
    if (!what && ((criteria!=SCTS_EMPTY) && (criteria!=SCTS_NOT_EMPTY))) ERETURN(false, "what is null"); 
    
    // logic
    
    switch (criteria) {
    
        case SCTS_EMPTY:
            what = (char32_t[]) {0};
            if (!(scta_filter((*list_ptr), what, matches_u, scts_free_pointer)))
                ERETURN(false, "^");
            break;
        
        case SCTS_NOT_EMPTY:
            what = (char32_t[]) {0};
            if (!(scta_filter((*list_ptr), what, not_matches_u, scts_free_pointer)))
                ERETURN(false, "^");
            break;
        
        case SCTS_CONTAINS:
            if (!(scta_filter((*list_ptr), what, contains_u, scts_free_pointer)))
                ERETURN(false, "^");
            break;
        case SCTS_NOT_CONTAINS:
            if (!(scta_filter((*list_ptr), what, not_contains_u, scts_free_pointer)))
                ERETURN(false, "^");
            break;
            
        case SCTS_MATCHES:
            if (!(scta_filter((*list_ptr), what, matches_u, scts_free_pointer)))
                ERETURN(false, "^");
            break;
            
        case SCTS_NOT_MATCHES:
            if (!(scta_filter((*list_ptr), what, not_matches_u, scts_free_pointer)))
                ERETURN(false, "^");
            break;
            
        default:
            ERETURN(false, "`criteria' is not a valid criteria"); 
    }
    
    return true;
}



bool scts_filter_c(char ***list_ptr, int criteria, char *what){
    // input checks
    if (!list_ptr || !list_ptr[0]) ERETURN(false, "list_ptr is null or points to null"); 
    switch (criteria) {
        case SCTS_EMPTY:
        case SCTS_NOT_EMPTY:
        case SCTS_CONTAINS:
        case SCTS_NOT_CONTAINS:
        case SCTS_MATCHES:
        case SCTS_NOT_MATCHES:
            break;
        default:
            ERETURN(false, "`criteria' is not a valid criteria"); 
    }
    if (!what && ((criteria!=SCTS_EMPTY) && (criteria!=SCTS_NOT_EMPTY))) ERETURN(false, "what is null"); 
    
    // logic
    
    switch (criteria) {
    
        case SCTS_EMPTY:
            what = "";
            if (!(scta_filter((*list_ptr), what, matches_c, scts_free_pointer)))
                ERETURN(false, "^");
            break;
        
        case SCTS_NOT_EMPTY:
            what = "";
            if (!(scta_filter((*list_ptr), what, not_matches_c, scts_free_pointer)))
                ERETURN(false, "^");
            break;
        
        case SCTS_CONTAINS:
            if (!(scta_filter((*list_ptr), what, contains_c, scts_free_pointer)))
                ERETURN(false, "^");
            break;
        case SCTS_NOT_CONTAINS:
            if (!(scta_filter((*list_ptr), what, not_contains_c, scts_free_pointer)))
                ERETURN(false, "^");
            break;
            
        case SCTS_MATCHES:
            if (!(scta_filter((*list_ptr), what, matches_c, scts_free_pointer)))
                ERETURN(false, "^");
            break;
            
        case SCTS_NOT_MATCHES:
            if (!(scta_filter((*list_ptr), what, not_matches_c, scts_free_pointer)))
                ERETURN(false, "^");
            break;
            
        default:
            ERETURN(false, "`criteria' is not a valid criteria"); 
    }
    
    return true;
}



//TEST
#ifdef test_filter

#include <assert.h>
#include <stdio.h>

int main() {
    sct_debug = stderr;
    {
        char32_t **list = scta_new(char32_t *);
        char32_t **fl = scta_new(char32_t *);
        scta_push(list, scts_transcode(" something "));
        scta_push(list, scts_transcode("something "));
        scta_push(list, scts_transcode("something"));
        scta_push(list, scts_transcode(""));
        scta_push(list, scts_transcode(""));
        scta_push(list, scts_transcode(" a thing"));
        scta_push(list, scts_transcode(" a thing "));
        
        for (int i = 0; list[i]; ++i) scta_push(fl, list[i]);
            
        assert(scts_filter(list, SCTS_EMPTY, NULL));
        assert(scta_length(list) == 5);
        for (int i = 0; list[i]; ++i) assert(scts_matches(list[i], 
            ((char32_t *[]) {fl[0], fl[1], fl[2], fl[5], fl[6], NULL})[i] ));

        assert(scts_filter(list, SCTS_CONTAINS, ((char32_t[]) {'s', 'o', 'm', 'e', 't', 'h', 'i', 'n', 'g', 0}) ));
        assert(scta_length(list) == 2);
        for (int i = 0; list[i]; ++i) assert(scts_matches(list[i], 
            ((char32_t *[]) {fl[5], fl[6], NULL})[i] ));

        assert(scts_filter(list, SCTS_MATCHES, ((char32_t[]) {' ', 'a', ' ', 't', 'h', 'i', 'n', 'g', 0}) ));
        assert(scta_length(list) == 1);
        for (int i = 0; list[i]; ++i) assert(scts_matches(list[i], 
            ((char32_t *[]) {fl[6], NULL})[i] ));

        assert(scts_filter(list, SCTS_NOT_MATCHES, ((char32_t[]) {' ', 'a', ' ', 't', 'h', 'i', 'n', 'g', 0}) ));
        assert(scta_length(list) == 0);
        
        for (int i = 0; list[i]; ++i) scts_free(list[i]);
        scta_free(list);
        scta_free(fl);
    }    
    
    {
        char **list;
        char **fl = scta_new(char *);

        list = scts_split(" something ,something ,something,,, a thing, a thing ", ",");
        assert(list);
        for (int i = 0; list[i]; ++i) scta_push(fl, list[i]);
        
            
        assert(scts_filter(list, SCTS_EMPTY, NULL));
        assert(scta_length(list) == 5);
        for (int i = 0; list[i]; ++i) assert(scts_matches(list[i], 
            ((char *[]) {fl[0], fl[1], fl[2], fl[5], fl[6], NULL})[i] ));

        assert(scts_filter(list, SCTS_CONTAINS, "something"));
        assert(scta_length(list) == 2);
        for (int i = 0; list[i]; ++i) assert(scts_matches(list[i], 
            ((char *[]) {fl[5], fl[6], NULL})[i] ));

        assert(scts_filter(list, SCTS_MATCHES, " a thing"));
        assert(scta_length(list) == 1);
        for (int i = 0; list[i]; ++i) assert(scts_matches(list[i], 
            ((char *[]) {fl[6], NULL})[i] ));

        assert(scts_filter(list, SCTS_NOT_MATCHES, " a thing"));
        assert(scta_length(list) == 0);
        
        for (int i = 0; list[i]; ++i) scts_free(list[i]);
        scta_free(list);
        scta_free(fl);
    }
    
    return 0;
}

#endif
