#define SCT__INTERNAL__
#include "scts.h"

//HEADER
#ifndef SCTS_PART_SUBSTRING
#define SCTS_PART_SUBSTRING
char32_t* scts_substring_iu(char32_t *primary, int from, int to);
char32_t* scts_substring_mu(char32_t **primary, int from, int to);
char* scts_substring_ic(char *primary, int from, int to);
char* scts_substring_mc(char **primary, int from, int to);
#define scts_substring(primary, from, to) (_Generic( (0, primary), \
    char32_t *:         scts_substring_iu, \
    char32_t **:        scts_substring_mu, \
    const char32_t *:   scts_substring_iu, \
    const char32_t **:  scts_substring_mu, \
    char *:             scts_substring_ic, \
    char **:            scts_substring_mc, \
    const char *:       scts_substring_ic, \
    const char **:      scts_substring_mc  \
    )(primary, from, to))

#define SCTS_EOS ((int)((~((unsigned int) 0))>>1)) //TODO switch to the standard def later

/*
sctdoc:scts_substring:

NAME
    scts_substring

SYNOPSIS
    [char*][char32_t*] scts_substring(
        [char*][&char*][char32_t*][&char32_t*] primary, int from, int to)
    
    char32_t* scts_substring_iu(char32_t *primary, int from, int to)
    char32_t* scts_substring_mu(char32_t **primary, int from, int to)
    char* scts_substring_ic(char *primary, int from, int to)
    char* scts_substring_mc(char **primary, int from, int to)
    
DESCRIPTION
    This function takes a string and computes a substring. if a mutable version
    is used, `primary' will point to the calculated substring. both functions
    return the substring.
    
    Indices can be negative, indicating the location from the end of the 
    string.
    
    SCTS_EOS is a special value that when passed as `to' denotes the end of the 
    string.
    
RETURN
    If a mutable function is called, the function returns the substring pointer 
    that is stored in `primary', and NULL if an error occurs. `primary' will 
    not be affected unless the call is successfull.
    
    If an imutable function is called, the function returns a new string 
    which must be freed to avoid memory leaks, and NULL if an error occurs.

EXAMPLE
    // mutable = scts_new("سر فلا كبا بك الفرس")
    scts_substring(&mutable, 0, 3) // mutable == "سر "
    scts_substring(mutable,  0, 1) // mutable == "سر "; returns "س"
    
    // immutable = "سلام"
    scts_substring(immutable, -1, SCTS_EOS) // ; returns "م"

SEE ALSO
    scts_new
    scts_transcode
*/

#endif

//LIB
char32_t* scts_substring_iu(char32_t *primary, int from, int to){
    // input checks
    if (!primary) ERETURN(NULL, "null string supplied");
    // logic
    int length = scts_length(primary);
    
    // if negative indexing is used
    if (from < 0) from+=length;
    if (to < 0) to+=length;
    
    // bound checks
    if (from < 0 || from > length) ERETURN(NULL, "`from' paramater is out of "
                                                 "bounds; supplied string length "
                                                 "is %d, effective `from' "
                                                 "value is %d", length, from);
    if (to == SCTS_EOS) to = length;
    if (to < 0 || to < from || to > length) ERETURN(NULL,  
                                            "`to' paramater is out of bounds; "
                                            "supplied string length is %d, "
                                            "effective `from' value is %d, "
                                            "and effective `to' value is %d", 
                                            length, from, to);
    
    // get a new string that is large enough to hold the substring
    char32_t *out = scta_new(char32_t);
    if (!out) ERETURN(NULL,  "Could not allocate memory for new string");
    if (!scta_set_length(out, to-from)) {
        scts_free(out);
        ERETURN(NULL,  "Could not allocate memory for new string length %d", to-from);
    }
    
    for (register int i = from; i != to; ++i) out[i-from] = primary[i];
    return out;
}

char32_t* scts_substring_mu(char32_t **primary, int from, int to){
    char32_t *n;
    
    if (!primary || !(*primary))  ERETURN(NULL, "null string supplied");
    
    n = scts_substring_iu(*primary, from, to);
    if (!n) ERETURN(NULL, "internal call to imutable counterpart failed");
    scts_free(*primary);
    *primary = n;
    return n;
}

char* scts_substring_ic(char *primary, int from, int to){
    char32_t *n;
    
    if (!primary) ERETURN(NULL, "null string supplied");
    
    n = scts_transcode(primary);
    if (!n) ERETURN(NULL, "could not transcode byte sequence");
    if (!scts_substring_mu(&n, from, to)) {
        scts_free(n);
        ERETURN(NULL, "internal call to mutable unicode counterpart failed");
    }
    primary = scts_transcode(n);
    if (!primary) {
        scts_free(n);
        ERETURN(NULL, "internal library failure"); // should never fail to transcode happen unless something is broken
    }
    scts_free(n);
    return primary;
}

char* scts_substring_mc(char **primary, int from, int to){
    char *n;

    if (!primary || !(*primary))  ERETURN(NULL, "null string supplied");
    
    n = scts_substring_ic(*primary, from, to);
    if (!n) ERETURN(NULL, "internal call to imutable counterpart failed");
    scts_free(*primary);
    *primary = n;
    return n;
}

//TEST
#ifdef test_substring

#include <assert.h>
#include <stdio.h>
#include <string.h>

int main() {
    sct_debug = stderr;
    char *test = "٠١٢٣٤٥٦٧٨٩";
    char *c[12];
    //
    assert(!strcmp(c[0]=scts_substring(test, 1, -1), "١٢٣٤٥٦٧٨"));
    assert(!strcmp(c[1]=scts_substring(test, 0, 0), ""));
    assert(!strcmp(c[2]=scts_substring(test, 1, 1), ""));
    assert(!strcmp(c[3]=scts_substring(test, 0, SCTS_EOS), "٠١٢٣٤٥٦٧٨٩"));
    assert(!strcmp(c[4]=scts_substring(test, 0, -9), "٠"));
    assert(!strcmp(c[5]=scts_substring(test, -10, -9), "٠"));
    assert(!strcmp(c[6]=scts_substring(test, 0, -10), ""));
    assert(!strcmp(c[7]=scts_substring(test, 1, 1), ""));
    assert(!strcmp(c[8]=scts_substring(test, 9, 10), "٩"));
    assert(!strcmp(c[9]=scts_substring(test, -2, -1), "٨"));
    assert(!strcmp(c[10]=scts_substring(test, 0, 1), "٠"));
    assert(!strcmp(c[11]=scts_substring(test, -1, SCTS_EOS), "٩"));
    
    sct_debug = NULL;
    assert(!scts_substring(test, 0, 11));
    assert(!scts_substring(test, -11, SCTS_EOS));
    assert(!scts_substring(test, 5, 4));
    assert(!scts_substring((char *) NULL, 0, 0));
    sct_debug = stderr;
    
    scts_free(c[0]);
    scts_free(c[1]);
    scts_free(c[2]);
    scts_free(c[3]);
    scts_free(c[4]);
    scts_free(c[5]);
    scts_free(c[6]);
    scts_free(c[7]);
    scts_free(c[8]);
    scts_free(c[9]);
    scts_free(c[10]);
    scts_free(c[11]);
    
    // Testing mutable wrapper
    char *mutable = scts_new("سر فلا كبا بك الفرس");
    assert(!strcmp(scts_substring(&mutable, 0, 3), "سر "));
    assert(!strcmp(scts_substring(&mutable, -1, 3), " "));
    sct_debug = NULL;
    assert(!scts_substring(&mutable, 800, 900));
    sct_debug = stderr;
    assert(!strcmp(scts_substring(&mutable, 0, 1), " "));
    scts_free(mutable);
    
    return 0;
}

#endif
