cat flow.c | 
    sed 's/scts_transcode/scts_new/g' | 
    sed 's/char32_t/char/g' | 
    sed 's/flow_iu/flow_ic/g' |
    sed 's/flow_mu/flow_mc/g' |
    sed 's/flow_u/flow_c/g' |
    sed 's/FLOW/FLOWC/g' | 
    sed 's/flow/flowc/g' | 
    cat > flowc.c
    
parts.test.sh flowc
