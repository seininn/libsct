/*
sctdoc:scta:

    scts: A UTF-8-Aware String Module

    Sulaiman (seininn) Mustafa     2014-04-27
    
    This module is a part of libstc, a modular, c11-compliant, 
    toolkit library. use stcmod to determine this module's 
    dependencies, if any.
    
    -----------------------------------------------------------------------
    
    EXAMPLES
    
    API
    
    DEBUGING
    
    SCT offers a verbose mode as a debuging aid. To enable this mode, set 
    sct_debug to where want the verbose output to be written to (stderr,
    fopen()).
    
*/

#include <stddef.h>
#include <stdio.h>
#include "scta.h"
#include "sctstd.h"

#ifdef __STDC_UTF_32__ //TODO remove and enable the following
#include <uchar.h>
#else
#include <stdint.h>
typedef uint32_t char32_t; 
#endif


#define SCTS_STOP                   ((int) 0)
#define SCTS_APPEND                 ((int) 1)
#define SCTS_INSERT                 ((int) 2)
#define SCTS_JOIN                   ((int) 3)
#define SCTS_REPLACE                ((int) 4)
#define SCTS_SPLIT                  ((int) 5)
#define SCTS_SUBSTRING              ((int) 6)
#define SCTS_SELECT                 ((int) 8)
#define SCTS_FILTER                 ((int) 9)

#define SCTS_EMPTY                  ((int) 10)
#define SCTS_NOT_EMPTY              ((int) 11)
#define SCTS_CONTAINS               ((int) 12)
#define SCTS_NOT_CONTAINS           ((int) 13)
#define SCTS_MATCHES                ((int) 14)
#define SCTS_NOT_MATCHES            ((int) 15)
#define SCTS_ASSERT_LENGTH          ((int) 16)

#define SCTS_PRINT                  ((int) 17)

