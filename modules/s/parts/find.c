#define SCT__INTERNAL__
#include "scts.h"

//HEADER
#ifndef SCTS_PART_FIND
#define SCTS_PART_FIND

int scts_find_u(char32_t *restrict haystack, char32_t *restrict needle);
int scts_find_c(char *restrict haystack,  char *restrict needle);

#define scts_find(haystack, needle) (_Generic( (0, haystack), \
    char32_t *:         scts_find_u, \
    const char32_t *:   scts_find_u, \
    char *:             scts_find_c, \
    const char *:       scts_find_c \
    )(haystack, needle))

/*
sctdoc:scts_find:

NAME
    scts_find

SYNOPSIS
    int scts_find([char*][char32_t*] heystack, [char*][char32_t*] needle)
    
    int scts_find_u(char32_t *haystack, char32_t *needle)
    int scts_find_c(char *haystack, char *needle)
    
DESCRIPTION
    This function locates the substring `needle' in the string `haystack', and
    returns the index at which it starts.

RETURN
    This function returns the starting point of the substring; and -1 if the 
    string was not found, `needle' is null, or if an error occured.

EXAMPLE
    scts_find("abcdef", "e"); // 4

SEE ALSO
    scts_matches
    scts_replace
*/

#endif

//LIB
int scts_find_u(char32_t *restrict haystack, char32_t *restrict needle){
    // input checks
    if (!haystack || !needle) ERETURN(-1, "null string supplied for the %s paramater", ((!haystack)?"first":"second"));
    
    // logic
    // nieve implimintation for now... add support later or maybe different function: scts_search?
    if (!needle[0]) return -1; // definition: to be more inline with other string APIs
    for (int i = 0; haystack[i]; ++i) {
        int j = 0;
        for (; haystack[i+j] == needle[j] && needle[j] != 0; ++j);
        if (needle[j] == 0) return i;   
        else i+=j;
    }
    return -1;
}

int scts_find_c(char *restrict haystack, char *restrict needle){
    // input checks
    if (!haystack || !needle) ERETURN(-1, "null string supplied for the %s paramater", ((!haystack)?"first":"second"));
    
    // logic
    char32_t *h = scts_transcode(haystack);
    char32_t *n = scts_transcode(needle);
    
    if (!h || !n) ERETURN(-1, "failed to transcode the %s paramater", ((!haystack)?"first":"second"));

    int result = scts_find_u(h, n);
    scts_free(h);
    scts_free(n);
    
    return result;
}


//TEST
#ifdef test_find

#include <assert.h>
#include <stdio.h>

int main() {
    sct_debug = stderr;
    char32_t *haystack;
    char32_t *needle;
    int result;
    
    //fprintf(stderr, "%d", result);
    #define find_unicode_transcoded(H, N, I) \
    haystack = scts_transcode(H);\
    needle = scts_transcode(N);\
    result = scts_find(haystack, needle);\
    assert(result == I);\
    scts_free(haystack);\
    scts_free(needle);


    find_unicode_transcoded("hello", "ello", 1);
    find_unicode_transcoded("سليمان", "مان", 3);
    find_unicode_transcoded("سليمان", "مانا", -1);
    find_unicode_transcoded("سليمان", "بطة", -1);

    assert(scts_find("hello", "ello") == 1);
    assert(scts_find("سليمان", "مان") == 3);
    assert(scts_find("سليمان", "ما") == 3);
    
    assert(scts_find("hello", "") == -1);
    assert(scts_find("", "") == -1);
    return 0;
}

#endif
