#define SCT__INTERNAL__
#define _POSIX_C_SOURCE 200809L
#include "scts.h"

//HEADER
#ifndef SCTS_PART_DEPPRINT
#define SCTS_PART_DEPPRINT
int scts_depprint_u(FILE *stream, char32_t* primary);
int scts_depprint_c(FILE *stream, char* primary);
#define scts_depprint(stream, primary) (_Generic( (0, primary), \
    char32_t *:         scts_depprint_u, \
    char *:             scts_depprint_c, \
    const char32_t *:   scts_depprint_u, \
    const char *:       scts_depprint_c \
    )(stream, primary))

//TODO full format support with ... vararg
//TODO write tests for print
//TODO unifiy return values and meanings

/*
sctdoc:scts_depprint:

NAME
    scts_depprint

SYNOPSIS
    int generic_dummy([char*][char32_t*] primary)
    
    int generic_dummy_c(char *primary)
    int generic_dummy_u(char32_t *primary)
    
DESCRIPTION
    ...

RETURN
    ...

EXAMPLE
    ...

SEE ALSO
    ...
*/

#endif

//LIB
int scts_depprint_u(FILE *stream, char32_t* primary){
    char seq[5];
    int i = 0;
    // input checks
    if (!primary) ERETURN(-1, "cannot depprint NULL string");
    // logic
    for (i = 0; primary[i]; ++i) {
        if (!scts_sequence_codepoint(primary[i], seq)) ERETURN(-1, "Could not sequence codepoint");
        fputs(seq, stream);
    }
    return i;
}

int scts_depprint_c(FILE *stream, char* primary){
    if (!primary) ERETURN(-1, "cannot depprint NULL string");
    return fputs(primary, stream);
}

//TEST
#ifdef test_depprint

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    sct_debug = stderr;
    /*
    char buf[200] = "";
    
    FILE *stream = fmemopen(buf, 200, "r+");
    if (!stream) abort();
        
    assert((l=scts_depprint(stream, "777\n" )) != -1);
    assert(!strcmp(buf, "777\n"));
    rewind(stream);
    
    assert((l=scts_depprint(stream, ((char32_t[]) {0x666, 0x666, 0x666, 0xa, 0}) )) != -1);
    assert(!strcmp(buf, "٦٦٦\n"));
    rewind(stream);
    */
    
    return 0;
}

#endif
