#define SCT__INTERNAL__
/*

    scta: Generic Dynamic Array Module

    Sulaiman (seininn) Mustafa     2014-02-26
    
    This module is a part of libstc, a modular, c99/c1x-compliant, 
    toolkit library. use stcmod to determine this module's 
    dependencies, if any.
    
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <limits.h>
#include "scta.h"

/*

IMPORTANT HOUSE RULES
=====================
    0.  Type dependant fields (a.k.a storge fields) must NEVER appear in the 
        array header (to allow type independant querys about length in 
        "polymorphic" macros). type-dependant fields in an array tail are ok.
        
    1.  SCTA_EXTERN_ SHOULD NOT APPEAR IN THIS FILE (Notble exception is the 
        appendex area at the end of this file)

    2.  Edit the comments to reflect any changes you have made. Comments include
        the api documentation and the macro function.
        
    3.  SEE MACRO FUNCTIONS

INTERNAL MEMORY LAYOUT
======================

    +------------------+
    |       SIZE       |  <- internal pointer points here
    +------------------+
    |      LENGTH      |
    +------------------+
    |      RETURN      |  used to be used for return status, might remove later
    +------------------+
    |   ARGUMENT_INT_1 |  argument regesters for functions to avoid multiple 
    +------------------+  macro argument invokations. unfortunetly, the array
    |   ARGUMENT_INT_2 |  argument must be invoked more than once. (that, or
    +------------------+  use statement expressions wich makes this non-c code)
    |     META_PTR     |  <- axulery ptr for attaching extra data to arrays, used internally
    +------------------+  :
    |   ARGUMENT_PTR_1 |  :
    +------------------+  :
    |   ARGUMENT_PTR_2 |  :
    +------------------+  '
    |     ELEMENT_0    |  <- external pointer points here 
    +------------------+
    +---------:--------+
    +---------:--------+
    +------------------+
    |     ELEMENT_N    |
    +------------------+
    |         0        |  Auto-null terminated
    +------------------+
    |      STORAGE     |  (same size as normal array elements)
    +------------------+
    
*/

#define ___INTERNAL_INT_ARRAY_PRINTER___\
    fprintf(stderr, "length: %d, size: %zu, { ", SCTA__INTERN_LEN(ra)[0], SCTA__INTERN_SIZE(ra)[0]);\
    for (int i = 0; i < SCTA__INTERN_LEN(ra)[0]+2; ++i) fprintf(stderr, "%d ", ((int *)((char *) ra + SCTA_HEAD))[i]);\
    fprintf(stderr, "} \n");


// XXX EXTERN SHOULD NOT APPEAR IN THIS DOCUMENT

bool make_space(void **ra, int at, int count){
    // makes a space inside *ra, and moves everything (including storage) to the right
    // count is the number of elements to remove and has nothing to do with size
    // increments length field of array
    size_t size = SCTA__INTERN_SIZE(*ra)[0];
    int length = SCTA__INTERN_LEN(*ra)[0];
    void *temp;
    
    if (!size) 
        ERETURN(false, "element size can not be zero.")
    
    if (length == INT_MAX) 
        ERETURN(false, "cannot increase array size do to int limit.")
        
    if (!(temp=realloc(*ra, (SCTA_HEAD)+ size*length + size*count + size + size))) 
        ERETURN(false, "memory operation failed: new size is %zu", ((SCTA_HEAD+size)+size*length+size+size))
    *ra = temp;
    
    
    memmove( ((char *) *ra) + SCTA_HEAD + at*size + size*count, 
             ((char *) *ra) + SCTA_HEAD + at*size, 
             (length*size) - (at*size) + size + size);
    
    
    SCTA__INTERN_LEN(*ra)[0]+=count;
    return true;
}

bool collapse_space(void **ra, int at, int count){
    // removes a space from *ra, and moves everything (including storage) to the left
    // count is the number of elements to remove and has nothing to do with size
    // decrements length field of array
    size_t size = SCTA__INTERN_SIZE(*ra)[0];
    int length = SCTA__INTERN_LEN(*ra)[0];
    void *temp;
    
    if (!size) 
        ERETURN(false, "element size can not be zero.")
    
    if (!length) 
        ERETURN(false, "cannot shorten a zero length array")
        
    
    memmove( ((char *) *ra) + SCTA_HEAD + at*size, 
             ((char *) *ra) + SCTA_HEAD + at*size + size*count, 
             (length*size) - (at*size + size*count) + size + size);
    
    if (!(temp=realloc(*ra, SCTA_HEAD + size*length - size*count + size + size))) 
        ERETURN(false, "memory operation failed: new size is %zu", ((SCTA_HEAD+size)+size*length-size))
    *ra = temp;
    
    SCTA__INTERN_LEN(*ra)[0] -= count;
    return true;
}


void* make_raw_array(size_t size){
    void *ra;
    
    if (!size) 
        ERETURN(NULL, "requested array member size is 0. minimum is 1.")
    
    if (!(ra = malloc(SCTA_HEAD + size + size)))
        ERETURN(NULL, "could not allocate enough memory: requested %zu", SCTA_HEAD + size)
        
    memset(ra, 0, SCTA_HEAD + size + size);
    
    SCTA__INTERN_SIZE(ra)[0] = size;
    SCTA__INTERN_LEN(ra)[0] = 0;
    
    return ra;
}







// MACRO FUNCTIONS - Dont forget:
// -- to differeantiat between internal functions and macro ones and:
// -- convert arrays to raw arrays and vice versa when nessary by sub/add SCTA__OFFSET_HEADER
// -- functions here must not use EXTERN functions for concistancy.
// -- arguments are stored in the array itself. the specification of this is in the function's comment
// -- arrays passed by macros are in there normal form and not raw form

void* scta_macro_new_(size_t size_of_elements){/**/
    // ra := raw array
    void *ra = make_raw_array(size_of_elements);
    if (ra) return ((char *)ra)+SCTA_HEAD;
    else return NULL;
}

void scta_free(void *c){ 
    if (!c) return;
    free((char *)c-SCTA_HEAD); 
}

bool scta_macro_set_length_(char **z, int newl){
    /*
        NAMING
            newl := new length
            oldl := old length
            ra := raw array (points at the begining of the memory layout 
                  defined above)
        MEMORY FIELDS USED:
            none.        
    */
    
    void *ra = *z-SCTA_HEAD;
    int oldl = SCTA__INTERN_LEN(ra)[0];
    int size = SCTA__INTERN_SIZE(ra)[0];
    
    if (newl > oldl) {
        if (!make_space(&ra, oldl, newl-oldl)) return false;
        memset((char *)ra+SCTA_HEAD+(oldl*SCTA__INTERN_SIZE(ra)[0]), 0, (newl-oldl)*size + size + size);
    }
    if (newl < oldl) {
        if (!collapse_space(&ra, newl, oldl-newl)) return false;
    }
    
    *z = (char *) ra + SCTA_HEAD;
    return true;    
}

bool scta_macro_insert_(char **z){
    /*
        inserts an element in an array, and increments the length field of the 
        array. Python-style indexing is posible.
        
        NAMING
            at := index at which the new element will be inserted.
            oldl := new length
            ra := raw array (points at the begining of the memory layout 
                  defined above)
        MEMORY FIELDS USED:
            INT1 : at - the indix where the new element should be inserted 
            STORAGE: the new element to be inderted
    */
    
    

    void *ra = *z-SCTA_HEAD;
    
    
    size_t size = SCTA__INTERN_SIZE(ra)[0];
    int oldl = SCTA__INTERN_LEN(ra)[0];

    int at = SCTA__INTERN_INT1(ra)[0];
    
    // if negative indexing: normalize
    if (at < 0) at = oldl + at;
    // if at is larger than length of array, set to insert to length
    if (at > oldl) at = oldl;
    
    
    if ( at < 0) 
        ERETURN(false, "inconsistant paramiters:\n" 
                                 "\tlength: %d\n" 
                                 "\top index at: %d\n", oldl, at)
    
    if (!make_space(&ra, at, 1)) return false;
    
    memmove(    ((char *) ra) + SCTA_HEAD + size*at, 
                SCTA__INTERN_STORAGE(ra), size);
    
    
    /*REMMOVE LATER */ memset((char *) ra + SCTA_HEAD + size * SCTA__INTERN_LEN(ra)[0], 0, size);
    *z = (char *) ra + SCTA_HEAD;
    return true;
}

bool scta_macro_insert_array_(char **z){
    /*
        inserts a set of element in an array, and increments the length field 
        of the array accordingly. Python-style indexing is posible.
        
        NAMING
            at := index at which the operation is preformed.
            oldl := new length
            ra := raw array (points at the begining of the memory layout 
                  defined above)
        MEMORY FIELDS USED:
            INT1 : at - the indix where the new element should be inserted 
            PTR1: pointer to the memory containing the elements that shoud be 
                  copied.
            INT2 : The number of elements that should be inserted.
            
    */

    void *ra = *z-SCTA_HEAD;    
    
    size_t size = SCTA__INTERN_SIZE(ra)[0];
    int oldl = SCTA__INTERN_LEN(ra)[0];

    int target_length = SCTA__INTERN_INT2(ra)[0];
    void *target_addr = SCTA__INTERN_PTR1(ra)[0];

    int at = SCTA__INTERN_INT1(ra)[0];
    
    
    // normalize index if negative
    if (at < 0) at = oldl + at;
    // set after last element if index is larger than array length
    if (at > oldl) at = oldl;
    
    if ( at < 0 || target_length < 0) 
        ERETURN(false, "inconsistant paramiters:\n" 
                                  "\tlength: %d\n" 
                                 "\top index at: %d\n", oldl, at)
    
    if (!make_space(&ra, at, target_length)) return false;
    
    memmove(    ((char *) ra) + SCTA_HEAD + size*at, 
                target_addr , size*target_length);
    
    
    /*REMMOVE LATER */ memset((char *) ra + SCTA_HEAD + size * SCTA__INTERN_LEN(ra)[0], 0, size);
    *z = (char *) ra + SCTA_HEAD;
    return true;
}


bool scta_macro_delete_subset_(char **z){
    /*
        deletes a subset that starts at an index and spans a specified number 
        of elements. the length field is modified to reflect the new length.
        Python-style indexing is posible.
        
        NAMING
            oldl := new length
            ra := raw array (points at the begining of the memory layout 
                  defined above)
        MEMORY FIELDS USED:
            INT1 : (input) index at which the deletion should start 
            INT2 : (input) the number of elements that should be deleted
            STORAGE: (output) fist element in deleted subset
            
    */
    
    void *ra = *z-SCTA_HEAD;    
    
    size_t size = SCTA__INTERN_SIZE(ra)[0];
    int oldl = SCTA__INTERN_LEN(ra)[0];

    int target_length = SCTA__INTERN_INT2(ra)[0];

    int at = SCTA__INTERN_INT1(ra)[0];
    // normalze if index is negative
    if (at < 0) at = oldl + at;
    // set index BEFORE last element if larger than length (so that the last 
    // element is deleted
    if (at > oldl) at = oldl-1;
    
    if ( oldl < (at+target_length) || at < 0) 
        ERETURN(false, "inconsistant paramiters:\n" 
                                 "\tlength: %d\n" 
                                 "\tdeletion starts at: %d\n" 
                                 "\tand spans %d element(s)\n", oldl, at, target_length)

    
    memmove( SCTA__INTERN_STORAGE(ra), 
            ((char *) ra) + SCTA_HEAD + size*at, size);
    
    if (!collapse_space(&ra, at, target_length)) return false;
    
    /*REMMOVE LATER */ memset((char *) ra + SCTA_HEAD + size * SCTA__INTERN_LEN(ra)[0], 0, size);
    *z = (char *) ra + SCTA_HEAD;
    return true;
}



// APPENDIX A: EXTERNAL CODE

// TODO
// make external code trully external (use the api and not internal code)
// consider partifing this module
// scta_collapse() : colapses sequential elements that are equal
// scta_filter() : removes elements that mach e

void* scta_macro_subset_(size_t size, int at, int length, void *p){
    void *n = scta_macro_new_(size);
    int arrlen = scta_length(p);
    
    if (!n || length < 0 || length > arrlen) return n;
    if (at < 0) at = arrlen + at; // python-sque negative indexing
    
    if (at >= arrlen || at < 0) return n;
    if(!scta_insert_array(n, 0, (char *) p + at*SCTA__EXTERN_SIZE(p)[0], length)) return NULL;
    else return n;
}

#define PTR(X) (((void **)(X))[0])

int scta_macro_find_(void *p, bool (*comp)(void *a, void *b)){
    char *a = p;
    int size = SCTA__EXTERN_SIZE(p)[0];
    int len = SCTA__EXTERN_LEN(p)[0];
    void *what = SCTA__EXTERN_STORAGE(p);
    
    int i = 0;
    if (comp) {
        for(; i < len; ++i) if (comp( PTR(a+(i*size)), PTR(what))) break;
    }
    else {
        for(; i < len; ++i) if (!memcmp(a+(i*size), what, size)) break;
    }
    
    if (i < len) return i;
    else return -1;
}

bool scta_macro_filter_(void **z, bool (*comp)(void *a, void *b), void (*freef)(void *a)){
    char *a = *z;
    int size = SCTA__EXTERN_SIZE(a)[0];
    int len = SCTA__EXTERN_LEN(a)[0];
    
    int i = 0;
    if (comp) {
        void *what = PTR(SCTA__EXTERN_STORAGE(a));
        for(; i < len; ++i) {
            if (comp(PTR(a+(i*size)), what)) {
                if (freef) freef(PTR(a+(i*size)));
                if (!scta_delete(a, i)) {
                    *z = a;
                    return false;
                }
                --len;
                --i;
            }
        }
    }
    else {
        for(; i < len; ++i) if (!memcmp(a+(i*size), SCTA__EXTERN_STORAGE(a), size)) {
            if (!scta_delete(a, i)) {
                *z = a;
                return false;
            }
            --len;
            --i;
        }
    }
    *z = a;   
    return true;
}
