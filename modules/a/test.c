/*
    TODO 
        - test find


*/


#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <assert.h>

#include "scta.h"

bool string_match(void *a, void *b){
    return !strcmp(a, b);
}

int main (){
    
    sct_debug=stderr;
    {// push tests
        {// insert size size(int) 
            int *a = scta_new(int);
            
            for (int i = 1; i < 11; ++i) {
                assert(scta_length(a) == i-1);
                assert(scta_push(a, i*2));
            }
            assert(a[0] == 2);
            assert(a[1] == 4);
            assert(a[2] == 6);
            assert(a[3] == 8);
            assert(a[4] == 10);
            assert(a[5] == 12);
            assert(a[6] == 14);
            assert(a[7] == 16);
            assert(a[8] == 18);
            assert(a[9] == 20);
            
            scta_free(a);    
        }
        
        {// insert size 1
            char *a = scta_new(char);
            
            for (int i = 0; i < 10; ++i) {
                assert(scta_length(a) == i);
                assert(scta_push(a, 'a'+i));
            }
            for (int i = 0; i < 10; ++i) assert(a[i] == 'a'+i);
            assert(scta_push(a, 0));
            assert(!strcmp(a, "abcdefghij"));
            scta_free(a);    
        }
        
        {// insert size 101
            struct dogs {
                char strings[100];
                bool spotted;
            };
            
            struct dogs *a = scta_new(struct dogs);
            
            for (int i = 0; i < 10; ++i) {
                assert(scta_length(a) == i);
                assert(scta_push(a, ((struct dogs) {.strings="const", .spotted="false"})));
            }
            for (int i = 0; i < 10; i+=2) assert(!strcmp(a[i].strings, "const"));
            
            scta_free(a);    
        }        
    }

    {// insert midpoint
        int *a = scta_new(int);
        
        assert(scta_push(a, 1));
        assert(scta_push(a, 1));
        assert(scta_push(a, 1));
        
        assert(scta_insert(a, 2, 10));        
        assert(a[0] == 1);
        assert(a[1] == 1);
        assert(a[2] == 10);
        assert(a[3] == 1);  
        assert(scta_length(a) == 4);
        
        assert(scta_insert(a, 0, 10));
        assert(a[0] == 10);
        assert(a[1] == 1);
        assert(a[2] == 1);
        assert(a[3] == 10);
        assert(a[4] == 1);  
        assert(scta_length(a) == 5);
        
        assert(scta_insert(a, 4, 20));
        assert(a[0] == 10);
        assert(a[1] == 1);
        assert(a[2] == 1);
        assert(a[3] == 10);
        assert(a[4] == 20);  
        assert(a[5] == 1);  
        assert(scta_length(a) == 6);
        
        assert(scta_insert(a, 400, 80));
        assert(a[0] == 10);
        assert(a[1] == 1);
        assert(a[2] == 1);
        assert(a[3] == 10);
        assert(a[4] == 20);  
        assert(a[5] == 1);  
        assert(a[6] == 80);  
        assert(scta_length(a) == 7);
        

        assert(scta_insert(a, -2, 13));
        assert(a[0] == 10);
        assert(a[1] == 1);
        assert(a[2] == 1);
        assert(a[3] == 10);
        assert(a[4] == 20);  
        assert(a[5] == 13);  
        assert(a[6] == 1);  
        assert(a[7] == 80);  
        assert(scta_length(a) == 8);
        
        assert(scta_insert(a, -1, 77));
        assert(a[0] == 10);
        assert(a[1] == 1);
        assert(a[2] == 1);
        assert(a[3] == 10);
        assert(a[4] == 20);  
        assert(a[5] == 13);  
        assert(a[6] == 1);  
        assert(a[7] == 77);  
        assert(a[8] == 80);  
        assert(scta_length(a) == 9);                

        assert(scta_insert(a, 0, 11));
        assert(a[0] == 11);
        assert(a[1] == 10);
        assert(a[2] == 1);
        assert(a[3] == 1);
        assert(a[4] == 10);
        assert(a[5] == 20);  
        assert(a[6] == 13);  
        assert(a[7] == 1);  
        assert(a[8] == 77);  
        assert(a[9] == 80);  
        assert(scta_length(a) == 10);                        
        
        scta_free(a);    
    }
    {// testing insert_array
        int *a = scta_new(int);
        
        assert(scta_push(a, 1));
        assert(scta_push(a, 2));
        assert(scta_push(a, 3));
        
        assert(scta_insert_array(a, 2, ((int[]){10, 20, 30}), 3));        
        assert(a[0] == 1);
        assert(a[1] == 2);
        assert(a[2] == 10);
        assert(a[3] == 20);
        assert(a[4] == 30);
        assert(a[5] == 3);  
        assert(scta_length(a) == 6);
        
        assert(scta_insert_array(a, 0, ((int[]){17, 27, 37}), 3));        
        assert(a[0] == 17);
        assert(a[1] == 27);
        assert(a[2] == 37);
        assert(a[3] == 1);
        assert(a[4] == 2);
        assert(a[5] == 10);
        assert(a[6] == 20);
        assert(a[7] == 30);
        assert(a[8] == 3);  
        assert(scta_length(a) == 9);
        
        assert(scta_insert_array(a, -2, ((int[]){88, 8}), 2));        
        assert(a[0] == 17);
        assert(a[1] == 27);
        assert(a[2] == 37);
        assert(a[3] == 1);
        assert(a[4] == 2);
        assert(a[5] == 10);
        assert(a[6] == 20);
        assert(a[7] == 88);
        assert(a[8] == 8); 
        assert(a[9] == 30);
        assert(a[10] == 3);  
        assert(scta_length(a) == 11);
        
        assert(scta_insert_array(a, 1e6, ((int[]){1e6}), 1));        
        assert(a[0] == 17);
        assert(a[1] == 27);
        assert(a[2] == 37);
        assert(a[3] == 1);
        assert(a[4] == 2);
        assert(a[5] == 10);
        assert(a[6] == 20);
        assert(a[7] == 88);
        assert(a[8] == 8); 
        assert(a[9] == 30);
        assert(a[10] == 3);  
        assert(a[11] == 1e6);  
        assert(scta_length(a) == 12);
        
        assert(scta_insert_array(a, 1e6, ((int[]){1e6}), 0));        
        assert(a[0] == 17);
        assert(a[1] == 27);
        assert(a[2] == 37);
        assert(a[3] == 1);
        assert(a[4] == 2);
        assert(a[5] == 10);
        assert(a[6] == 20);
        assert(a[7] == 88);
        assert(a[8] == 8); 
        assert(a[9] == 30);
        assert(a[10] == 3);  
        assert(a[11] == 1e6);  
        assert(scta_length(a) == 12);
        
        scta_free(a);
        
        a = scta_new(int);
        
        assert(scta_insert_array(a, 0, ((int[]){1, 2, 3, 4, 5 ,6}), 6));        
        assert(a[0] == 1);
        assert(a[1] == 2);
        assert(a[2] == 3);
        assert(a[3] == 4);
        assert(a[4] == 5);
        assert(a[5] == 6);  
        assert(scta_length(a) == 6);
        
        scta_free(a);
        
        // insert nothing 
        a = scta_new(int);
        assert(scta_insert_array(a, 0, ((int[]){1, 2, 3, 4, 5 ,6}), 0));        
        assert(scta_length(a) == 0);
        scta_free(a);
        
        // bad indexing
        a = scta_new(int);
        sct_debug=NULL;
        assert(!scta_insert_array(a, -1000, ((int[]){1, 2, 3, 4, 5 ,6}), 1));        
        sct_debug=stderr;
        assert(scta_length(a) == 0);
        scta_free(a);
    }
    
    
    {// testing scta_delete_subset
        int *a = scta_new(int);
        assert(scta_insert_array(a, 0, ((int[]){1, 2, 3, 4, 5 ,6}), 6));                
        
        assert(scta_length(a) == 6);        
        assert(scta_delete_subset(a, 1, 3));
        assert(a[0] == 1);
        assert(a[1] == 5);
        assert(a[2] == 6);
        assert(scta_length(a) == 3);        
        assert(scta_delete_subset(a, 1, 0));
        assert(a[0] == 1);
        assert(a[1] == 5);
        assert(a[2] == 6);    
        assert(scta_length(a) == 3);        
        assert(scta_delete(a, 1));
        assert(a[0] == 1);
        assert(a[1] == 6);  
        assert(scta_length(a) == 2);        
        scta_free(a);
               
    }
        
    {// testing scta_pop
        int *a = scta_new(int);
        assert(scta_insert_array(a, 0, ((int[]){10, 20, 30, 40, 50 ,60}), 6));                
        int pv = 0;
        
        assert(scta_pop(a, pv));
        assert(scta_length(a) == 5);
        assert(pv == 60);
        
        assert(scta_pop(a, pv));
        assert(pv == 50);
        assert(scta_length(a) == 4);
        
        assert(scta_pop(a, pv));
        assert(pv == 40);
        assert(scta_length(a) == 3);
        
        assert(scta_pop(a, pv));
        assert(pv == 30);
        assert(scta_length(a) == 2);
        
        assert(scta_pop(a, pv));
        assert(pv == 20);
        assert(scta_length(a) == 1);
        
        assert(scta_pop(a, pv));
        assert(pv == 10);
        assert(scta_length(a) == 0);
 
        sct_debug=NULL;       
        assert(!scta_pop(a, pv));
        sct_debug=stderr;
        
        scta_free(a);
    }
    
    
    {// testing scta_subset
        int *a = scta_new(int), *b;
        assert(scta_insert_array(a, 0, ((int[]){1, 2, 3, 4, 5 ,6}), 6));                
        
        assert((b=scta_subset(a, 3, 2)));
        assert(b[0] == 4);
        assert(b[1] == 5);
        assert(scta_length(b) == 2);        
        scta_free(b);
        
        
        assert((b=scta_subset(a, -1, 1)));
        assert(b[0] == 6);
        assert(scta_length(b) == 1);        
        scta_free(b);
        
        scta_free(a);
    }
    
    
    {// testing scta_in
        int *a = scta_new(int);
        assert(scta_push_array(a, ((int[]){1, 2, 5, 40, 55 ,6}), 6));                
        assert(scta_in(a, 1));
        assert(scta_in(a, 2));
        assert(!scta_in(a, 3));
        assert(scta_in(a, 5));
        assert(scta_in(a, 40));
        assert(scta_in(a, 55));
        assert(scta_in(a, 6));
        assert(!scta_in(a, 1000013));
        scta_free(a);
        
        
        struct point {
            int x;
            int y;
        };
        
        struct point *b = scta_new(struct point);
        struct point c;
        struct point pts[] = {
            {1, -34}, 
            {-90, 4}, 
        };
        assert(scta_push(b, pts[0]));
        assert(scta_push(b, pts[1]));
        
        assert(scta_in(b, pts[0]));
        assert(scta_in(b, pts[1]));

        c.x = 3;
        c.y = 4;
        assert(!scta_in(b, c));

        c.x = -90;
        c.y = 4;
        assert(scta_in(b, c));

        c.x = 3;
        c.y = 4;
        assert(!scta_in(b, c));        
        
        c.x = 0;
        c.y = 0;
        assert(!scta_in(b, c));
        
        scta_free(b);
    }
    
    {// testing scta_set_length
        int *a = scta_new(int);
        assert(scta_insert_array(a, 0, ((int[]){1, 2, 3, 4, 5 ,6}), 6));                
        assert(scta_set_length(a, 3));
        assert(scta_length(a) == 3);
        
        assert(scta_set_length(a, 6));
        assert(scta_length(a) == 6);
        assert(a[0] == 1);
        assert(a[1] == 2);
        assert(a[2] == 3);
        assert(a[3] == 0);
        assert(a[4] == 0);
        assert(a[5] == 0);
        
        scta_free(a);
        
        // grind test, just in case
        uint64_t *z = scta_new(uint64_t);
        assert(scta_set_length(z, 20000));
        
        for (int i = 0; i < 20000; ++i) z[i] = i*i;
        for (int i = 0; i < 20000; ++i) assert(z[i] == (uint64_t) i*i);
        assert(scta_set_length(z, 10000));
        assert(scta_set_length(z, 20000));
        for (int i = 0; i < 10000; ++i) assert(z[i] == (uint64_t) i*i);
        for (int i = 10000; i < 20000; ++i) assert(z[i] == 0);
        
        scta_free(z);
    }
 
    {
    
        int *list = scta_new(int);
        scta_push(list, 10);
        scta_push(list, 20);
        scta_push(list, 30);
        scta_free(list);
    
    
        float **list2 = scta_new(float *);
        scta_push_array(list2, ((float[]) {1e-1, 1e-2, 1e-3} ), 3);
        scta_free(list2);

        typedef struct {
            int x;
            int y;
            float w;
        } point;
        
        point *list3 =  scta_new(point);

        scta_push_array(list3, ((point[]) { 
            {.x=1, .y=1, .w=.3}, 
            {.x=1, .y=1, .w=.3}, 
            {.x=1, .y=1, .w=.3}}), 3);
        scta_free(list3);
    }

    {// testing selective feature test swtiching 
        #ifdef push
        #error "push should NOT be defined before defining SCTA_SHORT"
        #endif
                
        #define SCTA_SHORT
        #include "scta.h"
        
        #ifndef push
        #error "push should be defined after defining SCTA_SHORT"
        #endif
        
        #undef SCTA_SHORT
        #include "scta.h"
        #ifdef push
        #error "push should NOT be defined after undefining SCTA_SHORT"
        #endif
        
        #define SCTA_SHORT
        #include "scta.h"
        
        #ifndef push
        #error "push should be defined after defining SCTA_SHORT"
        #endif
        
        #undef SCTA_SHORT
        #include "scta.h"
        #ifdef push
        #error "push should NOT be defined after undefining SCTA_SHORT"
        #endif
    }
    
    {// tesing find
        int *a = scta_new(int);
        assert(scta_push_array(a, ((int[]){1, 2, 5, 40, 55 ,6}), 6));                
        assert(scta_find(a, 1, NULL) == 0);
        assert(scta_find(a, 2, NULL) == 1);
        assert(scta_find(a, 3, NULL) == -1);
        assert(scta_find(a, 5, NULL) == 2);
        assert(scta_find(a, 6, NULL) == 5);
        assert(scta_find(a, 1000013, NULL) == -1);
        scta_free(a);
        
        char **z = scta_new(char *);
        assert(scta_push(z, "cat"));
        assert(scta_push(z, "hat"));
        assert(scta_push(z, "fat"));
        assert(scta_push(z, "rat"));
        assert(scta_push(z, "bat"));
        assert(scta_find(z, "chuawa", string_match) == -1);
        assert(scta_find(z, "fat", string_match) == 2);
        assert(scta_find(z, "bat", string_match) == 4);
        scta_free(z);
        
    }
    
    { // testring filter
        int *a = scta_new(int);
        assert(scta_push_array(a, ((int[]){1, 2, 0, 5, 0, 0, 42, 2, 55 ,6}), 10));                
        assert(scta_filter(a, 0, NULL, NULL));
        assert(!memcmp(a, ((int[]){1, 2, 5, 42, 2, 55 ,6, 0}), sizeof(int)*8)); // terminating zero is tested explicitly in spite of it not being part of the array
        assert(scta_filter(a, 2, NULL, NULL));
        assert(!memcmp(a, ((int[]){1, 5, 42, 55 ,6, 0}), sizeof(int)*6)); // terminating zero is tested explicitly in spite of it not being part of the array
        assert(scta_filter(a, 2223, NULL, NULL));
        assert(!memcmp(a, ((int[]){1, 5, 42, 55 ,6, 0}), sizeof(int)*6)); // terminating zero is tested explicitly in spite of it not being part of the array
        scta_free(a);
        
        {
            char **z = scta_new(char *);
            assert(scta_push(z, "cat"));
            assert(scta_push(z, "hat"));//
            assert(scta_push(z, "fat"));//
            assert(scta_push(z, "cat"));
            assert(scta_push(z, "rat"));//
            assert(scta_push(z, "cat"));
            assert(scta_push(z, "cat"));
            assert(scta_push(z, "cat"));
            assert(scta_push(z, "bat"));//
            assert(scta_filter(z, "cat", string_match, NULL));
            assert(scta_find(z, "cat", string_match) == -1);
            assert(scta_length(z) == 4);
            scta_free(z);
        }
        {
            uint32_t **z = scta_new(uint32_t *);
            assert(scta_push(z, ((uint32_t[]) {'c', 'a', 't', 0})));
            assert(scta_push(z, ((uint32_t[]) {'h', 'a', 't', 0})));
            assert(scta_push(z, ((uint32_t[]) {'f', 'a', 't', 0})));
            assert(scta_push(z, ((uint32_t[]) {'c', 'a', 't', 0})));
            assert(scta_push(z, ((uint32_t[]) {'r', 'a', 't', 0})));
            assert(scta_push(z, ((uint32_t[]) {'c', 'a', 't', 0})));
            assert(scta_push(z, ((uint32_t[]) {'c', 'a', 't', 0})));
            assert(scta_push(z, ((uint32_t[]) {'c', 'a', 't', 0})));
            assert(scta_push(z, ((uint32_t[]) {'b', 'a', 't', 0})));
            assert(scta_filter(z, ((uint32_t[]) {'c', 'a', 't', 0}), string_match, NULL));
            assert(scta_find(z, ((uint32_t[]) {'c', 'a', 't', 0}), string_match) == -1);
            assert(scta_length(z) == 4);
            scta_free(z);
        }
    }
    
    {
        int *z = scta_new(int);
        scta_set_length(z, 10);
        for (int i = -10; i < 20; ++i) scta_set(z, i, i);
        assert(scta_get(z, 0) == 10);
        assert(scta_get(z, 1) == 11);
        assert(scta_get(z, 2) == 12);
        assert(scta_get(z, 3) == 13);
        assert(scta_get(z, 4) == 14);
        assert(scta_get(z, 5) == 15);
        assert(scta_get(z, 6) == 16);
        assert(scta_get(z, 7) == 17);
        assert(scta_get(z, 8) == 18);
        assert(scta_get(z, 9) == 19);
        assert(scta_get(z, 10) == 10);
        scta_free(z);
    }
    printf("[ok] scta version 0x%x %s\n", sct_version, __TIMESTAMP__);
    return 0;
}
