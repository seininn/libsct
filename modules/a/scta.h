/*

    scta: Generic Dynamic Array Module

    Sulaiman (seininn) Mustafa     2014-02-26
    
    This module is a part of libstc, a modular, c99/c1x-compliant, 
    toolkit library. use stcmod to determine this module's 
    dependencies, if any.
    
*/

#ifndef SCTA_H
#define SCTA_H


#include <stdio.h>
#include <stdbool.h>
#include <limits.h>

#include "sctstd.h"



/*
‎         .                 لا تستخدم الماكروات التالية واقتصر على الماكروات
‎            ╱ ╲           ‫المعرفة في "PUBLIC API". استغرار هذه الماكروات غير مضمون.
‎          ╱  | ╲
‎        ╱    ·  ╲           Do NOT use the following macros and limit your use
‎    ∙────────∙         to the macros defined under "PUBLIC API". This macros
                                  are not garanteed to be stable.

*/

#define SCTA__OFFSET_SIZE 0
#define SCTA__OFFSET_LEN  (SCTA__OFFSET_SIZE + sizeof(size_t))
#define SCTA__OFFSET_RET  (SCTA__OFFSET_LEN + sizeof(int))
#define SCTA__OFFSET_INT1 (SCTA__OFFSET_RET + sizeof(bool))
#define SCTA__OFFSET_INT2 (SCTA__OFFSET_INT1 + sizeof(int))
#define SCTA__OFFSET_META (SCTA__OFFSET_INT2 + sizeof(int))
#define SCTA__OFFSET_PTR1 (SCTA__OFFSET_META + sizeof(void *))
#define SCTA__OFFSET_PTR2 (SCTA__OFFSET_PTR1 + sizeof(void *))
#define SCTA__OFFSET_HEADER  (SCTA__OFFSET_PTR2 + sizeof(void *))

#define SCTA_HEAD SCTA__OFFSET_HEADER



#define SCTA___SIZE(PTR, OFFSET)  ((size_t *) (((char *) PTR) + SCTA__OFFSET_SIZE   + OFFSET))
#define SCTA___LEN(PTR, OFFSET)   ((int *)    (((char *) PTR) + SCTA__OFFSET_LEN    + OFFSET))
#define SCTA___RET(PTR, OFFSET)   ((bool *)   (((char *) PTR) + SCTA__OFFSET_RET    + OFFSET))
#define SCTA___INT1(PTR, OFFSET)  ((int *)    (((char *) PTR) + SCTA__OFFSET_INT1   + OFFSET))
#define SCTA___INT2(PTR, OFFSET)  ((int *)    (((char *) PTR) + SCTA__OFFSET_INT2   + OFFSET))
#define SCTA___META(PTR, OFFSET)  ((void **)  (((char *) PTR) + SCTA__OFFSET_META   + OFFSET))
#define SCTA___PTR1(PTR, OFFSET)  ((void **)  (((char *) PTR) + SCTA__OFFSET_PTR1   + OFFSET))
#define SCTA___PTR2(PTR, OFFSET)  ((void **)  (((char *) PTR) + SCTA__OFFSET_PTR2   + OFFSET))



// returns the size, length, and status of the true bigening of the array (internal)
#define SCTA__INTERN_SIZE(X) SCTA___SIZE(X, 0)
#define SCTA__INTERN_LEN(X)  SCTA___LEN(X, 0)
#define SCTA__INTERN_RET(X)  SCTA___RET(X, 0)
#define SCTA__INTERN_INT1(X) SCTA___INT1(X, 0)
#define SCTA__INTERN_INT2(X) SCTA___INT2(X, 0)
#define SCTA__INTERN_META(X) SCTA___META(X, 0)
#define SCTA__INTERN_PTR1(X) SCTA___PTR1(X, 0)
#define SCTA__INTERN_PTR2(X) SCTA___PTR2(X, 0)




// returns the size, length, and status of apparent bigening of the array (external)
#define SCTA__EXTERN_SIZE(X) SCTA___SIZE(X, -SCTA_HEAD)
#define SCTA__EXTERN_LEN(X)  SCTA___LEN(X,  -SCTA_HEAD)
#define SCTA__EXTERN_RET(X)  SCTA___RET(X,  -SCTA_HEAD)
#define SCTA__EXTERN_INT1(X) SCTA___INT1(X, -SCTA_HEAD)
#define SCTA__EXTERN_INT2(X) SCTA___INT2(X, -SCTA_HEAD)
#define SCTA__EXTERN_META(X) SCTA___META(X, -SCTA_HEAD)
#define SCTA__EXTERN_PTR1(X) SCTA___PTR1(X, -SCTA_HEAD)
#define SCTA__EXTERN_PTR2(X) SCTA___PTR2(X, -SCTA_HEAD)

#define SCTA__INTERN_STORAGE(X) (((char *) X)+SCTA__OFFSET_HEADER+(SCTA__INTERN_SIZE(X)[0]*SCTA__INTERN_LEN(X)[0]+SCTA__INTERN_SIZE(X)[0]))
#define SCTA__EXTERN_STORAGE(X) (((char *) X)+(SCTA__EXTERN_SIZE(X)[0]*SCTA__EXTERN_LEN(X)[0]+SCTA__EXTERN_SIZE(X)[0]))


void* scta_macro_new_(size_t size);
void scta_free(void *c);
bool scta_macro_set_length_(char **z, int newlength);
bool scta_macro_insert_(char **z);
bool scta_macro_insert_array_(char **z);
bool scta_macro_delete_subset_(char **z);
void* scta_macro_subset_(size_t size, int at, int length, void *p);
int  scta_macro_find_(void *p, bool (*comp)(void *a, void *b));
bool scta_macro_filter_(void **p, bool (*comp)(void *a, void *b), void (*freef)(void *a));

/*
            ╭──────────╮
            │          │
            │   ╭──╮   │
            │   ╰──╯   │
            │          │                         ╭────╮ ╭────╮ ╭─╮  
            │   ╭──────╯ ╭─╮    ╭─╮  ╭─╮ ╭──╮    │ ╭╮ │ │ ╭╮ │ │ │
            │   │ ╭─╮╭─╮ │ ╰──╮ │ │  │ │ │ ╭╯    │ ╰╯ │ │ ╰╯ │ │ │
            │   │ │ ││ │ │ ╭╮ │ │ │  │ │ │ │     │ ╭╮ │ │ ╭──╯ │ │
            │   │ │ ╰╯ │ │ ╰╯ │ │ ╰╮ │ │ │ ╰╮    │ ││ │ │ │    │ │
            ╰───╯ ╰────╯ ╰────╯ ╰──╯ ╰─╯ ╰──╯    ╰─╯╰─╯ ╰─╯    ╰─╯
         ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ 
*/


extern FILE *scta_config_debug;
extern int scta_config_version[3];


/*
sctdoc:scta:

    scta: Generic Dynamic Array Module

    Sulaiman (seininn) Mustafa     2014-02-26
    
    This module is a part of libstc, a modular, c99/c1x-compliant, 
    toolkit library. use stcmod to determine this module's 
    dependencies, if any.
    
    -----------------------------------------------------------------------
    
    1.  scta is a pure c99/c1x library for implementing dynamic arrays 
        in C programs.

        The following is a simple example demonstrating library usage:
    
        1.1.  basic types:

                {    
                    int *list = scta_new(int);
                    scta_push(list, 10);
                    scta_push(list, 20);
                    scta_push(list, 30);
                    scta_free(list);
                }
    
        1.2   Can be used to for argv-style lists (see bellow for details 
              regarding NULL termination): 
    
                {
                    float **list = scta_new(float *);
                    scta_push_array(list, 
                        ((float[]) {1e-1, 1e-2, 1e-3} ), 3);
                    scta_free(list);
                }

        1.3.  And can be used for structures:
    
                {    
                    typedef struct {
                        int x;
                        int y;
                        float w;
                    } point;
                    
                    point *list =  scta_new(point);

                    scta_push_array(list, ((point[]) { 
                        {.x=1, .y=1, .w=.3}, 
                        {.x=12, .y=21, .w=.6}, 
                        {.x=2, .y=2, .w=.9}}), 3);
                    scta_free(list);
                }
    
    2.  This library provides the following API:
    
            scta_new                initializes a dynamic array
            scta_free               frees a dynamic array
            scta_length             determine length of a dynamic array
            scta_set_length         expands or collapses a dynamic array
            scta_insert             insert an element into dynamic array
            scta_insert_array       insert elements of an array into 
                                    dynamic array
            scta_push               appends an element to dynamic array
            scta_push_array         appends elements of an array to 
                                    dynamic array
            scta_in                 tests whether a geven value is in a
                                    dynamic array
            scta_find               returns the index of the first 
                                    occurance of a givin value
    
    3.  scta provides api flexable shorthands for programmer convienience 
        and to improve source readability. A shorthand for a given 
        routine is it's name without the 'scta_' prefix. An exception to
        this is the shorthand for 'scta_free which' is shortened to 
        'afree' instead. These shorthands must be enabled before use.
    
        Shorthands can be selectivly enabled and disabled using a 
        machanizm similar to Feature Test Macros defined by the POSIX 
        standard. To enable shorthands, define SCTA_SHORT and reinclude 
        scta.h. To disable this shorthands, undefine SCTA_SHORT and 
        reinclude scta.h again. See following examples.
        
        Shorthands can be enabled and disabled safely inside functon 
        bodies, at the bigening of a source file, at the commandline,
        or any other place where a issuing preprocessor directives is
        valid.
        
        Example:
            #define SCTA_SHORT
            #include "scta.h"
            
            int main(){
                int *a = new(int);
                push(a, 1);
                afree(a);
            }

        Example:
            #include "scta.h"
            
            int main(){
                int *a = scta_new(int);
                
                #define SCTA_SHORT
                #include "scta.h"
                
                push(a, 1);

                #undef SCTA_SHORT
                #include "scta.h"                
                
                scta_push(a, 2);
                scta_free(a);
            }        
    
    0.  A NOTE TO C PROGRAMMERS: In the worst case cenario, the routines 
        provided by this module are implemented in the most nieve way 
        posible. This is certainly the case for scta_in and scta_find. 
        And while improvements are planed, you should always excersize 
        judgment when using these routines and not blindly use it in 
        situtions where it's simply uncalled for.
        
        C doesn't get its speed from the mere virtue of it beeing close to 
        hardware. It gets its speed from the idioms and design patterns 
        that C primitivness necessates.
        
        This module undos this. By abstracting the code needed to manage 
        dynamic arrays, This module makes it easy to abuse it by using 
        it in situations where it isn't required, or even in situations 
        were using this module would be wrongful.
        
        Take splitting strings and comparing the segments with a 
        predefinced value; in a high level language or libraries such
        as this module, a programmer would simply split a string and 
        loop through the results. On the other hand C programmer would 
        search for the delemiting character, then compare strings using 
        strncmp. The diference is that the C program is many times 
        faster without needing to allocate extra memory.
        
        In short, I recomend using this module ONLY when you find your-
        self implementing its api or if you start fiddling with realloc
        and multiples of sizeof().
    
*/


#define scta_new(type)                              (scta_macro_new_(sizeof(type)))
/*
sctdoc:scta_new:

NAME
    scta_new

SYNOPSIS
    bool scta_new(type)
    
DESCRIPTION
    Takes any type as its input, and returns a pointer to a zero length array
    with elements of the same type.

NOTES
    This function is implemented as a macro.

RETURN
    a pointer of type `type *' on success, and null otherwise.

EXAMPLE
    int *list = scta_new(int);
    char *list = scta_new(char);
    struct foo *list = scta_new(struct foo);
    struct foo **list = scta_new(struct foo *);

*/


/*
sctdoc:scta_free:
NAME
    scta_free
    
SYNOPSIS
    void scta_free(type* array)

DESCRIPTION    
    Takes a dynamic array and frees it. If `array' is NULL, the function 
    does nothing action.

RETURN
    None
*/


#define scta_length(A)                              (SCTA__EXTERN_LEN(A)[0])/*
sctdoc:scta_length:
NAME
    scta_length
    
SYNOPSIS
    int scta_length(type* array)

DESCRIPTION    
    Returns the length of the array 

RETURN
    The length of `array' as an int. This function does not fail.
*/

#define scta_set_length(A, newl)                    (scta_macro_set_length_((char **) &(A), newl))/*
sctdoc:scta_set_length:
NAME
    scta_set_length

SYNOPSIS
    bool scta_set_length(type* array, int newl)

DESCRIPTION    
    Expands or collapses an array. If newl is larger than the length of the 
    array, the array is expanded and all new elements are set to zero. If 
    newl is smaller than the length of the provided array, the array is 
    collapsed.
    
RETURN
    A boolean value indicating success or failure; stdbool definitions are used.
    

*/

#define scta_insert(A, where, what)                 ((SCTA__EXTERN_INT1(A)[0]=where),\
                                                     ((A)[scta_length(A)+1]=what),\
                                                     scta_macro_insert_((char **) &(A)))
/*
sctdoc:scta_insert:
NAME
    scta_insert

SYNOPSIS
    bool scta_insert(type* array, int index, type what)
                                                     
DESCRIPTION    
    This macro inserts value `what' in dynamic array `A' at `index'.

NOTES
    1.  `index' can be a negative value which denotes the index starting from 
        the end of the array.
    2.  if `index' is larger than the length of the array, it is taken to 
        indicate that `what' should be appended to the array.
    3.  `array' is evaluated more than once in this macro.
    
RETURN
    A boolean value indicating success or failure; stdbool definitions are used.

EXAMPLE
    int *a; // [1, 2, 3]
    if (!scta_insert(a, 1, 300)) {
        puts("this macro failed");
    }
    a; // [1, 300, 2, 3]
*/
                     
                                                     
#define scta_insert_array(A, where, target, length) ((SCTA__EXTERN_INT1(A)[0]=(where)),\
                                                     (SCTA__EXTERN_INT2(A)[0]=(length)),\
                                                     (SCTA__EXTERN_PTR1(A)[0]=(target)),\
                                                     scta_macro_insert_array_((char **) &(A)))
/*
sctdoc:scta_insert_array:
NAME
    scta_insert_array

SYNOPSIS
    bool scta_insert_array(type* array, int index, type* what, int n)
                                                     
DESCRIPTION    
    This macro inserts `n' elements starting at `what' in dynamic array 
    `A' at position `index'.

NOTES
    1.  `index' can be a negative value which denotes the index starting from 
        the end of the array.
    2.  if `index' is larger than the length of the array, it is taken to 
        indicate that `what' should be appended to the array.
    3.  `array' is evaluated more than once in this macro.
    
RETURN
    A boolean value indicating success or failure; stdbool definitions are used.

EXAMPLE
    int *a; // [1, 2, 3]
    int *b; // [9, 8, 7]
    if (!scta_insert_array(a, 1, b, 3)) {
        puts("this macro failed");
    }
    a; // [1, 9, 8, 7, 2, 3]
*/
                                                     
                                                                                             
#define scta_push(A, what)                          scta_insert((A), INT_MAX, (what))
/*
sctdoc:scta_push:
NAME
    scta_push

SYNOPSIS
    bool scta_push(type* array, type what)
                                                     
DESCRIPTION    
    This macro appends `what' to dynamic array `A'.

NOTES
    `array' is evaluated more than once in this macro.
    
RETURN
    A boolean value indicating success or failure; stdbool definitions are used.

EXAMPLE
    int *a; // [1, 2, 3]
    if (!scta_push(a, 4)) {
        puts("this macro failed");
    }
    a; // [1, 2, 3, 4]
*/


#define scta_push_array(A, target, length)          (scta_insert_array((A), INT_MAX, (target), (length)))
/*
sctdoc:scta_push_array:
NAME
    scta_push_array

SYNOPSIS
    bool scta_push_array(type* array, type* what, int n)
                                                     
DESCRIPTION    
    This macro appends `n' elements starting at `what' to dynamic array `A'.

NOTES
    `array' is evaluated more than once in this macro.
    
    
RETURN
    A boolean value indicating success or failure; stdbool definitions are used.

EXAMPLE
    int *a; // [1, 2, 3]
    int *b; // [4, 5, 6]

    if (!scta_push_array(a, b, 3)) {
        puts("this macro failed");
    }
    a; // [1, 2, 3, 4, 5, 6]
*/


#define scta_delete_subset(A, where, length)        ((SCTA__EXTERN_INT1(A)[0]=(where)),\
                                                     (SCTA__EXTERN_INT2(A)[0]=(length)),\
                                                     scta_macro_delete_subset_((char **) &(A)))
/*
sctdoc:scta_delete_subset:
NAME
    scta_delete_subset

SYNOPSIS
    bool scta_delete_subset(type* array, int index, int n)
                                                     
DESCRIPTION    
    This macro remove `n' elements starting at `index' from `array'.

NOTES
    `array' is evaluated more than once in this macro.
    
RETURN
    A boolean value indicating success or failure; stdbool definitions are used.

*/


#define scta_delete(A, where)                       (scta_delete_subset((A), (where), 1))
/*
sctdoc:scta_delete:
NAME
    scta_delete

SYNOPSIS
    bool scta_delete(type* array, int index)
                                                     
DESCRIPTION    
    This macro removes an element at `index' from `array'.

NOTES
    1.  `array' is evaluated more than once in this macro.
    
RETURN
    A boolean value indicating success or failure; stdbool definitions are used.

*/

#define scta_pop(A, var)                            (scta_delete_subset((A), INT_MAX, 1) ?\
                                                     ((var)=((A)[scta_length(A)+1]),true) :\
                                                     false)
/*
sctdoc:scta_pop:
NAME
    scta_pop

SYNOPSIS
    bool scta_pop(type* array, type variable)
                                                     
DESCRIPTION    
    This macro removes the last element of  `array' and assigns it as a value 
    to variable.

NOTES
    `array' is evaluated more than once in this macro.
    
RETURN
    A boolean value indicating success or failure; stdbool definitions are used.

*/

#define scta_subset(array, at, length)              (scta_macro_subset_(sizeof((array[0])), (at), (length), (array)))
/*
sctdoc:scta_subset:
NAME
    scta_subset

SYNOPSIS
    type* scta_subset(type* array, int at, int length)
                                                     
DESCRIPTION    
    This macro returns a new array containing `length' elements of `array' 
    starting at `at'. 

RETURN
    A pointer to a new scta array, or NULL if memory could not be allocated. 
    Errornus values for `at' and `length' will yeld an empty array.

*/
                                                     
#define scta_find(array, element, function)         (((array)[scta_length(array)+1]=element),\
                                                     scta_macro_find_((array), (function)))
/*
sctdoc:scta_find:
NAME
    scta_find

SYNOPSIS
    bool scta_find(type* array, type target, bool test(void *a, void *b))
                                                     
DESCRIPTION    
    This macro returns the index of an element in `array' if `test', with both 
    the element and `target' passed as arguemnts, returns true. 
    
    if test is NULL, the index of the first element that matches with `target' 
    bitwisely is returned.
    
RETURN
    the index of the matching element is returned, and -1 otherwise.

*/

#define scta_in(array, element)                     ((scta_find((array), (element), NULL))!=-1)
/*
sctdoc:scta_in:
NAME
    scta_in

SYNOPSIS
    bool scta_in(type* array, type target)
                                                     
DESCRIPTION    
    This macro tests whether `array' has an element that matches `target'. 

NOTES
    This function is nieve and will only preform byte-level comperasons
    (i.e. compares string pointers rather than the strings themselves), 
    you can ether use sctac_in (from module ac), or use the more 
    generalized search function scta_find.
    
RETURN
    true if target was found, and false otherwise.

*/

#define scta_filter(array, element, test, free)      (((array)[scta_length(array)+1]=element),\
                                                        scta_macro_filter_((void **) (&(array)), (test), (free)))
/*
sctdoc:scta_filter:
NAME
    scta_filter

SYNOPSIS
    bool scta_filter(type* array, type b, 
        bool (*test) (void *a, void *b), void (*free) (void *))
                                                     
DESCRIPTION    
    This macro filters out any element, A, in `array' if `test' 
    returns true when supplied A and `b'.
    
    if `test' is NULL, elements that match target exactly (regardless 
    of type) are removed.
    
    if 'free' and `test' are both not NULL, free is called on the 
    element before its removal.
    
RETURN
    true on success and false on error.

*/

#endif

#define scta_set(array, index, object) ((array)[((unsigned int)index)%(scta_length(array))] = (object))
/*
sctdoc:scta_set:
NAME
    scta_set

SYNOPSIS
    type scta_set(type *array, int index, type object)
                                                     
DESCRIPTION    
    This macro provides a very efficiant and safe method to set array values. 
    No function calls and branchs occur in this macro, and it is functionally
    equivilant to C's array access constructs, "[]", when used correctly.
    
RETURN
    returns the object unmodified

*/

#define scta_get(array, index) ((array)[((unsigned int)index)%(scta_length(array))])
/*
sctdoc:scta_get:
NAME
    scta_get

SYNOPSIS
    type scta_get(type *array, int index)
                                                     
DESCRIPTION    
    This macro provides a very efficiant and safe method to get array values. 
    No function calls and branchs occur in this macro, and it is functionally
    equivilant to C's array access constructs, "[]", when used correctly.
    
RETURN
    returns the an element of the array

*/

// defines shorthands
#if defined(SCTA_SHORT) && !defined(SCTA_SHORT_DEFINED)
#define SCTA_SHORT_DEFINED

#define new             scta_new
#define afree           scta_free
#define length          scta_length
#define set_length      scta_set_length
#define insert          scta_insert                                                     
#define insert_array    scta_insert_array
#define push            scta_push
#define push_array      scta_push_array
#define delete          scta_delete
#define delete_subset   scta_delete_subset
#define pop             scta_pop
#define subset          scta_subset
#define in              scta_in

#endif

// undefines shorthands
#if !defined(SCTA_SHORT) && defined(SCTA_SHORT_DEFINED)
#undef SCTA_SHORT_DEFINED

#undef new
#undef afree
#undef length       
#undef set_length   
#undef insert       
#undef insert_array 
#undef push         
#undef push_array   
#undef delete       
#undef delete_subset
#undef pop   
#undef subset
#undef in

#endif
