#ifndef SCTIO_H
#define SCTIO_H

// STATIC SECTION
/*
sctdoc:sctio:

    sctio: input/output facilities

    Sulaiman (seininn) Mustafa     2014-06-03
    
    This module is a part of libstc, a modular, c11-compliant, 
    toolkit library. use stcmod to determine this module's 
    dependencies, if any.
    
    -----------------------------------------------------------------------
    
    
    DEBUGING
    
    SCT offers a verbose mode as a debuging aid. To enable this mode, set 
    sct_debug_fd to where want the verbose output to be written to (stderr,
    fopen()).
    
*/

#include <stddef.h>
#include <stdio.h>
#include "scta.h"
#include "scts.h"




extern FILE* debug_fd;

// INTERNAL USE ONLY
#define EPRINT(...) {\
    if (debug_fd) {\
        fprintf(debug_fd, "%s:%s():%d: ", __FILE__, __func__, __LINE__);\
        fprintf(debug_fd, __VA_ARGS__);\
        fprintf(debug_fd, "\n");\
    }\
}

#define ERETURN(RET, ...) {\
    if (debug_fd) {\
        fprintf(debug_fd, "%s:%s():%d: ", __FILE__, __func__, __LINE__);\
        fprintf(debug_fd, __VA_ARGS__);\
        fprintf(debug_fd, "\n");\
    }\
    return (RET); \
}



// PARTIALS SECTION: AUTOMATICLY GENERATED
//--print
#ifndef SCTIO_PART_PRINT
#define SCTIO_PART_PRINT
char32_t* sctio_print_u(FILE *stream, char32_t* primary, ...);
char* sctio_print_c(FILE *stream, char* primary, ...);
#define sctio_print(primary, ...) (_Generic( primary, \
    FILE *:             sctio_print_f, \
    void *:             sctio_print_f, \
    default:            sctio_print_o, \
    )(primary, __VA_ARGS__))
    
#define sctio_print_o(primary, ...) (sctio_print_f(stdout, primary, __VA_ARGS__))

#define sctio_print_f(primary, secondary, ...) (_Generic( (0, secondary), \
    const char32_t *:   sctio_print_u, \
    char32_t *:         sctio_print_u, \
    const char *:       sctio_print_c, \
    char *:             sctio_print_c, \
    )(primary, secondary, __VA_ARGS__))

    
/*
sctdoc:sctio_print:

NAME
    sctio_print

SYNOPSIS
    int generic_dummy([char*][char32_t*] primary)
    
    int generic_dummy_c(char *primary)
    int generic_dummy_u(char32_t *primary)
    
DESCRIPTION
    ...

RETURN
    ...

EXAMPLE
    ...

SEE ALSO
    ...
*/

#endif


    
#endif
