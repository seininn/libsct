#define SCT__INTERNAL__
#include "stdio.h"
#include "scts.h"

//HEADER
#ifndef SCTIO_PART_PRINT
#define SCTIO_PART_PRINT
char32_t* sctio_fprint(FILE *stream, char32_t* primary, ...);
char32_t* sctio_print(char32_t* primary, ...);
char* sctio_fprintc(FILE *stream, char* primary, ...);
char* sctio_printc(char* primary, ...);
/*
sctdoc:sctio_print:

NAME
    sctio_print

SYNOPSIS
    int generic_dummy([char*][char32_t*] primary)
    
    int generic_dummy_c(char *primary)
    int generic_dummy_u(char32_t *primary)
    
DESCRIPTION
    ...

RETURN
    ...

EXAMPLE
    ...

SEE ALSO
    ...
*/

#endif

//LIB

char32_t* sctio_fprint(FILE *stream, char32_t* primary, ...){
    return sctio_fprint(FILE *stream, char32_t* primary, ...);
}
char32_t* sctio_fprint(FILE *stream, char32_t* primary, ...){
    char seq[5];
    int i = 0;
    // input checks
    if (!primary) ERETURN(NULL, "cannot print NULL string");
    // logic
    for (i = 0; primary[i]; ++i) {
        if (!scts_sequence_codepoint(primary[i], seq)) ERETURN(NULL, "Could not sequence codepoint");
        fputs(seq, stream);
    }
    return primary;
}

char* sctio_fprintc(FILE *stream, char* primary, ...){
    if (!primary) ERETURN(NULL, "cannot print NULL string");
    fputs(primary, stream);
    return primary;
}


//TEST
#ifdef test_print

#include <assert.h>
#include <stdio.h>

int main() {
    debug_fd = stderr;
    sctio_print("hello world");
    return 0;
}

#endif
