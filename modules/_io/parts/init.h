/*
sctdoc:sctio:

    sctio: input/output facilities

    Sulaiman (seininn) Mustafa     2014-06-03
    
    This module is a part of libstc, a modular, c11-compliant, 
    toolkit library. use stcmod to determine this module's 
    dependencies, if any.
    
    -----------------------------------------------------------------------
    
    
    DEBUGING
    
    SCT offers a verbose mode as a debuging aid. To enable this mode, set 
    sct_debug_fd to where want the verbose output to be written to (stderr,
    fopen()).
    
*/

#include <stddef.h>
#include <stdio.h>
#include "scta.h"
#include "scts.h"




extern FILE* debug_fd;

// INTERNAL USE ONLY
#define EPRINT(...) {\
    if (debug_fd) {\
        fprintf(debug_fd, "%s:%s():%d: ", __FILE__, __func__, __LINE__);\
        fprintf(debug_fd, __VA_ARGS__);\
        fprintf(debug_fd, "\n");\
    }\
}

#define ERETURN(RET, ...) {\
    if (debug_fd) {\
        fprintf(debug_fd, "%s:%s():%d: ", __FILE__, __func__, __LINE__);\
        fprintf(debug_fd, __VA_ARGS__);\
        fprintf(debug_fd, "\n");\
    }\
    return (RET); \
}


