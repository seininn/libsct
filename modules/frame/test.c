#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "sctframe.h"


void freecustom(void *ptr){
    free( (char *) ptr-4);
}

int main (int argc, char **argv){
    sct_debug = stderr;
    
    SCTFRAME_SCOPE_BEGIN
    
    
    assert(C(malloc(100)));
    assert(C(malloc(100)));
    
    SCTFRAME_SCOPE_BEGIN
    SCTFRAME_SCOPE_END
    
    SCTFRAME_SCOPE_BEGIN
        void *p;
        assert(C(malloc(100)));
        assert(p=C(malloc(100)));
        assert(SCTFRAME_DELETE(p));
        sct_debug = NULL;
        assert(!SCTFRAME_DELETE(p));
        assert(!C(NULL));
        sct_debug = stderr;
    SCTFRAME_SCOPE_END
    
    assert(C(malloc(100)));
    assert(C(malloc(100)));
    assert(C(malloc(100)));
    
    SCTFRAME_SET_FUNCTION(freecustom);
    
    assert(C((char *) malloc(100)+4));
    assert(C((char *) malloc(100)+4));
    
    SCTFRAME_SET_FUNCTION(NULL);
    assert(C(malloc(100)));
        
    SCTFRAME_SCOPE_END
    
    
    
    printf("[ok] %10s 0x%x  %s\n", "sctframe", sct_version, __TIMESTAMP__);
    return 0;
}
