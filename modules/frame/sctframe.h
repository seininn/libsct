/*
sctdoc:sctframe:

    scta: A convenience module for scope-based heap memory managment

    Sulaiman (seininn) Mustafa     2014-02-26
    
    This module is a part of libstc, a modular, c1x-compliant, 
    toolkit library. use stcmod to determine this module's 
    dependencies, if any.
    
    -----------------------------------------------------------------------
    
    Example:
    
        SCTFRAME_SCOPE_BEGIN
        C(malloc(100));
        SCTFRAME_SCOPE_END
    
    Frees all memory allocated in inbetween `SCTFRAME_SCOPE_BEGIN' and 
    `SCTFRAME_SCOPE_END'.
    
    Rules:
        1. `SCTFRAME_SCOPE_BEGIN' and `SCTFRAME_SCOPE_END' are equevilant to
            an opening brace `{' and a closing brace `}', respectivly. This 
            emplies that it has it's own variable scope, and that the scope 
            of any variable declared within the two macros is limited therein.
        
        2.  sctframes can be nested.
        
        3.  All pointers that are to be freed must be passed to the `C' macro.
        
        4.  By default, sctframe uses clib's `free' function to free the 
            pointers passed to it. However, this can be changed by using the
            `SCTFRAME_SET_FUNCTION' macro to set the free function. Any 
            subsequent pointers passed to `C' will be freed using this 
            function. To reset the free function, pass NULL to 
            `SCTFRAME_SET_FUNCTION'.
        
        5.  As usual, set sct_debug to stderr (or any other `FILE*') to gain
            better understanding about errors when they occur.
            
*/

#ifndef SCTFRAME_H
#define SCTFRAME_H

#include "sctstd.h"

extern FILE *sctframe_config_debug;
extern int sctframe_config_version[3];

struct record {
    void *address;
    void (*freef)(void *);
};

typedef struct {
    struct record *records;
    void (*cff)(void *); //current_free_function
} sctframe_t;


sctframe_t *sctframe_new();
void *sctframe_add(sctframe_t *f, void *pointer);
void sctframe_free(sctframe_t *f);
bool sctframe_delete(sctframe_t *f, void *pointer);


#define SCTFRAME_SCOPE_BEGIN              { sctframe_t *SCTFRAME__STATE__ = sctframe_new();

/*
sctdoc:SCTFRAME_SCOPE_BEGIN:

NAME
    SCTFRAME_SCOPE_BEGIN

SYNOPSIS
    SCTFRAME_SCOPE_BEGIN
    
DESCRIPTION
    This macro marks the start of a sctframe scope. This macro must be 
    accompanied by a `SCTFRAME_SCOPE_END'.

*/

#define C(pointer)                  (sctframe_add(SCTFRAME__STATE__, (pointer)))
/*
sctdoc:C:

NAME
    C

SYNOPSIS
    void* C(void *pointer);
    
DESCRIPTION
    Within a sctframe scope, this macro is used to add a pointer to the list
    of pointers to automaticly free when execution exits the sctframe scope.

RETURN
    This function returns the same pointer it was passed unless an error 
    occurs.
*/

#define SCTFRAME_DELETE(pointer)                  (sctframe_delete(SCTFRAME__STATE__, (pointer)))
/*
sctdoc:SCTFRAME_DELETE:

NAME
    SCTFRAME_DELETE

SYNOPSIS
    bool SCTFRAME_DELETE(void *pointer);
    
DESCRIPTION
    Within a sctframe scope, this macro is used to free a single pointer from 
    the list of pointers associated with the current sctframe.

RETURN
    true if the pointer was found and freed, and false otherwise.

*/

#define SCTFRAME_SET_FUNCTION(free) (SCTFRAME__STATE__->cff=(free))
/*
sctdoc:SCTFRAME_SET_FUNCTION:

NAME
    SCTFRAME_SET_FUNCTION

SYNOPSIS
    void (*)(void *) SCTFRAME_SET_FUNCTION(void (*)(void *))
    
DESCRIPTION
    Within a sctframe scope, this macro is used to set the function that is 
    used to free the pointers passed to susequent calls to `C'. By default, or 
    if passed NULL, clib's `free' function is used.

RETURN
    This function returns the same pointer it was passed unless an error 
    occurs.
*/

#define SCTFRAME_SCOPE_END                sctframe_free(SCTFRAME__STATE__); }
/*
sctdoc:SCTFRAME_SCOPE_END:

NAME
    SCTFRAME_SCOPE_END

SYNOPSIS
    SCTFRAME_SCOPE_END
    
DESCRIPTION
    This macro marks the end of a sctframe scope. This macro must be 
    accompanied by a `SCTFRAME_SCOPE_END'.

*/

#endif
