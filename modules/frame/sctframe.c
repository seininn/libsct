#define SCT__INTERNAL__
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "scta.h"
#include "sctframe.h"

sctframe_t *sctframe_new() {
    sctframe_t *f = malloc(sizeof(sctframe_t));
    if (!f) {
        ERETURN(NULL, "malloc failed");
    }
    f->records = scta_new(struct record);
    f->cff = NULL;
    if (!f->records) {
        free(f);
        ERETURN(NULL, "^");
    } 
    return f;
}



void *sctframe_add(sctframe_t *f, void *pointer) {
    struct record r;
    
    if (!(pointer)) ERETURN(NULL, "passed pointer was null");
    
    r.address = pointer;
    r.freef = f->cff;
    
    if (!scta_push(f->records, r)) ERETURN(NULL, "^");
    
    return r.address;
}

bool sctframe_delete(sctframe_t *f, void *pointer) { 
    
    if (!(pointer)) ERETURN(NULL, "passed pointer was null");
    
    int i = 0;
    int l = scta_length(f->records);
    for (;i < l; ++i) if (f->records[i].address == pointer) {
        if (f->records[i].freef) f->records[i].freef(f->records[i].address);
        else free(f->records[i].address);        
        if (!scta_delete(f->records, i)) ERETURN(NULL, "^");
        break;
    }
    if (i == l) {
        ERETURN(false, "pointer 0x%p was not found in the current sctframe", pointer);
    } else return true;
}

void sctframe_free(sctframe_t *f) {
    for (int i = 0; i < scta_length(f->records); ++i) {
        if (f->records[i].freef) f->records[i].freef(f->records[i].address);
        else free(f->records[i].address);
    }
    scta_free(f->records);
    free(f);
}

