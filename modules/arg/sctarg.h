/*

    sctarg: Bare-bones Comandline Option Parser

    Sulaiman (seininn) Mustafa     2014-03-03
    
    This module is a part of libstc, a modular, c99/c1x-compliant, 
    toolkit library. use stcmod to determine this module's 
    dependencies, if any.
    
*/


#ifndef SCTARG_H
#define SCTARG_H

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include "sctstd.h"


#define SCTARG_NONE                 0
#define SCTARG_REQUIRED_OPTION      0x1     // OPTION MUST BE SUPPLIED
#define SCTARG_REQUIRED_PARAMETER   0x2     // IF OPTION IS SUPPLIED, OPTION ARGUMENT MUST BE SUPPLIED AS WELL
#define SCTARG_OPTIONAL_PARAMETER   0x4


#define SCTARG_OK 0
#define SCTARG_ERROR_UNRECOGNIZED_OPTION 1
#define SCTARG_ERROR_OPTION_MISSING_PARAM 2
#define SCTARG_ERROR_MISSING_OPTION 3
#define SCTARG_ERROR_INTERNAL 4

struct available_option {
    char *name;
    char *description;
    uint8_t flags;
};


typedef struct {
    struct available_option *available;
    char **processed;
    
    struct {
        bool arabic;
    } config;
    
    struct {
        int status;
        char *optname;
    } error;
} sctarg_t;

sctarg_t* sctarg_new();
void sctarg_free(sctarg_t* a);
bool sctarg_option(sctarg_t* a, char *name, char *description, uint8_t flags); // TODO return dict here
bool sctarg_process_arguments(sctarg_t* a, char **argv, int *argc);
char* sctarg_get(sctarg_t* a, char *flag);

#endif
