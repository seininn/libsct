#define SCT__INTERNAL__
/*

    sctarg: Bare-bones Comandline Option Parser

    Sulaiman (seininn) Mustafa     2014-03-03
    
    This module is a part of libstc, a modular, c99/c1x-compliant, 
    toolkit library. use stcmod to determine this module's 
    dependencies, if any.
    
*/


#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "scta.h"
#include "sctd.h"

#include "sctarg.h"

struct {
    const char *arabic;
    const char *english;
} msgs[] = {
    [SCTARG_OK].arabic   = NULL,
    [SCTARG_OK].english  = NULL,
    [SCTARG_ERROR_UNRECOGNIZED_OPTION].arabic   = "%s: المحدد '%s' ليس ضمن المحددات التي يفهمها البرنامج.\n",
    [SCTARG_ERROR_UNRECOGNIZED_OPTION].english  = "%s: unrecognized option '%s'\n",
    [SCTARG_ERROR_OPTION_MISSING_PARAM].arabic  = "%s: يستلزم المحدد '%s' مخلا.\n",
    [SCTARG_ERROR_OPTION_MISSING_PARAM].english = "%s: '%s' requires an argument\n",
    [SCTARG_ERROR_MISSING_OPTION].arabic        = "%s: المحدد '%s' الإلزامي غير موفر.\n",
    [SCTARG_ERROR_MISSING_OPTION].english       = "%s: missing required argument '%s'\n",
    [SCTARG_ERROR_INTERNAL].arabic       = "%s: خلل أثناء تحليل المحددات.\n",
    [SCTARG_ERROR_INTERNAL].english      = "%s: an error occured durring argument parsing\n",
};

#define error_msg(arg_t, id, ...) {\
    (arg_t)->error.status = id;\
    EPRINT((((arg_t)->config.arabic)?(msgs[(arg_t)->error.status].arabic):(msgs[(arg_t)->error.status].english)), __VA_ARGS__);\
}


sctarg_t* sctarg_new(){
    sctarg_t *a;
    if (!(a = malloc(sizeof(sctarg_t)))) return NULL;
    a->available = scta_new(struct available_option);
    a->processed = sctd_new(char *);
    a->config.arabic = true;
    a->error.status=0;
    return a;
}

void sctarg_free(sctarg_t* a){
    scta_free(a->available);
    sctd_free(a->processed);
    free(a);
}

bool sctarg_option(sctarg_t* a, char *name, char *description, uint8_t flags){
    struct available_option n = { name, description, flags};
    return scta_push(a->available, n);
}

bool sctarg_process_arguments(sctarg_t* a, char **argv, int *argc){
    char **anch = argv;
    if (!scta_length(a->available)) return true;
    while (*argv) {
        int i;
        int name_length = 0;
        
        if (argv[0][0] == '-' && argv[0][1] == '-' && argv[0][2] == '\0') break;
        if (argv[0][0] == '-' && argv[0][1]) {
        
            // find an length of option name
            while (argv[0][name_length] && argv[0][name_length] != '=') ++name_length;
            
            // if arg name in available list
            for (i = 0; i < scta_length(a->available); ++i) 
                if (!strncmp(a->available[i].name, *argv, name_length)) break;
            
            // if not found
            if (i == scta_length(a->available)) {
                error_msg(a, SCTARG_ERROR_UNRECOGNIZED_OPTION, anch[0], *argv);
                return false;
            }
            
            
            struct { char *name; char *parameter;} p;
            
            p.name = a->available[i].name;
            
            if (a->available[i].flags&(SCTARG_REQUIRED_PARAMETER|SCTARG_OPTIONAL_PARAMETER)) {
                // if there is an equel sign in *argv and at least one character is present after it
                if (argv[0][name_length] && argv[0][name_length+1]) {
                    p.parameter = argv[0]+name_length+1;
                }
                else {
                    // if next argument is 
                    if (!argv[1] || (argv[1][0] == '-' && argv[1][1] != '\0')) {
                        if (a->available[i].flags&SCTARG_REQUIRED_PARAMETER) {
                            error_msg(a, SCTARG_ERROR_OPTION_MISSING_PARAM, anch[0], a->available[i].name);
                            return false;
                        }
                        else p.parameter = NULL;
                    }
                    else {
                        p.parameter = argv[1];
                        ++argv;
                    }
                }
            }
            else p.parameter = NULL;
            
            if (!sctd_set(a->processed, p.name, p.parameter)) {
                error_msg(a, SCTARG_ERROR_INTERNAL, anch[0]);
                return false;
            }
        }
        
        if (argv[0]) ++argv;
    }

    // making sure all required argument flags have been provided
    for (int i = 0; i < scta_length(a->available); ++i) {
        if (!(a->available[i].flags & SCTARG_REQUIRED_OPTION)) continue;
        if (!sctd_isset(a->processed, a->available[i].name)) {
            error_msg(a, SCTARG_ERROR_MISSING_OPTION, anch[0], a->available[i].name);
            return false;
        }
    }
    return true;
}


char* sctarg_get(sctarg_t* a, char *flag){
    if (!sctd_isset(a->processed, flag)) return NULL;
    char *param = sctd_get(a->processed, flag);
    if (!param) param = "";
    return param;
}
