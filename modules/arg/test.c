#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "sctarg.h"


//FIXTHIS 
#include "scta.h"
#include "sctd.h"

struct ret {
    sctarg_t* a;
    int argc;
    char **argv;
};

bool mockmain(int argc, char **argv, struct ret *out){

    assert(out->a);
    assert(sctarg_option(out->a, "--required", "this option is required", SCTARG_REQUIRED_OPTION));
    assert(sctarg_option(out->a, "--required-param", "the option has an argument that is required", SCTARG_REQUIRED_PARAMETER));
    assert(sctarg_option(out->a, "--optional-param", "the option has an argument that is optional", SCTARG_OPTIONAL_PARAMETER));
    assert(sctarg_option(out->a, "--basic-option", "this is a basic option", SCTARG_NONE));
    
    out->a->config.arabic=false;
    
    out->argc=argc;
    out->argv=argv;
    
    return sctarg_process_arguments(out->a, argv, &argc);
}



int main(int argc, char **argv){
    sct_debug = stderr;
    
    {
        struct ret r;
        r.a = sctarg_new();
        sct_debug = NULL;
        assert( ! mockmain(4, (char *[5]){"test", "arg1", "arg2", "arg3", NULL}, &r));
        assert(r.a->error.status == SCTARG_ERROR_MISSING_OPTION);
        sct_debug = stderr;
        
        assert(!strcmp(r.argv[0], "test"));
        assert(!strcmp(r.argv[1], "arg1"));
        assert(!strcmp(r.argv[2], "arg2"));
        assert(!strcmp(r.argv[3], "arg3"));
        
        
        sctarg_free(r.a);
    }
    
    {
        struct ret r;
        r.a = sctarg_new();
        
        assert( mockmain(5, (char *[6]){"test", "--required", "arg1", "arg2", "arg3", NULL}, &r));
        
        assert(!strcmp(r.argv[0], "test"));
        assert(!strcmp(r.argv[1], "--required"));
        assert(!strcmp(r.argv[2], "arg1"));
        assert(!strcmp(r.argv[3], "arg2"));
        assert(!strcmp(r.argv[4], "arg3"));
        
        // processed args        
        assert(!sctarg_get(r.a, "--imaginary"));
        assert(sctarg_get(r.a, "--required"));
        
        sctarg_free(r.a);
    }
    {
        struct ret r;
        r.a = sctarg_new();
        
        sct_debug = NULL;
        assert(! mockmain(6, (char *[7]){"test", "--required", "arg1", "arg2", "arg3", "--bogus", NULL}, &r));
        assert(r.a->error.status == SCTARG_ERROR_UNRECOGNIZED_OPTION);
        sct_debug = stderr;
        
        assert(!strcmp(r.argv[0], "test"));
        assert(!strcmp(r.argv[1], "--required"));
        assert(!strcmp(r.argv[2], "arg1"));
        assert(!strcmp(r.argv[3], "arg2"));
        assert(!strcmp(r.argv[4], "arg3"));
        assert(!strcmp(r.argv[5], "--bogus"));
        
        sctarg_free(r.a);
    }


    {
        struct ret r;
        r.a = sctarg_new();
        
        assert( mockmain(7, (char *[8]){"test", "--required", "arg1", "arg2", "arg3", "--", "--bogus", NULL}, &r));
        
        assert(!strcmp(r.argv[0], "test"));
        assert(!strcmp(r.argv[1], "--required"));
        assert(!strcmp(r.argv[2], "arg1"));
        assert(!strcmp(r.argv[3], "arg2"));
        assert(!strcmp(r.argv[4], "arg3"));
        assert(!strcmp(r.argv[5], "--"));
        assert(!strcmp(r.argv[6], "--bogus"));

        // processed args
        assert(sctarg_get(r.a, "--required"));

        sctarg_free(r.a);
    }
    
    {
        struct ret r;
        r.a = sctarg_new();
        
        assert( mockmain(6, (char *[7]){"test", "--required-param=300 00","arg1", "arg2", "arg3", "--required", NULL}, &r));
        
        assert(!strcmp(r.argv[0], "test"));
        assert(!strcmp(r.argv[1], "--required-param=300 00"));
        assert(!strcmp(r.argv[2], "arg1"));
        assert(!strcmp(r.argv[3], "arg2"));
        assert(!strcmp(r.argv[4], "arg3"));
        assert(!strcmp(r.argv[5], "--required"));
        


        // processed args
        assert(sctarg_get(r.a, "--required"));
        assert(sctarg_get(r.a, "--required-param"));
        assert(!strcmp(sctarg_get(r.a, "--required-param"), "300 00"));        
        
        
        sctarg_free(r.a);
    }
    
    
    {
        struct ret r;
        r.a = sctarg_new();
        assert( mockmain(6, (char *[7]){"test", "--required-param=300 00","arg1", "arg2", "arg3", "--required", NULL}, &r));
        
        assert(!strcmp(r.argv[0], "test"));
        assert(!strcmp(r.argv[1], "--required-param=300 00"));
        assert(!strcmp(r.argv[2], "arg1"));
        assert(!strcmp(r.argv[3], "arg2"));
        assert(!strcmp(r.argv[4], "arg3"));
        assert(!strcmp(r.argv[5], "--required"));
        
        
        assert(sctarg_get(r.a, "--required"));
        assert(sctarg_get(r.a, "--required-param"));
        assert(!strcmp(sctarg_get(r.a, "--required-param"), "300 00"));        
        
        sctarg_free(r.a);
    }

    {
        struct ret r;
        r.a = sctarg_new();
        assert( mockmain(7, (char *[8]){"test", "--required-param", "300 00", "arg1", "arg2", "arg3", "--required", NULL}, &r));
        
        assert(!strcmp(r.argv[0], "test"));
        assert(!strcmp(r.argv[1], "--required-param"));
        assert(!strcmp(r.argv[2], "300 00"));
        assert(!strcmp(r.argv[3], "arg1"));
        assert(!strcmp(r.argv[4], "arg2"));
        assert(!strcmp(r.argv[5], "arg3"));
        assert(!strcmp(r.argv[6], "--required"));
        
        
        assert(sctarg_get(r.a, "--required"));
        assert(sctarg_get(r.a, "--required-param"));
        assert(!strcmp(sctarg_get(r.a, "--required-param"), "300 00"));        
        
        sctarg_free(r.a);
    }    


    {
        struct ret r;
        r.a = sctarg_new();
        
        sct_debug = NULL;
        assert(!mockmain(7, (char *[8]){"test", "--required-param", "--basic-option", "arg1", "arg2", "arg3", "--required", NULL}, &r));
        assert(r.a->error.status == SCTARG_ERROR_OPTION_MISSING_PARAM);
        sct_debug = stderr;
        
        assert(!strcmp(r.argv[0], "test"));
        assert(!strcmp(r.argv[1], "--required-param"));
        assert(!strcmp(r.argv[2], "--basic-option"));
        assert(!strcmp(r.argv[3], "arg1"));
        assert(!strcmp(r.argv[4], "arg2"));
        assert(!strcmp(r.argv[5], "arg3"));
        assert(!strcmp(r.argv[6], "--required"));
        
        sctarg_free(r.a);
    }    
    
    
    {
        struct ret r;
        r.a = sctarg_new();
        assert(mockmain(6, (char *[7]){"test", "--required-param=--basic-option", "arg1", "arg2", "arg3", "--required", NULL}, &r));
        
        assert(!strcmp(r.argv[0], "test"));
        assert(!strcmp(r.argv[1], "--required-param=--basic-option"));
        assert(!strcmp(r.argv[2], "arg1"));
        assert(!strcmp(r.argv[3], "arg2"));
        assert(!strcmp(r.argv[4], "arg3"));
        assert(!strcmp(r.argv[5], "--required"));
        
        assert(sctarg_get(r.a, "--required"));
        assert(sctarg_get(r.a, "--required-param"));
        assert(!strcmp(sctarg_get(r.a, "--required-param"), "--basic-option"));        
        
        sctarg_free(r.a);
    } 
    
    
    {
        struct ret r;
        r.a = sctarg_new();
        assert(mockmain(6, (char *[7]){"test", "--required-param=--basic-option", "arg1", "arg2", "arg3", "--required", NULL}, &r));
        
        assert(!strcmp(r.argv[0], "test"));
        assert(!strcmp(r.argv[1], "--required-param=--basic-option"));
        assert(!strcmp(r.argv[2], "arg1"));
        assert(!strcmp(r.argv[3], "arg2"));
        assert(!strcmp(r.argv[4], "arg3"));
        assert(!strcmp(r.argv[5], "--required"));
        
        assert(sctarg_get(r.a, "--required"));
        assert(sctarg_get(r.a, "--required-param"));
        assert(!strcmp(sctarg_get(r.a, "--required-param"), "--basic-option"));        
        
        sctarg_free(r.a);
    }
    
    
    {
        struct ret r;
        r.a = sctarg_new();
        assert(mockmain(6, (char *[7]){"test", "--optional-param=--basic-option", "arg1", "arg2", "arg3", "--required", NULL}, &r));
        
        assert(!strcmp(r.argv[0], "test"));
        assert(!strcmp(r.argv[1], "--optional-param=--basic-option"));
        assert(!strcmp(r.argv[2], "arg1"));
        assert(!strcmp(r.argv[3], "arg2"));
        assert(!strcmp(r.argv[4], "arg3"));
        assert(!strcmp(r.argv[5], "--required"));


        assert(sctarg_get(r.a, "--required"));
        assert(sctarg_get(r.a, "--optional-param"));
        assert(!strcmp(sctarg_get(r.a, "--optional-param"), "--basic-option"));        
        
        sctarg_free(r.a);
    }
    
    {
        struct ret r;
        r.a = sctarg_new();
        
                
        assert(mockmain(7, (char *[8]){"test", "--optional-param", "--basic-option", "arg1", "arg2", "arg3", "--required", NULL}, &r));
        assert(r.a->error.status == SCTARG_OK);
        
        assert(!strcmp(r.argv[0], "test"));
        assert(!strcmp(r.argv[1], "--optional-param"));
        assert(!strcmp(r.argv[2], "--basic-option"));
        assert(!strcmp(r.argv[3], "arg1"));
        assert(!strcmp(r.argv[4], "arg2"));
        assert(!strcmp(r.argv[5], "arg3"));
        assert(!strcmp(r.argv[6], "--required"));
        
        assert(sctarg_get(r.a, "--required"));
        assert(sctarg_get(r.a, "--basic-option"));
        assert(sctarg_get(r.a, "--optional-param"));
        assert(!strcmp(sctarg_get(r.a, "--optional-param"), ""));        

        sctarg_free(r.a);
    }

    {
        struct ret r;
        r.a = sctarg_new();
        
                
        assert(mockmain(7, (char *[9]){"test", "--optional-param", "yolo", "--basic-option", "arg1", "arg2", "arg3", "--required", NULL}, &r));
        assert(r.a->error.status == SCTARG_OK);
        
        assert(!strcmp(r.argv[0], "test"));
        assert(!strcmp(r.argv[1], "--optional-param"));
        assert(!strcmp(r.argv[2], "yolo"));
        assert(!strcmp(r.argv[3], "--basic-option"));
        assert(!strcmp(r.argv[4], "arg1"));
        assert(!strcmp(r.argv[5], "arg2"));
        assert(!strcmp(r.argv[6], "arg3"));
        assert(!strcmp(r.argv[7], "--required"));
        
        assert(sctarg_get(r.a, "--required"));
        assert(sctarg_get(r.a, "--basic-option"));
        assert(!strcmp(sctarg_get(r.a, "--basic-option"), ""));        
        assert(sctarg_get(r.a, "--optional-param"));
        assert(!strcmp(sctarg_get(r.a, "--optional-param"), "yolo"));        

        sctarg_free(r.a);
    }
    
    printf("[ok] %s version 0x%x %s\n", "sctarg", sct_version, __TIMESTAMP__);
        
    return 0;
}

