#define _POSIX_C_SOURCE 199309L

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "sctos.h"
#include "scta.h"

int main (int argc, char **argv){
    
    if (argv[1] && argv[1][0] == '2') pause();
    
    {// testing sctos_load
        char *b;
        
        assert(b = sctos_load("test.d/test.txt"));
        assert(!strcmp(b, "test\n"));
        scta_free(b);
        
        sctos_config_verbose = false; 
        assert(!sctos_load("test.d/does-not-exist.txt"));
        sctos_config_verbose = true;
    }
    
    {// testing pidof family
        pid_t *list;
        assert((list = sctos_pidof("somegibrishthatshouldnotexist")));
        assert(scta_length(list) == 0);
        scta_free(list);
        
        if (!fork()) execve("./test", (char *[]){"test", "200", NULL}, NULL);
        if (!fork()) execve("./test", (char *[]){"test", "200", NULL}, NULL);

        assert((list = sctos_pidof("test")));
        assert(scta_length(list) == 3 || scta_length(list) == 2); // to accomadate for valgrind being the name of the main process
        if (list[0] != getpid()) kill(list[0], SIGTERM);        
        if (list[1] != getpid()) kill(list[1], SIGTERM);        
        if (list[2] && list[2] != getpid()) kill(list[2], SIGTERM);        
        scta_free(list);
    }
    
    
    printf("[ok] %s %x %s\n", "sctos", sct_version, __TIMESTAMP__);
    return 0;
}
