/*

    sctos: Usefull Operating System Routines
    Sulaiman (seininn) Mustafa
    Version 1:0:1
    Date: 2014-01-21
    Dependency list: sctstr
    
    This module is a part of libstc.
*/

#ifndef SCTOS_H
#define SCTOS_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/types.h>
#include "sctstd.h"

bool sctos_config_verbose;

bool sctos_walk(char *dirname, bool (*handler)(void *payload, char *path), void *payload);
/*
sctdoc:sctos_walk:
NAME
    sctos_walk
    
SYNOPSIS
    bool sctos_walk(    char *dirname, 
                        bool (*handler)(void *payload, char *path), 
                        void *payload);

DESCRIPTION    
    -
    
RETURN
    -
*/


char* sctos_load(char *path);
/*
sctdoc:sctos_load:
NAME
    sctos_load
    
SYNOPSIS
    char* sctos_load(char *path)

DESCRIPTION    
    opens `path', and allocates a buffer large enough to hold it's content, 
    reads it's content into the buffer, then returns the allocated buffer. 
    
RETURN
    Returns a pointer to the contents of the file or NULL on error. The 
    pointer must be freed using sctos_free()
*/


pid_t* sctos_pidof_uid(char *name, uid_t uid);
/*
sctdoc:sctos_pidof_uid:
NAME
    sctos_pidof_uid
    
SYNOPSIS
    pid_t* sctos_pidof_uid(char *name, uid_t uid)

DESCRIPTION    
    Returns all running processes with name `name' that are running as real
    uid `uid' as reported by proc.
    
RETURN
    Returns a null terminated list of pid_t on success and NULL on failure. 
    The pointer must be freed using sctos_free()
*/

pid_t* sctos_pidof(char *name);
/*
sctdoc:sctos_pidof:
NAME
    sctos_pidof
    
SYNOPSIS
    pid_t* sctos_pidof(char *name);

DESCRIPTION    
    Returns all running processes with name `name' that are running as real
    uid of the calling proccess.
    
RETURN
    Returns a null terminated list of pid_t on success and NULL on failure. 
    The pointer must be freed using sctos_free()
*/

void sctos_free(void *pointer);
/*
sctdoc:sctos_free:
NAME
    sctos_free
    
SYNOPSIS
    void sctos_free(void *pointer);

DESCRIPTION    
    Frees objects returned by functions that belong to the sctos module.
    
RETURN
    None
*/



#endif
