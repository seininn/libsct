#define SCT__INTERNAL__
/*

    sctos: Usefull Operating System Routines
    Sulaiman (seininn) Mustafa
    Version 1:0:1
    Date: 2014-01-21
    
    This is a module of the sctlibz collection; sctlibs is a 
    nerve-wrekingly inefficiant library zoo.
    
    
    ---
    
    TODO
        Messy! factorize and add tests. This pile of code is a gigantic bug 
        waiting to happen.
        

*/

#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>


#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "scta.h"
#include "scts.h"
#include "sctos.h"

FILE *sctos_config_debug = NULL;
bool sctos_config_verbose = true;
int sctos_config_version[3] = {1, 0, 1};

char* sctos_load(char *path){
    struct stat s;
    int fd;

    fd = open(path, 0);
    
    if (fd == -1) 
        ERETURN(NULL, "Unable to open '%s': %s\n", path, strerror(errno));
    
    if (stat(path, &s)) 
        ERETURN(NULL, "could not stat '%s': %s\n", path, strerror(errno));

    
    char *buf = scta_new(char);
    
    if (!scta_set_length(buf, s.st_size+1)) {
        scta_free(buf);
        ERETURN(NULL, "could not allocate %lld bytes for '%s': %s\n", (long long int) s.st_size, path, strerror(errno));
    }
    
    int rc, cur = 0; // cur is the number of bytes that have been written to buf
    
    while (1) {
        rc = read(fd, buf, s.st_size);
        if (rc > 0) cur += rc;
        
        if (rc > 0 && cur < s.st_size) continue;
        else if (cur == s.st_size) break;
        else if (rc == -1) {
            switch (errno) {
                case EAGAIN:
                #if EWOULDBLOCK != EAGAIN
                case EWOULDBLOCK:
                #endif
                case EINTR:
                    continue;
                default:
                    scta_free(buf);
                    ERETURN(NULL, "Unable to read from '%s': %s\n", path, strerror(errno));
            }
        }
        else {
            scta_free(buf);
            ERETURN(NULL, "Undefined error occured. rc value id %d: %s\n", rc, strerror(errno));
        }
    }
    
    close(fd);
    return buf;
}


bool sctos_walk(char *path, bool (*handler)(void *payload, char *path), void *payload){
    // add decrementing maxdepth later
    // add fail mode strict or (default) permisive
    DIR *dfd;
    struct dirent *entry;
    bool rv = true;
    
    if (sctos_config_debug) fprintf(sctos_config_debug, "== looking in '%s'\n", path);
    if (!(dfd = opendir(path))) {
        ERETURN(false, "could not open '%s': %s\n", path, strerror(errno));
    }
    
    while ((entry = readdir(dfd))) {
        char *newpath = scta_new(char);
        scts_append(newpath, path);
        scts_append(newpath, "/");
        scts_append(newpath, entry->d_name);
        
        struct stat st;
        if (stat(newpath, &st) == -1) {
            if (sctos_config_verbose || sctos_config_debug) perror(newpath);
            rv = false;
            scta_free(newpath);
            continue;
        }
        if (S_ISDIR(st.st_mode)) {
            if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) {
                scta_free(newpath);
                continue;
            }
            
            rv &= handler(payload, newpath);
            rv &= sctos_walk(newpath, handler, payload);
        }
        else {
            rv &= handler(payload, newpath);
        }
        scta_free(newpath);
    }
    
    closedir(dfd);
    return rv;
}


pid_t* sctos_pidof(char *name){
        uid_t uid = getuid();
        return sctos_pidof_uid(name, uid);
}

pid_t* sctos_pidof_uid(char *name, uid_t uid){
    DIR *dfd;
    struct dirent *entry;
    
    pid_t *list = scta_new(pid_t);
    if (!list) {
        ERETURN(false, "could not allocate output array\n");
        return NULL;
    }
    
    
    if (!name || !name[0]) {
        ERETURN(false, "empty name or null pointer supplied as `name'\n");
        return NULL;
    }
    
    int name_length = strlen(name);
    
    if (!(dfd = opendir("/proc"))) {
        ERETURN(false, "could not open '/proc': %s\n", strerror(errno));
    }
    
    while ((entry = readdir(dfd))) {
        #define max_path_size 256
        #define max_line_size 256
        
        char path[max_path_size];
        char line[max_line_size];
        FILE *sfd;
        
        if (entry->d_name[0] < '0' || entry->d_name[0] > '9') continue;
        
        snprintf(path, max_path_size, "/proc/%s/status", entry->d_name);
        
        if (!(sfd = fopen(path, "r"))) continue;
        
        // if proc name does not match 
        fgets(line, max_line_size, sfd);
        if (strncmp(line+6, name, name_length)) { // path+6 to skip "name:\t"
            fclose(sfd);
            continue; 
        }
        
        while (1) {
            fgets(line, max_line_size, sfd);
            if (feof(sfd)) break;
            if (strncmp(line, "Uid:\t", 5)) continue; // ignore anything other than Uid field in /proc/[]/status
            if (strtol(line+5, NULL, 10) != uid) continue;  // if process doesn;t habe the same real user
            if (!scta_push(list, (pid_t) strtol(entry->d_name, NULL, 10))) {
                scta_free(list);
                ERETURN(false, "failed to add pid to output list\n");
                return NULL;
            }
        }
        #undef max_path_size
        #undef max_line_size
        fclose(sfd);
    }
    
    closedir(dfd);
    return list;
}

void sctos_free(void *pointer){
    scta_free(pointer);
}

