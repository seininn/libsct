/*

PROTOCOL:
    WRAPUP:
        - Make sure private sections of the header are wrapped with SCT__INTERNAL__ tests
        - Make sure to handle all input or at least sane function inputs
//TODO
    - move private headder defines behind a  SCT__INTERNAL__ define to have a clean user environment
        check s, a, d, ...
            for private structs, etc
    
    - sctl:
        - port sctfetch
        - add flag generator $(sctl flags ml) -> -Irelative_to_your_loc_paths)
        - add dependency reporting and let sctl flags handle them.
    
    - add general tests to "parts" modules to test compiled module
    
    - write preform input analysis and add real tests 
    
    - fixes for sctcd:
        - use scts
        - unify api and add static buffer support
        - stop duplicating and use a macro (using scts_length should solve inconsistencies)
        
    - sctgpio: abstract gpio library with extendable architecture
        - sctgpio_open()
        - sctgpio_close()
        - sctgpio_bit_set()
        - sctgpio_bit_unset()
        - sctgpio_bit_read()
        - sctgpio_nbyte_set()
        - sctgpio_nbyte_unset()
        - sctgpio_nbyte_read()
        
    - sctpeer: 
        tor + lan: lists for tor pears need to be explictly define where lan can
                    use brodcast
        
    
    - sctb: a set of functions to work with binary data
        - serialize data:
            efficiant seekable format with stationary block. Format:
            [byte offset for lookup table, 8 bytes, little endian order]
            [block 1]
            [block 2]
            [block 3]
            [block n-1]
            [block n]
            ["lookup table"
            block 1 : element size : element count
            block 2 : element size : element count
            block 3 : element size : element count
            block n : element size : element count
            ]
            
            Advantages:
            - simple format that can be dealt with easly in other languages
            - blocks stay stationary with addition of new blocks (only lookup table gets pushed down by new block)
            - quick lookup: No need to read in entire file to find the i'th block, just read the small table pointed to 
              by the byte offset at the bigening of the file.
            
        - delta table diffing and merging
            - devide into sutible blocks and derive a list of hashsums
            
         
    
    - work on /io. and rename scts_depprint back to scts_print, but let it 
      continue being the dumb function that it is. (it's nessasary for 
      flow, and posible feature modules), and I don't want to tie /s with
      /io    
      
    - rather than io, have
        sctfile:
        - sctfile_write
        - sctfile_append
        - sctfile_read
    
        sctfs: filesystem
        - sctfs_find(name, flags_where) searches for file named name in common 
                                        locations such as ANSESTORS (git style),
                                        USER_CONFIG_DIR (user config directory of the 
                                        program ~/.config/argv[0]/name) and returns
                                        string.
        - sctfs_walk from sctos
        
    - rewrite frame to use:
        void fun(){
            SCTFRAME_BIGEN;
            SCTFRAME_SET_FREE_FUNCTION(free);
            void *l = C(malloc(100001));
            
            SCTFRAME_SET_FREE_FUNCTION(scta_free);
            l = C(scta_new(int));
            
            SCTFRAME_END;
        }
                                        
    
    - sctd has a big advantage in that it is stored sequentially in memory 
      (based on scta), which makes using it very cacheline friendly.
      
      This also makes looping a brease... ordering is NOT garanteed.
      lookups are, for the time being O(N), but can be easly converted to O(1)
      by using an intermediate hash table. is of the same complexity as lookups.
      deleting however, is always O(N) in the worst case (scta_delete)
      
      Deletions can be made O(1)ish (always < 10) if you are willing to give up
      memory sequalaty (use a hole array to keep track of unused cells without
      actually deleting them using scta_delete).
    
    
    - sctd add endianess support for input/output, flip bytes
    
    - update sct test system
        ! ditch make files
        - convert files to use bash scripts instead of make files
        - update checklib to be as smart as parts.*
        - unify how scripts initialize and work, even if a script does not 
          need the fancy shmancy
    
    - modify fetch script to:
        - work locally
        - to determine reqiured modules via source inspection (optional?)        

    - scts_print
        < center >

    - unification:
         - rename sct_debug to sct_sct_debug and link to the rest of the library
         - rename version string and unifiy with rest of library.
         - sct0.c
         

    - qualifiers
        - add proper const support
        - //   //    restrict //

    - functions
        - multiply(string, integer): repeate a string or split it at 
        - devide(string, indteger): ^
        - indent(char *str, int width, int spaces): indents a string
        - trim
        - strip
        - regex (returns from-to indeces)
        - prathasis 
    
    - modules
        - rect: move in rect library
        - ps: pixel screen: pure pixel buffer manipulation --
                                                - sctps_render(screen, buffer, [rect render region])
                                                - struct buffer {
                                                    width, height, ptr,
                                                    encoding, [encoding_hook],
                                                }
                                                - internal buffer: RRRR
                                                screen types: 
                                                    pixel screen
                                                    text screen
                                                - text screen has additional api
                                                  and acts like a terminal:
                                                    accepts escape sequences
                                                    cursor controls
                                                    
                                                  
                                                
                                                
        
        - vcd: visual codecs library (qrcode, barcodes?)
        - acd: audio codecs library (56k modem encoder implementation)
        - bcd: byte codecs library (64bit, gzip?)
        - ch:  cryptographic hashing functions -- not designed for on-line usage
                                                - (bar minimum implementation, no
                                                -   side-channel attack 
                                                    countermessures; e.g timing 
                                                    atk)
        - (update)  time: integrate itl and implement time querries/expressions
        
        
*/

#define SCT__INTERNAL__
#include "sctstd.h"
FILE* sct_debug;
uint32_t sct_version = 0x000001;

