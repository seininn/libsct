#ifndef SCTSTD_H
#define SCTSTD_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

extern FILE* sct_debug;
extern uint32_t sct_version;

// INTERNAL USE ONLY
#ifdef SCT__INTERNAL__

#define EPRINT(...) {\
    if (sct_debug) {\
        fprintf(sct_debug, "%s:%s():%d: ", __FILE__, __func__, __LINE__);\
        fprintf(sct_debug, __VA_ARGS__);\
        fprintf(sct_debug, "\n");\
    }\
}
#define ERETURN(RET, ...) {\
    if (sct_debug) {\
        fprintf(sct_debug, "%s:%s():%d: ", __FILE__, __func__, __LINE__);\
        fprintf(sct_debug, __VA_ARGS__);\
        fprintf(sct_debug, "\n");\
    }\
    return (RET); \
}

#endif

#endif

