/*
sctdoc:sctcd:

    sctcd: A generic codec module

    Sulaiman (seininn) Mustafa     2014-06-09
    
    This module is a part of libstc, a modular, c11-compliant, 
    toolkit library. use stcmod to determine this module's 
    dependencies, if any.
    
    -----------------------------------------------------------------------
    
    EXAMPLES
    
    API
    
    DEBUGING
    
    SCT offers a verbose mode as a debuging aid. To enable this mode, set 
    sct_debug_fd to where want the verbose output to be written to (stderr,
    fopen()).
    
*/

#include <stddef.h>
#include <stdio.h>
#include <stdint.h>
#include "sctstd.h"


#define SCTCD__LITTLE_ENDIAN (((uint8_t *)((uint16_t[]) {0xFFAA}))[0] == 0xAA)



#define SCTCD_WHITESPACE    0x1
#define SCTCD_URL           0x2
#define SCTCD_SINGLE        0x4
#define SCTCD_UTF8          0x1000
#define SCTCD_UTF32         0x2000

