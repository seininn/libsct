#define SCT__INTERNAL__
#include "sctcd.h"
#include "scta.h"

//HEADER
#ifndef SCTCD_PART_BASE16_DECODE
#define SCTCD_PART_BASE16_DECODE
uint8_t* sctcd_base16_decode_c(char *string);
#define sctcd_base16_decode(list) (_Generic( (0, (list)), \
    uint32_t *:       sctcd_base16_decode_u, \
    const uint32_t *: sctcd_base16_decode_u, \
    char *:       sctcd_base16_decode_c, \
    const char *: sctcd_base16_decode_c \
    )(list))
    
/*
sctdoc:sctcd_base16_decode:

NAME
    sctcd_base16_decode

SYNOPSIS
    uint8_t* sctcd_base16_decode([char *][char32_t *] string)
    
    uint8_t* sctcd_base16_decode_c(char *string);
    uint8_t* sctcd_base16_decode_u(uint32_t *string);
    
DESCRIPTION
    This function decodes base16 (hexadecimal) strings and returns a
    byte array as the result. The length of the decoded data can be 
    determined using sctcd_length.
    
RETURN
    This function returns a byte array as a result or `NULL' on failure.

EXAMPLE
    char *base16message = (char *) sctcd_base16_decode("68656C6C6F");
    puts(base16message); // hello
    sctcd_free(base16message);
}

SEE ALSO
    sctcd_length
    sctcd_base16_encode
*/

#endif

//LIB
#include <string.h>
int sctcd__base16_reverse_lookup[] = {
    ['0'] = 0,
    ['1'] = 1,
    ['2'] = 2,
    ['3'] = 3,
    ['4'] = 4,
    ['5'] = 5,
    ['6'] = 6,
    ['7'] = 7,
    ['8'] = 8,
    ['9'] = 9,
    ['A'] = 10,
    ['a'] = 10,
    ['B'] = 11,
    ['b'] = 11,
    ['C'] = 12,
    ['c'] = 12,
    ['D'] = 13,
    ['d'] = 13,
    ['E'] = 14,
    ['e'] = 14,
    ['F'] = 15,
    ['f'] = 15 };
    
uint8_t* sctcd_base16_decode_c(char *string){
    uint8_t *out; // ouput string
    int omax;
    int ocur = 0;
    
    int length = strlen(string);
    
    // calculating required memory
    if (length%2) ERETURN(NULL, "incomplete base2 string: not a multiple of 2");
    omax = length/2;
    
    if (!(out = scta_new(uint8_t))) ERETURN(NULL, "^");
    
    // stretching array to required length
    if (!scta_set_length(out, omax)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }
    
    // validating
    for (int i = 0; string[i]; ++i) {
        if (!(
            ((string[i] >= 'A') && (string[i] <= 'F')) ||
            ((string[i] >= 'a') && (string[i] <= 'f')) ||
            ((string[i] >= '0') && (string[i] <= '9')) )) {
            scta_free(out);
            ERETURN(NULL, "invalid base16 characters in string: %c (%d) @ %d", string[i], string[i], i);
        }
    }
    
    // decoding
    for (int i = 0; i < length; i+=2) {
        scta_get(out, ocur++) = (sctcd__base16_reverse_lookup[(int) string[i]]<<4)|sctcd__base16_reverse_lookup[(int) string[i+1]];
    }
    
    if (ocur > omax) { // remove later if calculation is varified / to accomidate the extra ++
        scta_free(out);
        ERETURN(NULL, "buffer overflow detected");
    }
    return out;
}


uint8_t* sctcd_base16_decode_u(uint32_t *string){ //TODO uchar32_t
    uint8_t *out; // ouput string
    int omax;
    int ocur = 0;
    
    int length = 0; 
    while (string[length++]); 
    --length;
    
    // calculating required memory
    if (length%2) ERETURN(NULL, "incomplete base2 string: not a multiple of 2");
    omax = length/2;
    
    if (!(out = scta_new(uint8_t))) ERETURN(NULL, "^");
    
    // stretching array to required length
    if (!scta_set_length(out, omax)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }
    
    // validating
    for (int i = 0; string[i]; ++i) {
        if (!(
            ((string[i] >= 'A') && (string[i] <= 'F')) ||
            ((string[i] >= 'a') && (string[i] <= 'f')) ||
            ((string[i] >= '0') && (string[i] <= '9')) )) {
            scta_free(out);
            ERETURN(NULL, "invalid base16 characters in string: %c (%d) @ %d", string[i], string[i], i);
        }
    }
    
    // decoding
    for (int i = 0; i < length; i+=2) {
        scta_get(out, ocur++) = (sctcd__base16_reverse_lookup[(int) string[i]]<<4)|sctcd__base16_reverse_lookup[(int) string[i+1]];
    }
    
    if (ocur > omax) { // remove later if calculation is varified / to accomidate the extra ++
        scta_free(out);
        ERETURN(NULL, "buffer overflow detected");
    }
    return out;
}


//TEST
#ifdef test_base16_decode

#include <assert.h>
#include <stdio.h>
#include <string.h>

int main() {
    sct_debug = stderr;
    char *c;
    assert(c=(char *)sctcd_base16_decode("41424344"));
    assert(!strcmp(c, "ABCD"));
    assert(sctcd_length(c) == 4);
    sctcd_free(c);
    
    uint8_t *b;
    assert(b=sctcd_base16_decode("FFaa66"));
    assert(b[0] == 0xff);
    assert(b[1] == 0xaa);
    assert(b[2] == 0x66);
    assert(!b[3]);
    assert(sctcd_length(b) == 3);
    sctcd_free(b);

    assert(b=sctcd_base16_decode(((uint32_t[]) {'F','F','a','a','6','6', 0})));
    assert(b[0] == 0xff);
    assert(b[1] == 0xaa);
    assert(b[2] == 0x66);
    assert(!b[3]);
    assert(sctcd_length(b) == 3);
    sctcd_free(b);
    
    sct_debug = NULL;
    assert(!sctcd_base16_decode("FFaa6"));
    assert(!sctcd_base16_decode("FFazz"));
    sct_debug = stderr;
    
    return 0;
}

#endif
