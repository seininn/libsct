#define SCT__INTERNAL__
#include "sctcd.h"
#include "scta.h"

//HEADER
#ifndef SCTCD_PART_BASE16_ENCODE
#define SCTCD_PART_BASE16_ENCODE
void* sctcd_base16_encode(uint8_t *mem, int length, int flags);

/*
sctdoc:sctcd_base616_encode:

NAME
    sctcd_base16_encode

SYNOPSIS
    void* sctcd_base16_encode(uint8_t *mem, int length, int flags);
    
DESCRIPTION
    This function encodes `length' number of bytes, starting at address 
    `mem', to their base16 (hexadecimel) notation.
    
    If `length' is -1, the function will then read from mem until the 
    first ocurrance of '\0', depending on the flags set.
    
    This function returns a pointer to `char' string by default, but can 
    return a char32_t if the appropreate flag is passed.
    
    The flags this function accepts are the following:
    
        SCTCD_SINGLE        `mem' points to a single number spanning 
                            `length' bytes and not `length' number of 
                            indivsual bytes.
                            
        SCTCD_UTF32         Returns the encoded data as a `char32_t' 
                            array.
                            
NOTE
    1.  sctcd_length is not always accurate for the objects returned by 
        this function. use normal string length functions to determine 
        length. TODO
    2.  This function operates on byte arrays, and not on strings. While 
        -1 was added as a convenience for developers, it will not work 
        for char32_t characters.
    
RETURN
    This function returns a pointer of type `char *' or `char32_t *' cast
    as `void *' on success, or NULL on failure.
    
    If the returned value is not `NULL' it must be freed using `sctcd_free'.

EXAMPLE
    char *base16message = sctcd_base16_encode((uint8_t *)"Hello", 5, 0);
    puts(base16message); // out: 68656C6C6F
    sctcd_free(base16message);

SEE ALSO
    sctcd_base16_decode
    sccd_free
    sctcd_base64_encode
    sctcd_base2_encode
*/

#endif

//LIB
#include <string.h>
int sctcd__base16_lookup[] = {
    [0] = '0', [1] = '1', [2] = '2', [3] = '3', [4] = '4', [5] = '5', 
    [6] = '6', [7] = '7', [8] = '8', [9] = '9', [10] = 'A',
    [11] = 'B', [12] = 'C', [13] = 'D', [14] = 'E', [15] = 'F' };
    
void* sctcd_base16_encode(uint8_t *mem, int length, int flags){
    char *out; // ouput string
    int omax = 0;
    int ocur = 0; // current position in `mem', must not exceed `omax'
    
    
    if (length < 0) {
        if (length == -1) length = strlen((char *)mem);
        else ERETURN(NULL, "bad length: must be a positive number or -1. Geven: %d", length);
    }
    
    
    // calculating required memory
    omax += length*2;
    //omax += ((flags&SCTCD_WHITESPACE)?(omax/64+1):0);
    
    if (!(out = scta_new(char))) ERETURN(NULL, "^");
    
    // stretching array to required length    
    if (!scta_set_length(out, omax)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }
    
    
    // encoding loop bounds:
    if (flags&SCTCD_SINGLE && SCTCD__LITTLE_ENDIAN) for (int i = length-1; i >= 0; --i) {
        scta_get(out, ocur++) = sctcd__base16_lookup[(mem[i]&0xF0)>>4];
        scta_get(out, ocur++) = sctcd__base16_lookup[mem[i]&0xF];
    }
    else for (int i = 0; i < length; ++i) {
        scta_get(out, ocur++) = sctcd__base16_lookup[(mem[i]&0xF0)>>4];
        scta_get(out, ocur++) = sctcd__base16_lookup[mem[i]&0xF];
    }
    
    
    if (flags&SCTCD_UTF32) { //TODO switch to char32_t when you switch to a newer compiler
        uint32_t *u32;
        if (!(u32 = scta_new(uint32_t))) {
            scta_free(out);
            ERETURN(NULL, "^");
        }
        if (!scta_set_length(u32, scta_length(out))) {
            scta_free(out);
            scta_free(u32);
            ERETURN(NULL, "^");        
        }
        for (int i = 0; out[i]; ++i) u32[i] = out[i];
        scta_free(out);
        return u32;
    }
    return out;
}

//TEST
#ifdef test_base16_encode

#include <assert.h>
#include <stdio.h>

int main() {
    sct_debug = stderr;
    char *t;
    
    if (SCTCD__LITTLE_ENDIAN) {
        assert(!strcmp("0066AAFF", t=sctcd_base16_encode((uint8_t *) ((uint32_t[1]) {0xFFAA6600}), 4, 0)));
    }
    else {
        assert(!strcmp("FFAA6600", t=sctcd_base16_encode((uint8_t *) ((uint32_t[1]) {0xFFAA6600}), 4, 0)));
    }
    sctcd_free(t);
    
    assert(!strcmp("41424344", t=sctcd_base16_encode( (uint8_t *) "ABCD", -1, 0)));
    sctcd_free(t);
    
    assert(!strcmp("0066AAFF", t=sctcd_base16_encode(((uint8_t[]) {0x00, 0x66, 0xAA, 0xFF}), 4, 0)));
    sctcd_free(t);
    
    assert(!strcmp("FFAA6600", t=sctcd_base16_encode(((uint8_t[]) {0x00, 0x66, 0xAA, 0xFF}), 4, SCTCD_SINGLE)));
    sctcd_free(t);
    return 0;
}

#endif
