#define SCT__INTERNAL__
#include "sctcd.h"
#include "scta.h"

//HEADER
#ifndef SCTCD_PART_FREE
#define SCTCD_PART_FREE
void sctcd_free(void *pointer);

/*
sctdoc:sctcd_free:

NAME
    sctcd_free

SYNOPSIS
    void sctcd_free(void *pointer);
    
DESCRIPTION
    This function frees resources returned by functions from this module.

RETURN
    None

EXAMPLE
    None

SEE ALSO
    None
*/

#endif

//LIB
void sctcd_free(void *pointer){
    scta_free(pointer);
}


//TEST
#ifdef test_free

#include <assert.h>
#include <stdio.h>

int main() {
    sct_debug = stderr;
    sctcd_free(sctcd_base64_encode((uint8_t *) "hi", -1, 0));
    return 0;
}

#endif
