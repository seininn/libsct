#define SCT__INTERNAL__
#include "sctcd.h"
#include "scta.h"

//HEADER
#ifndef SCTCD_PART_BASE64_ENCODE
#define SCTCD_PART_BASE64_ENCODE
void* sctcd_base64_encode(uint8_t *mem, int length, int flags);

/*
sctdoc:sctcd_base64_encode:

NAME
    sctcd_base64_encode

SYNOPSIS
    void* sctcd_base64_encode(uint8_t *mem, int length, int flags);
    
DESCRIPTION
    This function encodes `length' number of bytes, starting at address 
    `mem', to their base64 representation. This function is based on 
    rfc4648 as published by the ieft (Oct. 2006).
    
    If `length' is -1, the function will then read from mem until the 
    first ocurrance of '\0', depending on the flags set.
    
    This function returns a pointer to `char' string by default, but can 
    return a char32_t if the appropreate flag is passed.
    
    The flags this function accepts are the following:
    
        SCTCD_WHITESPACE    Inserts a newline every 64 characters in the 
                            resulting encoding.
                            
        SCTCD_URL           Produces a base64 encoding as described by 
                            rfc4648 under the name base64url.
                            
        SCTCD_UTF8          Returns the encoded data as a `char' array. 
                            This is the default.
                            
        SCTCD_UTF32         Returns the encoded data as a `char32_t' 
                            array.
                            
NOTE
    1.  sctcd_length is not always accurate for the objects returned by 
        this function. use normal string length functions to determine 
        length. TODO
    2.  This function operates on byte arrays, and not on strings. While 
        -1 was added as a convenience for developers, it will not work 
        for char32_t characters.
    
RETURN
    This function returns a pointer of type `char *' or `char32_t *' cast
    as `void *' on success, or NULL on failure.
    
    If the returned value is not `NULL' it must be freed using `sctcd_free'.

EXAMPLE
    char *base64message = sctcd_base64_encode((uint8_t *)"Hello", 5, 0);
    puts(base64message); // out: SGVsbG8=
    sctcd_free(base64message);

SEE ALSO
    sctcd_base64_decode
    sccd_free
    sctcd_base16_encode
    sctcd_base2_encode
*/

#endif

//LIB
#include <string.h>
const char sctcd__base64_lookup[64] = {
    [0] = 'A',	  [16] = 'Q',	    [32] = 'g',	      [48] = 'w',
    [1] = 'B',	  [17] = 'R',	    [33] = 'h',	      [49] = 'x',
    [2] = 'C',	  [18] = 'S',	    [34] = 'i',	      [50] = 'y',
    [3] = 'D',	  [19] = 'T',	    [35] = 'j',	      [51] = 'z',
    [4] = 'E',	  [20] = 'U',	    [36] = 'k',	      [52] = '0',
    [5] = 'F',	  [21] = 'V',	    [37] = 'l',	      [53] = '1',
    [6] = 'G',	  [22] = 'W',	    [38] = 'm',	      [54] = '2',
    [7] = 'H',	  [23] = 'X',	    [39] = 'n',	      [55] = '3',
    [8] = 'I',	  [24] = 'Y',	    [40] = 'o',	      [56] = '4',
    [9] = 'J',	  [25] = 'Z',	    [41] = 'p',	      [57] = '5',
    [10] = 'K',	  [26] = 'a',	    [42] = 'q',	      [58] = '6',
    [11] = 'L',	  [27] = 'b',	    [43] = 'r',	      [59] = '7',
    [12] = 'M',	  [28] = 'c',	    [44] = 's',	      [60] = '8',
    [13] = 'N',	  [29] = 'd',	    [45] = 't',	      [61] = '9',
    [14] = 'O',	  [30] = 'e',	    [46] = 'u',	      [62] = '+',
    [15] = 'P',	  [31] = 'f',	    [47] = 'v',	      [63] = '/'
};

void* sctcd_base64_encode(uint8_t *mem, int length, int flags){
    char *out; // ouput string
    int omax = 0;
    int mcur = 0; // current position in `mem', must not exceed `length'
    int ocur = 0; // current position in `mem', must not exceed `omax'
    
    if (flags&SCTCD_SINGLE) ERETURN(NULL, "SCTCD_SINGLE is not supported by this function.");
    
    if (length < 0) {
        if (length == -1) length = strlen((char *)mem);
        else ERETURN(NULL, "bad length: must be a positive number or -1. Geven: %d", length);
    }
    
    
    // calculating required memory
    omax += length*(int)(4./3+4);
    omax += ((flags&SCTCD_WHITESPACE)?(omax/64+1):0);
    
    if (!(out = scta_new(char))) ERETURN(NULL, "^");
    
    // stretching array to required length    
    if (!scta_set_length(out, omax)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }
    
    
    while (true) {
        uint32_t reg = 0;
        int oct = 0;
        
        // read from buffer 3 bytes at most 
        for (int i = 0; (mcur+i)<length && i < 3; ++i) {
            reg |= mem[mcur+i]<<((2-i)*8);
            ++oct;
        }
        
        // End of buffer to encode
        if (oct == 0) break;
        
        // optional whitespace enabled by SCTCD_WHITESPACE
        if (flags&SCTCD_WHITESPACE && ocur && !(ocur%64)) scta_get(out, ocur++) = '\n';
        
        // encoding
        scta_get(out, ocur++) = sctcd__base64_lookup[(reg>>18)&0x3F];
        scta_get(out, ocur++) = sctcd__base64_lookup[(reg>>12)&0x3F];
        if (oct >= 2) scta_get(out, ocur++) = sctcd__base64_lookup[(reg>>6)&0x3F];
        if (oct == 3) scta_get(out, ocur++) = sctcd__base64_lookup[reg&0x3F];
        
        // if oct is less than 3, then adding padding
        if (oct==1) {
            scta_get(out, ocur++) = '=';
            scta_get(out, ocur++) = '=';
        }
        else if (oct==2) {
            scta_get(out, ocur++) = '=';
        }        
        
        if (ocur >= omax) {  // remove later if calculation is varified
            scta_free(out);
            ERETURN(NULL, "buffer overflow detected");
        }
        mcur+=oct;
    }
    
    if (flags&SCTCD_URL) for (int i = 0; out[i]; ++i) {
        if (out[i] == '+') out[i] = '-';
        if (out[i] == '/') out[i] = '_';
    }
    
    if (flags&SCTCD_UTF32) { //TODO switch to char32_t when you switch to a newer compiler
        uint32_t *u32;
        if (!(u32 = scta_new(uint32_t))) {
            scta_free(out);
            ERETURN(NULL, "^");
        }
        if (!scta_set_length(u32, scta_length(out))) {
            scta_free(out);
            scta_free(u32);
            ERETURN(NULL, "^");        
        }
        for (int i = 0; out[i]; ++i) u32[i] = out[i];
        scta_free(out);
        return u32;
    }
    return out;
}


//TEST
#ifdef test_base64_encode

#include <assert.h>
#include <stdio.h>
#include <string.h>

int main() {
    sct_debug = stderr;
    {
        char *t;
        uint32_t *u; // TODO swtich to char32_t with new compiler
        assert(u=sctcd_base64_encode((uint8_t *) "", 0, SCTCD_UTF32));
        assert(!u[0]);
        sctcd_free(u);
        
        assert(t=sctcd_base64_encode((uint8_t *) "\0", 1, SCTCD_UTF8));
        assert(!strcmp(t, "AA=="));
        sctcd_free(t);
        
        assert(u=sctcd_base64_encode((uint8_t *) "\0", 1, SCTCD_UTF32));
        assert(u[0] == 'A');
        assert(u[1] == 'A');
        assert(u[2] == '=');
        assert(u[3] == '=');
        sctcd_free(u);
        
        assert(t=sctcd_base64_encode((uint8_t []) {0xff, 0xff, 0xff} , 3, 0));
        assert(!strcmp(t, "////"));
        sctcd_free(t);
        
        assert(t=sctcd_base64_encode((uint8_t []) {0xff, 0xff, 0xff} , 3, SCTCD_URL));
        assert(!strcmp(t, "____"));
        sctcd_free(t);
    }
    
    {
        char *t;
        assert(t=sctcd_base64_encode((uint8_t *)"", -1, 0)); assert(!strcmp(t, "")); sctcd_free(t);
        assert(t=sctcd_base64_encode((uint8_t *)"f", -1, 0)); assert(!strcmp(t, "Zg==")); sctcd_free(t);
        assert(t=sctcd_base64_encode((uint8_t *)"fo", -1, 0)); assert(!strcmp(t, "Zm8=")); sctcd_free(t);
        assert(t=sctcd_base64_encode((uint8_t *)"foo", -1, 0)); assert(!strcmp(t, "Zm9v")); sctcd_free(t);
        assert(t=sctcd_base64_encode((uint8_t *)"foob", -1, 0)); assert(!strcmp(t, "Zm9vYg==")); sctcd_free(t);
        assert(t=sctcd_base64_encode((uint8_t *)"fooba", -1, 0)); assert(!strcmp(t, "Zm9vYmE=")); sctcd_free(t);
        assert(t=sctcd_base64_encode((uint8_t *)"foobar", -1, 0)); assert(!strcmp(t, "Zm9vYmFy")); sctcd_free(t);
        assert(t=sctcd_base64_encode((uint8_t *)"Man", -1, 0)); assert(!strcmp(t, "TWFu")); sctcd_free(t);
        assert(t=sctcd_base64_encode((uint8_t *)"hi my name is bob. bob the concrete. "
                                            "I like potatos and tomatos. It./s true!"
                                            " I KID YOU NOT./", -1, SCTCD_WHITESPACE)); 
        assert(!strcmp(t, "aGkgbXkgbmFtZSBpcyBib2IuIGJvYiB0aGUgY29uY3JldGUuIEkgbGlrZSBwb3Rh\n"
                          "dG9zIGFuZCB0b21hdG9zLiBJdC4vcyB0cnVlISBJIEtJRCBZT1UgTk9ULi8=")); 
        sctcd_free(t);
    }
    
    return 0;
}

#endif
