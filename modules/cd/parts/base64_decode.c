#define SCT__INTERNAL__
#include "sctcd.h"
#include "scta.h"

//HEADER
#ifndef SCTCD_PART_BASE64_DECODE
#define SCTCD_PART_BASE64_DECODE

#define sctcd_base64_decode(list) (_Generic( (0, (list)), \
    uint32_t *:       sctcd_base64_decode_u, \
    const uint32_t *: sctcd_base64_decode_u, \
    char *:       sctcd_base64_decode_c, \
    const char *: sctcd_base64_decode_c \
    )(list)) //TODO char32_t
    
uint8_t* sctcd_base64_decode_c(char *input);
uint8_t* sctcd_base64_decode_u(uint32_t *input); //TODO fix this mess char32_t

/*
sctdoc:sctcd_base64_decode:

NAME
    sctcd_base64_decode

SYNOPSIS
    uint8_t* sctcd_base64_decode([char *][char32_t *] string)
    
    uint8_t* sctcd_base64_decode_c(char *string);
    uint8_t* sctcd_base64_decode_u(uint32_t *string);
    
DESCRIPTION
    This function decodes base64 and base64url strings and returns a
    byte array as the result. The length of the decoded data can be 
    determined using sctcd_length.
    
RETURN
    This function returns a byte array as a result or `NULL' on failure.

EXAMPLE
    char *base64message = (char *) sctcd_base64_decode("SGVsbG8=");
    puts(base64message); // hello
    sctcd_free(base64message);
}

SEE ALSO
    sctcd_length
    sctcd_base64_encode
*/

#endif

//LIB
int sctcd__base64_reverse_lookup[0xff] = {
    ['A'] = 0,    ['N'] = 13,   ['a'] = 26,	  ['n'] = 39,	['0'] = 52,
    ['B'] = 1,    ['O'] = 14,   ['b'] = 27,	  ['o'] = 40,	['1'] = 53,
    ['C'] = 2,    ['P'] = 15,   ['c'] = 28,	  ['p'] = 41,	['2'] = 54,
    ['D'] = 3,    ['Q'] = 16,   ['d'] = 29,	  ['q'] = 42,	['3'] = 55,
    ['E'] = 4,    ['R'] = 17,   ['e'] = 30,	  ['r'] = 43,	['4'] = 56,
    ['F'] = 5,    ['S'] = 18,   ['f'] = 31,	  ['s'] = 44,	['5'] = 57,
    ['G'] = 6,    ['T'] = 19,   ['g'] = 32,	  ['t'] = 45,	['6'] = 58,
    ['H'] = 7,    ['U'] = 20,   ['h'] = 33,	  ['u'] = 46,	['7'] = 59,
    ['I'] = 8,    ['V'] = 21,   ['i'] = 34,	  ['v'] = 47,	['8'] = 60,
    ['J'] = 9,    ['W'] = 22,   ['j'] = 35,	  ['w'] = 48,	['9'] = 61,
    ['K'] = 10,   ['X'] = 23,   ['k'] = 36,	  ['x'] = 49,	['+'] = 62,
    ['L'] = 11,   ['Y'] = 24,   ['l'] = 37,	  ['y'] = 50,	['/'] = 63,
    ['M'] = 12,   ['Z'] = 25,   ['m'] = 38,	  ['z'] = 51,   ['-'] = 62, 
                                                            ['_'] = 63
};
#include <string.h>
uint8_t* sctcd_base64_decode_c(char *input) {
    if (!input) ERETURN(NULL, "NULL input");
        
    int length = strlen(input);
    int olength = 0;
    uint8_t *out = scta_new(uint8_t);
    int ocur = 0;
    int icur = 0;
    
    if (!out) ERETURN(NULL, "^");
    if (!scta_set_length(out, length)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }
    
    // preforms better with validation out of the main loop (probably due to 
    // cpu catches and branch pridiction: do cachegrind on it to messure cache misses and see if branch pridiction can be messured)
    // Two O(2n) is faster than O(n).. something very wrong is happending for O(n)
    // LOOP 
    for (int i = 0; input[i]; ++i) {
        if (!(
            ((input[i] >= 'A') && (input[i] <= 'Z')) ||
            ((input[i] >= 'a') && (input[i] <= 'z')) ||
            ((input[i] >= '0') && (input[i] <= '9')) ||
            ((input[i] == '+') || (input[i] == '-')) ||
            ((input[i] == '/') || (input[i] == '_')) || (input[i] == '=')
        )) {
            scta_free(out);
            ERETURN(NULL, "invalid base64 characters in input: %c (%d) @ %d", input[icur+i], input[icur+i], icur+i);
        }
    }
    
    while (true){
        uint32_t reg = 0;
        uint32_t oct = 0;
        
        for (int i = 0; ((icur+i)<length && input[icur+i] != '=') && i < 4; ++i) {
            reg |= sctcd__base64_reverse_lookup[(int)input[icur+i]]<<((3-i)*6);
            ++oct;
        }
        
        // End of buffer to decode
        if (oct == 0) break;
        
        // decoding
        scta_get(out, ocur++) = (reg>>16)&0xFF;
        scta_get(out, ocur++) = (reg>>8)&0xFF;
        scta_get(out, ocur++) = reg&0xFF;
        olength += 3;
        // if last read character is a =
        if (oct < 4) {
            olength -= 4-oct;
            break;
        }
        icur+=oct;
    }
    if (!scta_set_length(out, olength)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }    
    return out;
}

// copyed and pasted..
uint8_t* sctcd_base64_decode_u(uint32_t *input) {
    if (!input) ERETURN(NULL, "NULL input");
        
    int length = 0; 
    while (input[length++]); 
    --length;
        
    int olength = 0;
    uint8_t *out = scta_new(uint8_t);
    int ocur = 0;
    int icur = 0;
    
    if (!out) ERETURN(NULL, "^");
    if (!scta_set_length(out, length)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }

    for (int i = 0; input[i]; ++i) {
        if (!(
            ((input[i] >= 'A') && (input[i] <= 'Z')) ||
            ((input[i] >= 'a') && (input[i] <= 'z')) ||
            ((input[i] >= '0') && (input[i] <= '9')) ||
            ((input[i] == '+') || (input[i] == '-')) ||
            ((input[i] == '/') || (input[i] == '_')) || (input[i] == '=')
        )) {
            scta_free(out);
            ERETURN(NULL, "invalid base64 characters in input: %c (%d) @ %d", input[icur+i], input[icur+i], icur+i);
        }
    }
        
    while (true){
        uint32_t reg = 0;
        uint32_t oct = 0;
        
        for (int i = 0; ((icur+i)<length && input[icur+i] != '=') && i < 4; ++i) {
            reg |= sctcd__base64_reverse_lookup[(int)input[icur+i]]<<((3-i)*6);
            ++oct;
        }
        
        // End of buffer to decode
        if (oct == 0) break;
        
        // decoding
        scta_get(out, ocur++) = (reg>>16)&0xFF;
        scta_get(out, ocur++) = (reg>>8)&0xFF;
        scta_get(out, ocur++) = reg&0xFF;
        olength += 3;
        // if last read character is a =
        if (oct < 4) {
            olength -= 4-oct;
            break;
        }
        icur+=oct;
    }
    if (!scta_set_length(out, olength)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }    
    return out;
}


//TEST
#ifdef test_base64_decode

#include <assert.h>
#include <stdio.h>

int main() {
    sct_debug = stderr;
    uint8_t *b;
    
    assert(!strcmp((char *)(b=sctcd_base64_decode("Zm9vYmFy")), "foobar"));
    assert(sctcd_length(b) == 6);
    sctcd_free(b);
    
    assert(!strcmp((char *)(b=sctcd_base64_decode(((uint32_t[]){'Z', 'm', '9', 'v', 'Y', 'm', 'F', 'y', 0}))), "foobar")); //TODO -> char32_t
    assert(sctcd_length(b) == 6);
    sctcd_free(b);
    
    sct_debug = NULL;
    assert(!sctcd_base64_decode("Zm9vYm)y"));
    sct_debug = stderr;

    assert(!strcmp((char *)(b=sctcd_base64_decode("Zm9vYmE=")), "fooba"));
    assert(sctcd_length(b) == 5);
    sctcd_free(b);    
    
    assert(!strcmp((char *)(b=sctcd_base64_decode(((uint32_t[]){'Z', 'm', '9', 'v', 'Y', 'm', 'E', '=', '0'}))), "fooba")); //TODO -> char32_t
    assert(sctcd_length(b) == 5);
    sctcd_free(b);    
    
    assert(!strcmp((char *)(b=sctcd_base64_decode("Zm9vYg==")), "foob"));
    assert(sctcd_length(b) == 4);
    sctcd_free(b);        

    assert(!strcmp((char *)(b=sctcd_base64_decode("")), ""));
    assert(sctcd_length(b) == 0);
    sctcd_free(b);    
    
    assert(!strcmp((char *)(b=sctcd_base64_decode("AAA=")), ""));
    assert(sctcd_length(b) == 2);
    sctcd_free(b);
    
    assert(!strcmp((char *)(b=sctcd_base64_decode("AA==")), ""));
    assert(sctcd_length(b) == 1);
    sctcd_free(b);

    assert(b=sctcd_base64_decode("__=="));
    assert( b[0] == 0xff);
    assert(sctcd_length(b) == 1);
    sctcd_free(b);
    
    assert(b=sctcd_base64_decode("___="));
    assert( b[0] == 0xff);
    assert( b[1] == 0xff);
    assert(sctcd_length(b) == 2);
    sctcd_free(b);

    // TODO test url chaeracters _ and -
    // TODO (int8_t[]) ( ' ', '...)
    
    return 0;
}

#endif
