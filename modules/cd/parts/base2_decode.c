#define SCT__INTERNAL__
#include "sctcd.h"
#include "scta.h"

//HEADER
#ifndef SCTCD_PART_BASE2_DECODE
#define SCTCD_PART_BASE2_DECODE
uint8_t* sctcd_base2_decode_c(char *string);

// TODO -> char32_t;
#define sctcd_base2_decode(list) (_Generic( (0, (list)), \
    uint32_t *:       sctcd_base2_decode_u, \
    const uint32_t *: sctcd_base2_decode_u, \
    char *:       sctcd_base2_decode_c, \
    const char *: sctcd_base2_decode_c \
    )(list))


/*
sctdoc:sctcd_base2_decode:

NAME
    sctcd_base2_decode

SYNOPSIS
    uint8_t* sctcd_base2_decode([char *][char32_t *] string)
    
    uint8_t* sctcd_base2_decode_c(char *string);
    uint8_t* sctcd_base2_decode_u(uint32_t *string);
    
DESCRIPTION
    This function decodes base2 strings and returns a byte array as the 
    result. The length of the decoded data can be determined using 
    sctcd_length.
    
RETURN
    This function returns a byte array as a result or `NULL' on failure.

EXAMPLE
    char *t = (char *) sctcd_base2_decode(  "01001000011001010110"
                                            "11000110110001101111"));
                                            // "Hello"
    sctcd_length(t); // == 5
    sctcd_free(t); 

SEE ALSO
    sctcd_length
    sctcd_base2_encode
*/

#endif

//LIB
#include <string.h>
uint8_t* sctcd_base2_decode_c(char *string){
    uint8_t *out; // ouput string
    int omax;
    int ocur = 0;
    
    int length = strlen(string);
    
    // calculating required memory
    if (length%8) ERETURN(NULL, "incomplete base2 string: not a multiple of 8");
    omax = length/8;
    
    if (!(out = scta_new(uint8_t))) ERETURN(NULL, "^");
    
    // stretching array to required length
    if (!scta_set_length(out, omax)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }
    for (int i = 0; i < length; i+=1) if (string[i] != '0' && string[i] != '1') {
        scta_free(out);
        ERETURN(NULL, "invalid character encoundered in base2 sequence: %c", string[i]);        
    }
    
    for (int i = 0; i < length; i+=8) {
        scta_get(out, ocur++) = ((string[i])<<7) | 
                      ((string[i+1]-'0')<<6) | 
                      ((string[i+2]-'0')<<5) | 
                      ((string[i+3]-'0')<<4) | 
                      ((string[i+4]-'0')<<3) | 
                      ((string[i+5]-'0')<<2) | 
                      ((string[i+6]-'0')<<1) | 
                      ((string[i+7]-'0'));
        
    }
    if (ocur >= omax+1) { // remove later if calculation is varified / to accomidate the extra ++
        scta_free(out);
        ERETURN(NULL, "buffer overflow detected");
    }
    return out;
}

uint8_t* sctcd_base2_decode_u(uint32_t *string){ // TODO -> char32_t;
    uint8_t *out; // ouput string
    int omax;
    int ocur = 0;
    
    int length = 0;
    while (string[length++]); // TODO -> char32_t; fix
    --length;
    
    // calculating required memory
    if (length%8) ERETURN(NULL, "incomplete base2 string: not a multiple of 8");
    omax = length/8;
    
    if (!(out = scta_new(uint8_t))) ERETURN(NULL, "^");
    
    // stretching array to required length
    if (!scta_set_length(out, omax)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }
    for (int i = 0; i < length; i+=1) if (string[i] != '0' && string[i] != '1') {
        scta_free(out);
        ERETURN(NULL, "invalid character encoundered in base2 sequence: %c", string[i]);        
    }
    
    for (int i = 0; i < length; i+=8) {
        scta_get(out, ocur++) = ((string[i])<<7) | 
                      ((string[i+1]-'0')<<6) | 
                      ((string[i+2]-'0')<<5) | 
                      ((string[i+3]-'0')<<4) | 
                      ((string[i+4]-'0')<<3) | 
                      ((string[i+5]-'0')<<2) | 
                      ((string[i+6]-'0')<<1) | 
                      ((string[i+7]-'0'));
    }
    if (ocur >= omax+1) { // remove later if calculation is varified / to accomidate the extra ++
        scta_free(out);
        ERETURN(NULL, "buffer overflow detected");
    }
    return out;
}


//TEST
#ifdef test_base2_decode
#include <string.h>
#include <assert.h>
#include <stdio.h>

int main() {
    sct_debug = stderr;
    char *t;
    assert(t = (char *)sctcd_base2_decode("0100100001100101011011000110110001101111"));
    assert(strcmp(t, "Hello") == 0);
    assert(sctcd_length(t) == 5);
    sctcd_free(t); 
    
    assert(t = (char *)sctcd_base2_decode("00000000000000000000000000000000"));
    assert(t[0] == 0);
    assert(t[2] == 0);
    assert(t[3] == 0);
    assert(t[4] == 0);
    assert(sctcd_length(t) == 4);
    sctcd_free(t); 
    
    sct_debug = NULL;
    assert(!sctcd_base2_decode("0000000000000000000000000000000"));
    assert(!sctcd_base2_decode("00000000000000000000200000000000"));
    sct_debug = stderr;

    assert(t = (char *)sctcd_base2_decode(((uint32_t[]) {'0', '1', '0', '0', '1', '0', '0', '0', '0', '1', '1', '0', '0', '1', '0', '1', '0', '1', '1', '0', '1', '1', '0', '0', '0', '1', '1', '0', '1', '1', '0', '0', '0', '1', '1', '0', '1', '1', '1', '1', 0}))); //TODO -> char32_t
    assert(strcmp(t, "Hello") == 0);
    assert(sctcd_length(t) == 5); 
    sctcd_free(t); 
    
    return 0;
}

#endif
