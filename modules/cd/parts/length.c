#define SCT__INTERNAL__
#include "sctcd.h"
#include "scta.h"

//HEADER
#ifndef SCTCD_PART_LENGTH
#define SCTCD_PART_LENGTH
int sctcd_length(void *pointer);

/*
sctdoc:sctcd_length:

NAME
    sctcd_length

SYNOPSIS
    int sctcd_length(void *pointer);
    
DESCRIPTION
    This function returns the length of arrays returned by function from 
    this module.

RETURN
    The length of the array if succesful or -1 on failure.

EXAMPLE
    None

SEE ALSO
    None
*/

#endif

//LIB
int sctcd_length(void *pointer){
    return scta_length(pointer);
}


//TEST
#ifdef test_length

#include <assert.h>
#include <stdio.h>
#include <string.h>

int main() {
    sct_debug = stderr;
    char *base64message = (char *) sctcd_base64_decode("SGVsbG8=");
    assert(strlen(base64message) == 5);
    sctcd_free(base64message);
    return 0;
}

#endif
