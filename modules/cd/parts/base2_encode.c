#define SCT__INTERNAL__
#include "sctcd.h"
#include "scta.h"

//HEADER
#ifndef SCTCD_PART_BASE2_ENCODE
#define SCTCD_PART_BASE2_ENCODE
void* sctcd_base2_encode(uint8_t *mem, int length, int flags);

/*
sctdoc:sctcd_base2_encode:

NAME
    sctcd_base2_encode

SYNOPSIS
    void* sctcd_base2_encode(uint8_t *mem, int length, int flags);
    
DESCRIPTION
    This function encodes `length' number of bytes, starting at address 
    `mem', to their base2 representation.
    
    If `length' is -1, the function will then read from mem until the 
    first ocurrance of '\0', depending on the flags set.
    
    This function returns a pointer to `char' string by default, but can 
    return a char32_t if the appropreate flag is passed.
    
    The flags this function accepts are the following:
    
        SCTCD_WHITESPACE    Inserts a space between every byte and a 
                            newline every 8 bytes.
        
        SCTCD_SINGLE        `mem' points to a single number spanning 
                            `length' bytes and not `length' number of 
                            indivsual bytes.
                            
        SCTCD_UTF8          Returns the encoded data as a `char' array. 
                            This is the default.
                            
        SCTCD_UTF32         Returns the encoded data as a `char32_t' 
                            array.

NOTE
    1.  sctcd_length is not always accurate for the objects returned by 
        this function. use normal string length functions to determine 
        length. TODO
    2.  This function operates on byte arrays, and not on strings. While 
        -1 was added as a convenience for developers, it will not work 
        for char32_t characters.
        
RETURN
    This function returns a pointer of type `char *' or `char32_t *' cast
    as `void *' on success, or NULL on failure.
    
    If the returned value is not `NULL' it must be freed using `sctcd_free'.

EXAMPLE
    char *base2message = sctcd_base2_encode((uint8_t *)"Hello", 5, 0);
    puts(base2message); // out: 0100100001100101011011000110110001101111
    sctcd_free(base2message);  

SEE ALSO
    sctcd_base2_decode
    sctcd_free
    sctcd_base16_encode
    sctcd_base64_encode
    
*/

#endif

//LIB
#include <string.h>
void* sctcd_base2_encode(uint8_t *mem, int length, int flags){
    char *out; // ouput string
    int omax = 0;
    int ocur = 0;
    
    // autolength
    if (length < 0) {
        if (length == -1) length = strlen((char *)mem);
        else ERETURN(NULL, "bad length: must be a positive number or -1. Geven: %d", length);
    }
    
    // calculating required memory
    omax += length*8; // eight characters per byte
    omax += ((flags&SCTCD_WHITESPACE)?(omax/8+1):0)+length;
    
    if (!(out = scta_new(char))) ERETURN(NULL, "^");
    
    // stretching array to required length
    if (!scta_set_length(out, omax)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }
    
    
    if (flags&SCTCD_SINGLE && SCTCD__LITTLE_ENDIAN) for (int i = length-1; i >= 0; --i) {
        if (flags&SCTCD_WHITESPACE && (i != (length -1))) {
            if (!(i%8)) scta_get(out, ocur++) = '\n';
            else scta_get(out, ocur++) = ' ';
        }
        
        scta_get(out, ocur++) = '0'+((mem[i]&0x80)>>7);
        scta_get(out, ocur++) = '0'+((mem[i]&0x40)>>6);
        scta_get(out, ocur++) = '0'+((mem[i]&0x20)>>5);
        scta_get(out, ocur++) = '0'+((mem[i]&0x10)>>4);
        scta_get(out, ocur++) = '0'+((mem[i]&0x08)>>3);
        scta_get(out, ocur++) = '0'+((mem[i]&0x04)>>2);
        scta_get(out, ocur++) = '0'+((mem[i]&0x02)>>1);
        scta_get(out, ocur++) = '0'+((mem[i]&0x01));
    }
    else for (int i = 0; i < length; ++i) {
        if (flags&SCTCD_WHITESPACE && i) {
            if (!(i%8)) scta_get(out, ocur++) = '\n';
            else scta_get(out, ocur++) = ' ';
        }
        
        scta_get(out, ocur++) = '0'+((mem[i]&0x80)>>7);
        scta_get(out, ocur++) = '0'+((mem[i]&0x40)>>6);
        scta_get(out, ocur++) = '0'+((mem[i]&0x20)>>5);
        scta_get(out, ocur++) = '0'+((mem[i]&0x10)>>4);
        scta_get(out, ocur++) = '0'+((mem[i]&0x08)>>3);
        scta_get(out, ocur++) = '0'+((mem[i]&0x04)>>2);
        scta_get(out, ocur++) = '0'+((mem[i]&0x02)>>1);
        scta_get(out, ocur++) = '0'+((mem[i]&0x01));
    }
    
    if (ocur >= omax) {
            scta_free(out);
            ERETURN(NULL, "buffer overflow detected");
    }
    
    if (flags&SCTCD_UTF32) {
        uint32_t *u32; //TODO -> char32_t
        if (!(u32 = scta_new(uint32_t))) {
            scta_free(out);
            ERETURN(NULL, "^");
        }
        if (!scta_set_length(u32, scta_length(out))) {
            scta_free(out);
            scta_free(u32);
            ERETURN(NULL, "^");        
        }
        for (int i = 0; out[i]; ++i) u32[i] = out[i];
        scta_free(out);
        return u32;
    }

    return out;
}


//TEST
#ifdef test_base2_encode

#include <assert.h>
#include <string.h>
#include <stdio.h>

int main() {
    sct_debug = stderr;
    char *t;
    uint32_t *u;
    assert(t=sctcd_base2_encode((uint8_t *) "Man4567890", 10, 0));
    assert(!strcmp(t, "01001101011000010110111000110100001101010011011000110111001110000011100100110000"));
    sctcd_free(t);

    assert(t=sctcd_base2_encode((uint8_t *) ((uint16_t[1]){0xFF00}), 2, SCTCD_SINGLE)); // This test is meaningless on big endian systems
    assert(!strcmp(t, "1111111100000000"));
    sctcd_free(t);
    
    assert(u=sctcd_base2_encode((uint8_t *) "M", 1, SCTCD_UTF32));
    assert(u[0] == '0');
    assert(u[1] == '1');
    assert(u[2] == '0');
    assert(u[3] == '0');
    assert(u[4] == '1');
    assert(u[5] == '1');
    assert(u[6] == '0');
    assert(u[7] == '1');
    assert(!u[8]);
    sctcd_free(u);
    
    assert(t=sctcd_base2_encode((uint8_t *) "Man4567890", 10, SCTCD_UTF8|SCTCD_WHITESPACE));
    assert(!strcmp(t,   "01001101 01100001 01101110 00110100 00110101 00110110 00110111 00111000\n"
                        "00111001 00110000"));
    sctcd_free(t);
    
    return 0;
}

#endif
