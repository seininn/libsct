#ifndef SCTCD_H
#define SCTCD_H

// STATIC SECTION
/*
sctdoc:sctcd:

    sctcd: A generic codec module

    Sulaiman (seininn) Mustafa     2014-06-09
    
    This module is a part of libstc, a modular, c11-compliant, 
    toolkit library. use stcmod to determine this module's 
    dependencies, if any.
    
    -----------------------------------------------------------------------
    
    EXAMPLES
    
    API
    
    DEBUGING
    
    SCT offers a verbose mode as a debuging aid. To enable this mode, set 
    sct_debug_fd to where want the verbose output to be written to (stderr,
    fopen()).
    
*/

#include <stddef.h>
#include <stdio.h>
#include <stdint.h>
#include "sctstd.h"


#define SCTCD__LITTLE_ENDIAN (((uint8_t *)((uint16_t[]) {0xFFAA}))[0] == 0xAA)



#define SCTCD_WHITESPACE    0x1
#define SCTCD_URL           0x2
#define SCTCD_SINGLE        0x4
#define SCTCD_UTF8          0x1000
#define SCTCD_UTF32         0x2000


// PARTIALS SECTION: AUTOMATICLY GENERATED
//--base16_decode
#ifndef SCTCD_PART_BASE16_DECODE
#define SCTCD_PART_BASE16_DECODE
uint8_t* sctcd_base16_decode_c(char *string);
#define sctcd_base16_decode(list) (_Generic( (0, (list)), \
    uint32_t *:       sctcd_base16_decode_u, \
    const uint32_t *: sctcd_base16_decode_u, \
    char *:       sctcd_base16_decode_c, \
    const char *: sctcd_base16_decode_c \
    )(list))
    
/*
sctdoc:sctcd_base16_decode:

NAME
    sctcd_base16_decode

SYNOPSIS
    uint8_t* sctcd_base16_decode([char *][char32_t *] string)
    
    uint8_t* sctcd_base16_decode_c(char *string);
    uint8_t* sctcd_base16_decode_u(uint32_t *string);
    
DESCRIPTION
    This function decodes base16 (hexadecimal) strings and returns a
    byte array as the result. The length of the decoded data can be 
    determined using sctcd_length.
    
RETURN
    This function returns a byte array as a result or `NULL' on failure.

EXAMPLE
    char *base16message = (char *) sctcd_base16_decode("68656C6C6F");
    puts(base16message); // hello
    sctcd_free(base16message);
}

SEE ALSO
    sctcd_length
    sctcd_base16_encode
*/

#endif


    
//--base16_encode
#ifndef SCTCD_PART_BASE16_ENCODE
#define SCTCD_PART_BASE16_ENCODE
void* sctcd_base16_encode(uint8_t *mem, int length, int flags);

/*
sctdoc:sctcd_base616_encode:

NAME
    sctcd_base16_encode

SYNOPSIS
    void* sctcd_base16_encode(uint8_t *mem, int length, int flags);
    
DESCRIPTION
    This function encodes `length' number of bytes, starting at address 
    `mem', to their base16 (hexadecimel) notation.
    
    If `length' is -1, the function will then read from mem until the 
    first ocurrance of '\0', depending on the flags set.
    
    This function returns a pointer to `char' string by default, but can 
    return a char32_t if the appropreate flag is passed.
    
    The flags this function accepts are the following:
    
        SCTCD_SINGLE        `mem' points to a single number spanning 
                            `length' bytes and not `length' number of 
                            indivsual bytes.
                            
        SCTCD_UTF32         Returns the encoded data as a `char32_t' 
                            array.
                            
NOTE
    1.  sctcd_length is not always accurate for the objects returned by 
        this function. use normal string length functions to determine 
        length. TODO
    2.  This function operates on byte arrays, and not on strings. While 
        -1 was added as a convenience for developers, it will not work 
        for char32_t characters.
    
RETURN
    This function returns a pointer of type `char *' or `char32_t *' cast
    as `void *' on success, or NULL on failure.
    
    If the returned value is not `NULL' it must be freed using `sctcd_free'.

EXAMPLE
    char *base16message = sctcd_base16_encode((uint8_t *)"Hello", 5, 0);
    puts(base16message); // out: 68656C6C6F
    sctcd_free(base16message);

SEE ALSO
    sctcd_base16_decode
    sccd_free
    sctcd_base64_encode
    sctcd_base2_encode
*/

#endif


    
//--base2_decode
#ifndef SCTCD_PART_BASE2_DECODE
#define SCTCD_PART_BASE2_DECODE
uint8_t* sctcd_base2_decode_c(char *string);

// TODO -> char32_t;
#define sctcd_base2_decode(list) (_Generic( (0, (list)), \
    uint32_t *:       sctcd_base2_decode_u, \
    const uint32_t *: sctcd_base2_decode_u, \
    char *:       sctcd_base2_decode_c, \
    const char *: sctcd_base2_decode_c \
    )(list))


/*
sctdoc:sctcd_base2_decode:

NAME
    sctcd_base2_decode

SYNOPSIS
    uint8_t* sctcd_base2_decode([char *][char32_t *] string)
    
    uint8_t* sctcd_base2_decode_c(char *string);
    uint8_t* sctcd_base2_decode_u(uint32_t *string);
    
DESCRIPTION
    This function decodes base2 strings and returns a byte array as the 
    result. The length of the decoded data can be determined using 
    sctcd_length.
    
RETURN
    This function returns a byte array as a result or `NULL' on failure.

EXAMPLE
    char *t = (char *) sctcd_base2_decode(  "01001000011001010110"
                                            "11000110110001101111"));
                                            // "Hello"
    sctcd_length(t); // == 5
    sctcd_free(t); 

SEE ALSO
    sctcd_length
    sctcd_base2_encode
*/

#endif


    
//--base2_encode
#ifndef SCTCD_PART_BASE2_ENCODE
#define SCTCD_PART_BASE2_ENCODE
void* sctcd_base2_encode(uint8_t *mem, int length, int flags);

/*
sctdoc:sctcd_base2_encode:

NAME
    sctcd_base2_encode

SYNOPSIS
    void* sctcd_base2_encode(uint8_t *mem, int length, int flags);
    
DESCRIPTION
    This function encodes `length' number of bytes, starting at address 
    `mem', to their base2 representation.
    
    If `length' is -1, the function will then read from mem until the 
    first ocurrance of '\0', depending on the flags set.
    
    This function returns a pointer to `char' string by default, but can 
    return a char32_t if the appropreate flag is passed.
    
    The flags this function accepts are the following:
    
        SCTCD_WHITESPACE    Inserts a space between every byte and a 
                            newline every 8 bytes.
        
        SCTCD_SINGLE        `mem' points to a single number spanning 
                            `length' bytes and not `length' number of 
                            indivsual bytes.
                            
        SCTCD_UTF8          Returns the encoded data as a `char' array. 
                            This is the default.
                            
        SCTCD_UTF32         Returns the encoded data as a `char32_t' 
                            array.

NOTE
    1.  sctcd_length is not always accurate for the objects returned by 
        this function. use normal string length functions to determine 
        length. TODO
    2.  This function operates on byte arrays, and not on strings. While 
        -1 was added as a convenience for developers, it will not work 
        for char32_t characters.
        
RETURN
    This function returns a pointer of type `char *' or `char32_t *' cast
    as `void *' on success, or NULL on failure.
    
    If the returned value is not `NULL' it must be freed using `sctcd_free'.

EXAMPLE
    char *base2message = sctcd_base2_encode((uint8_t *)"Hello", 5, 0);
    puts(base2message); // out: 0100100001100101011011000110110001101111
    sctcd_free(base2message);  

SEE ALSO
    sctcd_base2_decode
    sctcd_free
    sctcd_base16_encode
    sctcd_base64_encode
    
*/

#endif


    
//--base64_decode
#ifndef SCTCD_PART_BASE64_DECODE
#define SCTCD_PART_BASE64_DECODE

#define sctcd_base64_decode(list) (_Generic( (0, (list)), \
    uint32_t *:       sctcd_base64_decode_u, \
    const uint32_t *: sctcd_base64_decode_u, \
    char *:       sctcd_base64_decode_c, \
    const char *: sctcd_base64_decode_c \
    )(list)) //TODO char32_t
    
uint8_t* sctcd_base64_decode_c(char *input);
uint8_t* sctcd_base64_decode_u(uint32_t *input); //TODO fix this mess char32_t

/*
sctdoc:sctcd_base64_decode:

NAME
    sctcd_base64_decode

SYNOPSIS
    uint8_t* sctcd_base64_decode([char *][char32_t *] string)
    
    uint8_t* sctcd_base64_decode_c(char *string);
    uint8_t* sctcd_base64_decode_u(uint32_t *string);
    
DESCRIPTION
    This function decodes base64 and base64url strings and returns a
    byte array as the result. The length of the decoded data can be 
    determined using sctcd_length.
    
RETURN
    This function returns a byte array as a result or `NULL' on failure.

EXAMPLE
    char *base64message = (char *) sctcd_base64_decode("SGVsbG8=");
    puts(base64message); // hello
    sctcd_free(base64message);
}

SEE ALSO
    sctcd_length
    sctcd_base64_encode
*/

#endif


    
//--base64_encode
#ifndef SCTCD_PART_BASE64_ENCODE
#define SCTCD_PART_BASE64_ENCODE
void* sctcd_base64_encode(uint8_t *mem, int length, int flags);

/*
sctdoc:sctcd_base64_encode:

NAME
    sctcd_base64_encode

SYNOPSIS
    void* sctcd_base64_encode(uint8_t *mem, int length, int flags);
    
DESCRIPTION
    This function encodes `length' number of bytes, starting at address 
    `mem', to their base64 representation. This function is based on 
    rfc4648 as published by the ieft (Oct. 2006).
    
    If `length' is -1, the function will then read from mem until the 
    first ocurrance of '\0', depending on the flags set.
    
    This function returns a pointer to `char' string by default, but can 
    return a char32_t if the appropreate flag is passed.
    
    The flags this function accepts are the following:
    
        SCTCD_WHITESPACE    Inserts a newline every 64 characters in the 
                            resulting encoding.
                            
        SCTCD_URL           Produces a base64 encoding as described by 
                            rfc4648 under the name base64url.
                            
        SCTCD_UTF8          Returns the encoded data as a `char' array. 
                            This is the default.
                            
        SCTCD_UTF32         Returns the encoded data as a `char32_t' 
                            array.
                            
NOTE
    1.  sctcd_length is not always accurate for the objects returned by 
        this function. use normal string length functions to determine 
        length. TODO
    2.  This function operates on byte arrays, and not on strings. While 
        -1 was added as a convenience for developers, it will not work 
        for char32_t characters.
    
RETURN
    This function returns a pointer of type `char *' or `char32_t *' cast
    as `void *' on success, or NULL on failure.
    
    If the returned value is not `NULL' it must be freed using `sctcd_free'.

EXAMPLE
    char *base64message = sctcd_base64_encode((uint8_t *)"Hello", 5, 0);
    puts(base64message); // out: SGVsbG8=
    sctcd_free(base64message);

SEE ALSO
    sctcd_base64_decode
    sccd_free
    sctcd_base16_encode
    sctcd_base2_encode
*/

#endif


    
//--free
#ifndef SCTCD_PART_FREE
#define SCTCD_PART_FREE
void sctcd_free(void *pointer);

/*
sctdoc:sctcd_free:

NAME
    sctcd_free

SYNOPSIS
    void sctcd_free(void *pointer);
    
DESCRIPTION
    This function frees resources returned by functions from this module.

RETURN
    None

EXAMPLE
    None

SEE ALSO
    None
*/

#endif


    
//--length
#ifndef SCTCD_PART_LENGTH
#define SCTCD_PART_LENGTH
int sctcd_length(void *pointer);

/*
sctdoc:sctcd_length:

NAME
    sctcd_length

SYNOPSIS
    int sctcd_length(void *pointer);
    
DESCRIPTION
    This function returns the length of arrays returned by function from 
    this module.

RETURN
    The length of the array if succesful or -1 on failure.

EXAMPLE
    None

SEE ALSO
    None
*/

#endif


    
#endif
