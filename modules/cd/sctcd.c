#define SCT__INTERNAL__
#include "sctcd.h"

// STATIC SECTION
#include <stdio.h>
#include <stdbool.h>
#include "sctcd.h"
#include "scta.h"


// PARTIALS SECTION: AUTOMATICLY GENERATED
//--base16_decode
#include <string.h>
int sctcd__base16_reverse_lookup[] = {
    ['0'] = 0,
    ['1'] = 1,
    ['2'] = 2,
    ['3'] = 3,
    ['4'] = 4,
    ['5'] = 5,
    ['6'] = 6,
    ['7'] = 7,
    ['8'] = 8,
    ['9'] = 9,
    ['A'] = 10,
    ['a'] = 10,
    ['B'] = 11,
    ['b'] = 11,
    ['C'] = 12,
    ['c'] = 12,
    ['D'] = 13,
    ['d'] = 13,
    ['E'] = 14,
    ['e'] = 14,
    ['F'] = 15,
    ['f'] = 15 };
    
uint8_t* sctcd_base16_decode_c(char *string){
    uint8_t *out; // ouput string
    int omax;
    int ocur = 0;
    
    int length = strlen(string);
    
    // calculating required memory
    if (length%2) ERETURN(NULL, "incomplete base2 string: not a multiple of 2");
    omax = length/2;
    
    if (!(out = scta_new(uint8_t))) ERETURN(NULL, "^");
    
    // stretching array to required length
    if (!scta_set_length(out, omax)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }
    
    // validating
    for (int i = 0; string[i]; ++i) {
        if (!(
            ((string[i] >= 'A') && (string[i] <= 'F')) ||
            ((string[i] >= 'a') && (string[i] <= 'f')) ||
            ((string[i] >= '0') && (string[i] <= '9')) )) {
            scta_free(out);
            ERETURN(NULL, "invalid base16 characters in string: %c (%d) @ %d", string[i], string[i], i);
        }
    }
    
    // decoding
    for (int i = 0; i < length; i+=2) {
        scta_get(out, ocur++) = (sctcd__base16_reverse_lookup[(int) string[i]]<<4)|sctcd__base16_reverse_lookup[(int) string[i+1]];
    }
    
    if (ocur > omax) { // remove later if calculation is varified / to accomidate the extra ++
        scta_free(out);
        ERETURN(NULL, "buffer overflow detected");
    }
    return out;
}


uint8_t* sctcd_base16_decode_u(uint32_t *string){ //TODO uchar32_t
    uint8_t *out; // ouput string
    int omax;
    int ocur = 0;
    
    int length = 0; 
    while (string[length++]); 
    --length;
    
    // calculating required memory
    if (length%2) ERETURN(NULL, "incomplete base2 string: not a multiple of 2");
    omax = length/2;
    
    if (!(out = scta_new(uint8_t))) ERETURN(NULL, "^");
    
    // stretching array to required length
    if (!scta_set_length(out, omax)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }
    
    // validating
    for (int i = 0; string[i]; ++i) {
        if (!(
            ((string[i] >= 'A') && (string[i] <= 'F')) ||
            ((string[i] >= 'a') && (string[i] <= 'f')) ||
            ((string[i] >= '0') && (string[i] <= '9')) )) {
            scta_free(out);
            ERETURN(NULL, "invalid base16 characters in string: %c (%d) @ %d", string[i], string[i], i);
        }
    }
    
    // decoding
    for (int i = 0; i < length; i+=2) {
        scta_get(out, ocur++) = (sctcd__base16_reverse_lookup[(int) string[i]]<<4)|sctcd__base16_reverse_lookup[(int) string[i+1]];
    }
    
    if (ocur > omax) { // remove later if calculation is varified / to accomidate the extra ++
        scta_free(out);
        ERETURN(NULL, "buffer overflow detected");
    }
    return out;
}



    
//--base16_encode
#include <string.h>
int sctcd__base16_lookup[] = {
    [0] = '0', [1] = '1', [2] = '2', [3] = '3', [4] = '4', [5] = '5', 
    [6] = '6', [7] = '7', [8] = '8', [9] = '9', [10] = 'A',
    [11] = 'B', [12] = 'C', [13] = 'D', [14] = 'E', [15] = 'F' };
    
void* sctcd_base16_encode(uint8_t *mem, int length, int flags){
    char *out; // ouput string
    int omax = 0;
    int ocur = 0; // current position in `mem', must not exceed `omax'
    
    
    if (length < 0) {
        if (length == -1) length = strlen((char *)mem);
        else ERETURN(NULL, "bad length: must be a positive number or -1. Geven: %d", length);
    }
    
    
    // calculating required memory
    omax += length*2;
    //omax += ((flags&SCTCD_WHITESPACE)?(omax/64+1):0);
    
    if (!(out = scta_new(char))) ERETURN(NULL, "^");
    
    // stretching array to required length    
    if (!scta_set_length(out, omax)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }
    
    
    // encoding loop bounds:
    if (flags&SCTCD_SINGLE && SCTCD__LITTLE_ENDIAN) for (int i = length-1; i >= 0; --i) {
        scta_get(out, ocur++) = sctcd__base16_lookup[(mem[i]&0xF0)>>4];
        scta_get(out, ocur++) = sctcd__base16_lookup[mem[i]&0xF];
    }
    else for (int i = 0; i < length; ++i) {
        scta_get(out, ocur++) = sctcd__base16_lookup[(mem[i]&0xF0)>>4];
        scta_get(out, ocur++) = sctcd__base16_lookup[mem[i]&0xF];
    }
    
    
    if (flags&SCTCD_UTF32) { //TODO switch to char32_t when you switch to a newer compiler
        uint32_t *u32;
        if (!(u32 = scta_new(uint32_t))) {
            scta_free(out);
            ERETURN(NULL, "^");
        }
        if (!scta_set_length(u32, scta_length(out))) {
            scta_free(out);
            scta_free(u32);
            ERETURN(NULL, "^");        
        }
        for (int i = 0; out[i]; ++i) u32[i] = out[i];
        scta_free(out);
        return u32;
    }
    return out;
}


    
//--base2_decode
#include <string.h>
uint8_t* sctcd_base2_decode_c(char *string){
    uint8_t *out; // ouput string
    int omax;
    int ocur = 0;
    
    int length = strlen(string);
    
    // calculating required memory
    if (length%8) ERETURN(NULL, "incomplete base2 string: not a multiple of 8");
    omax = length/8;
    
    if (!(out = scta_new(uint8_t))) ERETURN(NULL, "^");
    
    // stretching array to required length
    if (!scta_set_length(out, omax)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }
    for (int i = 0; i < length; i+=1) if (string[i] != '0' && string[i] != '1') {
        scta_free(out);
        ERETURN(NULL, "invalid character encoundered in base2 sequence: %c", string[i]);        
    }
    
    for (int i = 0; i < length; i+=8) {
        scta_get(out, ocur++) = ((string[i])<<7) | 
                      ((string[i+1]-'0')<<6) | 
                      ((string[i+2]-'0')<<5) | 
                      ((string[i+3]-'0')<<4) | 
                      ((string[i+4]-'0')<<3) | 
                      ((string[i+5]-'0')<<2) | 
                      ((string[i+6]-'0')<<1) | 
                      ((string[i+7]-'0'));
        
    }
    if (ocur >= omax+1) { // remove later if calculation is varified / to accomidate the extra ++
        scta_free(out);
        ERETURN(NULL, "buffer overflow detected");
    }
    return out;
}

uint8_t* sctcd_base2_decode_u(uint32_t *string){ // TODO -> char32_t;
    uint8_t *out; // ouput string
    int omax;
    int ocur = 0;
    
    int length = 0;
    while (string[length++]); // TODO -> char32_t; fix
    --length;
    
    // calculating required memory
    if (length%8) ERETURN(NULL, "incomplete base2 string: not a multiple of 8");
    omax = length/8;
    
    if (!(out = scta_new(uint8_t))) ERETURN(NULL, "^");
    
    // stretching array to required length
    if (!scta_set_length(out, omax)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }
    for (int i = 0; i < length; i+=1) if (string[i] != '0' && string[i] != '1') {
        scta_free(out);
        ERETURN(NULL, "invalid character encoundered in base2 sequence: %c", string[i]);        
    }
    
    for (int i = 0; i < length; i+=8) {
        scta_get(out, ocur++) = ((string[i])<<7) | 
                      ((string[i+1]-'0')<<6) | 
                      ((string[i+2]-'0')<<5) | 
                      ((string[i+3]-'0')<<4) | 
                      ((string[i+4]-'0')<<3) | 
                      ((string[i+5]-'0')<<2) | 
                      ((string[i+6]-'0')<<1) | 
                      ((string[i+7]-'0'));
    }
    if (ocur >= omax+1) { // remove later if calculation is varified / to accomidate the extra ++
        scta_free(out);
        ERETURN(NULL, "buffer overflow detected");
    }
    return out;
}



    
//--base2_encode
#include <string.h>
void* sctcd_base2_encode(uint8_t *mem, int length, int flags){
    char *out; // ouput string
    int omax = 0;
    int ocur = 0;
    
    // autolength
    if (length < 0) {
        if (length == -1) length = strlen((char *)mem);
        else ERETURN(NULL, "bad length: must be a positive number or -1. Geven: %d", length);
    }
    
    // calculating required memory
    omax += length*8; // eight characters per byte
    omax += ((flags&SCTCD_WHITESPACE)?(omax/8+1):0)+length;
    
    if (!(out = scta_new(char))) ERETURN(NULL, "^");
    
    // stretching array to required length
    if (!scta_set_length(out, omax)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }
    
    
    if (flags&SCTCD_SINGLE && SCTCD__LITTLE_ENDIAN) for (int i = length-1; i >= 0; --i) {
        if (flags&SCTCD_WHITESPACE && (i != (length -1))) {
            if (!(i%8)) scta_get(out, ocur++) = '\n';
            else scta_get(out, ocur++) = ' ';
        }
        
        scta_get(out, ocur++) = '0'+((mem[i]&0x80)>>7);
        scta_get(out, ocur++) = '0'+((mem[i]&0x40)>>6);
        scta_get(out, ocur++) = '0'+((mem[i]&0x20)>>5);
        scta_get(out, ocur++) = '0'+((mem[i]&0x10)>>4);
        scta_get(out, ocur++) = '0'+((mem[i]&0x08)>>3);
        scta_get(out, ocur++) = '0'+((mem[i]&0x04)>>2);
        scta_get(out, ocur++) = '0'+((mem[i]&0x02)>>1);
        scta_get(out, ocur++) = '0'+((mem[i]&0x01));
    }
    else for (int i = 0; i < length; ++i) {
        if (flags&SCTCD_WHITESPACE && i) {
            if (!(i%8)) scta_get(out, ocur++) = '\n';
            else scta_get(out, ocur++) = ' ';
        }
        
        scta_get(out, ocur++) = '0'+((mem[i]&0x80)>>7);
        scta_get(out, ocur++) = '0'+((mem[i]&0x40)>>6);
        scta_get(out, ocur++) = '0'+((mem[i]&0x20)>>5);
        scta_get(out, ocur++) = '0'+((mem[i]&0x10)>>4);
        scta_get(out, ocur++) = '0'+((mem[i]&0x08)>>3);
        scta_get(out, ocur++) = '0'+((mem[i]&0x04)>>2);
        scta_get(out, ocur++) = '0'+((mem[i]&0x02)>>1);
        scta_get(out, ocur++) = '0'+((mem[i]&0x01));
    }
    
    if (ocur >= omax) {
            scta_free(out);
            ERETURN(NULL, "buffer overflow detected");
    }
    
    if (flags&SCTCD_UTF32) {
        uint32_t *u32; //TODO -> char32_t
        if (!(u32 = scta_new(uint32_t))) {
            scta_free(out);
            ERETURN(NULL, "^");
        }
        if (!scta_set_length(u32, scta_length(out))) {
            scta_free(out);
            scta_free(u32);
            ERETURN(NULL, "^");        
        }
        for (int i = 0; out[i]; ++i) u32[i] = out[i];
        scta_free(out);
        return u32;
    }

    return out;
}



    
//--base64_decode
int sctcd__base64_reverse_lookup[0xff] = {
    ['A'] = 0,    ['N'] = 13,   ['a'] = 26,	  ['n'] = 39,	['0'] = 52,
    ['B'] = 1,    ['O'] = 14,   ['b'] = 27,	  ['o'] = 40,	['1'] = 53,
    ['C'] = 2,    ['P'] = 15,   ['c'] = 28,	  ['p'] = 41,	['2'] = 54,
    ['D'] = 3,    ['Q'] = 16,   ['d'] = 29,	  ['q'] = 42,	['3'] = 55,
    ['E'] = 4,    ['R'] = 17,   ['e'] = 30,	  ['r'] = 43,	['4'] = 56,
    ['F'] = 5,    ['S'] = 18,   ['f'] = 31,	  ['s'] = 44,	['5'] = 57,
    ['G'] = 6,    ['T'] = 19,   ['g'] = 32,	  ['t'] = 45,	['6'] = 58,
    ['H'] = 7,    ['U'] = 20,   ['h'] = 33,	  ['u'] = 46,	['7'] = 59,
    ['I'] = 8,    ['V'] = 21,   ['i'] = 34,	  ['v'] = 47,	['8'] = 60,
    ['J'] = 9,    ['W'] = 22,   ['j'] = 35,	  ['w'] = 48,	['9'] = 61,
    ['K'] = 10,   ['X'] = 23,   ['k'] = 36,	  ['x'] = 49,	['+'] = 62,
    ['L'] = 11,   ['Y'] = 24,   ['l'] = 37,	  ['y'] = 50,	['/'] = 63,
    ['M'] = 12,   ['Z'] = 25,   ['m'] = 38,	  ['z'] = 51,   ['-'] = 62, 
                                                            ['_'] = 63
};
#include <string.h>
uint8_t* sctcd_base64_decode_c(char *input) {
    if (!input) ERETURN(NULL, "NULL input");
        
    int length = strlen(input);
    int olength = 0;
    uint8_t *out = scta_new(uint8_t);
    int ocur = 0;
    int icur = 0;
    
    if (!out) ERETURN(NULL, "^");
    if (!scta_set_length(out, length)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }
    
    // preforms better with validation out of the main loop (probably due to 
    // cpu catches and branch pridiction: do cachegrind on it to messure cache misses and see if branch pridiction can be messured)
    // Two O(2n) is faster than O(n).. something very wrong is happending for O(n)
    // LOOP 
    for (int i = 0; input[i]; ++i) {
        if (!(
            ((input[i] >= 'A') && (input[i] <= 'Z')) ||
            ((input[i] >= 'a') && (input[i] <= 'z')) ||
            ((input[i] >= '0') && (input[i] <= '9')) ||
            ((input[i] == '+') || (input[i] == '-')) ||
            ((input[i] == '/') || (input[i] == '_')) || (input[i] == '=')
        )) {
            scta_free(out);
            ERETURN(NULL, "invalid base64 characters in input: %c (%d) @ %d", input[icur+i], input[icur+i], icur+i);
        }
    }
    
    while (true){
        uint32_t reg = 0;
        uint32_t oct = 0;
        
        for (int i = 0; ((icur+i)<length && input[icur+i] != '=') && i < 4; ++i) {
            reg |= sctcd__base64_reverse_lookup[(int)input[icur+i]]<<((3-i)*6);
            ++oct;
        }
        
        // End of buffer to decode
        if (oct == 0) break;
        
        // decoding
        scta_get(out, ocur++) = (reg>>16)&0xFF;
        scta_get(out, ocur++) = (reg>>8)&0xFF;
        scta_get(out, ocur++) = reg&0xFF;
        olength += 3;
        // if last read character is a =
        if (oct < 4) {
            olength -= 4-oct;
            break;
        }
        icur+=oct;
    }
    if (!scta_set_length(out, olength)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }    
    return out;
}

// copyed and pasted..
uint8_t* sctcd_base64_decode_u(uint32_t *input) {
    if (!input) ERETURN(NULL, "NULL input");
        
    int length = 0; 
    while (input[length++]); 
    --length;
        
    int olength = 0;
    uint8_t *out = scta_new(uint8_t);
    int ocur = 0;
    int icur = 0;
    
    if (!out) ERETURN(NULL, "^");
    if (!scta_set_length(out, length)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }

    for (int i = 0; input[i]; ++i) {
        if (!(
            ((input[i] >= 'A') && (input[i] <= 'Z')) ||
            ((input[i] >= 'a') && (input[i] <= 'z')) ||
            ((input[i] >= '0') && (input[i] <= '9')) ||
            ((input[i] == '+') || (input[i] == '-')) ||
            ((input[i] == '/') || (input[i] == '_')) || (input[i] == '=')
        )) {
            scta_free(out);
            ERETURN(NULL, "invalid base64 characters in input: %c (%d) @ %d", input[icur+i], input[icur+i], icur+i);
        }
    }
        
    while (true){
        uint32_t reg = 0;
        uint32_t oct = 0;
        
        for (int i = 0; ((icur+i)<length && input[icur+i] != '=') && i < 4; ++i) {
            reg |= sctcd__base64_reverse_lookup[(int)input[icur+i]]<<((3-i)*6);
            ++oct;
        }
        
        // End of buffer to decode
        if (oct == 0) break;
        
        // decoding
        scta_get(out, ocur++) = (reg>>16)&0xFF;
        scta_get(out, ocur++) = (reg>>8)&0xFF;
        scta_get(out, ocur++) = reg&0xFF;
        olength += 3;
        // if last read character is a =
        if (oct < 4) {
            olength -= 4-oct;
            break;
        }
        icur+=oct;
    }
    if (!scta_set_length(out, olength)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }    
    return out;
}



    
//--base64_encode
#include <string.h>
const char sctcd__base64_lookup[64] = {
    [0] = 'A',	  [16] = 'Q',	    [32] = 'g',	      [48] = 'w',
    [1] = 'B',	  [17] = 'R',	    [33] = 'h',	      [49] = 'x',
    [2] = 'C',	  [18] = 'S',	    [34] = 'i',	      [50] = 'y',
    [3] = 'D',	  [19] = 'T',	    [35] = 'j',	      [51] = 'z',
    [4] = 'E',	  [20] = 'U',	    [36] = 'k',	      [52] = '0',
    [5] = 'F',	  [21] = 'V',	    [37] = 'l',	      [53] = '1',
    [6] = 'G',	  [22] = 'W',	    [38] = 'm',	      [54] = '2',
    [7] = 'H',	  [23] = 'X',	    [39] = 'n',	      [55] = '3',
    [8] = 'I',	  [24] = 'Y',	    [40] = 'o',	      [56] = '4',
    [9] = 'J',	  [25] = 'Z',	    [41] = 'p',	      [57] = '5',
    [10] = 'K',	  [26] = 'a',	    [42] = 'q',	      [58] = '6',
    [11] = 'L',	  [27] = 'b',	    [43] = 'r',	      [59] = '7',
    [12] = 'M',	  [28] = 'c',	    [44] = 's',	      [60] = '8',
    [13] = 'N',	  [29] = 'd',	    [45] = 't',	      [61] = '9',
    [14] = 'O',	  [30] = 'e',	    [46] = 'u',	      [62] = '+',
    [15] = 'P',	  [31] = 'f',	    [47] = 'v',	      [63] = '/'
};

void* sctcd_base64_encode(uint8_t *mem, int length, int flags){
    char *out; // ouput string
    int omax = 0;
    int mcur = 0; // current position in `mem', must not exceed `length'
    int ocur = 0; // current position in `mem', must not exceed `omax'
    
    if (flags&SCTCD_SINGLE) ERETURN(NULL, "SCTCD_SINGLE is not supported by this function.");
    
    if (length < 0) {
        if (length == -1) length = strlen((char *)mem);
        else ERETURN(NULL, "bad length: must be a positive number or -1. Geven: %d", length);
    }
    
    
    // calculating required memory
    omax += length*(int)(4./3+4);
    omax += ((flags&SCTCD_WHITESPACE)?(omax/64+1):0);
    
    if (!(out = scta_new(char))) ERETURN(NULL, "^");
    
    // stretching array to required length    
    if (!scta_set_length(out, omax)) {
        scta_free(out);
        ERETURN(NULL, "^");
    }
    
    
    while (true) {
        uint32_t reg = 0;
        int oct = 0;
        
        // read from buffer 3 bytes at most 
        for (int i = 0; (mcur+i)<length && i < 3; ++i) {
            reg |= mem[mcur+i]<<((2-i)*8);
            ++oct;
        }
        
        // End of buffer to encode
        if (oct == 0) break;
        
        // optional whitespace enabled by SCTCD_WHITESPACE
        if (flags&SCTCD_WHITESPACE && ocur && !(ocur%64)) scta_get(out, ocur++) = '\n';
        
        // encoding
        scta_get(out, ocur++) = sctcd__base64_lookup[(reg>>18)&0x3F];
        scta_get(out, ocur++) = sctcd__base64_lookup[(reg>>12)&0x3F];
        if (oct >= 2) scta_get(out, ocur++) = sctcd__base64_lookup[(reg>>6)&0x3F];
        if (oct == 3) scta_get(out, ocur++) = sctcd__base64_lookup[reg&0x3F];
        
        // if oct is less than 3, then adding padding
        if (oct==1) {
            scta_get(out, ocur++) = '=';
            scta_get(out, ocur++) = '=';
        }
        else if (oct==2) {
            scta_get(out, ocur++) = '=';
        }        
        
        if (ocur >= omax) {  // remove later if calculation is varified
            scta_free(out);
            ERETURN(NULL, "buffer overflow detected");
        }
        mcur+=oct;
    }
    
    if (flags&SCTCD_URL) for (int i = 0; out[i]; ++i) {
        if (out[i] == '+') out[i] = '-';
        if (out[i] == '/') out[i] = '_';
    }
    
    if (flags&SCTCD_UTF32) { //TODO switch to char32_t when you switch to a newer compiler
        uint32_t *u32;
        if (!(u32 = scta_new(uint32_t))) {
            scta_free(out);
            ERETURN(NULL, "^");
        }
        if (!scta_set_length(u32, scta_length(out))) {
            scta_free(out);
            scta_free(u32);
            ERETURN(NULL, "^");        
        }
        for (int i = 0; out[i]; ++i) u32[i] = out[i];
        scta_free(out);
        return u32;
    }
    return out;
}



    
//--free
void sctcd_free(void *pointer){
    scta_free(pointer);
}



    
//--length
int sctcd_length(void *pointer){
    return scta_length(pointer);
}



    
