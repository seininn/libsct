#!/bin/bash

#scta_get(out, ocur++) TODO

curdir=$(pwd)
while true; do
    cd ..
    [[ $(basename $(pwd)) == libsct ]] && {
        PATH=$PATH:"$(pwd)/tools"
        include_path="$(pwd)/tools"
        break
    }
    [[ $(basename $(pwd)) == "/" ]] && die "Could not find libsct tool directory"
done
cd $curdir

. "$include_path/io.lb"

print "Loading tools directory in PATH and spinning up a subshell.."

# path export
export PATH

# setting up the prompt
RC=$(mktemp)
cp ~/.bashrc "$RC"

cat >> "$RC" <<"CUSTOM"

update_ps1(){
    # exit status
    [[ $? -eq 0 ]] && ES=$(printf "\e[42m \e[0m")  || ES=$(printf "\e[41m \e[0m")
    
    # are we inside
    INSIDE=$(pwd | grep '/libsct' > /dev/null && echo "yes" || echo "no")
    
    # brach
    [[ $INSIDE == yes ]] && {
        BRANCH=$(git branch --list | grep '*' | sed 's/\* //')
        DIR=$(pwd |  sed 's:.*\/libsct\(\/\)*::')
        PS1=$(printf "$ES \e[1;36m${BRANCH} \e[1;34m$DIR\e[0m ")
    } || {
        PS1="{OUTSIDE}:$(pwd) "
    }
}

PROMPT_COMMAND='update_ps1'
CUSTOM


<<DDD
prompt_start='\[\033[0;107m\]\[\033[1;90m\]'
#prompt_mid='\[\033[00m\]\[\033[01;34m\]'
prompt_end=' \[\033[00m\] '
echo "PS1="'"\$(sct_status)"'"'$prompt_start'"'"(\$(sct_branch)) "'"'$prompt_mid'"'"\$(sct_dir)"'"'$prompt_end'"  >> "$RC"
DDD

bash --rcfile "$RC"
rm "$RC"


