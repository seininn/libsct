#!/bin/bash

die(){
    printf "\e[1;31m[("$(basename "$0")"):ERROR] $1\n\e[0m" 
    exit 1 
}
warn(){
    printf "\e[1;33m[("$(basename "$0")"):WARNING] $1\n\e[0m" 
}
print(){
    printf "\e[1;37m("$(basename "$0")"):$1\n\e[0m" 
}
ok(){
    [[ "$1" ]] && m="$1" || m='[ok]'
    printf "\e[1;32m("$(basename "$0")"):$m\n\e[0m" 
    exit 0
}

[[ "$(pwd)" == */libsct/modules/*/parts ]] || die "This is not a standard parts path. This script should be run inside the directory that contains the parts: */libsct/modules/*/parts"

# find tool branch
curdir=$(pwd)
while true; do
    cd ..
    [[ $(basename $(pwd)) == libsct ]] && {
        PATH=$PATH:"$(pwd)/tools"
        break
    }
    [[ $(basename $(pwd)) == "/" ]] && die "Could not find libsct tool directory"
done
cd $curdir
#done

[[ -f "part.template" ]] || die "'part.template' required but not found"
[[ "$1" ]] || die "template name required"
[[ -f "${1,,}.c" ]] && die "${1,,}.c already exists"

name="$1"

cat part.template | sed "s/\[partname\]/$name/g" | sed "s/\[PARTNAME\]/${name^^}/g" > "${1,,}.c"


ok
