#!/bin/bash
die(){
    printf "\e[1;31m[("$(basename "$0")"):ERROR] $1\n\e[0m" 
    exit 1 
}
warn(){
    printf "\e[1;33m[("$(basename "$0")"):WARNING] $1\n\e[0m" 
}
print(){
    printf "\e[1;37m("$(basename "$0")"):$1\n\e[0m" 
}
ok(){
    [[ "$1" ]] && m="$1" || m='[ok]'
    printf "\e[1;32m("$(basename "$0")"):$m\n\e[0m" 
    exit 0
}

[[ "$(pwd)" == */libsct/modules/*/parts ]] || die "This is not a standard parts path. This script should be run inside the directory that contains the parts: */libsct/modules/*/parts"

# find tool branch and add to path
curdir=$(pwd)
while true; do
    cd ..
    [[ $(basename $(pwd)) == libsct ]] && {
        PATH=$PATH:"$(pwd)/tools"
        break
    }
    [[ $(basename $(pwd)) == "/" ]] && die "Could not find libsct tool directory"
done
cd $curdir
#done



parts.gen.header.sh || die "could not generate header"



prefix="sct$(basename $(cd ..; pwd))"
csource="$prefix.c"
header="$prefix.h"
#CAPSPF=$(echo "$prefix" | tr [:lower:] [:upper:])

print "generating $csource"


echo '#define SCT__INTERNAL__' > $csource
echo '#include "'"$header"'"' >> $csource
echo "
// STATIC SECTION" >> $csource
[[ -f init.c ]] && cat init.c | grep -v '#define SCT__INTERNAL__' >> $csource



echo "
// PARTIALS SECTION: AUTOMATICLY GENERATED" >> $csource
# prototypes and macros
for f in *.c; do
    [[ "$f" == "init.c" ]] && continue    
    [[ "$f" == "$csource" ]] && continue
    
    echo "//--${f::-2}" >> $csource


    IFS=':'
    BEGIN=($(grep -b "//LIB" "$f"))
    END=($(grep -b "//TEST" "$f"))

    [[ "$BEGIN" ]] || die "'//LIB' was not found in '$f'"
    [[ "$END" ]] || die "'//TEST' was not found in '$f'"
    
    ((LENGTH = END-BEGIN))
    
    dd bs=1 count=$LENGTH skip=$BEGIN if="$f" 2>/dev/null | tail -n +2 >> $csource
    
    echo "
    " >> $csource
    
done

mv $csource $header ..

ok
