#!/bin/bash

die(){
    printf "\e[1;31m[("$(basename "$0")"):ERROR] $1\n\e[0m" 
    exit 1 
}

[[ "$(pwd)" == */libsct/modules/*/parts ]] || die "This is not a standard parts path. This script should be run inside the directory that contains the parts: */libsct/modules/*/parts"

# find tool branch and add to path
curdir=$(pwd)
while true; do
    cd ..
    [[ $(basename $(pwd)) == libsct ]] && {
        PATH=$PATH:"$(pwd)/tools"
        include_path="$(pwd)/tools"
        break
    }
    [[ $(basename $(pwd)) == "/" ]] && die "Could not find libsct tool directory"
done
cd $curdir
#done

. "$include_path/io.lb"

# generate a common header for inter-dependant parts to compile
parts.gen.header.sh || die "could not generate header"


# if the first argument is a file, test it only, otherwise test every source file
if [[ "$1" ]]; then 
    [[ -f "$1.c" ]] && list="$1.c"
    [[ -f "$1" ]] && {
        warn "ignoring trailing '.c' in '$1'"
        list="$1"
    }
fi 
[[ "$list" ]] || {
    [[ "$1" ]] &&   warn "'$1' does not exist, cominicng full tests..." ||
                    warn "No arguments supplied, cominicng full tests..." 
        
    
    list=*.c
}

# get the module name (s, m)
modname="$(basename $(cd ..; pwd))"

for f in $list; do
    [[ "$f" == "init.c" ]] && continue    
    f="${f::-2}"

    echo "testing '$f.c' (test_$f)"
    
    #genetating dependancy list    
    deps=$(cat "$f.c" "sct$modname.h" | grep -e '^#include "sct.*\.h"' | grep -o 'sct[a-z]*' | sed 's/^sct//g' | while read z; do \
            [[ "$z" != "$modname" ]] && printf "../../$z/sct$z.c -I ../../$z "; done)
        
    # compile
    #print "clang -o '$f'.test.bin -g -Wall -Wextra -pedantic -std=c1x  -D'test_$f' *.c $deps"
    clang -o "$f".test.bin -g -Wall -Wextra -pedantic -std=c1x  -D"test_$f" *.c $deps || die "'$f.c' build error"'!'
    # fast test
    ./"$f".test.bin || die "'$f.c' run error"'!'
    # memcheck (could be merged with fast test, but seperate for now)
    valgrind --tool=memcheck --leak-check=full --error-exitcode=1 ./"$f".test.bin 2>/dev/null 1>&2 || \
        valgrind --tool=memcheck --leak-check=full --error-exitcode=1 ./"$f".test.bin
    # scan-build?
    # clang --analyze -o "$f".test.bin -g -Wall -Wextra -pedantic -std=c1x  -D"test_$f" *.c ../../a/scta.c -I ../../a
        
done
[[ "$2" == "keep" ]] || {
    ls | grep -E '.test.bin' >/dev/null 2>&1 && rm *.test.bin
}
ok
