#!/bin/bash



[[ "$(pwd)" == */libsct/modules/*/parts ]] || echo "This is not a standard parts path. This script should be run inside the directory that contains the parts: */libsct/modules/*/parts"

# find tool branch and add to path
curdir=$(pwd)
while true; do
    cd ..
    [[ $(basename $(pwd)) == libsct ]] && {
        PATH=$PATH:"$(pwd)/tools"
        include_path="$(pwd)/tools"
        break
    }
    [[ $(basename $(pwd)) == "/" ]] && die "Could not find libsct tool directory"
done
cd $curdir
#done

. "$include_path/io.lb"


[[ -f init.h ]] || warn "Could not find init.h"


prefix="sct$(basename $(cd ..; pwd))"
CAPSPF=$(echo "$prefix" | tr [:lower:] [:upper:])
header="$prefix.h"


print "generating $header"

echo '#ifndef '"$CAPSPF"'_H
#define '"$CAPSPF"'_H' > $header


echo "
// STATIC SECTION" >> $header
[[ -f init.h ]] && cat init.h >> $header


echo "
// PARTIALS SECTION: AUTOMATICLY GENERATED" >> $header
# prototypes and macros
for f in *.c; do
    [[ "$f" == "init.c" ]] && continue    
    
    echo "//--${f::-2}" >> $header


    IFS=':'
    BEGIN=($(grep -b "//HEADER" "$f"))
    END=($(grep -b "//LIB" "$f"))

    [[ "$BEGIN" ]] || {
        echo "'//HEADER' was not found in '$f'"
        exit 1
    }
    [[ "$END" ]] || {
        echo "'//LIB' was not found in '$f'"
        exit 1
    }
    
    ((LENGTH = END-BEGIN))
    
    dd bs=1 count=$LENGTH skip=$BEGIN if="$f" 2>/dev/null | tail -n +2 >> $header
    
    echo "
    " >> $header
    
done


echo "#endif" >> $header

ok
